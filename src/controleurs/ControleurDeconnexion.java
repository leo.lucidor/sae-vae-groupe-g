package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import javafx.event.ActionEvent;

public class ControleurDeconnexion implements EventHandler<ActionEvent> {
    
    /**
     * L'application
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur de déconnexion
     * @param appli l'application
     */
    public ControleurDeconnexion(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Gère l'action de déconnexion
     * @param event l'évènement
     */
    @Override
    public void handle(ActionEvent event) {
        this.appli.pageConnexion();
        this.appli.setGrandPage(false);
    }
}