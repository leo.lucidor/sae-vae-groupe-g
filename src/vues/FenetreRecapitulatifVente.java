package vues;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import modeles.Admin;
import modeles.BaseDeDonnee;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import controleurs.ControleurBoutonSupprimerVenteVielle;
import controleurs.ControleurGenererRecapitulatif;


public class FenetreRecapitulatifVente extends StackPane {

    private static final int WIDTH_VENTE = 600;
    private static final int HEIGHT_VENTE = 200;
    private BaseDeDonnee bd;

    /**
     * Constructeur de la classe FenetreRecapitulatifVente.
     * 
     * @param bd               La base de données.
     * @param bpPage           Le BorderPane de la page.
     * @param rootTop          Le BorderPane racine du haut.
     * @param rootLeft         Le BorderPane racine de gauche.
     * @param borderMenuOption Le BorderPane du menu des options.
     * @param AdminModele      L'objet Admin.
     * @throws SQLException Si une erreur SQL se produit.
     */
    public FenetreRecapitulatifVente(BaseDeDonnee bd, BorderPane bpPage, BorderPane rootTop, BorderPane rootLeft, BorderPane borderMenuOption, Admin AdminModele) throws SQLException {
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        this.bd = bd;
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        hBoxLeft.getChildren().addAll(rootLeft);
        bpPage.setLeft(hBoxLeft);
        bpPage.setTop(rootTop);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        StackPane.setMargin(rootLeftOption, new Insets(48, 0, 0, 56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);
        Label labelRecapitulatif = new Label("Récapitulatif des ventes :");
        labelRecapitulatif.setStyle("-fx-font-size: 30px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
        labelRecapitulatif.setAlignment(Pos.CENTER);
        VBox vBoxRecapitulatifContainer = new VBox();
        VBox vBoxRecapitulatif = new VBox();

        List<List<String>> lesVentesValider = AdminModele.getAllVente();
        int count = 0;
        HBox currentHBox = new HBox();
        currentHBox.setAlignment(Pos.CENTER);
        currentHBox.setSpacing(10);
        vBoxRecapitulatif.getChildren().add(currentHBox);

        for (List<String> vente : lesVentesValider) {

            int idVe = Integer.parseInt(vente.get(0));
            List<String> infoVente = AdminModele.getInfoVente(idVe);
            int prixBase = Integer.parseInt(infoVente.get(1));
            int prixMin = Integer.parseInt(infoVente.get(2));
            String debutVente = infoVente.get(3);
            String finVente = infoVente.get(4);
            int idOb = Integer.parseInt(infoVente.get(5));
            int idSt = Integer.parseInt(infoVente.get(6));
            String nomVille = infoVente.get(7); 

            Label labelIdVe = new Label("Id de la vente : " + idVe);
            labelIdVe.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelPrixBase = new Label("Prix de base : " + prixBase);
            labelPrixBase.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelPrixMin = new Label("Prix minimum : " + prixMin);
            labelPrixMin.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelDebutVente = new Label("Début de la vente : " + debutVente);
            labelDebutVente.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelFinVente = new Label("Fin de la vente : " + finVente);
            labelFinVente.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelIdOb = new Label("Id de l'objet : " + idOb);
            labelIdOb.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelIdSt;
            if(idSt == 1){
                labelIdSt = new Label("Statut de la vente : A venir");
            }
            else if(idSt == 2){
                labelIdSt = new Label("Statut de la vente : En cours");
            }
            else if(idSt == 3){
                labelIdSt = new Label("Statut de la vente : A valider");
            }
            else if(idSt == 4){
                labelIdSt = new Label("Statut de la vente : Valider");
            }
            else{
                labelIdSt = new Label("Statut de la vente : Non conclue");
            }
            labelIdSt.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelNomVille = new Label("Nom de la ville de la vente : " + nomVille);
            labelNomVille.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");

            HBox hboxPrix = new HBox();
            hboxPrix.getChildren().addAll(labelPrixBase, labelPrixMin);
            hboxPrix.setSpacing(10);
            hboxPrix.setAlignment(Pos.CENTER);
            hboxPrix.setPadding(new Insets(0, 0, 10, 0));
            HBox hboxVenteObjet = new HBox();
            hboxVenteObjet.getChildren().addAll(labelIdVe, labelIdOb);
            hboxVenteObjet.setSpacing(10);
            hboxVenteObjet.setAlignment(Pos.CENTER);
            hboxVenteObjet.setPadding(new Insets(0, 0, 10, 0));
            HBox hboxDateVente = new HBox();
            hboxDateVente.getChildren().addAll(labelDebutVente, labelFinVente);
            hboxDateVente.setSpacing(10);
            hboxDateVente.setAlignment(Pos.CENTER);
            hboxDateVente.setPadding(new Insets(0, 0, 10, 0));
            HBox hboxStatutVente = new HBox();
            hboxStatutVente.getChildren().addAll(labelIdSt, labelNomVille);
            hboxStatutVente.setSpacing(10);
            hboxStatutVente.setAlignment(Pos.CENTER);
            hboxStatutVente.setPadding(new Insets(0, 0, 10, 0));

            VBox vBoxRecapitulatifVenteChild = new VBox();
            vBoxRecapitulatifVenteChild.setBackground(new Background(new BackgroundFill(Color.web("#D9D9D9"), new CornerRadii(0), Insets.EMPTY)));
            vBoxRecapitulatifVenteChild.setPrefSize(WIDTH_VENTE, HEIGHT_VENTE);

            HBox hBoxBoutons = new HBox();
            Button buttonGenererRecapitulatif = new Button("Générer le récapitulatif " + idVe);
            buttonGenererRecapitulatif.setOnAction(new ControleurGenererRecapitulatif(this, this.bd));
            buttonGenererRecapitulatif.setStyle("-fx-background-color: #EBACA2; -fx-font-size: 15px;");

            hBoxBoutons.getChildren().addAll(buttonGenererRecapitulatif);
            hBoxBoutons.setAlignment(Pos.CENTER);
            hBoxBoutons.setSpacing(10);

            vBoxRecapitulatifVenteChild.getChildren().addAll(hboxVenteObjet, hboxPrix, hboxDateVente, hboxStatutVente, hBoxBoutons);
            vBoxRecapitulatifVenteChild.setAlignment(Pos.CENTER);
            vBoxRecapitulatifVenteChild.setStyle("-fx-background-color: #4A919E;");

            currentHBox.getChildren().add(vBoxRecapitulatifVenteChild);
            count++;

            if (count == 3) {
                count = 0;
                currentHBox = new HBox();
                currentHBox.setAlignment(Pos.CENTER);
                currentHBox.setSpacing(10);
                vBoxRecapitulatif.getChildren().add(currentHBox);
            }
        }

        vBoxRecapitulatif.setAlignment(Pos.CENTER);
        vBoxRecapitulatif.setSpacing(30);
        vBoxRecapitulatif.setPadding(new Insets(10, 100, 10, 0));

        ScrollPane scrollPaneRecapitulatif = new ScrollPane(vBoxRecapitulatif);
        scrollPaneRecapitulatif.setStyle("-fx-background: #BED3C3;");
        scrollPaneRecapitulatif.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        vBoxRecapitulatifContainer.getChildren().addAll(labelRecapitulatif, scrollPaneRecapitulatif);
        vBoxRecapitulatifContainer.setAlignment(Pos.CENTER);
        vBoxRecapitulatifContainer.setSpacing(10);
        vBoxRecapitulatifContainer.setPadding(new Insets(20, 0, 0, 0));

        Text titreSupprimerVenteVielle = new Text("Bouton qui permet d'archiver les ventes finit depuis 2 ans dans un fichier CSV et supprime les ventes dans la base de données");
        titreSupprimerVenteVielle.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        titreSupprimerVenteVielle.setFill(Color.WHITE);
        
        Button bSupprimerVenteVielle = new Button("Supprimer");
        bSupprimerVenteVielle.setStyle("-fx-background-color: #EBACA2; -fx-font-size: 15px;");
        bSupprimerVenteVielle.setOnAction(new ControleurBoutonSupprimerVenteVielle(this, this.bd));
        vBoxRecapitulatifContainer.getChildren().addAll(titreSupprimerVenteVielle, bSupprimerVenteVielle);
        vBoxRecapitulatifContainer.setPadding(new Insets(15, 0, 15, 0));
        bpPage.setCenter(vBoxRecapitulatifContainer);
    }

    /**
     * Récupère les ventes anciennes depuis la base de données, les sauvegarde dans un fichier CSV
     * et les supprime de la base de données.
     *
     * @throws SQLException Si une erreur SQL se produit.
     */
    public void getVentesAnciennesCSV() throws SQLException{
        List<String> ventes = bd.getVenteTropVielle();
        try{
            String home = System.getProperty("user.home");

            FileWriter fw = new FileWriter(home + "/anciennesVentes.csv");
            fw.write("idVe/pseudout/nomob/prixbase/prixmin/descritionOb/nomVille/debutVe/finVe/nomCat/imgphOB\n");
            for (String idVe : ventes) {
                List<String> vente = this.bd.getVentebyIdVe(Integer.parseInt(idVe));
                fw.write(vente.get(0) + "/" + vente.get(1) + "/" + vente.get(2) + "/" + vente.get(3) + "/" + vente.get(4) + "/" + vente.get(5) + "/" + vente.get(6) + "/" + vente.get(7) + "/" + vente.get(8) + "/" + vente.get(9) + "/" + vente.get(10) + "\n");
            }
            fw.close();
        }
        catch (IOException e){
            System.out.println("Erreur lors de l'écriture du fichier CSV");
        }

        try{
            for(String idVe : ventes){
                bd.supprimerVente(Integer.parseInt(idVe));
            }

        }
        catch (SQLException e){
            System.out.println("Erreur lors de la suppression des ventes");
        }
    }
}