package controleurs;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import modeles.EmailValidator;
import modeles.Utilisateur;
import java.awt.Desktop;
import java.net.URI;

public class ControleurMotDePasseOublier implements EventHandler<ActionEvent> {
    
    /**
     * L'application principale
     */
    private ApplicationVAE appli;

    /**
     * constructeur du controleur
     * @param appli l'application principale
     * @param utilisateur l'utilisateur
     */
    public ControleurMotDePasseOublier(ApplicationVAE appli, Utilisateur utilisateur){
        this.appli = appli;
    }

    /**
     * Action à effectuer lors du déclenchement de l'événement
     * @param event l'événement
     */
    @Override
    public void handle(ActionEvent event) {
        String url = "https://accounts.google.com/AccountChooser/signinchooser?service=mail&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&flowName=GlifWebSignIn&flowEntry=AccountChooser"; // URL à ouvrir dans le navigateur
        try {
            if (Desktop.isDesktopSupported()) {
                // Ouvre l'URL en utilisant le navigateur par défaut
                System.out.println("Desktop is supported");
                String mail = this.appli.getMailRecuperationMDP();
                if(EmailValidator.isValidEmail(mail)){
                    // FONCTIONNE SUR WINDOWS MAIS PAS SUR LINUX
                    //EmailSender.sendEmail(mail, this.utilisateur.getMdpMail(mail));
                    Desktop.getDesktop().browse(new URI(url));
                }
                else {
                    this.appli.changerVisibiliteMauvaisMailRecupMDP();
                    System.out.println("Mail invalide");
                }
            } else {
                // Si Desktop n'est pas pris en charge, utilise une commande shell pour ouvrir l'URL
                System.out.println("Desktop is not supported");
            }
        } catch (Exception e) {
            // Gérez les éventuelles erreurs ici
        }    
    }
}