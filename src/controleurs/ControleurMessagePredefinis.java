package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import modeles.Message;
import modeles.Utilisateur;
import vues.FenetreEncherir;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurMessagePredefinis implements EventHandler<ActionEvent> {
    
    /**
     * modele du message
     */
    private Message messageModele;
    /**
     * modele de l'utilisateur
     */
    private Utilisateur utilisateur;
    /**
     * fenetre d'enchere
     */
    private FenetreEncherir fenetreEncherir;
    /**
     * id de l'utilisateur destinataire
     */
    private int idUtDestination;
    
    /**
     * Constructeur du controleur de message predefinis
     * @param appli application
     * @param messageModele modele du message
     * @param utilisateur modele de l'utilisateur
     * @param idUtDestination id de l'utilisateur destinataire
     * @param fenetreEncherir fenetre d'enchere
     */
    public ControleurMessagePredefinis(ApplicationVAE appli, Message messageModele, Utilisateur utilisateur, int idUtDestination, FenetreEncherir fenetreEncherir){
        this.messageModele = messageModele;
        this.utilisateur = utilisateur;
        this.fenetreEncherir = fenetreEncherir;
        this.idUtDestination = idUtDestination;
    }

    /**
     * Methode qui permet d'envoyer un message predefinis
     * @param event evenement
     */
    @Override
    public void handle(ActionEvent event) {
        try {
            this.messageModele.envoyerMessage(utilisateur.getId(), idUtDestination, fenetreEncherir.getTextAreaMessage());
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
