package controleurs;
import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modeles.BaseDeDonnee;
import modeles.Objet;
import vues.FenetreAjouterVente;

public class ControleurValiderVente implements EventHandler<ActionEvent>{
    private FenetreAjouterVente fenetreAjouterVente;
    private BaseDeDonnee bdd;

    public ControleurValiderVente(FenetreAjouterVente fenetreAjouterVente, BaseDeDonnee bdd) {
        this.fenetreAjouterVente = fenetreAjouterVente;
        this.bdd = bdd;
    }

    @Override
    public void handle(ActionEvent event) {
        Objet objet = this.fenetreAjouterVente.getObjetVente();
        System.out.println(objet);

        objet.setNom(this.fenetreAjouterVente.getNomVente());
        objet.setDescription(this.fenetreAjouterVente.getDescription());
        objet.setPrix(this.fenetreAjouterVente.getPrixDepart());
        objet.setPrixMin(this.fenetreAjouterVente.getPrixMin());
        objet.setCategorie(this.fenetreAjouterVente.getObjetVente().getCategorie());
        objet.setIdUtilisateur(this.fenetreAjouterVente.getObjetVente().getIdUtilisateur());
        objet.setImage1(this.fenetreAjouterVente.getImageVente1());
        objet.setImage2(this.fenetreAjouterVente.getImageVente2());
        objet.setImage3(this.fenetreAjouterVente.getImageVente3());
        objet.setImage4(this.fenetreAjouterVente.getImageVente4());
        objet.setDateDebut(this.fenetreAjouterVente.getDateDebut());
        objet.setDateFin(this.fenetreAjouterVente.getDateFin());
        objet.setVille(this.fenetreAjouterVente.getVille());

        int prixMin = objet.getPrixMin();
        int prixBase = objet.getPrixBase();
        String dateDebut = objet.getDateDebut();
        String dateFin = objet.getDateFin();
        String nom = objet.getNom();
        String description = objet.getDescription();
        int categorie = objet.getCategorie();
        int idUtilisateur = objet.getIdUtilisateur();
        String ville = objet.getVille();
        
        int statut = 1; 
        
        int idOb = this.bdd.insertObjet(nom, description, categorie, idUtilisateur);
    

        try {
            System.out.println("Insertion de la vente dans la base de données");
            this.bdd.insertVente(prixBase, prixMin, dateDebut, dateFin, idOb, statut, ville);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        System.out.println("Vente ajoutée");
    }
}
