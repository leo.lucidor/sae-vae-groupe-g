package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import javafx.event.ActionEvent;

public class ControleurRetourMenuConnexion implements EventHandler<ActionEvent> {
    
    /**
     * L'application principale
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur
     * @param appli l'application principale
     */
    public ControleurRetourMenuConnexion(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Action effectuée lors du clic sur le bouton "Retour"
     * @param event l'évènement
     */
    @Override
    public void handle(ActionEvent event) {
        this.appli.pageConnexion();
    }
}
