package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurProfil implements EventHandler<ActionEvent> {
    
    /**
     * L'application principale
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur
     * @param appli l'application principale
     */
    public ControleurProfil(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Gère l'action sur le bouton profil
     * @param event l'évènement
     */
    @Override
    public void handle(ActionEvent event) {
        try {
            this.appli.pageProfil();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}