package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import modeles.Utilisateur;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurRetourPageAccueil implements EventHandler<ActionEvent> {
    
    /**
     * Application principale
     */
    private ApplicationVAE appli;
    /**
     * Utilisateur modele
     */
    private Utilisateur utilisateur;
    
    /**
     * Constructeur ControleurRetourPageAccueil
     * @param appli application principale
     * @param utilisateur modele
     */
    public ControleurRetourPageAccueil(ApplicationVAE appli, Utilisateur utilisateur){
        this.appli = appli;
        this.utilisateur = utilisateur;
    }

    /**
     * Action de retour à la page d'accueil
     * @param event
     */
    @Override
    public void handle(ActionEvent event) {
        int role = this.utilisateur.getRole();
        if(role == 1){
            try {
                this.appli.pageAcceuilAdministrateur();
            } catch (SQLException e) {
                System.out.println("Erreur lors de l'affichage de la page d'accueil administrateur");
            }
            this.appli.setGrandPage(true);
        }
        else {
            this.appli.pageAcceuilUtilisateur();
            this.appli.setGrandPage(true);
        }
    }
}