package controleurs;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.CheckBox;
import vues.FenetreSetting;

public class ControleurMontrerMDP implements EventHandler<ActionEvent>{
    
    /**
     * La fenetre de setting
     */
    private FenetreSetting fenetreSetting;

    /**
     * Constructeur du controleur
     * @param fenetreSetting la fenetre de setting
     */
    public ControleurMontrerMDP(FenetreSetting fenetreSetting){
        this.fenetreSetting = fenetreSetting;
    }

    /**
     * Méthode qui permet de montrer ou cacher le mot de passe
     * @param event l'action event
     */
    @Override
    public void handle(ActionEvent event) {
        CheckBox rb = (CheckBox) event.getSource();
        if (!rb.isSelected()){
            this.fenetreSetting.cacherMDP();
            }
        else{
                this.fenetreSetting.montrerMDP();
            }
        }
    }