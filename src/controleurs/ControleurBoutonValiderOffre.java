package controleurs;
import java.sql.SQLException;
import java.time.LocalDateTime;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import modeles.BaseDeDonnee;
import vues.FenetreEncherir;

public class ControleurBoutonValiderOffre implements EventHandler<ActionEvent>{

    /**
     * Application principale
     */
    private ApplicationVAE appli;
    /**
     * FenetreEncherir
     */
    private FenetreEncherir fenetreEncherir;
    /**
     * Montant de l'offre
     */
    private Integer offreMontant;
    /**
     * Prix actuel de la vente
     */
    private Integer prixActuel;
    /**
     * Id de l'utilisateur
     */
    private int idUt;
    /**
     * Id de la vente
     */
    private int idVe;
    /**
     * Base de donnée
     */
    private BaseDeDonnee bd;
    
    /**
     * Constructeur ControleurBoutonValiderOffre
     * @param appli Application principale
     * @param bd Base de donnée
     * @param fenetreEncherir FenetreEncherir
     * @param idUt Id de l'utilisateur
     * @param idVe Id de la vente
     */
    public ControleurBoutonValiderOffre(ApplicationVAE appli,BaseDeDonnee bd,FenetreEncherir fenetreEncherir, int idUt, int idVe) {
        this.appli = appli;
        this.fenetreEncherir = fenetreEncherir;
        this.bd = bd;
        this.idUt = idUt;
        this.idVe = idVe;
    }

    /**
     * Permet de valider une offre
     * @param event ActionEvent
     */
    @Override
    public void handle(ActionEvent event){
        this.offreMontant = this.fenetreEncherir.getMontantOffre();
        this.prixActuel = this.fenetreEncherir.getPrixActuel();
        try {
            if (offreMontant > prixActuel && Integer.parseInt(this.bd.getVentebyIdVe(this.idVe).get(11)) == 2){
            LocalDateTime dateheure = LocalDateTime.now();
            String dateheureString = dateheure.toString().replace("T", " ");
            try {
                this.bd.insertOffreEncherir(this.idUt,this.idVe,dateheureString,this.offreMontant);
                this.appli.popUpMessageOffreValider().showAndWait();
                System.out.println("Offre enregistrée");
                this.appli.pagePanier();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else{
            this.appli.popUpMessageOffreNonValider().showAndWait();
            }
        } 
        catch (SQLException e) {
            e.printStackTrace(); 
        }
    }
}