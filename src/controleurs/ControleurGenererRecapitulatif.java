package controleurs;
import java.io.IOException;
import java.sql.SQLException;
import com.itextpdf.text.DocumentException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import modeles.BaseDeDonnee;
import modeles.PdfGenerator;
import vues.FenetreRecapitulatifVente;

public class ControleurGenererRecapitulatif implements EventHandler<ActionEvent>{;
    
    /**
     * Base de donnée
     */
    private BaseDeDonnee bdd;

    /**
     * Constructeur du controleur
     * @param fenetreRecapitulatifVente
     * @param bd Base de donnée
     */
    public ControleurGenererRecapitulatif(FenetreRecapitulatifVente fenetreRecapitulatifVente, BaseDeDonnee bdd) {
        this.bdd = bdd;
    }

    /**
     * Génère le pdf du récapitulatif de la vente
     * @param event Evenement
     */
    @Override
    public void handle(ActionEvent event) {
        Button button = (Button) event.getSource();
        int idVe = Integer.parseInt(button.getText().replaceAll("[^0-9]", ""));
        try {
            PdfGenerator.generateRecapitulatif("RecapitulatifVente"+ idVe + ".pdf", bdd.getVentebyIdVe(idVe));
            //Prévenir l'utilisateur que le pdf a été généré
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText("Le pdf a été généré");
            alert.setContentText("Le pdf a été généré dans le dossier du projet");
            alert.showAndWait();
        } catch (IOException | DocumentException | SQLException e) {
            e.printStackTrace();
        }

    }
}
