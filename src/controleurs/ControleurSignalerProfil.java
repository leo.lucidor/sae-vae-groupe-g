package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import modeles.Utilisateur;
import javafx.event.ActionEvent;

public class ControleurSignalerProfil implements EventHandler<ActionEvent> {
    
    /**
     * Application principale
     */
    private ApplicationVAE appli;
    /**
     * Utilisateur modele
     */
    private Utilisateur utilisateur;
    /**
     * id de l'utilisateur qui est signalé
     */
    private int idUtilisateur2;
    /**
     * id de l'utilisateur qui signale
     */
    private int idUtilisateur;
    
    /**
     * Constructeur du controleur ControleurSignalerProfil
     * @param appli application principale
     * @param utilisateur modele
     * @param idUtilisateur2 id de l'utilisateur qui est signalé
     * @param idUtilisateur id de l'utilisateur qui signale
     */
    public ControleurSignalerProfil(ApplicationVAE appli, Utilisateur utilisateur, int idUtilisateur2, int idUtilisateur){
        this.appli = appli;
        this.idUtilisateur2 = idUtilisateur2;
        this.idUtilisateur = idUtilisateur;
        this.utilisateur = utilisateur;
    }

    /**
     * Méthode handle
     * @param event 
     */
    @Override
    public void handle(ActionEvent event) {
        String raisonReport = this.appli.popUpSignalerVente();
        try {
            utilisateur.signalerProfil(raisonReport, idUtilisateur, idUtilisateur2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
