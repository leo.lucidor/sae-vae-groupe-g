package controleurs;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import modeles.PasswordValidator;
import modeles.Utilisateur;
import vues.FenetreSetting;

public class ControleurModifierMDP implements EventHandler<ActionEvent>{
    
    /**
     * fenetre setting
     */
    private FenetreSetting fenetreSetting;
    /**
     * utilisateur modele
     */
    private Utilisateur utilisateur;

    /**
     * Constructeur ControleurModifierMDP
     * @param fenetreSetting fenetre setting
     * @param utilisateur utilisateur modele
     */
    public ControleurModifierMDP(FenetreSetting fenetreSetting, Utilisateur utilisateur){
        this.fenetreSetting = fenetreSetting;
        this.utilisateur = utilisateur;
    }

    /**
     * Methode handle
     * @param event event
     */
    @Override
    public void handle(ActionEvent event){
        Button btn = (Button) event.getSource();
        if (btn.getText().equals("Modifier")){
        this.fenetreSetting.modifierMDP();
        }
        else if (btn.getText().equals("Valider")){
            try{
                if(!PasswordValidator.isValidPassword(this.fenetreSetting.getMDP())){
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Erreur");
                    alert.setHeaderText("Erreur de mot de passe");
                    alert.setContentText("Le mot de passe doit contenir au moins 8 caractères, une majuscule, une minuscule, un chiffre et un caractère spécial");
                    alert.showAndWait();
                }
                else{
                this.fenetreSetting.validerMDP();
                this.utilisateur.setMdp(this.fenetreSetting.getMDP());
                this.utilisateur.majInfoUtilisateur(this.utilisateur.getPseudo(), this.utilisateur.getMail(), this.fenetreSetting.getMDP(), this.utilisateur.getRole(), this.utilisateur.getPrenom(), this.utilisateur.getNom(), this.utilisateur.getAge(), this.utilisateur.getGenre(), this.utilisateur.getGenre(), this.utilisateur.getId(), this.utilisateur.getIdPhoto());
                this.fenetreSetting.cacherMDP();
                }
            } catch (SQLException e){
                System.out.println(e);
            }
        }

    }

}
