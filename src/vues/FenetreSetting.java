package vues;
import controleurs.ControleurModifierCarte;
import controleurs.ControleurModifierMDP;
import controleurs.ControleurMontrerMDP;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import modeles.Utilisateur;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class FenetreSetting extends StackPane {
    private double taillePolice;
    private TextField mdpField;
    private Utilisateur userModele;
    private Button modifierMDP;
    private CheckBox montrerMDP;
    private Button modifierCarte;
    private TextField numCarteField;
    private TextField dateCarteField;
    private TextField cvvCarteField;
    private TextField nomCarteField;
    
    public FenetreSetting(BorderPane bpPage, BorderPane rootTop, BorderPane rootLeftAdmin, Utilisateur userModele, BorderPane borderMenuOption, Label textMenu, Label textPageAccueil, Label textVendreUnProduit, Label textVenteAuxEncheres, Label textMessagerie, Label textMonPanier, Label textInformations, Label textSetting, Label textQuitter, Button quitter, Button deconnexion, Button menu,  Button acceuil, Button message, Button info, Button setting, Button profil, Button enchere, Button panier, Button ajouterEnchere){
      //  BorderPane bpPage = new BorderPane();
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        Label text = new Label("setting");
        text.setStyle("-fx-text-fill: #00aef0;");
        bpPage.setCenter(text);
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        BorderPane rootLeft = new BorderPane();
        if(userModele.getRole() == 2){
            rootLeft = new MenuLeft(acceuil, quitter, message, info, setting, enchere, panier, ajouterEnchere, bpPage);
        }
        else {
            rootLeft = rootLeftAdmin;
        }
        hBoxLeft.getChildren().addAll(rootLeft, rootLeftOption);
        bpPage.setLeft(hBoxLeft);
        //BorderPane rootTop = new MenuTop(userModele, textMenu, deconnexion, profil, menu, bpPage);
        bpPage.setTop(rootTop);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        HBox.setMargin(rootLeftOption, new Insets(48,0,0,56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);

        this.userModele = userModele;
        // création de la zone principale de la page settings
        BorderPane zonePrincipale = new BorderPane();

        // création du titre
        Text titrePage = new Text("Paramètres");
        VBox titre = new VBox();

        // Modification du style du titre
        titrePage.setFont(Font.font("arial", FontWeight.BOLD, 48));
        titrePage.setFill(Color.WHITE);
        titre.getChildren().add(titrePage);

        // Ajout dans le top de la zone principale
        zonePrincipale.setTop(titrePage);

        // création d'un TabPane et ses Tab
        TabPane ongletsParamètres = new TabPane();
        Tab compte = new Tab("Compte");
        compte.setClosable(false);
        compte.setStyle("-fx-background-color: white;");
        Tab paiements = new Tab("Paiements");
        paiements.setClosable(false);
        paiements.setStyle("-fx-background-color: white;");
        
        // Paramètres de police et tailles d'écriture
        Text titreTaillePolice = new Text("Polices et taille d'écriture");
        titreTaillePolice.setFont(Font.font("arial", FontWeight.BOLD, 28+this.taillePolice));
        titreTaillePolice.setFill(Color.WHITE);
        titreTaillePolice.setUnderline(true);
        Slider taillePolice = new Slider(-15, 15, 0);
        taillePolice.setShowTickLabels(true);
        taillePolice.setShowTickMarks(true);

        // Connexion du slider au controleur
        //taillePolice.valueProperty().addListener(new ControleurSliderPolice(taillePolice));

        // Placement et espacement de la VBox Affichage
        VBox.setMargin(titreTaillePolice, new Insets(20));

        // Modification de l'onglet Compte
        VBox conteneurCompte = new VBox();
        // Création des éléments de l'onglet Compte
        Text titreCompte = new Text("Compte");
        titreCompte.setFont(Font.font("arial", FontWeight.BOLD, 28+this.taillePolice));
        titreCompte.setFill(Color.WHITE);
        titreCompte.setUnderline(true);
        Text titrePseudo = new Text("Pseudo");
        titrePseudo.setFont(Font.font("arial", FontWeight.BOLD, 18+this.taillePolice));
        titrePseudo.setFill(Color.WHITE);
        Text pseudo = new Text(userModele.getPseudo());
        pseudo.setFont(Font.font("arial", FontWeight.NORMAL, 18+this.taillePolice));
        pseudo.setFill(Color.WHITE);

        // Création du bouton de modification du mot de passe
        Text titreMDP = new Text("Mot de passe");
        titreMDP.setFont(Font.font("arial", FontWeight.BOLD, 18+this.taillePolice));
        titreMDP.setFill(Color.WHITE);
        this.montrerMDP = new CheckBox("Afficher le mot de passe");
        this.montrerMDP.setFont(Font.font("arial", FontWeight.NORMAL, 18+this.taillePolice));
        this.montrerMDP.setTextFill(Color.WHITE);
        this.montrerMDP.setSelected(false);
        this.montrerMDP.setOnAction(new ControleurMontrerMDP(this));
        this.mdpField = new TextField("*".repeat(userModele.getMdp().length()));
        this.mdpField.setFont(Font.font("arial", FontWeight.NORMAL, 18+this.taillePolice));
        this.mdpField.setEditable(false);
        mdpField.setOnAction(new ControleurModifierMDP(this, userModele));
        this.modifierMDP = new Button("Modifier");
        this.modifierMDP.setFont(Font.font("arial", FontWeight.NORMAL, 18+this.taillePolice));
        this.modifierMDP.setOnAction(new ControleurModifierMDP(this, userModele));

        // Vbox mdp
        HBox mdpBox = new HBox();
        mdpBox.getChildren().addAll(this.mdpField, this.modifierMDP);


        // Placement et espacement de la VBox Compte
        VBox.setMargin(titreCompte, new Insets(20));
        VBox.setMargin(titrePseudo, new Insets(20));
        VBox.setMargin(pseudo, new Insets(20));
        VBox.setMargin(titreMDP, new Insets(20));
        HBox.setMargin(mdpBox, new Insets(20));
        VBox.setMargin(this.montrerMDP, new Insets(20));
        HBox.setMargin(mdpField, new Insets(20));
        HBox.setMargin(modifierMDP, new Insets(20));

        // Ajout et placement des éléments dans la VBox Compte
        conteneurCompte.getChildren().addAll(titreCompte, titrePseudo, pseudo, titreMDP, mdpBox, this.montrerMDP);

        // Ajout de la VBox Compte dans l'onglet Compte
        compte.setContent(conteneurCompte);

        // Modification de l'onglet Paiements
        VBox conteneurPaiements = new VBox();
        // Création des éléments de l'onglet Paiements
        Text titrePaiements = new Text("Paiements");
        titrePaiements.setFont(Font.font("arial", FontWeight.BOLD, 28+this.taillePolice));
        titrePaiements.setFill(Color.WHITE);
        titrePaiements.setUnderline(true);
        Text titreCarte = new Text("Carte");
        titreCarte.setFont(Font.font("arial", FontWeight.BOLD, 18+this.taillePolice));
        titreCarte.setFill(Color.WHITE);
        Label nomCarte = new Label("Nom du titulaire");
        nomCarte.setFont(Font.font("arial", FontWeight.NORMAL, 18+this.taillePolice));
        nomCarte.setTextFill(Color.WHITE);
        this.nomCarteField = new TextField();
        nomCarteField.setFont(Font.font("arial", FontWeight.NORMAL, 18+this.taillePolice));
        nomCarteField.setEditable(false);
        nomCarteField.setPromptText("Nom du titulaire");
        Label numCarte = new Label("Numéro à 16 chiffres");
        numCarte.setFont(Font.font("arial", FontWeight.NORMAL, 18+this.taillePolice));
        numCarte.setTextFill(Color.WHITE);
        Label dateCarte = new Label("Date d'expiration");
        dateCarte.setFont(Font.font("arial", FontWeight.NORMAL, 18+this.taillePolice));
        dateCarte.setTextFill(Color.WHITE);
        this.dateCarteField = new TextField();
        dateCarteField.setFont(Font.font("arial", FontWeight.NORMAL, 18+this.taillePolice));
        dateCarteField.setEditable(false);
        dateCarteField.setPromptText("MM/AA");
        Label cvvCarte = new Label("CVV");
        cvvCarte.setFont(Font.font("arial", FontWeight.NORMAL, 18+this.taillePolice));
        cvvCarte.setTextFill(Color.WHITE);
        this.cvvCarteField = new TextField();
        cvvCarteField.setFont(Font.font("arial", FontWeight.NORMAL, 18+this.taillePolice));
        cvvCarteField.setEditable(false);
        this.numCarteField = new TextField();
        numCarte.setFont(Font.font("arial", FontWeight.NORMAL, 18+this.taillePolice));
        numCarteField.setEditable(false);
        numCarteField.setPromptText("XXXX-XXXX-XXXX-XXXX");
        this.modifierCarte = new Button("Modifier");
        this.modifierCarte.setFont(Font.font("arial", FontWeight.NORMAL, 18+this.taillePolice));
        this.modifierCarte.setOnAction(new ControleurModifierCarte(this));

        // Placement et espacement du GridPane de la carte
        GridPane carteGrid = new GridPane();
        carteGrid.add(nomCarte, 0, 0);
        carteGrid.add(nomCarteField, 1, 0);
        carteGrid.add(numCarte, 0, 1);
        carteGrid.add(numCarteField, 1, 1);
        carteGrid.add(dateCarte, 0, 2);
        carteGrid.add(dateCarteField, 1, 2);
        carteGrid.add(cvvCarte, 0, 3);
        carteGrid.add(cvvCarteField, 1, 3);
        carteGrid.add(modifierCarte, 1, 4);
        carteGrid.setHgap(10);
        carteGrid.setVgap(10);

        // Placement et espacement de la VBox Paiements
        VBox.setMargin(titrePaiements, new Insets(20));
        VBox.setMargin(titreCarte, new Insets(20));
        VBox.setMargin(carteGrid, new Insets(20));

        // Ajout et placement des éléments dans la VBox Paiements
        conteneurPaiements.getChildren().addAll(titrePaiements, titreCarte, carteGrid);

        // Ajout de la VBox Paiements dans l'onglet Paiements
        paiements.setContent(conteneurPaiements);

        // ajout des tab dans le tabpane
        ongletsParamètres.getTabs().addAll(compte, paiements);

        //Ajout du TabPane dans la Zone Principale
        zonePrincipale.setCenter(ongletsParamètres);

        // Ajout de la zone principale dans le borderpane de la fenetre
        bpPage.setCenter(zonePrincipale);

    }

    public void setTaillePolice(double taillePolice){
        this.taillePolice = taillePolice;
    }

    public void montrerMDP(){
        this.mdpField.setText(this.userModele.getMdp());
    }

    public void cacherMDP(){
        this.mdpField.setText("*".repeat(this.userModele.getMdp().length()));
    }

    public void modifierMDP(){
        this.mdpField.setEditable(true);
        this.mdpField.setText(this.userModele.getMdp());
        this.modifierMDP.setText("Valider");
        this.montrerMDP.setDisable(true);
    }

    public Utilisateur getUserModele(){
        return this.userModele;
    }

    public String getMDP(){
        return this.mdpField.getText();
    }

    public void validerMDP(){
        this.mdpField.setEditable(false);
        this.modifierMDP.setText("Modifier");
        this.montrerMDP.setDisable(false);
    }

    public void modifierCarte(){
        this.numCarteField.setEditable(true);
        this.modifierCarte.setText("Valider");
        this.dateCarteField.setEditable(true);
        this.cvvCarteField.setEditable(true);
    }

    public void validerCarte(){
        this.numCarteField.setEditable(false);
        this.modifierCarte.setText("Modifier");
        this.dateCarteField.setEditable(false);
        this.cvvCarteField.setEditable(false);
    }
}