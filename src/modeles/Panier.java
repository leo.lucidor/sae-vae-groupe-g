package modeles;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Panier {

    /**
     * La base de données utilisée pour les opérations du panier.
     */
    private BaseDeDonnee bd;

    /**
    * Constructeur de la classe Panier.
    *
    * @param user l'utilisateur lié au panier
    * @param bd   la base de données utilisée pour les opérations du panier
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public Panier(Utilisateur user, BaseDeDonnee bd) throws SQLException{
        this.bd = bd;
    }

    /**
    * Récupère le contenu du panier d'un utilisateur spécifié.
    *
    * @param id l'identifiant de l'utilisateur
    * @return une liste contenant les éléments du panier
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public List<List<String>> getPanier(int id) throws SQLException{
        return this.bd.getPanier(id);
    }

    /**
    * Trie la liste du panier par prix croissant.
    *
    * @param listePanier la liste du panier à trier
    * @return la liste du panier triée par prix croissant
    */
    public List<List<String>> trieParPrix(List<List<String>> listePanier){
        List<List<String>> listePanierTrie = listePanier;
        listePanierTrie.sort(new ComparatorPanierPrix());
        return listePanierTrie;
    }

    /**
    * Trie la liste du panier par date croissante.
    *
    * @param listePanier la liste du panier à trier
    * @return la liste du panier triée par date croissante
    */
    public List<List<String>> trierParDate(List<List<String>> listePanier){
        List<List<String>> listePanierTrie = listePanier;
        listePanierTrie.sort(new ComparatorPanierDate());
        return listePanierTrie;
    }
    
    /**
    * Trie la liste du panier par vendeur.
    *
    * @param listePanier la liste du panier à trier
    * @return la liste du panier triée par vendeur
    */
    public List<List<String>> trierParVendeur(List<List<String>> listePanier){
        List<List<String>> listePanierTrie = listePanier;
        listePanierTrie.sort(new ComparatorPanierVendeur());
        return listePanierTrie;
    }

    /**
    * Trie la liste du panier en plaçant les éléments d'une catégorie spécifiée en premier.
    *
    * @param listePanier la liste du panier à trier
    * @param categorie   la catégorie d'articles à placer en premier
    * @return la liste du panier triée avec les articles de la catégorie spécifiée en premier
    */
    public List<List<String>> trierParCategorie(List<List<String>> listePanier, String categorie){
        List<List<String>> listePanierTrie = new ArrayList<>();
        List<List<String>> listeTemp = new ArrayList<>();
        for (List<String> article : listePanier){
            if (article.get(4).equals(categorie)){
                listePanierTrie.add(article);
            }
            else{
                listeTemp.add(article);
            }
        }
        listePanierTrie.addAll(listeTemp);
        return listePanierTrie;
    }
}
