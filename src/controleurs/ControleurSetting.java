package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import javafx.event.ActionEvent;

public class ControleurSetting implements EventHandler<ActionEvent> {
    
    /**
     * L'application principale
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur
     * @param appli l'application principale
     */
    public ControleurSetting(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Gestion de l'événement sur le bouton "Paramètres"
     * @param event l'événement
     */
    @Override
    public void handle(ActionEvent event) {
        this.appli.pageSetting();
    }
}