package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurMessage implements EventHandler<ActionEvent> {
    
    /**
     * L'application principale
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur
     * @param appli l'application principale
     */
    public ControleurMessage(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Gestion de l'évènement sur le bouton "Message"
     * @param event l'évènement
     */
    @Override
    public void handle(ActionEvent event) {
        try {
            this.appli.pageMessage();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}