package controleurs;
import javafx.event.EventHandler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import modeles.Panier;
import vues.FenetrePanier;

public class ControleurFiltragePanier implements EventHandler<ActionEvent> {

    /**
     * Panier de l'utilisateur
     */
    Panier panier;
    /**
     * Fenêtre du panier
     */
    FenetrePanier fenetrePanier;
    /**
     * Liste des filtres à appliquer
     */
    List<String> listeFiltres = new ArrayList<>();  
 
    /**
     * Constructeur du controleur de filtrage du panier
     * @param panier Panier de l'utilisateur
     * @param fenetrePanier Fenêtre du panier
     */
    public ControleurFiltragePanier(Panier panier, FenetrePanier fenetrePanier) {
        this.panier = panier;
        this.fenetrePanier = fenetrePanier;
    }

    /**
     * Réinitialise la liste des filtres
     * @param listeFiltres Liste des filtres à réinitialiser
     */
    @Override
    public void handle(ActionEvent event) {
        RadioButton rb = null;
        Button b = null;
        if (event.getSource() instanceof RadioButton) {
            rb = (RadioButton) event.getSource();
        }
        else if (event.getSource() instanceof Button) {
            b = (Button) event.getSource();
        }
        
        if (rb != null) {
            switch (rb.getText()) {
                case "Vêtement":
                    listeFiltres.add("1");
                    if (listeFiltres.contains("2")) {
                        listeFiltres.remove("2");
                    }
                    else if (listeFiltres.contains("3")) {
                        listeFiltres.remove("3");
                    }
                    else if (listeFiltres.contains("4")) {
                        listeFiltres.remove("4");
                    }
                    break;
                case "Meuble":
                    listeFiltres.add("2");
                    if (listeFiltres.contains("1")) {
                        listeFiltres.remove("1");
                    }
                    else if (listeFiltres.contains("3")) {
                        listeFiltres.remove("3");
                    }
                    else if (listeFiltres.contains("4")) {
                        listeFiltres.remove("4");
                    }
                    break;
                case "Ustensile de cuisine":
                    listeFiltres.add("3");
                    if (listeFiltres.contains("2")) {
                        listeFiltres.remove("2");
                    }
                    else if (listeFiltres.contains("1")) {
                        listeFiltres.remove("1");
                    }
                    else if (listeFiltres.contains("4")) {
                        listeFiltres.remove("4");
                    }
                    break;
                case "Outil":
                    listeFiltres.add("4");
                    if (listeFiltres.contains("2")) {
                        listeFiltres.remove("2");
                    }
                    else if (listeFiltres.contains("3")) {
                        listeFiltres.remove("3");
                    }
                    else if (listeFiltres.contains("1")) {
                        listeFiltres.remove("1");
                    }
                    break;
                case "Prix croissant":
                    listeFiltres.add("5");
                    if (listeFiltres.contains("6")) {
                        listeFiltres.remove("6");
                    }
                    break;
                case "Prix décroissant":
                    listeFiltres.add("6");
                    if (listeFiltres.contains("5")) {
                        listeFiltres.remove("5");
                    }
                    break;
                case "La plus récente":
                    listeFiltres.add("7");
                    if (listeFiltres.contains("8")) {
                        listeFiltres.remove("8");
                    }
                    break;
                case "La plus ancienne":
                    listeFiltres.add("8");
                    if (listeFiltres.contains("7")) {
                        listeFiltres.remove("7");
                    }
                    break;
                case "Vendeur alphabétique":
                    listeFiltres.add("9");
                    if (listeFiltres.contains("10")) {
                        listeFiltres.remove("10");
                    }
                    break;
                case "Vendeur anti-alphabétique":
                    listeFiltres.add("10");
                    if (listeFiltres.contains("9")) {
                        listeFiltres.remove("9");
                    }
                    break;
                default:
                    break;
            }
        }
        else if (b != null) {
            switch (b.getText()) {
                case "Filtrer":
                    this.resetFiltrage();
                    filtrage();
                    this.fenetrePanier.updatePanier();
                    break;
                case "Réinitialiser":
                    this.resetFiltrage();
                    this.fenetrePanier.resetButtonsFiltrage();
                    this.fenetrePanier.updatePanier();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Réinitialise la liste des filtres
     */
    public void filtrage() {
        for (int i = 0; i < listeFiltres.size(); i++) {
            switch (listeFiltres.get(i)) {
                case "1":
                    this.fenetrePanier.setLePanier(this.panier.trierParCategorie(this.fenetrePanier.getLePanier(), "Vêtement"));
                    break;
                case "2":
                    this.fenetrePanier.setLePanier(this.panier.trierParCategorie(this.fenetrePanier.getLePanier(), "Meuble"));
                    break;
                case "3":
                    this.fenetrePanier.setLePanier(this.panier.trierParCategorie(this.fenetrePanier.getLePanier(), "Ustensile Cuisine"));
                    break;
                case "4":
                    this.fenetrePanier.setLePanier(this.panier.trierParCategorie(this.fenetrePanier.getLePanier(), "Outil"));
                    break;
                case "5":
                    this.fenetrePanier.setLePanier(this.panier.trieParPrix(this.fenetrePanier.getLePanier()));
                    break;
                case "6":
                    List<List<String>> listePrixReversed = this.panier.trieParPrix(this.fenetrePanier.getLePanier());
                    Collections.reverse(listePrixReversed);
                    this.fenetrePanier.setLePanier(listePrixReversed);
                    break;
                case "7":
                    List<List<String>> listeDateReversed = this.panier.trierParDate(this.fenetrePanier.getLePanier());
                    Collections.reverse(listeDateReversed);
                    this.fenetrePanier.setLePanier(listeDateReversed);
                    break;
                case "8":
                    this.fenetrePanier.setLePanier(this.panier.trierParDate(this.fenetrePanier.getLePanier()));
                    break;
                case "9":
                    this.fenetrePanier.setLePanier(this.panier.trierParVendeur(this.fenetrePanier.getLePanier()));
                    break;
                case "10":
                    List<List<String>> listeVendeurReversed = this.panier.trierParVendeur(this.fenetrePanier.getLePanier());
                    Collections.reverse(listeVendeurReversed);
                    this.fenetrePanier.setLePanier(listeVendeurReversed);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Réinitialise la liste des filtres
     */
    public void resetFiltrage() {
        this.fenetrePanier.setLePanier(this.panier.trierParDate(this.fenetrePanier.getLePanier()));
    }
        
}
