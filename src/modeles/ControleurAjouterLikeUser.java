package modeles;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import java.sql.SQLException;

import javafx.event.ActionEvent;

    /**
    * Cette classe implémente l'interface Comparator pour comparer des listes de chaînes de caractères en fonction d'un critère spécifique.
    * La comparaison est basée sur le contenu de la quatrième colonne (indice 3) des listes.
    */
public class ControleurAjouterLikeUser implements EventHandler<ActionEvent> {
    
    /**
     * application principale
     */
    private ApplicationVAE appli;
    /**
     * id de l'utilisateur
     */
    private int idUtilisateur;
    /**
     * utilisateur modele
     */
    private Utilisateur userModele;
    
    /**
     * Constructeur ControleurAjouterLikeUser
     * @param appli application principale
     * @param idUtilisateur id de l'utilisateur
     * @param userModele utilisateur modele
     */
    public ControleurAjouterLikeUser(ApplicationVAE appli, int idUtilisateur, Utilisateur userModele){
        this.appli = appli;
        this.idUtilisateur = idUtilisateur;
        this.userModele = userModele;
    }

    /**
     * Méthode handle
     * @param event event
     */
    @Override
    public void handle(ActionEvent event) {
        try {
            this.userModele.incrementerLike(this.idUtilisateur);
            this.appli.pageProfilExterne(idUtilisateur);
        } catch (SQLException e) {
           e.printStackTrace();
        }
    }
}