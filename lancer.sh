#!/bin/bash
find ./src/ -type f -name "*.java" > sources.txt
javac -d bin --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls --class-path ./jar/itextpdf.jar @sources.txt
javadoc -d doc --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls --class-path ./jar/itextpdf.jar @sources.txt
java -cp ./bin:/usr/share/java/mariadb-java-client.jar:./jar/itextpdf.jar --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls vues.ApplicationVAE