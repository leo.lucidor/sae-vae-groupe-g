package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import modeles.BaseDeDonnee;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javafx.event.ActionEvent;

public class ControleurBoutonPanierSurencherir implements EventHandler<ActionEvent>{

    /**
     * Application principale
     */
    private ApplicationVAE appli;
    /**
     * Base de donnee
     */
    private BaseDeDonnee bd;
    /**
     * Id de la vente
     */
    private String idve;
    /**
     * Nom de l'objet
     */
    private String nomObjet;
    /**
     * Vendeur de l'objet
     */
    private String vendeur;
    /**
     * Prix actuel de l'objet
     */
    private String prixActuel;

    /**
     * Constructeur du controleur du bouton panier surencherir
     * @param appli application principale
     * @param bd base de donnee
     * @param idve id de la vente
     * @param nomObjet nom de l'objet
     * @param vendeur vendeur de l'objet
     * @param prixActuel prix actuel de l'objet
     */
    public ControleurBoutonPanierSurencherir(ApplicationVAE appli, BaseDeDonnee bd, String idve, String nomObjet, String vendeur, String prixActuel) {
        this.appli = appli;
        this.bd = bd;
        this.idve = idve;
        this.nomObjet = nomObjet;
        this.vendeur = vendeur;
        this.prixActuel = prixActuel;
    }

    /**
     * Methode qui permet de gerer l'action du bouton panier surencherir
     * @param event evenement
     */
    @Override
    public void handle(ActionEvent event) {
        System.out.println(idve + nomObjet + vendeur);
        try {
            List<String> infoEnchere = new ArrayList<>();
            infoEnchere = this.bd.getInfoByIdVe(Integer.parseInt(this.idve));
            this.appli.pageEncherir(this.idve, this.nomObjet, infoEnchere.get(2), this.prixActuel, this.vendeur);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
