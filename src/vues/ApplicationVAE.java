package vues;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import java.sql.SQLException;
import controleurs.ControleurAjouterVente;
import controleurs.ControleurBoutonInscription;
import controleurs.ControleurCertificat;
import controleurs.ControleurChoixTypeUserAdmin;
import controleurs.ControleurDeconnexion;
import controleurs.ControleurEnchere;
import controleurs.ControleurInfo;
import controleurs.ControleurInscription;
import controleurs.ControleurMenu;
import controleurs.ControleurMessage;
import controleurs.ControleurMotDePasseOublier;
import controleurs.ControleurPageMotDePasseOublie;
import controleurs.ControleurPageSignalement;
import controleurs.ControleurPageUserInactif;
import controleurs.ControleurPanier;
import controleurs.ControleurProfil;
import controleurs.ControleurQuitter;
import controleurs.ControleurRecapVente;
import controleurs.ControleurRetourMenuConnexion;
import controleurs.ControleurRetourPageAccueil;
import controleurs.ControleurSetting;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import modeles.Admin;
import modeles.BaseDeDonnee;
import modeles.Message;
import modeles.Utilisateur;
import modeles.Vente;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ApplicationVAE extends Application {

    /**
     * la base de donnée de l'application
     */
    private BaseDeDonnee bdVAE;
    /**
     * la raison du signalement
     */
    private String raisonText;
    /**
     * le bouton pour afficher le menu
     */
    private Button boutonMenu;
    /**
     * le bouton pour cacher le menu
     */
    private boolean menuCacher;
    /**
     * le bouton le mot de passe oublié
     */
    private Button mdpOublie;
    /**
     * le bouton pour afficher la fenetre de signalement
     */
    private Button boutonFenetreSignalement;
    /**
     * le bouton pour afficher la fenetre d'accueil utilisateur
     */
    private Button boutonAccueil;
    /**
     * le bouton pour afficher la fenetre d'accueil administrateur
     */
    private Button boutonAccueilAdmin;
    /**
     * le bouton pour quitter l'application
     */
    private Button boutonQuitter;
    /**
     * le bouton pour quitter l'application en mode administrateur
     */
    private Button boutonQuitterAdmin;
    /**
     * le bouton pour afficher la fenetre d'inscription
     */
    private Button boutonInscription;
    /**
     * le bouton pour finaliser l'inscription
     */
    private Button boutonFinalisationInscription;
    /**
     * le bouton pour se connecter
     */
    private Button boutonConnexion;
    /**
     * le bouton pour se deconnecter
     */
    private Button boutonDeconnexion;
    /**
     * le bouton pour revenir a la page de connexion
     */
    private Button boutonRetourMenuConnexion;
    /**
     * le bouton pour afficher la page message
     */
    private Button boutonMessage;
    /**
     * le bouton pour afficher la page message en mode administrateur
     */
    private Button boutonMessageAdmin;
    /**
     * le bouton pour afficher la page information
     */
    private Button boutonInfo;
    /**
     * le bouton pour afficher la page information en mode administrateur
     */
    private Button boutonInfoAdmin;
    /**
     * le bouton pour afficher la page setting
     */
    private Button boutonSetting;
    /**
     * le bouton pour afficher la page setting en mode administrateur
     */
    private Button boutonSettingAdmin;
    /**
     * le bouton pour afficher la page profil
     */
    private Button boutonProfil;
    /**
     * le bouton pour afficher la page enchere
     */
    private Button boutonEnchere;
    /**
     * le bouton pour afficher la page panier
     */
    private Button boutonPanier;
    /**
     * le bouton pour afficher la page recap vente
     */
    private Button boutonRecapVente;
    /**
     * le bouton pour afficher la page certificat de vente
     */
    private Button boutonCertificatVente;
    /**
     * le bouton pour afficher la page user inactif
     */
    private Button boutonUserInactif;
    /**
     * le bouton pour afficher la page ajout vente
     */
    private Button boutonAjouterVente;
    /**
     * le bouton pour recuperer le mot de passs (mail)
     */
    private Button boutonRecuperationMDP;
    /**
     * le text pour le menu
     */
    private Label textMenu;
    /**
     * le text pour la page d'accueil
     */
    private Label textPageAccueil;
    /**
     * le text pour vendre un produit
     */
    private Label textVendreUnProduit;
    /**
     * le text pour les ventes aux enchères
     */
    private Label textVenteAuxEncheres;
    /**
     * le text pour les messages
     */
    private Label textMessagerie;
    /**
     * le text pour le panier
     */
    private Label textMonPanier;
    /**
     * le text pour les informations
     */
    private Label textInformations;
    /**
     * le text pour les paramètres
     */
    private Label textSetting;
    /**
     * le text pour quitter l'application
     */
    private Label textQuitter;
    /**
     * la scene de l'application
     */
    private Scene scene;
    /**
     * le borderPane root de l'application
     */
    private BorderPane root;
    /**
     * la fenetre de l'application
     */
    private Stage stage;
    /**
     * pour savoir si on est dans les menu de connexion ou dans l'application
     */
    private boolean grandePage;
    /**
     * le menu option
     */
    private BorderPane borderMenuOption;
    /**
     * le menu de gauche
     */
    private BorderPane borderMenuLeftUser;
    /**
     * le menu de gauche en mode administrateur
     */
    private BorderPane borderMenuLeftAdmin;
    /**
     * le menu du haut
     */
    private BorderPane borderMenuTop;
    /**
     * le centre de la page
     */
    private BorderPane bpPageCenter;
    /**
     * le modele utilisateur
     */
    private Utilisateur userModele;
    /**
     * le modele vente
     */
    private Vente venteModele;
    /**
     * le modele message
     */
    private Message messageModele;
    /**
     * le modele admin
     */
    private Admin adminModele;
    /**
     * le textfield pour l'identifiant
     */
    private TextField identifiant;
    /**
     * le textfield pour le mot de passe
     */
    private PasswordField mdp;
    /**
     * le textfield pour le pseudo
     */
    private TextField pseudoInscription;
    /**
     * le textfield pour le mail
     */
    private TextField mailInscription;
    /**
     * le textfield pour le mail de recuperation du mot de passe
     */
    private TextField mailRecuperationMDP;
    /**
     * le textfield pour le mot de passe administrateur
     */
    private PasswordField mdpAdministrateurInscription;
    /**
     * le textfield pour le mot de passe de l'inscription
     */
    private PasswordField mdpInscription;
    /**
     * la combobox pour le role de l'inscription
     */
    private ComboBox<String> roleInscription;
    /**
     * le label pour la mauvaise connexion
     */
    private Label mauvaiseConnexion;
    /**
     * le label pour le mauvais pseudo
     */
    private Label mauvaisPseudo;
    /**
     * le label pour le mauvais mail
     */
    private Label mauvaisMail;
    /**
     * le label pour le mauvais mot de passe
     */
    private Label mauvaisMdp;
    /**
     * le label pour le mauvais mail de recuperation du mot de passe
     */
    private Label mauvaisMailRecuperationMDP;
    /**
     * l'image de photo de profil
     */
    private ImageView profilIMG;

    /**
     * Permet de pouvoir executer 
     * @param args les arguments à executer
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Permet de lancer l'application
     * @param primaryStage la fenetre de l'application
     * @throws Exception
     */
    @Override
    public void init() throws SQLException, ClassNotFoundException{
        this.bdVAE = new BaseDeDonnee();
        this.profilIMG = new ImageView(new Image("file:./images/iconProfil.png"));
        this.mauvaisMailRecuperationMDP = new Label("Le mail n'est pas valide");
        this.mauvaiseConnexion = new Label("Le pseudo ou le mot de passe est incorrect");
        this.mauvaisPseudo = new Label("Le pseudo ne contient pas 3 caractères");
        this.mauvaisMail = new Label("Le mail n'est pas valide");
        this.mauvaisMdp = new Label("       Le mot de passe n'est pas valide \n (8 caractères, 1 chiffre, 1 majuscule, 1 caractère spécial)");
        this.identifiant = new TextField();
        this.identifiant.setText("adm1");
        this.pseudoInscription = new TextField();
        this.mailInscription = new TextField();
        this.mdpInscription = new PasswordField();
        this.mailRecuperationMDP = new TextField();
        this.mdpAdministrateurInscription = new PasswordField();
        this.roleInscription = new ComboBox<>();
        String admin = new String("Administrateur");
        String user = new String("Utilisateur");
        this.roleInscription.getItems().addAll(admin, user);
        this.mdp = new PasswordField();
        this.mdp.setText("adm1");
        this.userModele = new Utilisateur(1, this.profilIMG, this.bdVAE);
        this.venteModele = new Vente(this.bdVAE);
        this.adminModele = new Admin(this.bdVAE);
        this.messageModele = new Message(this.bdVAE);
        this.boutonMenu = new Button("");
        this.menuCacher = false;
        this.boutonRecuperationMDP = new Button("Récupérer mon mot de passe");
        this.mdpOublie = new Button("Mot de passe oublié ?");
        this.boutonFenetreSignalement = new Button("");
        this.boutonAccueil = new Button("");
        this.boutonAccueilAdmin = new Button();
        this.boutonQuitter = new Button("");
        this.boutonQuitterAdmin = new Button();
        this.boutonConnexion = new Button("Connexion");
        this.boutonInscription = new Button("Inscription");
        this.boutonFinalisationInscription = new Button("Inscription");
        this.boutonDeconnexion = new Button("Déconnexion");
        this.boutonRetourMenuConnexion = new Button("");
        this.boutonMessage = new Button("");
        this.boutonMessageAdmin = new Button("");
        this.boutonInfo = new Button("");
        this.boutonInfoAdmin = new Button();
        this.boutonSetting = new Button("");
        this.boutonSettingAdmin = new Button();
        this.boutonProfil = new Button("");
        this.boutonEnchere = new Button("");
        this.boutonPanier = new Button("");
        this.boutonRecapVente = new Button("");
        this.boutonCertificatVente = new Button("");
        this.boutonUserInactif = new Button("");
        this.boutonAjouterVente = new Button("");
        this.textMenu = new Label("Menu");
        this.textPageAccueil = new Label("Page d'accueil");
        this.textVendreUnProduit = new Label("Vendre un produit");
        this.textVenteAuxEncheres = new Label("Vente aux enchères");
        this.textMessagerie = new Label("Messagerie");
        this.textMonPanier = new Label("Mon panier");
        this.textInformations = new Label("Informations");
        this.textSetting = new Label("Paramètres");
        this.textQuitter = new Label("Quitter");
        this.boutonRecuperationMDP.setOnAction(new ControleurMotDePasseOublier(this, this.userModele));
        this.mdpOublie.setOnAction(new ControleurPageMotDePasseOublie(this));
        this.boutonAccueil.setOnAction(new ControleurRetourPageAccueil(this, this.userModele));
        this.boutonAccueilAdmin.setOnAction(new ControleurRetourPageAccueil(this, this.userModele));
        this.boutonMenu.setOnAction(new ControleurMenu(this, this.borderMenuOption));
        this.boutonQuitter.setOnAction(new ControleurQuitter(this));
        this.boutonQuitterAdmin.setOnAction(new ControleurQuitter(this));
        this.boutonInscription.setOnAction(new ControleurBoutonInscription(this));
        this.boutonFinalisationInscription.setOnAction(new ControleurInscription(this, this.mdpAdministrateurInscription, this.userModele, this.roleInscription));
        this.boutonConnexion.setOnAction(new ControleurChoixTypeUserAdmin(this, this.userModele));
        this.boutonRetourMenuConnexion.setOnAction(new ControleurRetourMenuConnexion(this));
        this.boutonDeconnexion.setOnAction(new ControleurDeconnexion(this));
        this.boutonMessage.setOnAction(new ControleurMessage(this));
        this.boutonMessageAdmin.setOnAction(new ControleurMessage(this));
        this.boutonInfo.setOnAction(new ControleurInfo(this));
        this.boutonInfoAdmin.setOnAction(new ControleurInfo(this));
        this.boutonSetting.setOnAction(new ControleurSetting(this));
        this.boutonSettingAdmin.setOnAction(new ControleurSetting(this));
        this.boutonProfil.setOnAction(new ControleurProfil(this));
        this.boutonEnchere.setOnAction(new ControleurEnchere(this));
        this.boutonPanier.setOnAction(new ControleurPanier(this));
        this.boutonRecapVente.setOnAction(new ControleurRecapVente(this));
        this.boutonAjouterVente.setOnAction(new ControleurAjouterVente(this));
        this.boutonFenetreSignalement.setOnAction(new ControleurPageSignalement(this));
        this.boutonCertificatVente.setOnAction(new ControleurCertificat(this));
        this.boutonUserInactif.setOnAction(new ControleurPageUserInactif(this));
        this.textMenu.setVisible(false);
        root = new FenetreAccueil(this.mdpOublie, this.boutonInscription, this.boutonConnexion, this.identifiant, this.mdp, this.mauvaiseConnexion);
        scene = new Scene(root, 700, 700);
        this.grandePage = false;
        this.roleInscription.valueProperty().addListener((observable, oldValue, newValue) -> {
            // Code à exécuter lorsque la sélection de la ComboBox change
            if(newValue.equals("Administrateur")){
                this.changerVisibiliteMdpAdminInscription("Admin");
            }
            else {
                this.changerVisibiliteMdpAdminInscription("User");
            }
        });
        this.bpPageCenter = new BorderPane();
        this.borderMenuOption = new MenuOptions(this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter);
        this.borderMenuLeftAdmin = new MenuLeftAdmin(this.boutonUserInactif, this.boutonCertificatVente, this.boutonRecapVente, this.boutonFenetreSignalement, this.boutonAccueilAdmin, this.boutonQuitterAdmin, this.boutonMessageAdmin, this.boutonInfoAdmin, this.boutonSettingAdmin, this.bpPageCenter);
        this.borderMenuLeftUser = new MenuLeft(this.boutonAccueil, this.boutonQuitter, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente, this.bpPageCenter);
        this.borderMenuTop = new MenuTop(this.userModele, this.textMenu, this.boutonDeconnexion, this.boutonProfil, this.boutonMenu, this.bpPageCenter);
        this.borderMenuOption.setVisible(false);
        this.borderMenuOption.setBackground(new Background(new BackgroundFill(Color.web("#1F1F1F"), new CornerRadii(0), Insets.EMPTY)));
    }

    /**Fonction qui lance l'application
     * @param primaryStage la fenetre principale de l'application
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        this.stage.setTitle("Page connexion");
        this.stage.setScene(scene);
        this.stage.setResizable(false);
        this.stage.show();
        // permet de changer l'icone de la fenêtre
        Image iconAppli = new Image("file:./images/logoAppli.png");
        stage.getIcons().add(iconAppli);
    }

    /**Fonction qui permet de prendre la base de données de l'application
     * @return BaseDeDonnee
     */
    public BaseDeDonnee getBd(){
        return this.bdVAE;
    }

    /**
     * fonction qui retourn le menu cacher de l'application
     * @return boolean menuCacher
     */
    public boolean getMenuCacher(){
        return this.menuCacher;
    }

    /**
     * fonction qui permet de changer la visibilité du mauvais mail
     */
    public void changerVisibiliteMauvaisMailRecupMDP(){
        this.mauvaisMailRecuperationMDP.setVisible(true);
    }

    /**
     * fonction qui permet de changer la visibilité de connexion
     */
    public void changerVisibiliteConnexion(){
        this.mauvaiseConnexion.setVisible(true);
    }

    /**
     * fonction qui permet de changer la visibilité du pseudo
     */
    public void changerVisibilitePseudo(){
        this.mauvaisMail.setVisible(false);
        this.mauvaisMdp.setVisible(false);
        this.mauvaisPseudo.setVisible(true);
    }

    /**
     * fonction qui permet de changer la visibilité du mail
     */
    public void changerVisibiliteMail(){
        this.mauvaisPseudo.setVisible(false);
        this.mauvaisMdp.setVisible(false);
        this.mauvaisMail.setVisible(true);
    }

    /**
     * fonction qui permet de changer la visibilité du mdp
     */
    public void changerVisibiliteMdp(){
        this.mauvaisPseudo.setVisible(false);
        this.mauvaisMail.setVisible(false);
        this.mauvaisMdp.setVisible(true);
    }
   
    /**
     * fonction qui permet de changer la visibilité du menu
     */
    public void changerVisibiliteMenu(){
        if(this.menuCacher){
            this.borderMenuOption.setVisible(true);
            this.textMenu.setVisible(true);
            this.menuCacher = false;
        }
        else{
            this.borderMenuOption.setVisible(false);
            this.textMenu.setVisible(false);
            this.menuCacher = true;
        }
    }

    /**
     * fonction qui permet de changer la visibilité du mdp admin
     */
    public void changerVisibiliteMdpAdminInscription(String type){
        if(type.equals("Admin")){
            this.mdpAdministrateurInscription.setVisible(true);
        }
        else {
            this.mdpAdministrateurInscription.setVisible(false);
        }
    }

    /**
     * fonction qui retourne le text du textfield identifiant
     * @return String identifiant
     */
    public String getIdentifiant(){
        return this.identifiant.getText();
    }

    /**
     * fonction qui retourne le text du textfield mdp
     * @return String mdp
     */
    public String getMotDePasse() throws SQLException{
        return this.mdp.getText();
    }

    /**
     * fonction qui retourne le text du textfield identifiant inscription
     * @return String identifiant inscription
     */
    public String getPseudoInscription(){
        return this.pseudoInscription.getText();
    }

    /**
     * fonction qui retourne le text du textfield mail inscription
     * @return String mail inscription
     */
    public String getMailInscription(){
        return this.mailInscription.getText();
    }

    /**
     * fonction qui retourne le text du textfield mdp inscription
     * @return String mdp inscription
     */
    public String getMdpInscription(){
        return this.mdpInscription.getText();
    }

    /**
     * fonction qui retourne le text du textfield mdp admin inscription
     * @return String mdp admin inscription
     */
    public String getMailRecuperationMDP(){
        return this.mailRecuperationMDP.getText();
    }

    /**
     * fonction qui met permet de mettre tout les boutons utilisateur visible
     */
    public void setEnableAllBoutonPageUser(){
        this.boutonAccueil.setDisable(false);
        this.boutonMessage.setDisable(false);
        this.boutonInfo.setDisable(false);
        this.boutonSetting.setDisable(false);
        this.boutonEnchere.setDisable(false);
        this.boutonPanier.setDisable(false);
        this.boutonAjouterVente.setDisable(false);
        this.boutonProfil.setDisable(false);
    }

    /**
     * fonction qui permet de mettre tout les boutons admin visible
     */
    public void setEnableAllBoutonPageAdmin(){
        this.boutonAccueilAdmin.setDisable(false);
        this.boutonMessageAdmin.setDisable(false);
        this.boutonInfoAdmin.setDisable(false);
        this.boutonSettingAdmin.setDisable(false);
        this.boutonFenetreSignalement.setDisable(false);
        this.boutonRecapVente.setDisable(false);
        this.boutonProfil.setDisable(false);
        this.boutonCertificatVente.setDisable(false);
        this.boutonUserInactif.setDisable(false);
    }

    /**
     * fonction qui met permet de mettre la page de connexion
     */
    public void pageConnexion(){
        if(this.grandePage){
            this.stage.close();
            Pane root = new FenetreAccueil(this.mdpOublie, this.boutonInscription, this.boutonConnexion, this.identifiant, this.mdp, this.mauvaiseConnexion);
            this.scene = new Scene(root, 700, 700);
            this.stage.setTitle("Page connexion");
            this.stage.setScene(scene);
            this.stage.setResizable(false);
            this.stage.setMaximized(false);
            this.stage.show();
        }
        else{
            Pane root = new FenetreAccueil(this.mdpOublie, this.boutonInscription, this.boutonConnexion, this.identifiant, this.mdp, this.mauvaiseConnexion);
            this.scene.setRoot(root);
            this.stage.setTitle("Page connexion");
        }
    }

    /**
     * fonction qui met permet de mettre la page de mot de passe oublié
     */
    public void pageMotDePasseOublie(){
        Pane root = new FenetreMotDePasseOublie(this.boutonRetourMenuConnexion, this.mailRecuperationMDP, this.boutonRecuperationMDP, this.mauvaisMailRecuperationMDP);
        this.scene.setRoot(root);
        this.stage.setTitle("Page mot de passe oublié");
    }

    /**
     * fonction qui met permet de mettre la page d'incsription
     */
    public void pageInscription(){
        Pane root = new FenetreInscription(this.mdpAdministrateurInscription, this.mauvaisPseudo, this.mauvaisMail, this.mauvaisMdp, this.roleInscription, this.pseudoInscription, this.mailInscription, this.mdpInscription, this.boutonFinalisationInscription, this.boutonRetourMenuConnexion);
        this.stage.setTitle("Page inscription");
        this.scene.setRoot(root);
    }

    /**
     * fonction qui met permet de mettre la page d'accueil utilisateur
     */
    public void pageAcceuilUtilisateur(){
        this.setEnableAllBoutonPageUser();
        this.boutonAccueil.setDisable(true);
        if(this.grandePage){
            Pane root = new FenetreUser(this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftUser, this.boutonAccueil, this.boutonQuitter, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente, this.userModele, this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter, this.boutonDeconnexion, this.boutonMenu, this.boutonProfil);
            this.scene.setRoot(root);
            this.stage.setTitle("Page acceuil utilisateur");
        }
        else{
            this.stage.close();
            this.identifiant.clear();
            this.mdp.clear();
            Pane root = new FenetreUser(this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftUser, this.boutonAccueil, this.boutonQuitter, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente, this.userModele, this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter, this.boutonDeconnexion, this.boutonMenu, this.boutonProfil);
            this.scene = new Scene(root, 800, 500);
            this.stage.setTitle("Page acceuil utilisateur");
            this.stage.setScene(scene);
            this.stage.setResizable(true);
            this.stage.setMaximized(true);
            this.stage.show();
        }
    }

    /**
     * fonction qui met permet de mettre la page d'accueil administrateur
     * @throws SQLException
     */
    public void pageAcceuilAdministrateur() throws SQLException{
        this.setEnableAllBoutonPageAdmin();
        this.boutonAccueilAdmin.setDisable(true);
        if(this.grandePage){
            Pane root = new FenetreAdmin(this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin,  this.borderMenuOption);
            this.scene.setRoot(root);
            this.stage.setTitle("Page acceuil administrateur");
        }
        else{
            this.stage.close();
            this.identifiant.clear();
            this.mdp.clear();
            Pane root = new FenetreAdmin(this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin,  this.borderMenuOption);
            this.scene = new Scene(root, 800, 500);
            this.stage.setTitle("Page acceuil administrateur");
            this.stage.setScene(scene);
            this.stage.setResizable(true);
            this.stage.setMaximized(true);
            this.stage.show();
        }
    } 

    /**
     * fonction qui met permet de mettre la valeur de la page
     */
    public void setGrandPage(boolean val){
        this.grandePage = val;
    }
    
    /**
     * fonction qui met permet de mettre la page message
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void pageMessage() throws ClassNotFoundException, SQLException{
        if(userModele.getRole() == 2){
            this.setEnableAllBoutonPageUser();
            this.boutonMessage.setDisable(true);
        }
        else {
            this.setEnableAllBoutonPageAdmin();
            this.boutonMessageAdmin.setDisable(true);
        }
        Pane root = new FenetreMessage(this, this.messageModele, this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin, this.userModele, this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page message");
    }  

    /**
     * fonction qui met permet de mettre la page information
     */
    public void pageInfo(){
        if(userModele.getRole() == 2){
            this.setEnableAllBoutonPageUser();
            this.boutonInfo.setDisable(true);
        }
        else {
            this.setEnableAllBoutonPageAdmin();
            this.boutonInfoAdmin.setDisable(true);
        }
        Pane root = new FenetreInfo(this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin, this.userModele, this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page information");
    } 

    /**
     * fonction qui met permet de mettre la page encherir
     * @param idVente id de la vente
     * @param nomObjet nom de l'objet
     * @param prixBase prix de base
     * @param prixMini prix minimum
     * @param vendeur vendeur
     * @throws SQLException
     * @throws NumberFormatException
     */
    public void pageEncherir(String idVente, String nomObjet, String prixBase, String prixMini, String vendeur) throws SQLException, NumberFormatException, ClassNotFoundException{
        this.setEnableAllBoutonPageUser();
        this.boutonEnchere.setDisable(true);
        Pane root = new FenetreEncherir(this.messageModele, this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin, this.bdVAE, this, this.userModele, idVente, nomObjet, prixBase, prixMini, vendeur, this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page encherir");
    } 

    /**
     * fonction qui met permet de mettre la page setting
     */
    public void pageSetting(){
        if(userModele.getRole() == 2){
            this.setEnableAllBoutonPageUser();
            this.boutonSetting.setDisable(true);
        }
        else {
            this.setEnableAllBoutonPageAdmin();
            this.boutonSettingAdmin.setDisable(true);
        }
        Pane root = new FenetreSetting(this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin, this.userModele, this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page paramètre");
    } 

    /**
     * fonction qui met permet de mettre la page profil
     * @throws SQLException
     */
    public void pageProfil() throws SQLException{
        if(userModele.getRole() == 2){
            this.setEnableAllBoutonPageUser();
        }
        else {
            this.setEnableAllBoutonPageAdmin();
        }
        this.boutonProfil.setDisable(true);
        Pane root = new FenetreProfil(this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin, this.userModele, this, this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page profil");
    }
    
    /**
     * fonction qui met permet de mettre la page choix photo profil
     */
    public void pageChoixPhotoProfil(){
        Pane root = new FenetreChoixPhotoProfil(this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin, this.userModele, this, this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page choix photo profil");
    }

    /**
     * fonction qui met permet de mettre la page modification profil
     */
    public void pageModificationProfil(){
        Pane root = new FenetreModifierProfil(this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin, this.userModele, this, borderMenuOption, textMenu, textPageAccueil, textVendreUnProduit, textVenteAuxEncheres, textMessagerie, textMonPanier, textInformations, textSetting, textQuitter, boutonQuitter, boutonDeconnexion, boutonMenu, boutonAjouterVente, boutonMessage, boutonInfo, boutonSetting, boutonProfil, boutonEnchere, boutonPanier, boutonAccueil);
        this.scene.setRoot(root);
        this.stage.setTitle("Modification de profil");
    }

    /**
     * fonction qui met permet de mettre la page enchere
     */
    public void pageEnchere(){
        this.setEnableAllBoutonPageUser();
        this.boutonEnchere.setDisable(true);
        Pane root = new FenetreEnchere(this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin, this.userModele, this, this.venteModele, this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page enchère");
    } 

    /**
     * fonction qui met permet de mettre la page panier
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void pagePanier() throws SQLException, ClassNotFoundException{
        this.setEnableAllBoutonPageUser();
        this.boutonPanier.setDisable(true);
        Pane root = new FenetrePanier(this, this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftUser, this.bdVAE, this.userModele, this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page panier");
    } 

    /**
     * fonction qui met permet de mettre la page ajouter vente
     */
    public void pageAjouterVente(){
        this.setEnableAllBoutonPageUser();
        this.boutonAjouterVente.setDisable(true);
        Pane root = new FenetreAjouterVente(this, this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftUser, this.userModele, this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page ajouter vente");
    }

    /**
     * fonction qui met permet de mettre la page de signalement
     * @throws SQLException
     */
    public void pageSignalement() throws SQLException{
        this.setEnableAllBoutonPageAdmin();
        this.boutonFenetreSignalement.setDisable(true);
        Pane root = new FenetreSignalement(this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin, this.borderMenuOption, this.adminModele, this);
        this.scene.setRoot(root);
        this.stage.setTitle("Page signalement");
    }

    /**
     * fonction qui met permet de mettre la page de recapitulatif vente
     * @throws SQLException
     */
    public void pageRecapVente() throws SQLException {
        this.setEnableAllBoutonPageAdmin();
        this.boutonRecapVente.setDisable(true);
        Pane root = new FenetreRecapitulatifVente(this.bdVAE, this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin, this.borderMenuOption, this.adminModele);
        this.scene.setRoot(root);
        this.stage.setTitle("Page récapiulatif vente");
    }

    /**
     * fonction qui met permet de mettre la page de certificat vente
     * @throws SQLException
     */
    public void pageCertificatVente() throws SQLException {
        this.setEnableAllBoutonPageAdmin();
        this.boutonCertificatVente.setDisable(true);
        Pane root = new FenetreCertificatVente(this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin, this.borderMenuOption, this.adminModele);
        this.scene.setRoot(root);
        this.stage.setTitle("Page certificat vente");
    }

    /**
     * fonction qui met permet de mettre la page des utilisateurs inactifs
     * @throws SQLException
     */
    public void pageUserInactif() throws SQLException {
        this.setEnableAllBoutonPageAdmin();
        this.boutonUserInactif.setDisable(true);
        Pane root = new FenetreUserInactif(this, this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin, this.borderMenuOption, this.adminModele);
        this.scene.setRoot(root);
        this.stage.setTitle("Page utilisateurs inactifs");
    }

    /**
     * fonction qui met permet de mettre la page des utilisateurs externe
     * @param idUser id de l'utilisateur
     * @throws SQLException
     */
    public void pageProfilExterne(int idUser) throws SQLException {
        Pane root = new FenetreProfilExterne(idUser, this.bpPageCenter, this.borderMenuTop, this.borderMenuLeftAdmin, this.userModele, this, this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page profil externe");
    }

    /**
     * fonction qui lance un popUp pour signaler une vente
     */
    public String popUpSignalerVente() {
        Stage popupStage = new Stage();
        popupStage.initModality(Modality.APPLICATION_MODAL);
        popupStage.setTitle("Signaler");

        Label titleLabel = new Label("Motif de signalement :");
        TextArea reasonTextArea = new TextArea();

        Button submitButton = new Button("Envoyer");
        submitButton.setOnAction(event -> {
            this.raisonText = reasonTextArea.getText();
            popupStage.close();
        });

        VBox popupRoot = new VBox(titleLabel, reasonTextArea, submitButton);
        popupRoot.setSpacing(10);
        popupRoot.setPadding(new Insets(10));

        Scene popupScene = new Scene(popupRoot, 300, 200);
        popupStage.setScene(popupScene);
        popupStage.showAndWait();
        return this.raisonText;
    }
 
    /**
     * fonction qui lance un popUp pour quitter l'application
     */
    public void popUpQuitterApplication(){ 
        // Création de la fenêtre de dialogue Alert
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Quitter l'application");
        alert.setHeaderText("Vous êtes sur de vouloir quitter l'application ?");
        
        // Définition des boutons de la fenêtre de dialogue
        ButtonType yesButton = new ButtonType("Oui");
        ButtonType noButton = new ButtonType("Non");

        alert.getButtonTypes().setAll(yesButton, noButton);

        // Attente de la réponse de l'utilisateur
        javafx.stage.Window window = alert.getDialogPane().getScene().getWindow();
        window.setOnCloseRequest(event -> window.hide());

        alert.showAndWait().ifPresent(buttonType -> {
            if (buttonType == yesButton) {
                Platform.exit();
            } else if (buttonType == noButton) {
                alert.close();
            }
        });
    }

    /**
     * fonction qui lance un popUp pour valider une offre
     */
    public Alert popUpMessageOffreValider(){  
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Félicitation"); 
        alert.setHeaderText("Vous avez validé votre offre !!!");
        alert.setContentText("Vous allez être redirigé vers votre panier. ");
        return alert;
    }

    /**
     * fonction qui lance un popUp pour une offre non valide
     */
    public Alert popUpMessageOffreNonValider(){  
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Entrée non valide"); 
        alert.setHeaderText("Le montant n'est pas validé");
        alert.setContentText("Veuillez mettre un montant supérieur au prix actuel");
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        return alert;
    }

    /**
     * fonction qui lance un popUp pour un message de rétractation
     */
    public Alert popUpMessageRetractation() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Rétractation");
        alert.setHeaderText("Vous allez annuler votre offre !");
        alert.setContentText("Êtes-vous sûr de vouloir annuler votre offre ?");
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        ButtonType buttonTypeNon = new ButtonType("Non");
        ButtonType buttonTypeOui = new ButtonType("Oui");
        alert.getButtonTypes().setAll(buttonTypeOui, buttonTypeNon);
        return alert;
    }

}