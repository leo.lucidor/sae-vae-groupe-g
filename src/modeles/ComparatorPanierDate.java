package modeles;
import java.util.Comparator;
import java.util.List;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ComparatorPanierDate implements Comparator<List<String>>{
    
    /**
     * Compare deux listes de String selon la date de création du panier
     */
    @Override
    public int compare(List<String> l1, List<String> l2){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
        LocalDateTime date1 = LocalDateTime.parse(l1.get(7), formatter);
        LocalDateTime date2 = LocalDateTime.parse(l2.get(7), formatter);
        return date1.compareTo(date2);
    }
}