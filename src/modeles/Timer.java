package modeles;
import javafx.scene.control.Label;
import controleurs.ControleurTimer;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;

public class Timer extends Label {

    /**
     * timeline : la timeline du timer
     */
    private Timeline timeline;
    /**
     * keyFrame : la keyFrame du timer
     */
    private KeyFrame keyFrame;
    /**
     * controleur : le controleur du timer
     */
    private ControleurTimer controleur;
    /**
     * fin : la durée de fin du timer
     */
    private String fin;

    /**
    * Timer personnalisé affichant le temps écoulé depuis une durée de fin spécifiée.
    *
    * @param fin la durée de fin du timer au format "HH:mm:ss"
    */
    public Timer(String fin) {
        super("00:00:00:00:00");

        this.fin = fin;
        this.controleur = new ControleurTimer(this);
        this.keyFrame = new KeyFrame(Duration.millis(1000), this.controleur);
        this.timeline = new Timeline();
        this.timeline.setCycleCount(Animation.INDEFINITE);
        this.timeline.getKeyFrames().add(this.keyFrame);
    }

    /**
    * Définit le temps affiché par le timer.
    *
    * @param temps le temps écoulé en secondes
    */
    public void setTime(long temps) {
        if (temps > 0) {
            long sec = temps % 60;
            long min = (temps / 60) % 60;
            long hour = (temps / (60 * 60)) % 24;
            long day = (temps / (60 * 60 * 24)) % 365;
            long year = (temps / (60 * 60 * 24 * 365));
            this.setText(String.format("%02d:%03d:%02d:%02d:%02d", year, day, hour, min, sec));
        }
        else {
            this.setText("00:00:00:00:00");
        }
    }

    /**
    * Démarre le timer.
    */
    public void start() {
        this.timeline.play();
    }

    /**
    * Arrête le timer.
    */
    public void stop() {
        this.timeline.stop();
    }

    /**
    * Réinitialise le timer en remettant le temps à zéro.
    */
    public void reset() {
        this.timeline.stop();
        this.setText("00:00:00:00:00");
    }


    /**
    * Retourne la durée de fin du timer.
    *
    * @return la durée de fin du timer au format "HH:mm:ss"
    */
    public String getFin() {
        return this.fin;
    }

    /**
    * Retourne le temps écoulé depuis le démarrage du timer.
    *
    * @return le temps écoulé en secondes
    */
    public long getTempsEcoule() {
        return this.controleur.getTempsEcoule();
    }

}