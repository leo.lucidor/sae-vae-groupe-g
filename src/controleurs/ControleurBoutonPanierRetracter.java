package controleurs;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import vues.ApplicationVAE;
import modeles.BaseDeDonnee;

import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurBoutonPanierRetracter implements EventHandler<ActionEvent>{
    
    /**
     * Application principale
     */
    private ApplicationVAE appli;
    /**
     * Base de données
     */
    private BaseDeDonnee bd;
    /**
     * Identifiant de la vente
     */
    private String idve;
    /**
     * Identifiant de l'utilisateur
     */
    private int idut;

    /**
     * Constructeur du controleur du bouton panier
     * @param appli application principale
     * @param bd base de données
     * @param idve identifiant de la vente
     * @param idut identifiant de l'utilisateur
     */
    public ControleurBoutonPanierRetracter(ApplicationVAE appli, BaseDeDonnee bd, String idve, int idut) {
        this.appli = appli;
        this.bd = bd;
        this.idve = idve;
        this.idut = idut;
    }

    /**
     * Gestion du bouton panier
     * @param event clic sur le bouton panier
     */
    @Override
    public void handle(ActionEvent event) {

            Alert alert = this.appli.popUpMessageRetractation();
            alert.showAndWait().ifPresent(response -> {
                if (response == alert.getButtonTypes().get(0)) {
                    try {
                        this.bd.supprimerEncherir(Integer.parseInt(this.idve), this.idut);
                        this.appli.pagePanier();
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    } catch (ClassNotFoundException e) {
                        System.out.println(e.getMessage());
                    }
                }
            });
    }
}

