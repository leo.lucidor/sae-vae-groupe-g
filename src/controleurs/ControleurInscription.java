package controleurs;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import vues.ApplicationVAE;
import modeles.EmailValidator;
import modeles.PasswordValidator;
import modeles.PseudoValidator;
import modeles.Utilisateur;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurInscription implements EventHandler<ActionEvent> {
    
    /**
     * application principale
     */
    private ApplicationVAE appli;
    /**
     * utilisateur modele
     */
    private Utilisateur utilisateur;
    /**
     * role de l'utilisateur
     */
    private ComboBox<String> role;
    /**
     * mot de passe de l'administrateur
     */
    private PasswordField mdpAdminInscription;
    
    /**
     * Constructeur ControleurInscription
     * @param appli application principale
     * @param mdpAdminInsciption mot de passe de l'administrateur
     * @param utilisateur utilisateur modele
     * @param role role de l'utilisateur
     */
    public ControleurInscription(ApplicationVAE appli, PasswordField mdpAdminInsciption, Utilisateur utilisateur, ComboBox<String> role){
        this.appli = appli;
        this.utilisateur = utilisateur;
        this.role = role;
        this.mdpAdminInscription = mdpAdminInsciption;
    }

    /**
     * Methode handle
     * @param event evenement
     */
    @Override
    public void handle(ActionEvent event) {
        String roleText = this.role.getValue();
        String pseudo = this.appli.getPseudoInscription();
        String mail = this.appli.getMailInscription();
        String mdp = this.appli.getMdpInscription();
        if(PseudoValidator.isValidPseudo(pseudo)){
            if(EmailValidator.isValidEmail(mail)){
                if(PasswordValidator.isValidPassword(mdp)){
                    if(roleText.equals("Administrateur")){
                        System.out.println("insertion administrateur");
                        try {
                            if(this.mdpAdminInscription.getText().equals("admin")){
                                this.utilisateur.insertUtilisateur(pseudo, mail, mdp, 1);
                                this.appli.pageAcceuilAdministrateur();
                                this.appli.setGrandPage(true);
                            }
                        } catch (SQLException e) {
                            System.out.println("Erreur lors de l'insertion de l'utilisateur");
                            e.printStackTrace();
                        }
                    }
                    else {
                        System.out.println("insertion utilisateur");
                        try {
                            this.utilisateur.insertUtilisateur(pseudo, mail, mdp, 2);
                            this.appli.pageAcceuilUtilisateur();
                            this.appli.setGrandPage(true);
                        } catch (SQLException e) {
                            System.out.println("Erreur lors de l'insertion de l'utilisateur");
                            e.printStackTrace();
                        }
                    }
                }
                else {
                    this.appli.changerVisibiliteMdp();
                    System.out.println("mot de passe non valide (1 caractere spécial, 8 caractere, 1 chiffre, 1 majuscule)");
                }
            }
            else {
                this.appli.changerVisibiliteMail();
                System.out.println("email non valide");
            }
        }
        else {
            this.appli.changerVisibilitePseudo();
            System.out.println("pseudo trop court minimum 3 caractere");
        }
    }
}
