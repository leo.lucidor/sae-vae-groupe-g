package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import javafx.event.ActionEvent;

public class ControleurBoutonInscription implements EventHandler<ActionEvent> {
    
    /**
     * L'application VAE 
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur
     * @param appli l'application VAE
     */
    public ControleurBoutonInscription(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Méthode qui gère l'action sur le bouton inscription
     * @param event l'évènement
     */
    @Override
    public void handle(ActionEvent event) {
        this.appli.pageInscription();
    }
}
