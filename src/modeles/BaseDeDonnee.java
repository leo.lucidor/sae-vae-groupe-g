package modeles;
import java.io.BufferedReader;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import java.sql.SQLException;

public class BaseDeDonnee {
    Connection connection = null;

    public BaseDeDonnee() throws SQLException, ClassNotFoundException{
        Class.forName("org.mariadb.jdbc.Driver");
        this.connection = DriverManager.getConnection( "jdbc:mysql://158.178.203.225:80/VAE", "root", "ROOTVAE20237" );
    }

    public void creation(){
        try {
            // Chargement du pilote JDBC SQLite
            Class.forName("org.sqlite.JDBC");

            // Création de la base de données et des tables
            Statement statement = this.connection.createStatement();

            // Création des tables
            String createTableQuery = creationBD("./bd/creation_vae.sql");
            statement.executeUpdate(createTableQuery);
            statement.close();
            // Insertion du jeu de donnés
            statement = this.connection.createStatement();
            String insertDataQuery = creationBD("./bd/jeu_essai_vae.sql");
            statement.executeUpdate(insertDataQuery);
            statement.close();
        }
        catch (ClassNotFoundException e) {
            System.err.println("Impossible de charger le pilote JDBC SQLite");
        } 
        catch (SQLException e) {
            System.err.println("Erreur lors de la connexion à la base de données SQLite");
            e.printStackTrace();
        } 
        finally {
            // Fermeture de la connexion
            if (this.connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                    // Gestion des exceptions
                }
            }
        }
    }

    private String creationBD(String filePath){
        try{
          // Le fichier d'entrée
            File file = new File(filePath);    
            // Créer l'objet File Reader
            FileReader fr = new FileReader(file);  
            // Créer l'objet BufferedReader        
            BufferedReader br = new BufferedReader(fr);
            StringBuffer sb = new StringBuffer();    
            String line;
            while((line = br.readLine()) != null){
                // ajoute la ligne au buffer
                sb.append(line);      
            }
            fr.close();    
            return sb.toString();
        }
        catch(IOException e){
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Méthode qui revoit le max d'un élément d'une table
     * @param table la table dans laquelle on veux effectuer la requette
     * @param id la valeur de la colonne où on effectue la raquette 
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    int maxNum(String table, String id) throws SQLException{
		Statement st = this.connection.createStatement();
		//execution de la requete
		ResultSet rs = st.executeQuery("select max(" + id + ") from " + table);
		//chargement de la 1ere ligne
		rs.next();
		//consultation de la ligne
		int res  =rs.getInt(1);
		rs.close();
		return res;
	}


    /**
     * Méthode permetant d'insérer un utilisateur dans la base de données
     * @param pseudo le pseudo de l'utilisateur
     * @param mail le mail de l'utilisateur
     * @param mdp le mot de passe de l'utilisateur
     * @param active 'O' ou 'N' en fonction de si l'utilisateur est actif ou non
     * @param role le role de l'utilisateur (1 ou 2) admin ou utilisateur
     * @param prenomut le prenom de l'utilisateur
     * @param nomut le nom de l'utilisateur
     * @param age l'age de l'utilisateur
     * @param genre le genre de l'utilisateur
     * @param numtel le telephone de l'utilisateur
     * @param idph l'identifiant de l'image de la photo de profil
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void insertUtilisateur(String pseudo, String mail, String mdp, String active, int role, String prenomut, String nomut, int age, String genre, String numtel, int idph)throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("insert into UTILISATEUR(idUt,pseudoUt,emailUT,mdpUt,activeUt,idRole,prenomut,nomut,age,genre,numtel,idphut) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        ps.setInt(1, maxNum("UTILISATEUR", "idUt") + 1);
        ps.setString(2, pseudo);
        ps.setString(3, mail);
        ps.setString(4, mdp);
        ps.setString(5, active);
        ps.setInt(6, role);
        ps.setString(7, prenomut);
        ps.setString(8, nomut);
        ps.setInt(9, age);
        ps.setString(10, genre);
        ps.setString(11, numtel);
        ps.setInt(12, idph);
        ps.executeUpdate();
    }

    /**
     * Méthode permettant d'inserer un objet dans la base de données
     * @param nom le nom de l'objet
     * @param description la description de l'objet
     * @param categorie la categorie de l'objet
     * @param idUtilisateur l'identifiant de l'utilisateur à que appartient l'objet
     * @param prixBase le prix de base de l'objet
     * @param prixMin le prix min de l'objet
     * @param dateDebut la date de debut de la vente de l'objet
     * @param dateFin la date de fin de la vente de l'objet
     * @param statut le statut de la vente
     * @param nomVille le nom de la ville
     * @return l'identifiant de l'objet ajouté
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public int insertObjet(String nom, String description, int categorie, int idUtilisateur, int prixBase, int prixMin, String dateDebut, String dateFin, int statut , String nomVille) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("insert into OBJET(idOb,nomOb,descriptionOb,idCat,idUt) values (?, ?, ?, ?, ?)");
        int idob = maxNum("OBJET", "idOb") + 1;
        ps.setInt(1, idob);
        ps.setString(2, nom);
        ps.setString(3, description);
        ps.setInt(4, categorie);
        ps.setInt(5, idUtilisateur);
        ps.executeUpdate();
        this.insertVente(prixBase, prixMin, dateDebut, dateFin, idob, statut, nomVille);
        return idob;
    }

    public int insertObjet(String nom, String description, int categorie, int idUtilisateur){
        try{
        PreparedStatement ps = this.connection.prepareStatement("insert into OBJET(idOb,nomOb,descriptionOb,idCat,idUt) values (?, ?, ?, ?, ?)");
        ps.setInt(1, maxNum("OBJET", "idOb") + 1);
        ps.setString(2, nom);
        ps.setString(3, description);
        ps.setInt(4, categorie);
        ps.setInt(5, idUtilisateur);
        ps.executeUpdate();
        return maxNum("OBJET", "idOb");
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
            return -1;
        }
    }

    /**
     * @param prixBase le prix de base de l'objet
     * @param prixMin le prix min de l'objet
     * @param dateDebut la date de debut de la vente de l'objet
     * @param dateFin la date de fin de la vente de l'objet
     * @param statut le statut de la vente
     * @param nomVille le nom de la ville
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void insertVente(int prixBase, int prixMin, String dateDebut, String dateFin, int idob, int statut , String nomVille) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("INSERT INTO VENTE(idve,prixbase,prixmin,debutve,finve,idob,idst,nomville) values (?,?,?,?,?,?,?,?)");
        ps.setInt(1, maxNum("VENTE", "idve") + 1);
        ps.setInt(2, prixBase);
        ps.setInt(3, prixMin);
        ps.setString(4, dateDebut);
        ps.setString(5, dateFin);
        ps.setInt(6, idob);
        ps.setInt(7, statut);
        ps.setString(8, nomVille);
        ps.executeUpdate();
    }

    /**
     * Méthode pour inserer une photo de profil dans la base de données
     * @param lienImage le lien(chemin) de la photo de profil
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void insertPhoto(String lienImage) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("INSERT INTO PHOTOUT(idPhUt, imgphUt) values (?, ?)");
        ps.setInt(1, maxNum("PHOTOUT", "idPhUt") + 1);
        ps.setString(2, lienImage);
        ps.executeUpdate();
    }

    /**
     * Méthode pour inserer une photo dans la base de données
     * @param lienImage le lien(chemin) de la photo de l'objet
     * @param idOb l'identifiant de l'objet à qui on veux associer l'image
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void insertPhotoObjet(File lienImage, int idOb){
        try{
            PreparedStatement ps = this.connection.prepareStatement("INSERT INTO PHOTOOB(idPhOb, titrephOb, imgphOb, idob) values (?, ?, ?, ?)");
            BufferedImage image = ImageIO.read(lienImage);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            int i = lienImage.toString().lastIndexOf(".");
            String extension = lienImage.toString().substring(i+1);
            ImageIO.write(image, extension, outputStream);
            byte[] imageBytes = outputStream.toByteArray();

            Blob blob = connection.createBlob();
            blob.setBytes(1, imageBytes);

            ps.setInt(1, maxNum("PHOTOOB", "idPhOb") + 1);
            ps.setString(2, lienImage.getName());
            ps.setBlob(3, blob);
            ps.setInt(4, idOb);
            ps.executeUpdate();
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
            System.out.println("file");
        }
        catch(SQLException e){
            e.printStackTrace();
            System.out.println("sql");
        }
        catch(IOException e){
            System.out.println("io");
            e.printStackTrace();
        }
    }


    /**
     * Récupère les objets Image associés à un identifiant d'objet donné.
     * @param idOb L'identifiant de l'objet pour lequel récupérer les images.
     * @return Une liste d'objets Image associés à l'identifiant d'objet donné.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<Image> getImageObjet(int idOb) throws SQLException{
        List<Image> res = new ArrayList<>();
        PreparedStatement ps = this.connection.prepareStatement("SELECT imgphOb FROM PHOTOOB WHERE idOb = ?");
        ps.setInt(1, idOb);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            Blob blob = rs.getBlob(1);
            InputStream inputStream = blob.getBinaryStream();
            BufferedImage image;
            try {
                image = ImageIO.read(inputStream);
                res.add(image);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return res;
    }


    /**
     * Récupère l'identifiant d'utilisateur associé à un identifiant de vente donné.
     * @param idve L'identifiant de vente pour lequel récupérer l'identifiant d'utilisateur.
     * @return L'identifiant d'utilisateur associé à l'identifiant de vente donné.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public int getIdUserByIdVe(int idve) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("SELECT idut from UTILISATEUR natural join OBJET natural join VENTE where idve = ?");
        ps.setInt(1, idve);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getInt(1);
    }

    /**
     * Récupère tous les signalements de vente sous forme d'une liste de listes de chaînes de caractères.
     *
     * @return Une liste contenant tous les signalements de vente.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<List<String>> getAllSignalementVente() throws SQLException{
        List<List<String>> res = new ArrayList<>();
        Statement st = this.connection.createStatement();
        ResultSet rs = st.executeQuery("select idSignalement, motif, dateSignalement, idut, idve from SIGNALEMENT");
        while (rs.next()){
            List<String> ligne = new ArrayList<>();
            ligne.add(Integer.toString(rs.getInt(1)));
            ligne.add(rs.getString(2));
            ligne.add(rs.getString(3));
            ligne.add(Integer.toString(rs.getInt(4)));
            ligne.add(Integer.toString(rs.getInt(5)));
            res.add(ligne);
        }
        rs.close();
        return res;
    }


    /**
     * Récupère tous les signalements de profil utilisateur sous forme d'une liste de listes de chaînes de caractères.
     *
     * @return Une liste contenant tous les signalements de profil utilisateur.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<List<String>> getAllSignalementProfil() throws SQLException{
        List<List<String>> res = new ArrayList<>();
        Statement st = this.connection.createStatement();
        ResultSet rs = st.executeQuery("select idSignalementUt, motif, dateSignalement, idPersonneSignale, idSignaleur from SIGNALEMENTUTILISATEUR");
        while (rs.next()){
            List<String> ligne = new ArrayList<>();
            ligne.add(Integer.toString(rs.getInt(1)));
            ligne.add(rs.getString(2));
            ligne.add(rs.getString(3));
            ligne.add(Integer.toString(rs.getInt(4)));
            ligne.add(Integer.toString(rs.getInt(5)));
            res.add(ligne);
        }
        rs.close();
        return res;
    }


    /**
     * Récupère les informations d'une vente spécifique en fonction de son identifiant.
     *
     * @param idVe L'identifiant de la vente pour laquelle récupérer les informations.
     * @return Une liste de chaînes de caractères contenant les informations de la vente.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<String> getVentebyIdVe(int idVe) throws SQLException {
        List<String> res = new ArrayList<>();
        PreparedStatement ps = this.connection.prepareStatement("select idVe, pseudout, nomob, prixbase, prixmin, descriptionOb, nomville, debutve, finve, nomcat, imgphOB, idst from VENTE natural join OBJET natural join UTILISATEUR natural join PHOTOOB natural join STATUT NATURAL JOIN CATEGORIE where idve = " + idVe);
        ResultSet rs = ps.executeQuery();
        rs.next();
        res.add(Integer.toString(rs.getInt(1)));
        res.add(rs.getString(2));
        res.add(rs.getString(3));
        res.add(Integer.toString(rs.getInt(4)));
        res.add(Integer.toString(rs.getInt(5)));
        res.add(rs.getString(6));
        res.add(rs.getString(7));
        res.add(rs.getString(8));
        res.add(rs.getString(9));
        res.add(rs.getString(10));
        res.add(rs.getString(11));
        res.add(Integer.toString(rs.getInt(12)));
        
        rs.close();
        return res;
    }

    /**
     * Récupère les informations d'une vente spécifique en fonction de son identifiant.
     *
     * @param idVe L'identifiant de la vente pour laquelle récupérer les informations.
     * @return Une liste de chaînes de caractères contenant les informations de la vente.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<String> getInfoByIdVe(int idVe) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("select idve, prixbase, prixmin, debutve, finve, idob, idst, nomville from VENTE where idve = ?");
        ps.setInt(1, idVe);
        ResultSet rs = ps.executeQuery();
        List<String> res = new ArrayList<>();
        rs.next();
        res.add(Integer.toString(rs.getInt(1)));
        res.add(Integer.toString(rs.getInt(2)));
        res.add(Integer.toString(rs.getInt(3)));
        res.add(rs.getString(4));
        res.add(rs.getString(5));
        res.add(Integer.toString(rs.getInt(6)));
        res.add(rs.getString(7));
        res.add(rs.getString(8));
        rs.close();
        return res;
    }

    /**
     * Crée un nouveau signalement de vente.
     *
     * @param motif Le motif du signalement.
     * @param idut L'identifiant de l'utilisateur effectuant le signalement.
     * @param idve L'identifiant de la vente signalée.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void creerSignalementVente(String motif, int idut, int idve) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("INSERT INTO SIGNALEMENT(idSignalement, motif, dateSignalement, idut, idve) values (?, ?, NOW(), ?, ?)");
        ps.setInt(1, maxNum("SIGNALEMENT", "idSignalement") + 1);
        ps.setString(2, motif);
        ps.setInt(3, idut);
        ps.setInt(4, idve);
        ps.executeUpdate();
    }


    /**
     * Crée un nouveau signalement d'utilisateur.
     * 
     * @param motif Le motif du signalement.
     * @param idut signaleur
     * @param idut2 signaler
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void creerSignalementUser(String motif, int idut, int idut2) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("INSERT INTO SIGNALEMENTUTILISATEUR(idSignalementUt, motif, dateSignalement, idPersonneSignale, idSignaleur) values (?, ?, NOW(), ?, ?)");
        ps.setInt(1, maxNum("SIGNALEMENTUTILISATEUR", "idSignalementUt") + 1);
        ps.setString(2, motif);
        ps.setInt(3, idut);
        ps.setInt(4, idut2);
        ps.executeUpdate();
    }

    /**
     * Supprime un signalement de vente.
     *
     * @param idSignalement L'identifiant du signalement de vente à supprimer.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void supprimerSignalementVente(int idSignalement) throws SQLException{
        System.out.println(idSignalement);
        PreparedStatement ps = this.connection.prepareStatement("DELETE FROM SIGNALEMENT WHERE idSignalement = ?");
        ps.setInt(1, idSignalement);
        ps.executeUpdate();
    }

    /**
     * Supprime un signalement de profil utilisateur.
     *
     * @param idSignalement L'identifiant du signalement de profil utilisateur à supprimer.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void supprimerSignalementUser(int idSignalement) throws SQLException{
        System.out.println(idSignalement);
        PreparedStatement ps = this.connection.prepareStatement("DELETE FROM SIGNALEMENTUTILISATEUR WHERE idSignalementUt = ?");
        ps.setInt(1, idSignalement);
        ps.executeUpdate();
    }

    /**
     * Crée un nouveau message avec les informations fournies.
     *
     * @param message Le contenu du message.
     * @param expediteur L'identifiant de l'expéditeur du message.
     * @param destinataire L'identifiant du destinataire du message.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void nouveauMessage(String message, int expediteur, int destinataire) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("INSERT INTO MESSAGE(idMessage,contenuMsg,dateEnvoi,expediteur,destinataire) values (?,?,NOW(),?,?)");
        ps.setInt(1, maxNum("MESSAGE", "idMessage") + 1);
        ps.setString(2, message);
        ps.setInt(3, expediteur);
        ps.setInt(4, destinataire);
        ps.executeUpdate();
    }

    /**
     * Supprime les messages échangés entre deux utilisateurs spécifiques.
     *
     * @param expediteur L'identifiant de l'expéditeur des messages.
     * @param destinataire L'identifiant du destinataire des messages.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void supprimerMessage(int expediteur, int destinataire) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("DELETE FROM MESSAGE WHERE expediteur = ? and destinataire = ?");
        ps.setInt(1, expediteur);
        ps.setInt(2, destinataire);
        ps.executeUpdate();
        ps.close();
        ps = this.connection.prepareStatement("DELETE FROM MESSAGE WHERE expediteur = ? and destinataire = ?");
        ps.setInt(1, destinataire);
        ps.setInt(2, expediteur);
        ps.executeUpdate();
    }

    /**
     * Récupère toutes les ventes en cours avec leurs informations.
     *
     * @return Une liste de listes de chaînes de caractères contenant les informations des ventes.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<List<String>> getAllVente() throws SQLException{
        List<List<String>> res = new ArrayList<>();
        Statement st = this.connection.createStatement();
        ResultSet rs = st.executeQuery("select idVe, prixBase, prixMin, debutVe, finVe, idOb from VENTE natural join STATUT where idSt = 4");
        while (rs.next()){
            List<String> ligne = new ArrayList<>();
            ligne.add(Integer.toString(rs.getInt(1)));
            ligne.add(Integer.toString(rs.getInt(2)));
            ligne.add(Integer.toString(rs.getInt(3)));
            ligne.add(rs.getString(4));
            ligne.add(rs.getString(5));
            ligne.add(Integer.toString(rs.getInt(6)));
            res.add(ligne);
        }
        rs.close();
        return res;
    }

    /**
     * Supprime une enchère spécifique effectuée par un utilisateur sur une vente.
     *
     * @param idve L'identifiant de la vente.
     * @param idut L'identifiant de l'utilisateur ayant effectué l'enchère.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void supprimerEncherir(int idve, int idut) throws SQLException{
        System.out.println(idve);
        System.out.println(idut);
        PreparedStatement ps = this.connection.prepareStatement("DELETE FROM ENCHERIR WHERE idve = ? and idut = ?");
        ps.setInt(1, idve);
        ps.setInt(2, idut);
        ps.executeUpdate();
    }

    /**
     * Récupère tous les utilisateurs inactifs avec leurs informations.
     *
     * @return Une liste de listes de chaînes de caractères contenant les informations des utilisateurs inactifs.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<List<String>> getAllUserInactif() throws SQLException{
        List<List<String>> res = new ArrayList<>();
        Statement st = this.connection.createStatement();
        ResultSet rs = st.executeQuery("select idUt, pseudoUt, emailUt, mdpUt, activeUt, idRole, prenomUt, nomUt, age, genre, numTel, idPhUt, likes from UTILISATEUR where activeUt = 'N'");
        while (rs.next()){
            List<String> ligne = new ArrayList<>();
            ligne.add(Integer.toString(rs.getInt(1)));
            ligne.add(rs.getString(2));
            ligne.add(rs.getString(3));
            ligne.add(rs.getString(4));
            ligne.add(rs.getString(5));
            ligne.add(Integer.toString(rs.getInt(6)));
            ligne.add(rs.getString(7));
            ligne.add(rs.getString(8));
            ligne.add(Integer.toString(rs.getInt(9)));
            ligne.add(rs.getString(10));
            ligne.add(rs.getString(11));
            ligne.add(Integer.toString(rs.getInt(12)));
            ligne.add(Integer.toString(rs.getInt(13)));
            res.add(ligne);
        }
        rs.close();
        return res;
    }

    /**
     * Récupère les messages d'un utilisateur spécifique en fonction de leur statut (expéditeur ou destinataire).
     *
     * @param statut Le statut des messages à récupérer ("expediteur" ou "destinataire").
     * @param idut L'identifiant de l'utilisateur.
     * @return Une liste de listes de chaînes de caractères contenant les informations des messages.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<List<String>> getMessage(String statut,int idut) throws SQLException{
        List<List<String>> res = new ArrayList<>();
        PreparedStatement ps = this.connection.prepareStatement("SELECT idMessage, contenuMsg, expediteur, destinataire, dateEnvoi FROM MESSAGE where " + statut + " = ?");
        ps.setInt(1, idut);
        ResultSet rs = ps.executeQuery();
        while (rs.next()){

            List<String> ligne = new ArrayList<>();
            ligne.add(Integer.toString(rs.getInt(1)));
            ligne.add(rs.getString(2));
            ligne.add(Integer.toString(rs.getInt(3)));
            ligne.add(Integer.toString(rs.getInt(4)));
            ligne.add(rs.getString(5));
            res.add(ligne);
        }
        rs.close();
        return res;
    }

    /**
     * Récupère une information spécifique d'un utilisateur en fonction de l'attribut et du pseudo d'utilisateur fournis.
     *
     * @param attribut L'attribut à récupérer (colonne de la table UTILISATEUR).
     * @param pseudoUt Le pseudo de l'utilisateur.
     * @return La valeur de l'attribut de l'utilisateur spécifié.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public int getInfoUt(String attribut ,String pseudoUt) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("select ? from UTILISATEUR where pseudoUt = ?");
        ps.setString(1, attribut);
        ps.setString(2, pseudoUt);

        ResultSet rs = ps.executeQuery();

        rs.next();

        int res = rs.getInt(1);
        rs.close(); 
        return res;
    }

    /**
     * Récupère l'identifiant d'un utilisateur en fonction de son pseudo.
     *
     * @param pseudo Le pseudo de l'utilisateur.
     * @return L'identifiant de l'utilisateur.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public int getId(String pseudo) throws SQLException{
        // création d'un prepared statement pour recuperer le mdp
        PreparedStatement ps = this.connection.prepareStatement("select idut from UTILISATEUR where pseudoUt = ?");
		ps.setString(1, pseudo);
        //execution de la requete
		ResultSet rs = ps.executeQuery();
		//chargement de la 1ere ligne
		rs.next();
		//consultation de la ligne
		int res  = rs.getInt(1);
		rs.close();
		return res;
    }

    /**
     * Change la photo d'un utilisateur en mettant à jour l'identifiant de la photo dans la table UTILISATEUR.
     *
     * @param pseudo Le pseudo de l'utilisateur.
     * @param idph L'identifiant de la nouvelle photo.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void changerPhoto(String pseudo, int idph) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("UPDATE UTILISATEUR set idPhUt = ? where pseudout = ?");
        ps.setInt(1, idph);
        ps.setString(2, pseudo);
        ps.executeUpdate();
    }

    /**
     * Met à jour les informations d'un utilisateur spécifique dans la table UTILISATEUR.
     *
     * @param idUt L'identifiant de l'utilisateur.
     * @param pseudoUt Le nouveau pseudo de l'utilisateur.
     * @param emailUt Le nouvel email de l'utilisateur.
     * @param mdpUt Le nouveau mot de passe de l'utilisateur.
     * @param prenomUt Le nouveau prénom de l'utilisateur.
     * @param nomUt Le nouveau nom de l'utilisateur.
     * @param age Le nouvel âge de l'utilisateur.
     * @param genre Le nouveau genre de l'utilisateur.
     * @param numTel Le nouveau numéro de téléphone de l'utilisateur.
     * @param idPhUt Le nouvel identifiant de la photo de profil de l'utilisateur.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void miseAJourInfoUtilisateur(int idUt, String pseudoUt,String emailUT, String mdpUt, String prenomUt, String nomUt, int age, String genre, String numTel, int idPhUt) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("UPDATE UTILISATEUR set emailUt = ?, mdpUt = ?, prenomUt = ?, nomUt = ?, age = ?, genre = ?, numTel = ?, idPhUt = ?, pseudout = ? where idut = ?");
        ps.setString(1, emailUT);
        ps.setString(2, mdpUt);
        ps.setString(3, prenomUt);
        ps.setString(4, nomUt);
        ps.setInt(5, age);
        ps.setString(6, genre);
        ps.setString(7, numTel);
        ps.setInt(8, idPhUt);
        ps.setString(9, pseudoUt);
        ps.setInt(10, idUt);
        ps.executeUpdate();
    }

    /**
     * Incrémente le nombre de likes d'un utilisateur spécifique dans la table UTILISATEUR.
     *
     * @param idUt L'identifiant de l'utilisateur.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void ajouteLike(int idUt) throws SQLException{
        Statement st = this.connection.createStatement();
        st.executeQuery("UPDATE UTILISATEUR set likes = likes + 1 where idut = " + idUt);
    }

    /**
     * Décrémente le nombre de likes d'un utilisateur spécifique dans la table UTILISATEUR.
     *
     * @param idUt L'identifiant de l'utilisateur.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void enleverLike(int idUt) throws SQLException{
        Statement st = this.connection.createStatement();
        st.executeQuery("UPDATE UTILISATEUR set likes = likes - 1 where idut = " + idUt);
    }
    
    /**
     * Fonction qui renvoie une liste de idVe qui sont finit depuis 2 ans 
     * 
     * @return Liste de strings de idVe
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<String> getVenteTropVielle() throws SQLException{
        List<String> res = new ArrayList<>();
        PreparedStatement ps = this.connection.prepareStatement("select idVe from VENTE where finve < DATE_SUB(CURRENT_DATE,INTERVAL 2 YEAR)");
        ResultSet rs = ps.executeQuery();

        while(rs.next()){
            res.add(Integer.toString(rs.getInt(1)));
        }

        return res;
    }


    /**
     * Récupère les informations d'un utilisateur spécifique à partir de son identifiant.
     *
     * @param id L'identifiant de l'utilisateur.
     * @return Une liste contenant les informations de l'utilisateur dans l'ordre suivant : idUt, pseudoUt, emailUt, mdpUt, activeUt, idrole, prenomUt, nomUt, age, genre, numTel, idPhUt, likes.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<String> getInfoUserById(int id) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("select idUt, pseudoUt, emailUt, mdpUt, activeUt, idrole, prenomUt, nomUt, age, genre, numTel, idPhUt, likes from UTILISATEUR where idUt = ?");
        ps.setInt(1, id);
        //execution de la requete
        ResultSet rs = ps.executeQuery();
        rs.next();
        List<String> res = new ArrayList<>();
        res.add(Integer.toString(rs.getInt(1)));
        res.add(rs.getString(2));
        res.add(rs.getString(3));
        res.add(rs.getString(4));
        res.add(rs.getString(5));
        res.add(Integer.toString(rs.getInt(6)));
        res.add(rs.getString(7));
        res.add(rs.getString(8));
        res.add(Integer.toString(rs.getInt(9)));
        res.add(rs.getString(10));
        res.add(rs.getString(11));
        res.add(Integer.toString(rs.getInt(12)));
        res.add(Integer.toString(rs.getInt(13)));
        rs.close();
        return res;
    }

    /**
     * Récupère le mot de passe d'un utilisateur à partir de son pseudo.
     *
     * @param pseudo Le pseudo de l'utilisateur.
     * @return Le mot de passe de l'utilisateur.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public String getMdpPseudo(String pseudo) throws SQLException{
        // création d'un prepared statement pour recuperer le mdp
        PreparedStatement ps = this.connection.prepareStatement("select mdpUt from UTILISATEUR where pseudoUt = ?");
		ps.setString(1, pseudo);
        //execution de la requete
		ResultSet rs = ps.executeQuery();
		//chargement de la 1ere ligne
		rs.next();
		//consultation de la ligne
		String res  =rs.getString(1);
		rs.close();
		return res;
    }

    /**
     * Récupère le mot de passe d'un utilisateur à partir de son identifiant.
     *
     * @param id L'identifiant de l'utilisateur.
     * @return Le mot de passe de l'utilisateur.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public String getMdp(int id) throws SQLException{
        // création d'un prepared statement pour recuperer le mdp
        PreparedStatement ps = this.connection.prepareStatement("select mdpUt from UTILISATEUR where idut = ?");
		ps.setInt(1, id);
        //execution de la requete
		ResultSet rs = ps.executeQuery();
		//chargement de la 1ere ligne
		rs.next();
		//consultation de la ligne
		String res  =rs.getString(1);
		rs.close();
		return res;
    }

    /**
     * Récupère le mot de passe d'un utilisateur à partir de son adresse e-mail.
     *
     * @param email L'adresse e-mail de l'utilisateur.
     * @return Le mot de passe de l'utilisateur.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public String getMdpMail(String email) throws SQLException{
        // création d'un prepared statement pour recuperer le mdp
        PreparedStatement ps = this.connection.prepareStatement("select mdpUt from UTILISATEUR where emailut = ?");
		ps.setString(1, email);
        //execution de la requete
		ResultSet rs = ps.executeQuery();
		//chargement de la 1ere ligne
		rs.next();
		//consultation de la ligne
		String res  =rs.getString(1);
		rs.close();
		return res;
    }

    public String getRole(int id) throws SQLException{
        // création d'un prepared statement pour recuperer le role
        PreparedStatement ps = this.connection.prepareStatement("select idrole from UTILISATEUR where idut = ?");
		ps.setInt(1, id);
        //execution de la requete
		ResultSet rs = ps.executeQuery();
		//chargement de la 1ere ligne
		rs.next();
		//consultation de la ligne
		String res  =rs.getString(1);
		rs.close();
		return res;
    }

    public List<List<String>> getEnchereVente(int idVe) throws SQLException{
        List<List<String>> res = new ArrayList<>();
        PreparedStatement ps = this.connection.prepareStatement("select prenomUt, nomUt, motant, dateheure from ENCHERIR natural join UTILISATEUR where idVe = ?");
        ps.setInt(1, idVe);
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
            List<String> ligne = new ArrayList<>();
            ligne.add(rs.getString(1));
            ligne.add(rs.getString(2));
            ligne.add(Integer.toString(rs.getInt(3)));
            ligne.add(rs.getString(4));
            res.add(ligne);
        }
        rs.close();
        return res;
    }

    // nathan debut

    // Méthode pour la page Encherir et le Modele Encherir
    public void insertOffreEncherir(int idUt, int idVe, String dateheure, int montant) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("INSERT INTO ENCHERIR(idUt,idVe,dateheure,montant) values (?,?,?,?)");
        ps.setInt(1, idUt);
        ps.setInt(2, idVe);
        ps.setString(3, dateheure);
        ps.setInt(4, montant);
        ps.executeUpdate();
    }

    /**
     * Récupère les offres d'enchères pour une vente donnée.
     *
     * @param idVe L'identifiant de la vente.
     * @return Une liste contenant les offres d'enchères sous forme de listes de chaînes de caractères.
     *         Chaque liste représente une offre et contient le pseudo de l'utilisateur, la date et l'heure de l'enchère, et le montant de l'enchère.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<List<String>> getOffreEncherir(int idVe) throws SQLException{
        List<List<String>> res = new ArrayList<>();
        Statement st = this.connection.createStatement();
        ResultSet rs = st.executeQuery("SELECT pseudout,dateheure,montant FROM ENCHERIR NATURAL JOIN UTILISATEUR WHERE idVe = " + idVe + " ORDER BY montant DESC LIMIT 5");
        
        while(rs.next()){
            List<String> ligne = new ArrayList<>();
            ligne.add(rs.getString(1));
            ligne.add(rs.getString(2));
            ligne.add(Integer.toString(rs.getInt(3)));
            res.add(ligne);
        }
        rs.close();
        return res;

    }

    /**
     * Récupère le montant actuel de l'enchère pour une vente donnée.
     *
     * @param idVe L'identifiant de la vente.
     * @return Le montant actuel de l'enchère.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public int getMontantActuelEnchere(int idVe) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("SELECT montant FROM ENCHERIR WHERE idVe = ? and dateheure = (SELECT MAX(dateheure) FROM ENCHERIR WHERE idVe = ?)");
        ps.setInt(1, idVe);
        ps.setInt(2, idVe);
        ResultSet rs = ps.executeQuery(); 

        int res = 0;

        if(rs.next()){
            res = rs.getInt(1);
        }
        else{
            res = Integer.valueOf(this.getInfoByIdVe(idVe).get(1));
        }
        rs.close();
        return res;
    }

    /**
     * Récupère les informations des ventes en cours (statut 1 ou 2).
     *
     * @return Une liste contenant les informations des ventes.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<List<String>> getVente() throws SQLException {
        List<List<String>> res = new ArrayList<>();
        PreparedStatement ps = this.connection.prepareStatement("select idVe, pseudout, nomob, prixbase, prixmin, descriptionOb, nomville, debutve, finve, nomcat, imgphOB, idSt, idUt from VENTE natural join OBJET natural join UTILISATEUR natural join PHOTOOB natural join STATUT NATURAL JOIN CATEGORIE where idSt = 2 or idst = 1");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            List<String> ligne = new ArrayList<>();
            ligne.add(Integer.toString(rs.getInt(1)));
            ligne.add(rs.getString(2));
            ligne.add(rs.getString(3));
            ligne.add(Integer.toString(rs.getInt(4)));
            ligne.add(Integer.toString(rs.getInt(5)));
            ligne.add(rs.getString(6));
            ligne.add(rs.getString(7));
            ligne.add(rs.getString(8));
            ligne.add(rs.getString(9));
            ligne.add(rs.getString(10));
            ligne.add(rs.getString(11));
            ligne.add(Integer.toString(rs.getInt(12)));
            ligne.add(Integer.toString(rs.getInt(13)));
            res.add(ligne);
        }
        rs.close();
        return res;
    }
    
    /**
     * Récupère les informations des ventes d'un utilisateur à partir de son nom d'utilisateur.
     *
     * @param nom Le nom d'utilisateur.
     * @return Une liste contenant les informations des ventes de l'utilisateur.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<String> getVenteUtilisateur(String nom) throws SQLException{
        List<String> res = new ArrayList<String>();
        // création d'un prepared statement pour recuperer les ventes
        PreparedStatement ps = this.connection.prepareStatement("select pseudout, nomob, prixbase, prixmin from VENTE natural join OBJET natural join UTILISATEUR where pseudoUt = ?");
        ps.setString(1, nom + '%');
        //execution de la requete
        ResultSet rs = ps.executeQuery();
        //chargement de la 1ere ligne
        while (rs.next()){
            String ligne = "";
            ligne += rs.getString(1) + ", ";
            ligne += rs.getString(2) + ", ";
            ligne += rs.getInt(3) + ", ";
            ligne += rs.getInt(4);
            res.add(ligne);
        }
        //consultation de la ligne
        rs.close();
        return res;
    }

    /**
     * Récupère les informations sur le panier d'un utilisateur.
     *
     * @param id L'identifiant de l'utilisateur.
     * @return Une liste contenant les informations sur les ventes dans le panier de l'utilisateur.
     *         Chaque élément de la liste est une liste de chaînes de caractères représentant les informations d'une vente.
     *         Les informations sont dans l'ordre suivant : [idve, imgphOB, pseudout, nomob, nomcat, nomst, debutve, finve, pseudout2, montant, idst].
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<List<String>> getPanier(int id) throws SQLException {
        List<List<String>> res = new ArrayList<>();
        // création d'un prepared statement pour recuperer le panier de l'utilisateur                                                                                                                                                        
        PreparedStatement ps = this.connection.prepareStatement("SELECT V.idve, P.imgphOB, U.pseudout, O.nomob, C.nomcat, S.nomst, V.debutve, V.finve, U2.pseudout, Z.montant, V.idst " +
                                                                "FROM VENTE V INNER JOIN OBJET O ON V.idob = O.idob INNER JOIN CATEGORIE C ON O.idcat = C.idcat INNER JOIN STATUT S ON V.idst = S.idst INNER JOIN UTILISATEUR U ON O.idut = U.idut " +
                                                                "LEFT JOIN (SELECT idve, idut, montant FROM ENCHERIR WHERE (idve, dateheure) IN (SELECT idve, MAX(dateheure) FROM ENCHERIR GROUP BY idve)) AS Z ON V.idve = Z.idve " +
                                                                "LEFT JOIN UTILISATEUR U2 ON Z.idut = U2.idut LEFT JOIN PHOTOOB P ON O.idob = P.idob JOIN ENCHERIR E ON V.idve = E.idve WHERE E.idut = ? AND V.idst != 4");
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            List<String> ligne = new ArrayList<>();
            ligne.add(Integer.toString(rs.getInt(1)));
            ligne.add(rs.getString(2));
            ligne.add(rs.getString(3));
            ligne.add(rs.getString(4));
            ligne.add(rs.getString(5));
            ligne.add(rs.getString(6));
            ligne.add(rs.getString(7));
            ligne.add(rs.getString(8));
            ligne.add(rs.getString(9));
            ligne.add(Integer.toString(rs.getInt(10)));
            res.add(ligne);
        }
        rs.close();
        return res;
    }

    /**
     * Récupère l'historique des enchères non remportées par un utilisateur.
     *
     * @param idut L'identifiant de l'utilisateur.
     * @return Une liste contenant les informations sur les enchères non remportées par l'utilisateur.
     *         Chaque élément de la liste est une liste de chaînes de caractères représentant les informations d'une enchère.
     *         Les informations sont dans l'ordre suivant : [idve, pseudout, nomob, montant].
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<List<String>> getHistoriqueDesEncheresNonRemportees(String idut) throws SQLException {
        PreparedStatement ps = this.connection.prepareStatement("SELECT V.idve, U.pseudout, O.nomob, Z.montant FROM VENTE V INNER JOIN OBJET O ON V.idob = O.idob INNER JOIN CATEGORIE C ON O.idcat = C.idcat INNER JOIN STATUT S ON V.idst = S.idst INNER JOIN UTILISATEUR U ON O.idut = U.idut  LEFT JOIN (SELECT idve, idut, montant FROM ENCHERIR WHERE (idve, dateheure) IN (SELECT idve, MAX(dateheure) FROM ENCHERIR GROUP BY idve)) AS Z ON V.idve = Z.idve LEFT JOIN UTILISATEUR U2 ON Z.idut = U2.idut LEFT JOIN PHOTOOB P ON O.idob = P.idob JOIN ENCHERIR E ON V.idve = E.idve WHERE E.idut = ? AND U2.pseudout != ? AND S.idst = 4;");
        ps.setString(1, idut);
        ps.setString(2, this.getPseudoUtilisateur(idut));
        ResultSet rs = ps.executeQuery();
        List<List<String>> res = new ArrayList<>();
        while (rs.next()) {
            List<String> ligne = new ArrayList<>();
            ligne.add(Integer.toString(rs.getInt(1)));
            ligne.add(rs.getString(2));
            ligne.add(rs.getString(3));
            ligne.add(Integer.toString(rs.getInt(4)));
            res.add(ligne);
        }
        rs.close();
        return res;
    }

    /**
     * Récupère le pseudonyme d'un utilisateur à partir de son identifiant.
     *
     * @param id L'identifiant de l'utilisateur.
     * @return Le pseudonyme de l'utilisateur.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public String getPseudoUtilisateur(String id) throws SQLException {
        PreparedStatement ps = this.connection.prepareStatement("SELECT pseudout FROM UTILISATEUR WHERE idut = ?");
        ps.setString(1, id);
        ResultSet rs = ps.executeQuery();
        rs.next();
        String res = rs.getString(1);
        rs.close();
        return res;
    }

    /**
     * Supprime une vente et toutes les informations associées à celle-ci.
     *
     * @param idve L'identifiant de la vente à supprimer.
     * @throws SQLException En cas d'erreur lors de l'exécution des requêtes SQL.
     */
    public void supprimerVente(int idve) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("DELETE FROM ENCHERIR WHERE idve = ?");
        ps.setInt(1, idve);
        ps.executeUpdate();
        ps.close();
        ps = this.connection.prepareStatement("DELETE FROM SIGNALEMENT WHERE idve = ?");
        ps.setInt(1, idve);
        ps.executeUpdate();
        ps.close();
        ps = this.connection.prepareStatement("DELETE FROM VENTE WHERE idve = ?");
        ps.setInt(1, idve);
        ps.executeUpdate();
        ps.close();
        ps = this.connection.prepareStatement("DELETE FROM PHOTOOB WHERE idob = ?");
        ps.setInt(1, idve);
        ps.executeUpdate();
        ps.close();
        ps = this.connection.prepareStatement("DELETE FROM OBJET WHERE idob = ?");
        ps.setInt(1, idve);
        ps.executeUpdate();
    }

    /**
     * Supprime un utilisateur et toutes les informations associées à celui-ci.
     *
     * @param idut L'identifiant de l'utilisateur à supprimer.
     * @throws SQLException En cas d'erreur lors de l'exécution des requêtes SQL.
     */
    public void supprimerUtilisateur(int idut) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("DELETE FROM ENCHERIR WHERE idut = ?");
        ps.setInt(1, idut);
        ps.executeUpdate();
        ps.close();
        ps = this.connection.prepareStatement("DELETE FROM OBJET WHERE idut = ?");
        ps.setInt(1, idut);
        ps.executeUpdate();
        ps.close();
        ps = this.connection.prepareStatement("DELETE FROM MESSAGE WHERE expediteur = ? or destinataire = ?");
        ps.setInt(1, idut);
        ps.setInt(2, idut);
        ps.executeUpdate();
        ps.close();
        ps = this.connection.prepareStatement("DELETE FROM UTILISATEUR WHERE idut = ?");
        ps.setInt(1, idut);
        ps.executeUpdate();
    }

    /**
     * Change le statut d'activité d'un utilisateur.
     *
     * @param idut   L'identifiant de l'utilisateur.
     * @param active Le nouvel état d'activité de l'utilisateur ("N" pour inactif, "O" pour actif).
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public void changementStatutUtilisateur(int idut, String active) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("UPDATE UTILISATEUR set activeut = ? WHERE idut = ?");
        ps.setString(1, active);
        ps.setInt(2, idut);
        ps.executeUpdate();
    }

    /**
     * Récupère les informations sur les ventes d'un utilisateur.
     *
     * @param idut L'identifiant de l'utilisateur.
     * @return Une liste contenant les informations des ventes de l'utilisateur.
     *         Chaque élément de la liste est une liste de chaînes de caractères représentant les informations d'une vente.
     *         Les informations d'une vente sont dans l'ordre : identifiant de la vente, nom de l'objet, identifiant du statut.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<List<String>> getInfoVenteUtilisateur(int idut) throws SQLException{
        List<List<String>> res = new ArrayList<>();
        PreparedStatement ps = this.connection.prepareStatement("select idve, nomob, idst from VENTE natural join OBJET o natural join UTILISATEUR u where u.idut = ?");
        ps.setInt(1, idut);
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
            List<String> ligne = new ArrayList<>();
            ligne.add(Integer.toString(rs.getInt(1)));
            ligne.add(rs.getString(2));
            ligne.add(Integer.toString(rs.getInt(3)));
            res.add(ligne);
        }
        rs.close();
        return res;
    }

    /**
     * Récupère les informations sur les ventes remportées par un utilisateur.
     *
     * @param id L'identifiant de l'utilisateur.
     * @return Une liste contenant les informations des ventes remportées par l'utilisateur.
     *         Chaque élément de la liste est une liste de chaînes de caractères représentant les informations d'une vente.
     *         Les informations d'une vente sont dans l'ordre : identifiant de la vente, pseudo de l'utilisateur vendeur, nom de l'objet, montant de l'enchère.
     * @throws SQLException En cas d'erreur lors de l'exécution de la requête SQL.
     */
    public List<List<String>> getInfoVenteUtilisateurGagnee(String id) throws SQLException{
        List<List<String>> res = new ArrayList<>();
        PreparedStatement ps = this.connection.prepareStatement("SELECT V.idve, U.pseudout, O.nomob, Z.montant FROM VENTE V INNER JOIN OBJET O ON V.idob = O.idob INNER JOIN CATEGORIE C ON O.idcat = C.idcat INNER JOIN STATUT S ON V.idst = S.idst INNER JOIN UTILISATEUR U ON O.idut = U.idut  LEFT JOIN (SELECT idve, idut, montant FROM ENCHERIR WHERE (idve, dateheure) IN (SELECT idve, MAX(dateheure) FROM ENCHERIR GROUP BY idve)) AS Z ON V.idve = Z.idve LEFT JOIN UTILISATEUR U2 ON Z.idut = U2.idut LEFT JOIN PHOTOOB P ON O.idob = P.idob JOIN ENCHERIR E ON V.idve = E.idve WHERE E.idut = ? AND U2.pseudout = ? AND S.idst = 4 EXCEPT SELECT V.idve, U.pseudout, O.nomob, Z.montant FROM VENTE V INNER JOIN OBJET O ON V.idob = O.idob INNER JOIN CATEGORIE C ON O.idcat = C.idcat INNER JOIN STATUT S ON V.idst = S.idst INNER JOIN UTILISATEUR U ON O.idut = U.idut  LEFT JOIN (SELECT idve, idut, montant FROM ENCHERIR WHERE (idve, dateheure) IN (SELECT idve, MAX(dateheure) FROM ENCHERIR GROUP BY idve)) AS Z ON V.idve = Z.idve LEFT JOIN UTILISATEUR U2 ON Z.idut = U2.idut LEFT JOIN PHOTOOB P ON O.idob = P.idob JOIN ENCHERIR E ON V.idve = E.idve WHERE E.idut = ? AND U2.pseudout != ? AND S.idst = 4;");
        ps.setString(1, id);
        ps.setString(2, this.getPseudoUtilisateur(id));
        ps.setString(3, id);
        ps.setString(4, this.getPseudoUtilisateur(id));
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
            List<String> ligne = new ArrayList<>();
            ligne.add(Integer.toString(rs.getInt(1)));
            ligne.add(rs.getString(2));
            ligne.add(rs.getString(3));
            ligne.add(Integer.toString(rs.getInt(4)));
            res.add(ligne);
        }
        rs.close();
        return res;
    }

    public void reset(){
        String reset;
        try{
            // Le fichier d'entrée
            File file = new File("./bd/destruction_vae.sql");    
              // Créer l'objet File Reader
            FileReader fr = new FileReader(file);  
              // Créer l'objet BufferedReader        
            BufferedReader br = new BufferedReader(fr);
            StringBuffer sb = new StringBuffer();    
            String line;
            while((line = br.readLine()) != null){
                // ajoute la ligne au buffer
                sb.append(line);      
            }
            reset = sb.toString();
            fr.close();
            // Chargement du pilote JDBC SQLite
            Class.forName("org.sqlite.JDBC");
            Statement statement = this.connection.createStatement();
            statement.executeUpdate(reset);
            statement.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            System.err.println("Impossible de charger le pilote JDBC SQLite");
        } 
        catch (SQLException e) {
            System.err.println("Erreur lors de la connexion à la base de données SQLite");
            e.printStackTrace();
        }
    }
}
