CREATE TABLE `CATEGORIE` (
  `idcat` decimal(3,0),
  `nomcat` varchar(50),
  PRIMARY KEY (`idcat`)
);


CREATE TABLE `ENCHERIR` (
  `idut` decimal(6,0),
  `idve` decimal(8,0),
  `dateheure` datetime,
  `montant` decimal(8,2),
  PRIMARY KEY (`idut`, `idve`, `dateheure`),
  FOREIGN KEY (`idve`) REFERENCES `VENTE` (`idve`),
  FOREIGN KEY (`idut`) REFERENCES `UTILISATEUR` (`idut`)
);

CREATE TABLE `OBJET` (
  `idob` decimal(6,0),
  `nomob` varchar(50),
  `descriptionob` text,
  `idut` decimal(6,0),
  `idcat` decimal(3,0),
  PRIMARY KEY (`idob`),
  FOREIGN KEY (`idcat`) REFERENCES `CATEGORIE` (`idcat`)
);

CREATE TABLE `PHOTOOB` (
  `idphOB` decimal(6,0),
  `titrephOB` varchar(50),
  `imgphOB` blob,
  `idob` decimal(6,0),
  PRIMARY KEY (`idphOb`),
  FOREIGN KEY (`idob`) REFERENCES `OBJET` (`idob`)
);

CREATE TABLE `ROLE` (
  `idrole` decimal(2,0),
  `nomrole` varchar(30),
  PRIMARY KEY (`idrole`)
);

CREATE TABLE `STATUT` (
  `idst` char,
  `nomst` varchar(30),
  PRIMARY KEY (`idst`)
);

CREATE TABLE `PHOTOUT` (
  `idphUt` decimal(6,0),
  `imgphUt` varchar(100),
  PRIMARY KEY (`idphUt`)
);

CREATE TABLE `UTILISATEUR` (
  `idut` decimal(6,0),
  `pseudout` varchar(20) unique,
  `emailut` varchar(100),
  `mdput` varchar(100),
  `activeut` char(1),
  `idrole` decimal(2,0),
  `prenomut`varchar(20),
  `nomut`varchar(20),
  `age`decimal(6,0),
  `genre`varchar(20),
  `numTel`varchar(10),
  `idPhUt`decimal(6,0),
  PRIMARY KEY (`idut`),
  FOREIGN KEY (`idrole`) REFERENCES `ROLE` (`idrole`),
  FOREIGN KEY (`idPhUt`) REFERENCES `PHOTOUT` (`idPhUt`)
);

CREATE TABLE `VENTE` (
  `idve` decimal(8,0),
  `prixbase` decimal(8,2),
  `prixmin` decimal(8,2),
  `debutve` datetime,
  `finve` datetime,
  `idob` decimal(6,0),
  `idst` char,
  `nomville` varchar(50),
  PRIMARY KEY (`idve`),
  FOREIGN KEY (`idob`) REFERENCES `OBJET` (`idob`)
);

CREATE TABLE `MESSAGE` (
  `idMessage` decimal(8,0),
  `contenuMsg` varchar(500),
  `dateEnvoi` datetime,
  `expediteur` decimal(6,0),
  `destinataire` decimal(6,0),
  PRIMARY KEY (`idMessage`),
  FOREIGN KEY (`expediteur`) REFERENCES `UTILISATEUR` (`idut`),
  FOREIGN KEY (`destinataire`) REFERENCES `UTILISATEUR` (`idut`)
);

CREATE TABLE `SIGNALEMENT`(
  `idSignalement` decimal(3,0),
  `motif` varchar(100),
  `dateSignalement` date,
  `idut` decimal(6,0),
  `idve` decimal(8,0),
  PRIMARY KEY(`idSignalement`),
  FOREIGN KEY (`idut`) REFERENCES `UTILISATEUR` (`idut`),
  FOREIGN KEY (`idve`) REFERENCES `VENTE` (`idve`)
);