package controleurs;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import vues.ApplicationVAE;

public class ControleurModifierProfil implements EventHandler<ActionEvent>{
   
    /**
     * L'application principale
     */
    private ApplicationVAE appli;

    /**
     * Constructeur ControleurModifierProfil
     * @param appli l'application principale
     */
    public ControleurModifierProfil(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Méthode handle
     * @param event l'évènement
     */
    @Override
    public void handle(ActionEvent event){
        Button modificationBouton = (Button) event.getSource();
        if (modificationBouton.getText().equals("Modifier") || modificationBouton.getText().equals("Retour")){
            this.appli.pageModificationProfil();
        }
        else if(modificationBouton.getText().equals("")){
            this.appli.pageChoixPhotoProfil();
        }
    }
}
