package modeles;

public class Objet {

    /**
     * le nom de l'objet
     */
    private String nom;
    /**
     * la description de l'objet
     */
    private String description;
    /**
     * le prix de base de l'objet
     */
    private int prix;
    /**
     * le prix minimal de l'objet
     */
    private int prixMin;
    /**
     * la catégorie de l'objet
     */
    private int categorie;
    /**
     * l'id utilisateur de l'objet
     */
    private int idUtilisateur;
    /**
     * la date de début de l'objet
     */
    private String dateDebut;
    /**
     * la date de fin de l'objet
     */
    private String dateFin;
    /**
     * la première image de l'objet
     */
    private String image1;
    /**
     * la deuxième image de l'objet
     */
    private String image2;
    /**
     * la troisième image de l'objet
     */
    private String image3;
    /**
     * la quatrième image de l'objet
     */
    private String image4;
    /**
     * la ville de l'objet
     */
    private String ville;

    /**
     * Constructeur de la classe Objet
     */
    public Objet(){
        this.nom = "";
        this.description = "";
        this.prix = 0;
        this.prixMin = 0;
        this.categorie = 0;
        this.idUtilisateur = 0;
        this.image1 = "";
        this.image2 = "";
        this.image3 = "";
        this.image4 = "";
        this.dateDebut = "";
        this.dateFin = "";
        this.ville = "";
    }

    /**
     * récupère le nom
     */
    public String getNom() {
        return nom;
    }
    /**
     * récupère la description
     */
    public String getDescription() {
        return description;
    }
    /**
     * récupère le prix de base d'un objet
     */
    public int getPrixBase() {
        return prix;
    }
    /**
     * récupère le prix minimal d'un objet
     */
    public int getPrixMin() {
        return prixMin;
    }
    /**
     * récupère la catégorie d'un objet
     */
    public int getCategorie() {
        return categorie;
    }
    /**
     * récupère l'id utilisateur d'un objet
     */
    public int getIdUtilisateur() {
        return idUtilisateur;
    }
    /**
     * récupère la première image d'un objet
     */
    public String getImage1() {
        return image1;
    }
    /**
     * récupère la deuxième image d'un objet
     */
    public String getImage2() {
        return image2;
    }

    /**
     * récupère la troisième image d'un objet
     */
    public String getImage3() {
        return image3;
    }

    /**
     * récupère la quatrième image d'un objet
     */
    public String getImage4() {
        return image4;
    }

    /**
     * récupère la datre de début d'un objet
     */
    public String getDateDebut() {
        return dateDebut;
    }

    /**
     * récupère la date de fin d'un objet
     */
    public String getDateFin() {
        return dateFin;
    }

    /**
     * récupère la ville de l'objet
     */
    public String getVille() {
        return this.ville;
    }

    /**
     * modifie la ville d'un objet
     */
    public void setVille(String ville) {
        this.ville = ville;
    }


    /**
     * modifie le nom d'un objet
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * modifie la description d'un objet
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * modifie le prix d'un objet
     */
    public void setPrix(int prix) {
        this.prix = prix;
    }

    /**
     * modifie le prix minimum d'un objet
     */
    public void setPrixMin(int prixMin) {
        this.prixMin = prixMin;
    }

    /**
     * modifie la catégorie d'un objet
     */
    public void setCategorie(int categorie) {
        this.categorie = categorie;
    }

    /**
     * modifie l'id utilisateur d'un objet
     */
    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    /**
     * modifie la date début d'un objet
     */
    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * modifie la date de fin d'un objet
     */
    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    /**
     * modifie une image 1
     */
    public void setImage1(String image1) {
        this.image1 = image1;
    }

    /**
     * modifie une image 2
     */
    public void setImage2(String image2) {
        this.image2 = image2;
    }
    /**
     * modifie une image 3
     */
    public void setImage3(String image3) {
        this.image3 = image3;
    }

    /**
     * modifie une image 4
     */
    public void setImage4(String image4) {
        this.image4 = image4;
    }

    // public boolean verifDateDebutFin() {
    //     DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
    //     LocalDateTime date1 = LocalDateTime.parse(this.dateDebut, formatter);
    //     LocalDateTime date2 = LocalDateTime.parse(this.dateFin, formatter);

    //     return date1.isBefore(date2);
    // }

    /**
     * vérifie si la date de début est bien avant la date de fin
     */
    @Override
    public String toString() {
        return "Objet [categorie=" + categorie + ", description=" + description + ", idUtilisateur=" + idUtilisateur
                + ", nom=" + nom + ", prix=" + prix + ", prixMin=" + prixMin + ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + "]";
    }
}
