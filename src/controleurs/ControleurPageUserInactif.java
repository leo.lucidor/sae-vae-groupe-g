package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurPageUserInactif implements EventHandler<ActionEvent> {
    
    /**
     * L'application principale
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur
     * @param appli l'application principale
     */
    public ControleurPageUserInactif(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Action lorsque l'utilisateur clique sur le bouton "Retour"
     * @param event l'event
     */
    @Override
    public void handle(ActionEvent event) {
        try {
            this.appli.pageUserInactif();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
