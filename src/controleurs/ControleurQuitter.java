package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import javafx.event.ActionEvent;

public class ControleurQuitter implements EventHandler<ActionEvent> {
    
    /**
     * L'application principale
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur
     * @param appli l'application principale
     */
    public ControleurQuitter(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Gestion de l'événement sur le bouton quitter
     * @param event l'événement
     */
    @Override
    public void handle(ActionEvent event) {
        this.appli.popUpQuitterApplication();
    }
}