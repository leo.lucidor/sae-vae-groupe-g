package controleurs;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import vues.FenetreAjouterVente;

public class ControleurCategorieVente implements EventHandler<ActionEvent>{
    private FenetreAjouterVente fenetreAjouterVente;

    public ControleurCategorieVente(FenetreAjouterVente fenetreajouterVente) {
        this.fenetreAjouterVente = fenetreajouterVente;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void handle(ActionEvent event) {
        ComboBox<String> cb = (ComboBox<String>) event.getSource();
        String categorie = cb.getValue();
        System.out.println(categorie);

        if (categorie.equals("Vêtements")){
        this.fenetreAjouterVente.setCategorie(1);
        }
        else if (categorie.equals("Meubles")){
        this.fenetreAjouterVente.setCategorie(3);
        }
        else if (categorie.equals("Ustensile de cuisine")){
        this.fenetreAjouterVente.setCategorie(2);
        }
        else if (categorie.equals("Outil")){
        this.fenetreAjouterVente.setCategorie(4);
        }
    }
}
