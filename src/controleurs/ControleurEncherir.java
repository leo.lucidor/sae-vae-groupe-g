package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import vues.BibVente;

import java.sql.SQLException;
import java.util.List;

import javafx.event.ActionEvent;

public class ControleurEncherir implements EventHandler<ActionEvent> {
    
    /**
     * Application principale  
     */
    private ApplicationVAE appli;
    /**
     * Liste des informations de la vente
     */
    private List<String> vente;
    
    /**
     * Constructeur du controleur
     * @param appli application principale
     * @param vente liste des informations de la vente
     */
    public ControleurEncherir(ApplicationVAE appli, List<String> vente){
        this.appli = appli;
        this.vente = vente;
    }

    /**
     * Gestion de l'action
     * @param event evenement
     */
    @Override
    public void handle(ActionEvent event) {
        String idVente = BibVente.getIdVe(this.vente);
        String nomObjet = BibVente.getNomObjet(this.vente);
        String prixBase = BibVente.getPrixBase(this.vente);
        String prixMini = BibVente.getPrixMini(this.vente);
        //String dateFin = BibVente.getDateFin(this.vente);
        //String description = BibVente.getDescription(this.vente);
        String vendeur = BibVente.getVendeur(this.vente);

        try {
            this.appli.pageEncherir(idVente, nomObjet, prixBase, prixMini, vendeur);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
