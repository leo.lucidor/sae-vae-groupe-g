package vues;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import java.sql.SQLException;
import java.util.List;

import modeles.Admin;
import controleurs.ControleurSignalementProfil;
import controleurs.ControleurSignalementVente;


public class FenetreSignalement extends StackPane {
    
    private static final int WIDTH_SIGNALEMENT_VENTE = 600;
    private static final int HEIGHT_SIGNALEMENT_VENTE = 200;
    
    /**
     * Constructeur de la classe FenetreSignalement.
     *
     * @param bpPage              Le BorderPane principal de la page.
     * @param rootTop             Le BorderPane du haut de la page.
     * @param rootLeft            Le BorderPane de gauche de la page.
     * @param borderMenuOption    Le BorderPane contenant les options de menu.
     * @param adminModele         Le modèle administrateur.
     * @param appli               L'application VAE.
     * @throws SQLException      Si une erreur SQL se produit.
     */
    public FenetreSignalement(BorderPane bpPage, BorderPane rootTop, BorderPane rootLeft, BorderPane borderMenuOption, Admin adminModele, ApplicationVAE appli) throws SQLException{
        //BorderPane bpPage = new BorderPane();
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        //BorderPane rootLeft = new MenuLeftAdmin(boutonSignalement, acceuil, quitter, message, info, setting, bpPage);
        hBoxLeft.getChildren().addAll(rootLeft);
        bpPage.setLeft(hBoxLeft);
        //BorderPane rootTop = new MenuTop(userModele, textMenu, deconnexion, profil, menu, bpPage);
        bpPage.setTop(rootTop);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        StackPane.setMargin(rootLeftOption, new Insets(48,0,0,56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);

        
        List<List<String>> lesSignalementVente = adminModele.getAllSignalementVente();
        VBox vBoxSignalementVente = new VBox();
        for(List<String> signalement : lesSignalementVente){
            String motifSignalement = signalement.get(1);
            int idUt = Integer.parseInt(signalement.get(3));
            int idVe = Integer.parseInt(signalement.get(4));
            List<String> infoVente = adminModele.getInfoVente(idVe);

            int prixBase = Integer.parseInt(infoVente.get(1));
            int prixMin = Integer.parseInt(infoVente.get(2));
            List<String> infoUtilisateur = adminModele.getInfoUtilisateur(idUt);
            //List<String> infoVente = adminModele.getInfoVente(idVe);
           // String prixBase = infoVente.get(2);
           // String prixMini = infoVente.get(3);
            String nomUt = infoUtilisateur.get(7);
            String prenomUt = infoUtilisateur.get(6);
            Label labelNomPrenom = new Label("Nom :   " + nomUt + "       Prenom :   " + prenomUt);
            labelNomPrenom.setStyle("-fx-font-size: 20px; -fx-font-weight: bold; -fx-text-fill: #FFFFFF;");
            labelNomPrenom.setAlignment(Pos.CENTER);
           Label labelPrix = new Label("Prix de base :   " + prixBase + "       Prix minimum :   " + prixMin);
           labelPrix.setStyle("-fx-font-size: 20px; -fx-font-weight: bold; -fx-text-fill: #012245;");
            labelPrix.setAlignment(Pos.CENTER);
            Label motif = new Label("Motif du signalement: \n" + motifSignalement);
            motif.setStyle("-fx-font-size: 20px; -fx-font-weight: bold; -fx-text-fill: #FFFFFF;");
            motif.setAlignment(Pos.CENTER);

            VBox vBoxSignalement = new VBox();
            vBoxSignalement.setBackground(new Background(new BackgroundFill(Color.web("#D9D9D9"), new CornerRadii(0), Insets.EMPTY)));
            vBoxSignalement.setPrefSize(WIDTH_SIGNALEMENT_VENTE, HEIGHT_SIGNALEMENT_VENTE);

            HBox hBoxBoutons = new HBox();
            Button buttonSupprimerSignalement = new Button("Supprimer le signalement");
            Button buttonSupprimerVente = new Button("Supprimer la vente");
            Button buttonDesactiverUtilisateur = new Button("Desactiver l'utilisateur");
            buttonSupprimerSignalement.setStyle("-fx-background-color: #EBACA2; -fx-font-size: 15px;");
            buttonSupprimerVente.setStyle("-fx-background-color: #EBACA2; -fx-font-size: 15px;");
            buttonDesactiverUtilisateur.setStyle("-fx-background-color: #EBACA2; -fx-font-size: 15px;");
            buttonSupprimerSignalement.setOnAction(new ControleurSignalementVente(adminModele, Integer.parseInt(signalement.get(0)), idVe, idUt, appli));
            buttonSupprimerVente.setOnAction(new ControleurSignalementVente(adminModele, Integer.parseInt(signalement.get(0)), idVe, idUt, appli));
            buttonDesactiverUtilisateur.setOnAction(new ControleurSignalementVente(adminModele, Integer.parseInt(signalement.get(0)), idVe, idUt, appli));
            hBoxBoutons.getChildren().addAll(buttonSupprimerSignalement, buttonSupprimerVente, buttonDesactiverUtilisateur);
            hBoxBoutons.setAlignment(Pos.CENTER);
            hBoxBoutons.setSpacing(10);

            vBoxSignalement.getChildren().addAll(labelNomPrenom, motif, hBoxBoutons);
            vBoxSignalement.setAlignment(Pos.CENTER);
            vBoxSignalement.setStyle("-fx-background-color: #4A919E;");
            vBoxSignalement.setSpacing(10);
            vBoxSignalementVente.getChildren().add(vBoxSignalement);
        }
        vBoxSignalementVente.setAlignment(Pos.CENTER);
        vBoxSignalementVente.setSpacing(30);
        vBoxSignalementVente.setPadding(new Insets(10, 30, 10, 30));

        List<List<String>> lesSignalementProfil = adminModele.getAllSignalementProfil();
        VBox vBoxSignalementProfil = new VBox();
        for(List<String> signalement : lesSignalementProfil){
            String motifSignalement = signalement.get(1);
            int idUt = Integer.parseInt(signalement.get(3));
            List<String> infoUtilisateur = adminModele.getInfoUtilisateur(idUt);
            String nomUt = infoUtilisateur.get(7);
            String prenomUt = infoUtilisateur.get(6);
            Label labelNomPrenom = new Label("Nom :   "+nomUt+"       Prenom :   "+prenomUt);
            labelNomPrenom.setStyle("-fx-font-size: 20px; -fx-font-weight: bold; -fx-text-fill: #FFFFFF;");
            labelNomPrenom.setAlignment(Pos.CENTER);
            Label labelAgeMail = new Label("Age :   "+infoUtilisateur.get(8)+"             Mail :   "+infoUtilisateur.get(2));
            labelAgeMail.setStyle("-fx-font-size: 20px; -fx-font-weight: bold; -fx-text-fill: #FFFFFF;");
            labelAgeMail.setAlignment(Pos.CENTER);
            Label motif = new Label("Motif du signalement: \n" + motifSignalement);
            motif.setStyle("-fx-font-size: 20px; -fx-font-weight: bold; -fx-text-fill: #FFFFFF;");
            motif.setAlignment(Pos.CENTER);

            VBox vBoxSignalement = new VBox();
            vBoxSignalement.setBackground(new Background(new BackgroundFill(Color.web("#D9D9D9"), new CornerRadii(0), Insets.EMPTY)));
            vBoxSignalement.setPrefSize(WIDTH_SIGNALEMENT_VENTE, HEIGHT_SIGNALEMENT_VENTE);
            vBoxSignalement.setAlignment(Pos.CENTER);

            HBox hBoxBoutons = new HBox();
            Button buttonDesactiverProfil = new Button("Desactiver le profil");
            buttonDesactiverProfil.setStyle("-fx-background-color: #EBACA2; -fx-font-size: 15px;");
            buttonDesactiverProfil.setOnAction(new ControleurSignalementProfil(adminModele, idUt, Integer.parseInt(signalement.get(0)), appli));

            hBoxBoutons.getChildren().addAll(buttonDesactiverProfil);
            hBoxBoutons.setAlignment(Pos.CENTER);
            hBoxBoutons.setSpacing(10);

            vBoxSignalement.getChildren().addAll(labelNomPrenom, labelAgeMail, motif, hBoxBoutons);
            
            vBoxSignalement.setStyle("-fx-background-color: #4A919E;");
            vBoxSignalement.setSpacing(10);
            vBoxSignalementProfil.getChildren().add(vBoxSignalement);
        }
        vBoxSignalementProfil.setAlignment(Pos.CENTER);
        vBoxSignalementProfil.setSpacing(30);
        vBoxSignalementProfil.setPadding(new Insets(10, 30, 10, 30));
        

        ScrollPane scrollPaneSignalementVente = new ScrollPane(vBoxSignalementVente);
        ScrollPane scrollPaneSignalementProfil = new ScrollPane(vBoxSignalementProfil);
        scrollPaneSignalementVente.setHbarPolicy(ScrollBarPolicy.NEVER);
        scrollPaneSignalementProfil.setHbarPolicy(ScrollBarPolicy.NEVER);
        scrollPaneSignalementVente.setStyle("-fx-background: #BED3C3;");
        scrollPaneSignalementProfil.setStyle("-fx-background: #BED3C3;");
        VBox vBoxSignalementVenteContainer = new VBox();
        VBox vBoxSignalementProfilContainer = new VBox();
        Label labelSignalementVente = new Label("Signalements de vente :");
        labelSignalementVente.setStyle("-fx-font-size: 30px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
        labelSignalementVente.setPadding(new Insets(0, 0, 20, 110));
        Label labelSignalementProfil = new Label("Signalements de profil :");
        labelSignalementProfil.setStyle("-fx-font-size: 30px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
        labelSignalementProfil.setPadding(new Insets(0, 0, 20, 110));
        vBoxSignalementVenteContainer.getChildren().addAll(labelSignalementVente, scrollPaneSignalementVente);
        vBoxSignalementProfilContainer.getChildren().addAll(labelSignalementProfil, scrollPaneSignalementProfil);
        HBox hBoxSignalement = new HBox();
        hBoxSignalement.getChildren().addAll(vBoxSignalementVenteContainer, vBoxSignalementProfilContainer);
        hBoxSignalement.setAlignment(Pos.CENTER);
        hBoxSignalement.setSpacing(100);
        hBoxSignalement.setPadding(new Insets(100, 0, 0, 0));
        bpPage.setCenter(hBoxSignalement);
    }
}
