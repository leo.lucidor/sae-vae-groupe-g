package vues;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import modeles.Admin;

import java.sql.SQLException;
import java.util.List;

import controleurs.ControleurUserInactif;

public class FenetreUserInactif extends StackPane {

    private static final int WIDTH_VENTE = 600;
    private static final int HEIGHT_VENTE = 300;

    /**

    Représente une fenêtre pour afficher les utilisateurs inactifs dans l'application.
    @param appli L'objet de l'application principale.
    @param bpPage Le BorderPane pour la zone de contenu principale.
    @param rootTop Le BorderPane racine pour la section supérieure de la fenêtre.
    @param rootLeft Le BorderPane racine pour la section gauche de la fenêtre.
    @param borderMenuOption Le BorderPane pour les options de menu.
    @param AdminModele L'objet modèle d'administrateur.
    @throws SQLException Si une exception SQL se produit.
    */
    public FenetreUserInactif(ApplicationVAE appli, BorderPane bpPage, BorderPane rootTop, BorderPane rootLeft, BorderPane borderMenuOption, Admin AdminModele) throws SQLException {

        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        hBoxLeft.getChildren().addAll(rootLeft);
        bpPage.setLeft(hBoxLeft);
        bpPage.setTop(rootTop);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        StackPane.setMargin(rootLeftOption, new Insets(48, 0, 0, 56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);
        Label labelUserInactif = new Label("Utilisateurs Inactifs :");
        labelUserInactif.setStyle("-fx-font-size: 30px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
        labelUserInactif.setAlignment(Pos.CENTER);
        VBox vBoxUserInactifContainer = new VBox();
        VBox vBoxUserInactif = new VBox();

        List<List<String>> lesUserInactif = AdminModele.getAllUtilisateurInactif();
        int count = 0;
        HBox currentHBox = null;

        for (List<String> UserInactif : lesUserInactif) {
            String nomUt = UserInactif.get(6);
            String prenomUt = UserInactif.get(7);
            String mailUt = UserInactif.get(2);
            String telUt = UserInactif.get(10);
            String statutUt = UserInactif.get(4);

            Label labelNom = new Label("Nom : " + nomUt);
            labelNom.setStyle("-fx-font-size: 20px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelPrenom = new Label("Prénom : " + prenomUt);
            labelPrenom.setStyle("-fx-font-size: 20px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelMail = new Label("Mail : " + mailUt);
            labelMail.setStyle("-fx-font-size: 20px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelTel = new Label("Téléphone : " + telUt);
            labelTel.setStyle("-fx-font-size: 20px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            labelTel.setPadding(new Insets(0, 0, 10, 0));
            if (statutUt.equals("N")) {
                statutUt = "Inactif";
            } else {
                statutUt = "Actif";
            }
            Label labelStatut = new Label("Statut : " + statutUt);
            labelStatut.setStyle("-fx-font-size: 20px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");

            VBox vBoxUserInactifChild = new VBox();
            vBoxUserInactifChild.setBackground(new Background(new BackgroundFill(Color.web("#4A919E"), new CornerRadii(0), Insets.EMPTY)));
            vBoxUserInactifChild.setPrefSize(WIDTH_VENTE, HEIGHT_VENTE);

            HBox hBoxBoutons = new HBox();
            Button buttonSupprimerUser = new Button("Supprimer l'utilisateur");
            buttonSupprimerUser.setStyle("-fx-background-color: #EBACA2; -fx-font-size: 15px;");
            buttonSupprimerUser.setOnAction(new ControleurUserInactif(AdminModele, Integer.parseInt(UserInactif.get(0)), appli));
            Button buttonReactiverUser = new Button("Réactiver l'utilisateur");
            buttonReactiverUser.setStyle("-fx-background-color: #EBACA2; -fx-font-size: 15px;");
            buttonReactiverUser.setOnAction(new ControleurUserInactif(AdminModele, Integer.parseInt(UserInactif.get(0)), appli));

            hBoxBoutons.getChildren().addAll(buttonSupprimerUser, buttonReactiverUser);
            hBoxBoutons.setAlignment(Pos.CENTER);
            hBoxBoutons.setSpacing(10);

            vBoxUserInactifChild.getChildren().addAll(labelNom, labelPrenom, labelStatut, labelMail, labelTel, hBoxBoutons);
            vBoxUserInactifChild.setSpacing(10);
            vBoxUserInactifChild.setAlignment(Pos.CENTER);

            if (count % 3 == 0) {
                currentHBox = new HBox();
                currentHBox.setAlignment(Pos.CENTER);
                currentHBox.setSpacing(10);
                vBoxUserInactif.getChildren().add(currentHBox);
            }

            currentHBox.getChildren().add(vBoxUserInactifChild);
            count++;
        }

        vBoxUserInactif.setAlignment(Pos.CENTER);
        vBoxUserInactif.setSpacing(30);
        vBoxUserInactif.setPadding(new Insets(10, 100, 10, 0));

        ScrollPane scrollPaneUserInactif = new ScrollPane(vBoxUserInactif);
        scrollPaneUserInactif.setStyle("-fx-background: #BED3C3;");
        scrollPaneUserInactif.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        vBoxUserInactifContainer.getChildren().addAll(labelUserInactif, scrollPaneUserInactif);
        vBoxUserInactifContainer.setAlignment(Pos.CENTER);
        vBoxUserInactifContainer.setSpacing(10);
        vBoxUserInactifContainer.setPadding(new Insets(20, 0, 0, 0));
        bpPage.setCenter(vBoxUserInactifContainer);
    }
}
