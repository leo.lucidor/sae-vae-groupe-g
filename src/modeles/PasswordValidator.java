package modeles;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator {
    
    /**
     * Vérifie si le mot de passe est valide.
     */
    public static boolean isValidPassword(String password) {
        // Vérification de la longueur minimale
        if (password.length() < 8) {
            return false;
        }
        
        // Vérification de la présence d'une majuscule, d'un caractère spécial et d'un chiffre
        Pattern pattern = Pattern.compile("^(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&+=])(?=\\S+$).*$");
        Matcher matcher = pattern.matcher(password);
        
        return matcher.matches();
    }
}
