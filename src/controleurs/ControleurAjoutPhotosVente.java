package controleurs;
import java.io.File;

import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import vues.FenetreAjouterVente;

public class ControleurAjoutPhotosVente implements EventHandler<MouseEvent> {
    
    /**
     * FenetreAjouterVente
     */
    private FenetreAjouterVente fenetreAjouterVente;
    /**
     * FileChooser.ExtensionFilter
     */
    private FileChooser.ExtensionFilter imageChooser;
    /** 
     * int i
     */
    private int i;


    /**
     * Constructeur ControleurAjoutPhotosVente
     * @param fenetreAjouterVente
     * @param image
     * @param i
     */
    public ControleurAjoutPhotosVente(FenetreAjouterVente fenetreAjouterVente, FileChooser.ExtensionFilter image, int i) {
        this.fenetreAjouterVente = fenetreAjouterVente;
        this.imageChooser = image;
        this.i = i;
    }
    

    /**
     * Methode handle
     * @param event
     */
    @Override
    public void handle(MouseEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(imageChooser);
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            Image image = new Image(file.toURI().toString());
            if (i==1){
            fenetreAjouterVente.ajouterImageVente1(image);
            }
            if (i==2){
                fenetreAjouterVente.ajouterImageVente2(image);
            }
            if (i==3){
                fenetreAjouterVente.ajouterImageVente3(image);
            }
            if (i==4){
                fenetreAjouterVente.ajouterImageVente4(image);
            }
        }
    }
}