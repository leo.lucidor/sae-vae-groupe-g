package modeles;
import java.sql.SQLException;
import java.util.List;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * La classe Utilisateur représente un utilisateur du système.
 */
public class Utilisateur {
    /**
     * La base de données utilisée pour accéder aux informations utilisateur.
     */
    private BaseDeDonnee baseDeDonnee;

    /**
     * L'identifiant de l'utilisateur.
     */
    private int id;

    /**
     * L'identifiant de la photo de profil de l'utilisateur.
     */
    private int idPhoto;

    /**
     * Le rôle de l'utilisateur.
     */
    private int role;

    /**
     * Le pseudo de l'utilisateur.
     */
    private String pseudo;

    /**
     * L'adresse e-mail de l'utilisateur.
     */
    private String mail;

    /**
     * Le mot de passe de l'utilisateur.
     */
    private String mdp;

    /**
     * Le genre de l'utilisateur.
     */
    private String genre;

    /**
     * Le nom de l'utilisateur.
     */
    private String nom;

    /**
     * Le prénom de l'utilisateur.
     */
    private String prenom;

    /**
     * L'âge de l'utilisateur.
     */
    private int age;

    /**
     * Le nombre de likes reçus par l'utilisateur.
     */
    private int like;

    /**
     * Le numéro de téléphone de l'utilisateur.
     */
    private String tel;

    /**
     * L'indicateur d'activité de l'utilisateur.
     */
    private String actif;

    /**
     * L'avatar de l'utilisateur.
     */
    private ImageView imgAvatar;

    /**
     * Constructeur de la classe Utilisateur.
     *
     * @param id         L'identifiant de l'utilisateur.
     * @param imgAvatar  L'avatar de l'utilisateur.
     * @param bd         La base de données à utiliser.
     * @throws SQLException             Si une erreur SQL se produit.
     * @throws ClassNotFoundException Si la classe de la base de données n'est pas trouvée.
     */
    public Utilisateur(int id, ImageView imgAvatar, BaseDeDonnee bd) throws SQLException, ClassNotFoundException {
        this.baseDeDonnee = bd;
        this.id = id;
        this.pseudo = this.baseDeDonnee.getInfoUserById(this.id).get(1);
        this.mail = this.baseDeDonnee.getInfoUserById(this.id).get(2);
        this.mdp = this.baseDeDonnee.getInfoUserById(this.id).get(3);
        this.actif = this.baseDeDonnee.getInfoUserById(this.id).get(4);
        this.role = Integer.parseInt(this.baseDeDonnee.getInfoUserById(this.id).get(5));
        this.prenom = this.baseDeDonnee.getInfoUserById(this.id).get(6);
        this.nom = this.baseDeDonnee.getInfoUserById(this.id).get(7);
        this.age = Integer.parseInt(this.baseDeDonnee.getInfoUserById(this.id).get(8));
        this.genre = this.baseDeDonnee.getInfoUserById(this.id).get(9);
        this.tel = this.baseDeDonnee.getInfoUserById(this.id).get(10);
        this.idPhoto = Integer.parseInt(this.baseDeDonnee.getInfoUserById(this.id).get(11));
        this.imgAvatar = imgAvatar;
    }

    /**
     * Met à jour les informations de l'utilisateur après la connexion.
     *
     * @param id L'identifiant de l'utilisateur.
     * @throws SQLException Si une erreur SQL se produit.
     */
    public void setInfoUser(int id) throws SQLException {
        List<String> lesinfos = this.baseDeDonnee.getInfoUserById(id);
        this.setId(Integer.parseInt(lesinfos.get(0)));
        this.setPseudo(lesinfos.get(1));
        this.setMail(lesinfos.get(2));
        this.setMdp(lesinfos.get(3));
        this.setActif(lesinfos.get(4));
        this.setRole(Integer.parseInt(lesinfos.get(5)));
        this.setPrenom(lesinfos.get(6));
        this.setNom(lesinfos.get(7));
        this.setAge(Integer.parseInt(lesinfos.get(8)));
        this.setGenre(lesinfos.get(9));
        this.setTel(lesinfos.get(10));
        this.setIdPhoto(Integer.parseInt(lesinfos.get(11)));
        this.setLike(Integer.parseInt(lesinfos.get(12)));
        this.imgAvatar = new ImageView(new Image("file:./images/photo_UT/image" + this.getIdPhoto() + ".png"));
    }

    /**
     * Retourne l'image de profil de l'utilisateur.
     *
     * @return L'image de profil.
     */
    public Image getImageUser() {
        return this.imgAvatar.getImage();
    }

    /**
     * Retourne l'avatar de l'utilisateur.
     *
     * @return L'avatar.
     */
    public ImageView getImageViewUser() {
        return this.imgAvatar;
    }

    /**
     * Retourne l'identifiant de l'utilisateur.
     *
     * @return L'identifiant de l'utilisateur.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Retourne l'identifiant de la photo de profil de l'utilisateur.
     *
     * @return L'identifiant de la photo de profil.
     */
    public int getIdPhoto() {
        return this.idPhoto;
    }

    /**
     * Retourne l'état d'activité de l'utilisateur.
     *
     * @return L'état d'activité.
     */
    public String getActif() {
        return this.actif;
    }

    /**
     * Retourne le numéro de téléphone de l'utilisateur.
     *
     * @return Le numéro de téléphone.
     */
    public String getTel() {
        return this.tel;
    }

    /**
     * Retourne le nombre de likes reçus par l'utilisateur.
     *
     * @return Le nombre de likes.
     */
    public int getLike() {
        return this.like;
    }

    /**
     * Retourne le rôle de l'utilisateur.
     *
     * @return Le rôle de l'utilisateur.
     */
    public int getRole() {
        return this.role;
    }

    /**
     * Retourne le pseudo de l'utilisateur.
     *
     * @return Le pseudo de l'utilisateur.
     */
    public String getPseudo() {
        return this.pseudo;
    }

    /**
     * Retourne l'adresse e-mail de l'utilisateur.
     *
     * @return L'adresse e-mail de l'utilisateur.
     */
    public String getMail() {
        return this.mail;
    }

    /**
     * Retourne le mot de passe de l'utilisateur.
     *
     * @return Le mot de passe de l'utilisateur.
     */
    public String getMdp() {
        return this.mdp;
    }

    /**
     * Retourne le genre de l'utilisateur.
     *
     * @return Le genre de l'utilisateur.
     */
    public String getGenre() {
        return this.genre;
    }

    /**
     * Retourne le nom de l'utilisateur.
     *
     * @return Le nom de l'utilisateur.
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Retourne le prénom de l'utilisateur.
     *
     * @return Le prénom de l'utilisateur.
     */
    public String getPrenom() {
        return this.prenom;
    }

    /**
     * Retourne l'âge de l'utilisateur.
     *
     * @return L'âge de l'utilisateur.
     */
    public int getAge() {
        return this.age;
    }

    /**
     * Définit le rôle de l'utilisateur.
     *
     * @param role Le rôle de l'utilisateur.
     */
    public void setRole(int role) {
        this.role = role;
    }

    /**
     * Définit le pseudo de l'utilisateur.
     *
     * @param pseudo Le pseudo de l'utilisateur.
     */
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    /**
     * Définit l'adresse e-mail de l'utilisateur.
     *
     * @param mail L'adresse e-mail de l'utilisateur.
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * Définit le mot de passe de l'utilisateur.
     *
     * @param mdp Le mot de passe de l'utilisateur.
     */
    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    /**
     * Définit le genre de l'utilisateur.
     *
     * @param genre Le genre de l'utilisateur.
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * Définit le nom de l'utilisateur.
     *
     * @param nom Le nom de l'utilisateur.
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Définit le prénom de l'utilisateur.
     *
     * @param prenom Le prénom de l'utilisateur.
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Définit l'âge de l'utilisateur.
     *
     * @param age L'âge de l'utilisateur.
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Incrémente le nombre de likes de l'utilisateur et ajoute un like dans la base de données.
     *
     * @throws SQLException Si une erreur SQL se produit.
     */
    public void incrementerLike() throws SQLException {
        this.like++;
        this.baseDeDonnee.ajouteLike(this.id);
    }

    /**
     * Décrémente le nombre de likes de l'utilisateur et enlève un like de la base de données.
     *
     * @throws SQLException Si une erreur SQL se produit.
     */
    public void decrementerLike() throws SQLException {
        this.like--;
        this.baseDeDonnee.enleverLike(this.id);
    }

    /**
     * Définit le nombre de likes de l'utilisateur.
     *
     * @param like Le nombre de likes.
     */
    public void setLike(int like) {
        this.like = like;
    }

    /**
     * Définit le numéro de téléphone de l'utilisateur.
     *
     * @param tel Le numéro de téléphone.
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * Définit l'état d'activité de l'utilisateur.
     *
     * @param actif L'état d'activité.
     */
    public void setActif(String actif) {
        this.actif = actif;
    }

    /**
     * Définit l'identifiant de la photo de profil de l'utilisateur.
     *
     * @param idPhoto L'identifiant de la photo de profil.
     */
    public void setIdPhoto(int idPhoto) {
        this.idPhoto = idPhoto;
        this.imgAvatar = new ImageView(new Image("file:./images/photo_UT/image"+this.getIdPhoto()+".png"));
    }

    /**
     * Définit l'identifiant de l'utilisateur.
     *
     * @param id L'identifiant de l'utilisateur.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Methode qui permet de changer la photo de profil de l'utilisateur à partir d'un lien
     * @param id int :L'identifiant de la photo
     * @param lien String : lien menant à l'image
     */
    public void setIdPhotoPersonaliser(String lien, int idPhoto) throws SQLException{
        this.idPhoto = idPhoto;
        this.imgAvatar = new ImageView(new Image(lien));
    }

    // requete sql

    /**
     * Methode qui permet de récupérer le mot de pass de l'utilisateur
     * @param psudo Sting : pseudo de l'utilisateur qu'on veut le mot de pass
     * @return String : mot de pass de l'utilisateur voulu 
     */
    public String getMdp (String pseudo) throws SQLException{
        try{
            return this.baseDeDonnee.getMdpPseudo(pseudo);
        } catch (SQLException e){
            return this.baseDeDonnee.getMdpMail(pseudo);
        }
    }

    /**
    * Récupère le mot de passe associé à une adresse e-mail.
    *
    * @param mail l'adresse e-mail pour laquelle récupérer le mot de passe
    * @return le mot de passe associé à l'adresse e-mail
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public String getMdpMail (String mail) throws SQLException{
        return this.baseDeDonnee.getMdpMail(mail);
    }

    /**
    * Récupère l'identifiant d'un utilisateur à partir de son pseudo.
    *
    * @param pseudo le pseudo de l'utilisateur
    * @return l'identifiant de l'utilisateur
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public int getId(String pseudo) throws SQLException{
        return this.baseDeDonnee.getId(pseudo);
    }

    /**
    * Récupère le rôle d'un utilisateur à partir de son identifiant.
    *
    * @param id l'identifiant de l'utilisateur
    * @return le rôle de l'utilisateur
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public String getRole(int id) throws SQLException{
        return this.baseDeDonnee.getRole(id);
    }

    /**
    * Récupère le mot de passe d'un utilisateur à partir de son identifiant.
    *
    * @param id l'identifiant de l'utilisateur
    * @return le mot de passe de l'utilisateur
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public String getMdp(int id) throws SQLException{
        return this.baseDeDonnee.getMdp(id);
    }

    /**
    * Récupère les ventes associées à un utilisateur.
    *
    * @param nom le nom de l'utilisateur
    * @return la liste des ventes associées à l'utilisateur
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public List<String> getVenteUtilisateur(String nom) throws SQLException{
        return this.baseDeDonnee.getVenteUtilisateur(nom);
    }

    /**
    * Insère un nouvel utilisateur dans la base de données.
    *
    * @param pseudo le pseudo de l'utilisateur
    * @param mail l'adresse e-mail de l'utilisateur
    * @param mdp le mot de passe de l'utilisateur
    * @param role le rôle de l'utilisateur
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public void insertUtilisateur(String pseudo, String mail, String mdp, int role) throws SQLException{
        this.baseDeDonnee.insertUtilisateur(pseudo, mail, mdp, "O", role, null, null, 0, null, null, 0);
    }

    /**
    * Met à jour les informations d'un utilisateur dans la base de données.
    *
    * @param pseudo le pseudo de l'utilisateur
    * @param mail l'adresse e-mail de l'utilisateur
    * @param mdp le mot de passe de l'utilisateur
    * @param role le rôle de l'utilisateur
    * @param prenom le prénom de l'utilisateur
    * @param nom le nom de l'utilisateur
    * @param age l'âge de l'utilisateur
    * @param genre le genre de l'utilisateur
    * @param tel le numéro de téléphone de l'utilisateur
    * @param id l'identifiant de l'utilisateur
    * @param idPhoto l'identifiant de la photo de profil de l'utilisateur
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public void majInfoUtilisateur(String pseudo, String mail, String mdp, int role, String prenom, String nom, int age, String genre, String tel, int id, int idPhoto) throws SQLException{
        this.baseDeDonnee.miseAJourInfoUtilisateur(id, pseudo, mail, mdp, prenom, nom, age, genre, tel, idPhoto);
    }

    /**
    * Insère une photo personnelle d'un utilisateur dans la base de données.
    *
    * @param lien le lien vers la photo à insérer
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public void inserePhotoPersoUtilisateur(String lien) throws SQLException{
        this.baseDeDonnee.insertPhoto(lien);
    }

    /**
    * Récupère toutes les informations d'un utilisateur à partir de son identifiant.
    *
    * @param idUt l'identifiant de l'utilisateur
    * @return la liste des informations de l'utilisateur
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public List<String> getAllinfoUtilisateur(int idUt) throws SQLException{
        return this.baseDeDonnee.getInfoUserById(idUt);
    }

    /**
    * Récupère le nombre maximum d'identifiant de photo.
    *
    * @return le nombre maximum d'identifiant de photo
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public int nbMaxIdPhoto() throws SQLException{
        return this.baseDeDonnee.maxNum("PHOTOUT", "idPhUt");
    }

    /**
    * Incrémente le nombre de likes d'un utilisateur.
    *
    * @param idUt l'identifiant de l'utilisateur
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public void incrementerLike(int idUt) throws SQLException{
        this.baseDeDonnee.ajouteLike(idUt);
    }

    /**
    * Décrémente le nombre de likes d'un utilisateur.
    *
    * @param idUt l'identifiant de l'utilisateur
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public void decrementerLike(int idUt) throws SQLException{
        this.baseDeDonnee.enleverLike(idUt);
    }

    /**
    * Vérifie si un utilisateur est actif.
    *
    * @param idUt l'identifiant de l'utilisateur
    * @return true si l'utilisateur est actif, false sinon
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public boolean estActif(int idUt) throws SQLException{
        String actif = this.baseDeDonnee.getInfoUserById(idUt).get(4);
        if(actif.equals("O")){
            return true;
        } else {
            return false;
        }
    }

    /**
    * Récupère les informations d'un utilisateur à partir de son identifiant.
    *
    * @param idUtilisateur l'identifiant de l'utilisateur
    * @return la liste des informations de l'utilisateur
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public List<String> getInfoUtilisateur(int idUtilisateur) throws SQLException{
        return this.baseDeDonnee.getInfoUserById(idUtilisateur);
    }

    /**
    * Récupère l'historique des enchères non remportées par un utilisateur.
    *
    * @param id l'identifiant de l'utilisateur
    * @return la liste de l'historique des enchères non remportées
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public List<List<String>> getHistoriqueDesEncheresNonRemportees(String id) throws SQLException{
        return this.baseDeDonnee.getHistoriqueDesEncheresNonRemportees(id);
    }

    /**
    * Récupère les informations des ventes d'un utilisateur.
    *
    * @param id l'identifiant de l'utilisateur
    * @return la liste des informations des ventes de l'utilisateur
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public List<List<String>> getInfoVenteUtilisateur(int id) throws SQLException{
        return this.baseDeDonnee.getInfoVenteUtilisateur(id);
    }

    /**
    * Récupère les informations des ventes remportées par un utilisateur.
    *
    * @param id l'identifiant de l'utilisateur
    * @return la liste des informations des ventes remportées par l'utilisateur
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public List<List<String>> getInfoVenteUtilisateurGagnee(String id) throws SQLException{
        return this.baseDeDonnee.getInfoVenteUtilisateurGagnee(id);
    }

    /**
    * Signale le profil d'un utilisateur.
    *
    * @param motif le motif du signalement
    * @param profilActuel l'identifiant du profil actuel
    * @param profilSignaler l'identifiant du profil à signaler
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public void signalerProfil(String motif, int profilActuel, int profilSignaler) throws SQLException{
        this.baseDeDonnee.creerSignalementUser(motif, profilActuel, profilSignaler);
    }

    /**
    * Met à jour les informations de l'utilisateur actuel.
    *
    * @throws SQLException en cas d'erreur lors de l'accès à la base de données
    */
    public void majInfoUtilisateur() throws SQLException{
        this.baseDeDonnee.miseAJourInfoUtilisateur(this.id, this.pseudo, this.mail, this.mdp, this.prenom, this.nom, this.age, this.genre, this.tel, this.idPhoto);
    }

}