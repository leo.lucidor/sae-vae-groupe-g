package controleurs;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modeles.BaseDeDonnee;
import vues.FenetreRecapitulatifVente;

public class ControleurBoutonSupprimerVenteVielle implements EventHandler<ActionEvent>{
    
    /**
     * Feneetre de recapitulatif des ventes
     */
    private FenetreRecapitulatifVente fenetre;

    /**
     * Constructeur du controleur du bouton supprimer vente vielle
     * @param fenetre fenetre de recapitulatif des ventes
     * @param bd base de donnee
     */
    public ControleurBoutonSupprimerVenteVielle(FenetreRecapitulatifVente fenetre, BaseDeDonnee bd){
        this.fenetre = fenetre;
    }

    /**
     * Methode qui permet de supprimer les ventes vielle
     * @param event evenement
     */
    @Override
    public void handle(ActionEvent event) {
        try {
            this.fenetre.getVentesAnciennesCSV();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}