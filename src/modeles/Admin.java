package modeles;
import java.sql.SQLException;
import java.util.List;

public class Admin {
    private BaseDeDonnee bd;
    
    /**
     * Constructeur de la classe Admin
     */
    public Admin(BaseDeDonnee bd) throws SQLException, ClassNotFoundException{
        this.bd = bd;
    }
    
    /**
     * Méthode permettant de récupérer tous les signalements de vente
     */
    public List<List<String>> getAllSignalementVente() throws SQLException{
        return this.bd.getAllSignalementVente();
    }

    /**
     * Méthode permettant de récupérer tous les signalements de profil
     */
    public List<List<String>> getAllSignalementProfil() throws SQLException{
        return this.bd.getAllSignalementProfil();
    }


    /**
     * Méthode permettant de récupérer les informations d'un utilisateur
     */
    public List<String> getInfoUtilisateur(int idUtilisateur) throws SQLException{
        return this.bd.getInfoUserById(idUtilisateur);
    }


    /**
     * Méthode permettant de récupérer les informations d'une vente
     */
    public List<String> getInfoVente(int idVente) throws SQLException{
        return this.bd.getInfoByIdVe(idVente);
    }

    /**
     * Méthode permettant de récupérer les informations d'un signalement de vente
     */
    public void supprimerVente(int idVente) throws SQLException{
        this.bd.supprimerVente(idVente);
    }

    /**
     * Méthode permettant de récupérer les informations d'un signalement de vente
     */
    public void desactiverUtilisateur(int idUtilisateur) throws SQLException{
        this.bd.changementStatutUtilisateur(idUtilisateur, "N");
    }

    /**
     * Méthode permettant de récupérer les informations d'un signalement de vente
     */
    public void activerUtilisateur(int idUtilisateur) throws SQLException{
        this.bd.changementStatutUtilisateur(idUtilisateur, "O");
    }

    /**
     * Méthode permettant de récupérer les informations d'un signalement de vente
     */
    public void supprimerSignalementVente(int idSignalement) throws SQLException{
        this.bd.supprimerSignalementVente(idSignalement);
    }

    /**
     * Méthode permettant de récupérer les informations d'un signalement de vente
     */
    public void supprimerSignalementProfil(int idSignalement) throws SQLException{
        this.bd.supprimerSignalementUser(idSignalement);
    }

    /**
     * Méthode permettant de récupérer les informations d'un signalement de vente
     */
    public List<List<String>> getAllVente() throws SQLException{
        return this.bd.getAllVente();
    }

    /**
     * Méthode permettant de récupérer les informations d'un signalement de vente
     */
    public List<List<String>> getAllUtilisateurInactif() throws SQLException{
        return this.bd.getAllUserInactif();
    }

    /**
     * Méthode permettant de récupérer les informations d'un signalement de vente
     */
    public void supprimerUtilisateur(int idUtilisateur) throws SQLException{
        this.bd.supprimerUtilisateur(idUtilisateur);
    }
}