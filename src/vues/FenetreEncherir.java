package vues;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import java.sql.SQLException;
import java.util.List;
import controleurs.ControleurBoutonValiderOffre;
import controleurs.ControleurMessagePredefinis;
import controleurs.ControleurProfilVendeur;
import controleurs.ControleurRetourPageEnchere;
import modeles.BaseDeDonnee;
import modeles.Message;
import modeles.ModeleEncherir;
import modeles.Utilisateur;
import javafx.scene.layout.Priority;
import javafx.scene.image.Image;

/**
 * Cette classe représente une fenêtre pour enchérir sur une vente aux enchères.
 * Elle hérite de la classe StackPane.
 */
public class FenetreEncherir extends StackPane {

    private TextField montantOffre;
    private Integer prixActuel;
    private ModeleEncherir modele;
    private ApplicationVAE appli;
    private String dateDebut;
    private TextArea textAreaMessageVendeur;

    /**
 * Constructeur de la classe FenetreEncherir.
 * Crée une fenêtre d'enchères pour l'objet spécifié.
 *
 * @param messageModele   Le message du modèle utilisé.
 * @param bpPage          Le BorderPane de la page principale.
 * @param rootTop         Le BorderPane de la partie supérieure de la page.
 * @param rootLeft        Le BorderPane de la partie gauche de la page.
 * @param bd              La base de données utilisée.
 * @param appli           L'application principale.
 * @param userModele      L'utilisateur du modèle.
 * @param idVente         L'identifiant de la vente.
 * @param nomObjet        Le nom de l'objet en vente.
 * @param prixBase        Le prix de départ de la vente.
 * @param prixMini        Le prix minimum pour enchérir.
 * @param vendeur         Le vendeur de l'objet en vente.
 * @param borderMenuOption Le BorderPane du menu d'options.
 * @param textMenu        Le Label du texte du menu.
 * @param textPageAccueil Le Label du texte de la page d'accueil.
 * @param textVendreUnProduit Le Label du texte "Vendre un produit".
 * @param textVenteAuxEncheres Le Label du texte "Vente aux enchères".
 * @param textMessagerie Le Label du texte de la messagerie.
 * @param textMonPanier   Le Label du texte du panier.
 * @param textInformations Le Label du texte des informations.
 * @param textSetting     Le Label du texte des paramètres.
 * @param textQuitter     Le Label du texte "Quitter".
 * @param quitter         Le bouton "Quitter".
 * @param deconnexion     Le bouton de déconnexion.
 * @param menu            Le bouton du menu.
 * @param acceuil         Le bouton de retour à l'accueil.
 * @param message         Le bouton de la messagerie.
 * @param info            Le bouton des informations.
 * @param setting         Le bouton des paramètres.
 * @param profil          Le bouton du profil.
 * @param enchere         Le bouton des enchères.
 * @param panier          Le bouton du panier.
 * @param ajouterEnchere  Le bouton pour ajouter une enchère.
 *
 * @throws SQLException                Si une erreur se produit lors de l'accès à la base de données.
 * @throws NumberFormatException       Si une erreur se produit lors de la conversion d'une chaîne en nombre.
 * @throws ClassNotFoundException    Si la classe spécifiée n'est pas trouvée lors du chargement.
 */
    public FenetreEncherir(Message messageModele, BorderPane bpPage, BorderPane rootTop, BorderPane rootLeft, BaseDeDonnee bd, ApplicationVAE appli, Utilisateur userModele, String idVente, String nomObjet, String prixBase, String prixMini, String vendeur, BorderPane borderMenuOption, Label textMenu, Label textPageAccueil, Label textVendreUnProduit, Label textVenteAuxEncheres, Label textMessagerie, Label textMonPanier, Label textInformations, Label textSetting, Label textQuitter, Button quitter, Button deconnexion, Button menu,  Button acceuil, Button message, Button info, Button setting, Button profil, Button enchere, Button panier, Button ajouterEnchere) throws SQLException, NumberFormatException, ClassNotFoundException{
      //  BorderPane bpPage = new BorderPane();
        this.modele = new ModeleEncherir(bd, userModele.getId(), Integer.parseInt(idVente));
        this.appli = appli;
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        Label text = new Label("encherir une vente");
        text.setStyle("-fx-text-fill: #00aef0;");
        //bpPage.setCenter(enchereActuel); // warning
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        BorderPane rootLeft2 = new MenuLeft(acceuil, quitter, message, info, setting, enchere, panier, ajouterEnchere, bpPage);
        hBoxLeft.getChildren().addAll(rootLeft2, rootLeftOption);
        bpPage.setLeft(hBoxLeft);
       // BorderPane rootTop = new MenuTop(userModele, textMenu, deconnexion, profil, menu, bpPage);
        bpPage.setTop(rootTop);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        StackPane.setMargin(rootLeftOption, new Insets(48,0,0,56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);

        //initialisation des attributs
        prixActuel = this.modele.getMontantActuel();
        montantOffre = new TextField();

        BorderPane panelCentral = new BorderPane();

        HBox leTop = new HBox();
        VBox leLeft = new VBox();
        VBox leCenter = new VBox();
        HBox leRight = new HBox();
        BorderPane leBot = new BorderPane();

        //Titre de la page
        leTop.setAlignment(Pos.TOP_CENTER);
        Text titrePage = new Text("Enchère en cours ");
        titrePage.setFont(Font.font("arial", FontWeight.BOLD, 50));
        titrePage.setFill(Color.WHITE);
        HBox.setMargin(titrePage, new Insets(25,0,0,0));
        leTop.getChildren().addAll(titrePage);
        

        //Création de la partie gauche du centre de la page
        BorderPane.setMargin(leLeft, new Insets(0,0,0,25));

        //Partie Nom du produit
        HBox hbNomProduit = new HBox();
        Text titreNomProduit = new Text("Nom du produit : ");
        titreNomProduit.setFont(Font.font("arial", FontWeight.BOLD, 20));
        titreNomProduit.setFill(Color.WHITE);
        Text nomProduit = new Text(nomObjet);
        nomProduit.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        nomProduit.setFill(Color.WHITE);
        HBox.setMargin(titreNomProduit, new Insets(50,0,0,0));
        HBox.setMargin(nomProduit, new Insets(50,0,0,0));
        hbNomProduit.getChildren().addAll(titreNomProduit,nomProduit);

        //Partie Statut de l'enchère
        HBox hbStatut = new HBox();
        Text titreStatut = new Text("Statut : ");
        titreStatut.setFont(Font.font("arial", FontWeight.BOLD, 20));
        titreStatut.setFill(Color.WHITE);
        Text statut = new Text(bd.getVentebyIdVe(Integer.parseInt(idVente)).get(11));
        statut.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        statut.setFill(Color.WHITE);
        HBox.setMargin(titreStatut, new Insets(25,0,0,0));
        HBox.setMargin(statut, new Insets(25,0,0,0));
        hbStatut.getChildren().addAll(titreStatut,statut);

        //Partie Date de la vente
        HBox hbDate = new HBox();
        Text titreDateDebut = new Text("Date de la vente : ");
        titreDateDebut.setFont(Font.font("arial", FontWeight.BOLD, 20));
        titreDateDebut.setFill(Color.WHITE);
        this.dateDebut = bd.getVentebyIdVe(Integer.parseInt(idVente)).get(7);
        Text dateDebut = new Text(this.dateDebut);
        dateDebut.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        dateDebut.setFill(Color.WHITE);
        HBox.setMargin(titreDateDebut, new Insets(25,0,0,0));
        HBox.setMargin(dateDebut, new Insets(25,25,0,0));

        Text titreDateFin = new Text("Fin de la vente : ");
        titreDateFin.setFont(Font.font("arial", FontWeight.BOLD, 20));
        titreDateFin.setFill(Color.WHITE);
        Text dateFin = new Text(bd.getVentebyIdVe(Integer.parseInt(idVente)).get(8));
        dateFin.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        dateFin.setFill(Color.WHITE);
        HBox.setMargin(titreDateFin, new Insets(25,25,0,0));
        HBox.setMargin(dateFin, new Insets(25,25,0,0));

        hbDate.getChildren().addAll(titreDateDebut,dateDebut,titreDateFin,dateFin);

        //Partie des prix
        HBox hbPrix = new HBox();
        Text titrePrixDepart = new Text("Prix de départ : ");
        titrePrixDepart.setFont(Font.font("arial", FontWeight.BOLD, 20));
        titrePrixDepart.setFill(Color.WHITE);
        Text prixDepart = new Text(prixBase+" € ");
        prixDepart.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        prixDepart.setFill(Color.WHITE);
        HBox.setMargin(titrePrixDepart, new Insets(25,0,0,0));
        HBox.setMargin(prixDepart, new Insets(25,25,0,0));

        Text titrePrixActuel = new Text("Prix actuel : ");
        titrePrixActuel.setFont(Font.font("arial", FontWeight.BOLD, 20));
        titrePrixActuel.setFill(Color.WHITE);
        Text textFieldPrixActuel = new Text(String.valueOf(prixActuel)+" € ");
        textFieldPrixActuel.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        textFieldPrixActuel.setFill(Color.WHITE);
        HBox.setMargin(titrePrixActuel, new Insets(25,0,0,0));
        HBox.setMargin(textFieldPrixActuel, new Insets(25,0,0,0));
        hbPrix.getChildren().addAll(titrePrixDepart,prixDepart,titrePrixActuel,textFieldPrixActuel);

        //Partie infos vendeurs 
        HBox hbInfosVendeurs = new HBox();
        Text titreInfosVendeurs = new Text("Vendeur(se) : ");
        titreInfosVendeurs.setFont(Font.font("arial", FontWeight.BOLD, 20));
        titreInfosVendeurs.setFill(Color.WHITE);
        Text infosVendeurs = new Text(vendeur);
        infosVendeurs.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        infosVendeurs.setFill(Color.WHITE);
        HBox.setMargin(titreInfosVendeurs, new Insets(25,0,0,0));
        HBox.setMargin(infosVendeurs, new Insets(25,25,0,0));
        Button voirProfilVendeur = new Button("Voir le profil");
        voirProfilVendeur.setStyle("-fx-background-color: #4A919E;");
        voirProfilVendeur.setOnAction(new ControleurProfilVendeur(appli, Integer.parseInt(idVente)));
        HBox.setMargin(voirProfilVendeur, new Insets(25,0,0,0));

        hbInfosVendeurs.getChildren().addAll(titreInfosVendeurs,infosVendeurs,voirProfilVendeur);

        //Partie historique des offres
        VBox vbHistoriqueOffre = new VBox();
        Text titreHistoriqueOffre = new Text("Historique des offres : ");
        titreHistoriqueOffre.setFont(Font.font("arial", FontWeight.BOLD, 20));
        titreHistoriqueOffre.setFill(Color.WHITE);
        VBox.setMargin(titreHistoriqueOffre, new Insets(25,0,0,0));

        int indX = 0;
        int indY = 1;
        GridPane gpHistoriqueOffre = new GridPane();
        //gpHistoriqueOffre.setStyle("-fx-background-color: #BED3C3;");
        VBox.setMargin(gpHistoriqueOffre, new Insets(25,0,0,0));
        GridPane.setHgrow(gpHistoriqueOffre, Priority.ALWAYS);
        GridPane.setVgrow(gpHistoriqueOffre, Priority.ALWAYS);
        gpHistoriqueOffre.setHgap(10); // Espacement horizontal entre les colonnes

        gpHistoriqueOffre.setGridLinesVisible(true);
        Text infoPseudo = new Text("Pseudo");
        infoPseudo.setFont(Font.font("arial", FontWeight.BOLD, 20));
        infoPseudo.setFill(Color.WHITE);
        gpHistoriqueOffre.add(infoPseudo,0,0);

        Text infoDate = new Text("Date");
        infoDate.setFont(Font.font("arial", FontWeight.BOLD, 20));
        infoDate.setFill(Color.WHITE);
        gpHistoriqueOffre.add(infoDate,1,0);

        Text infoMontant = new Text("Montant");
        infoMontant.setFont(Font.font("arial", FontWeight.BOLD, 20));
        infoMontant.setFill(Color.WHITE);
        gpHistoriqueOffre.add(infoMontant,2,0);

        for(List<String> ligne : bd.getOffreEncherir(Integer.parseInt(idVente))){
            indX = 0;
            for(String infoOffre: ligne){
                Text textInfoOffre = new Text(infoOffre);
                textInfoOffre.setFont(Font.font("arial", FontPosture.REGULAR, 20));
                textInfoOffre.setFill(Color.WHITE);
                gpHistoriqueOffre.add(textInfoOffre,indX,indY);

                indX+=1;
                
            }
            indY += 1;
        }


        VBox vBoxMessageVendeur = new VBox();
        vBoxMessageVendeur.setSpacing(15);
        Label labelMessageVendeur = new Label("Envoyer un message au vendeur : ");
        labelMessageVendeur.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: #ffffff;");
        labelMessageVendeur.setPadding(new Insets(15,0,0,0));
        this.textAreaMessageVendeur = new TextArea();
        textAreaMessageVendeur.setPromptText("Envoyer un message au vendeur..");
        Button boutonEnvoyerMessageVendeur = new Button("Envoyer");
        boutonEnvoyerMessageVendeur.setStyle("-fx-background-color: #4A919E;");
        boutonEnvoyerMessageVendeur.setOnAction(new ControleurMessagePredefinis(appli, messageModele, userModele, messageModele.getIdUtByIdVe(Integer.parseInt(idVente)), this));
        vBoxMessageVendeur.getChildren().addAll(labelMessageVendeur, textAreaMessageVendeur, boutonEnvoyerMessageVendeur);



        vbHistoriqueOffre.getChildren().addAll(titreHistoriqueOffre,gpHistoriqueOffre);


        leLeft.getChildren().addAll(hbNomProduit,hbStatut,hbDate,hbPrix,hbInfosVendeurs,vbHistoriqueOffre, vBoxMessageVendeur);


        //LeCenter

        //Le séparateur du Left et Right du BorderPane panelCentral
        leCenter.setAlignment(Pos.CENTER);
        Rectangle seperateurCentre = new Rectangle(5,500);
        seperateurCentre.setFill(Color.rgb(190,211,195));
        leCenter.getChildren().addAll(seperateurCentre);


        //Création de la partie droite du centre de la page

        //Partie Image du produit
        VBox vbLeRight = new VBox();
        leRight.setPadding(new Insets(10,10,10,10));
        leRight.setAlignment(Pos.TOP_LEFT);
        leRight.setMinWidth(500);
        leRight.setMaxWidth(500);
        //HBox.setMargin(vbLeRight, new Insets(0,250,0,0));

        Text titreImagesProduit = new Text("Images du produit : ");
        VBox.setMargin(titreImagesProduit, new Insets(50,0,50,0));
        titreImagesProduit.setFont(Font.font("arial", FontWeight.BOLD, 20));
        titreImagesProduit.setFill(Color.WHITE);
        Image image = new Image("file:"+bd.getVentebyIdVe(Integer.parseInt(idVente)).get(10));
        ImageView imagesProduit = new ImageView(image);
        imagesProduit.setFitHeight(300);
        imagesProduit.setFitWidth(300);

        //Partie Description du produit
        Text titreDescription = new Text("Description : ");
        titreDescription.setFont(Font.font("arial", FontWeight.BOLD, 20));
        titreDescription.setFill(Color.WHITE);
        VBox.setMargin(titreDescription, new Insets(50,0,0,0));
        Text description = new Text(bd.getVentebyIdVe(Integer.parseInt(idVente)).get(5));
        description.wrappingWidthProperty().set(450);
        description.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        description.setFill(Color.WHITE);
        VBox.setMargin(description, new Insets(25,0,0,0));


        //Partie Catégorie du produit
        Text titreCategories = new Text("Catégories : ");
        VBox.setMargin(titreCategories, new Insets(25,0,0,0));
        titreCategories.setFont(Font.font("arial", FontWeight.BOLD, 20));
        titreCategories.setFill(Color.WHITE);
        Text categorie = new Text(bd.getVentebyIdVe(Integer.parseInt(idVente)).get(9));
        categorie.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        categorie.setFill(Color.WHITE);
        VBox.setMargin(categorie, new Insets(25,0,0,0));

        //On rassemble les éléments dans la VBox vbLeRight()
        vbLeRight.getChildren().addAll(titreImagesProduit,imagesProduit,titreDescription,description,titreCategories,categorie);
        leRight.getChildren().addAll(vbLeRight);


        //Le Bot, création de la partie basse du centre de la page
        //Partie gauche
        HBox partieGauche = new HBox();
        Text titreMontantOffre = new Text("Montant de l'offre : ");
        titreMontantOffre.setFont(Font.font("arial", FontWeight.BOLD, 20));
        titreMontantOffre.setFill(Color.WHITE);
        HBox.setMargin(titreMontantOffre, new Insets(25,0,0,25));
        HBox.setMargin(montantOffre, new Insets(25,0,0,0));
        partieGauche.getChildren().addAll(titreMontantOffre,montantOffre);

        //Partie droite
        HBox partieDroite = new HBox();
        Button valider = new Button("Valider");
        valider.setStyle("-fx-background-color: #4A919E;");
        valider.setMinWidth(200);
        valider.setMinHeight(25);
        valider.setOnAction(new ControleurBoutonValiderOffre(this.appli,bd,this, userModele.getId(), Integer.parseInt(idVente)));
        HBox.setMargin(valider, new Insets(25,25,25,25));
        Button retour = new Button("Retour");
        retour.setStyle("-fx-background-color: #CE6A6B");
        retour.setMinWidth(200);
        retour.setMinHeight(25);
        retour.setOnAction(new ControleurRetourPageEnchere(appli));
        HBox.setMargin(retour, new Insets(25,25,25,25));

        partieDroite.getChildren().addAll(retour,valider); 

        //On rassemble tous les élements de la partie VBox dans leBot
        leBot.setLeft(partieGauche);
        leBot.setRight(partieDroite);
        

        //On rassemble tous les éléments dans le borderPane panelCentral
        panelCentral.setTop(leTop);
        panelCentral.setLeft(leLeft);
        panelCentral.setCenter(leCenter);
        panelCentral.setRight(leRight);
        panelCentral.setBottom(leBot);

        bpPage.setCenter(panelCentral);
    } 

    public String getDateDebut(){
        return this.dateDebut;
    }

    public Integer getMontantOffre() {
        try{
            return Integer.parseInt(this.montantOffre.getText());
        }
        catch(NumberFormatException e){
            System.out.println("Erreur de format de montant");
        }
        return -1;
    }

    public Integer getPrixActuel(){
        return prixActuel;
    }
    
    public String getTextAreaMessage(){
        String text = this.textAreaMessageVendeur.getText();
        this.textAreaMessageVendeur.clear();
        return text;
    }

}