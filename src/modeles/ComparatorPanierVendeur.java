package modeles;
import java.util.Comparator;
import java.util.List;

public class ComparatorPanierVendeur implements Comparator<List<String>>{
    
    /**
     * Compare deux listes de String selon le prix de l'objet
     */
    @Override
    public int compare(List<String> l1, List<String> l2){
        return l1.get(2).compareTo(l2.get(2));
    }
}