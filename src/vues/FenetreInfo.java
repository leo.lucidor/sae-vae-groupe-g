package vues;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import modeles.Utilisateur;


/**
 * Cette classe représente une fenêtre d'informations affichée dans une pile de panneaux.
 * Elle hérite de la classe StackPane.
 */
public class FenetreInfo extends StackPane {

    /**
     * Constructeur de la classe FenetreInfo.
     *
     * @param bpPage             Le panneau de bordure de la page principale.
     * @param rootTop            Le panneau de bordure de la partie supérieure de la fenêtre.
     * @param rootLeftAdmin      Le panneau de bordure de la partie gauche de la fenêtre pour l'administrateur.
     * @param userModele         L'objet Utilisateur représentant le modèle d'utilisateur.
     * @param borderMenuOption   Le panneau de bordure pour les options de menu.
     * @param textMenu           Le label de texte du menu.
     * @param textPageAccueil    Le label de texte pour la page d'accueil.
     * @param textVendreUnProduit Le label de texte pour la vente d'un produit.
     * @param textVenteAuxEncheres Le label de texte pour la vente aux enchères.
     * @param textMessagerie     Le label de texte pour la messagerie.
     * @param textMonPanier      Le label de texte pour le panier.
     * @param textInformations   Le label de texte pour les informations.
     * @param textSetting        Le label de texte pour les paramètres.
     * @param textQuitter        Le label de texte pour quitter.
     * @param quitter            Le bouton pour quitter.
     * @param deconnexion        Le bouton pour la déconnexion.
     * @param menu               Le bouton pour le menu.
     * @param acceuil            Le bouton pour la page d'accueil.
     * @param message            Le bouton pour la messagerie.
     * @param info               Le bouton pour les informations.
     * @param setting            Le bouton pour les paramètres.
     * @param profil             Le bouton pour le profil.
     * @param enchere            Le bouton pour la vente aux enchères.
     * @param panier             Le bouton pour le panier.
     * @param ajouterEnchere     Le bouton pour ajouter une enchère.
     */
    public FenetreInfo(BorderPane bpPage, BorderPane rootTop, BorderPane rootLeftAdmin, Utilisateur userModele, BorderPane borderMenuOption, Label textMenu, Label textPageAccueil, Label textVendreUnProduit, Label textVenteAuxEncheres, Label textMessagerie, Label textMonPanier, Label textInformations, Label textSetting, Label textQuitter, Button quitter, Button deconnexion, Button menu,  Button acceuil, Button message, Button info, Button setting, Button profil, Button enchere, Button panier, Button ajouterEnchere){
        //BorderPane bpPage = new BorderPane();
        VBox vbPage = new VBox();
        Label titre = new Label("Information");
        TabPane tabPane = new TabPane();
        titre.setStyle("-fx-text-fill: #ffffff;");
        titre.setFont(new Font("Arial", 35));
        VBox.setMargin(titre, new Insets(200,200,200,200));
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        

        //vbPage.setMargin(tabPane, new Insets(100,100,100,100));
    



        // création des onglets
        Tab tab1 = new Tab("Description de l'application");
        tab1.setClosable(false);
        Tab tab2 = new Tab("Tutoriel des fonctionnalités");
        tab2.setClosable(false);
        Tab tab3 = new Tab("Règle de l'application");
        tab3.setClosable(false);
        Tab tab4 = new Tab("Politique de confidentialité");
        tab4.setClosable(false);




        // Création des BorderPane pour affiché les informations quand on clique sur un onglet
            BorderPane bpDescri = new BorderPane();
            BorderPane bpRegle = new BorderPane();
            BorderPane bpPolitique = new BorderPane();

        
        //Création des informations à mettre dans le borderPane description
        Label descript = new Label("Description de l'application");
        Label attention = new Label("L'application de vente aux enchères est une plateforme en ligne dédiée aux enchères publiques. \nElle permet aux utilisateurs de découvrir et de participer à une large gamme d'enchères pour des produits variés. \nLes utilisateurs peuvent créer des comptes, parcourir les enchères en cours, enchérir sur les produits qui les intéressent \net suivre l'évolution des enchères. L'application fournit des informations détaillées sur chaque produit, telles que la description, \nles photos et les informations sur le vendeur. Les utilisateurs peuvent également créer des profils, \noù ils peuvent suivre leurs enchères, consulter leur historique d'enchères et gérer leurs transactions. \nL'application offre une expérience conviviale et sécurisée, permettant aux utilisateurs de participer facilement aux enchères\net de profiter d'une large sélection de produits passionnants.");
        descript.setFont(new Font("Arial", 25));
        descript.setStyle("-fx-text-fill:#012245;");
        attention.setStyle("-fx-text-fill: #012245;");
        attention.setFont(new Font("Arial", 18));
        bpDescri.setTop(descript);
        bpDescri.setCenter(attention);
        BorderPane.setMargin(descript, new Insets(100, 0,0,125));
        BorderPane.setMargin(attention, new Insets(30,0,0,-450));


        //Création des informations à mettre dans le borderPane Politique de confidentialité
        Label pol = new Label("Politique de confidentialité");
        Label intro = new Label("Merci d'utiliser notre logiciel de vente aux enchères. \n\nNous attachons une grande importance à la confidentialité et à la protection de vos informations personnelles. Cette politique de confidentialité vise à vous informer sur les types\nde données que nous collectons, comment nous les utilisons, les mesures de sécurité que nous mettons en place, et vos droits en ce qui concerne vos informations personnelles.\nCollecte et utilisation des informations personnelles\nNous collectons les informations personnelles que vous nous fournissez volontairement lors de la création d'un compte ou de la participation aux enchères. Ces \ninformations peuvent inclure votre nom, votre adresse e-mail, votre adresse postale, votre numéro de téléphone, ainsi que d'autres informations nécessaires pour \nvous fournir nos services. Nous utilisons vos informations personnelles pour faciliter votre participation aux enchères, traiter les transactions, vous informer \nsur les résultats des enchères, vous envoyer des notifications pertinentes, et vous fournir un support client efficace.\n\n\nProtection des informations personnelles\n\nNous mettons en place des mesures de sécurité techniques et organisationnelles appropriées pour protéger vos informations personnelles contre tout accès non \nautorisé, utilisation abusive, divulgation ou altération. Nous nous engageons à préserver la confidentialité et l'intégrité de vos données.\nPartage des informations personnelles. \nNous ne vendons, ne louons ni ne partageons vos informations personnelles avec des tiers à des fins de marketing. Cependant, dans le cadre de nos activités \ncommerciales légitimes, nous pouvons partager vos informations avec des partenaires de confiance, tels que des services de paiement ou de livraison, pour \nfaciliter le bon déroulement des enchères et des transactions.\n\nNous vous remercions de votre confiance et de votre utilisation de notre logiciel de vente aux enchères. Votre vie privée est importante pour nous et nous nous \nengageons à la protéger de manière sécurisée et responsable.");
        pol.setFont(new Font("Arial", 25));
        pol.setStyle("-fx-text-fill:#012245;");
        intro.setStyle("-fx-text-fill: #012245;");
        intro.setFont(new Font("Arial", 18));
        bpPolitique.setTop(pol);
        bpPolitique.setCenter(intro);
        BorderPane.setMargin(pol, new Insets(100, 0,0,125));
        BorderPane.setMargin(intro, new Insets(30,0,200,0));


        //Création des données à mettre dans le borderPane Règle de l'application
        Label r = new Label("Règles de l'application");
        Label regle = new Label("Création d'un compte unique : \nChaque utilisateur doit créer un seul compte personnel. L'utilisation de plusieurs comptes pour contourner les règles ou tromper les autres \nutilisateurs est strictement interdite.\n\nConfidentialité et protection des informations : \nLes utilisateurs doivent respecter la politique de confidentialité de l'application et ne pas divulguer les informations personnelles d'autres utilisateurs\n sans leur consentement.\n\nComportement respectueux : \nLes utilisateurs doivent adopter un comportement respectueux envers les autres utilisateurs. Les insultes, le harcèlement, la discrimination ou toute autre \nforme de comportement nuisible ne seront pas tolérés.\n\nExactitude des informations : \nLes utilisateurs sont responsables de fournir des informations précises, complètes et à jour lors de la création de leur compte et lors de la mise en vente \nd'objets. Toute tentative de tromperie ou de fournir de fausses informations est strictement interdite.\n\nAuthenticité des objets : \nLes utilisateurs doivent garantir que les objets mis en vente sont authentiques et conformes à leur description. La vente d'objets contrefaits, illégaux \nou frauduleux est strictement interdite.");
        r.setFont(new Font("Arial", 25));
        r.setStyle("-fx-text-fill:#012245;");
        regle.setStyle("-fx-text-fill: #012245;");
        regle.setFont(new Font("Arial", 18));
        bpRegle.setTop(r);
        bpRegle.setCenter(regle);
        BorderPane.setMargin(r, new Insets(100, 0,0,125));
        BorderPane.setMargin(regle, new Insets(0,0,200,-200));



        

        // Ajout des labels aux onglets
        tab1.setContent(bpDescri);
        tab1.setStyle("-fx-background-color: #BED3C3;");
        tab3.setContent(bpRegle);
        tab3.setStyle("-fx-background-color:#BED3C3;");
        tab4.setContent(bpPolitique);
        tab4.setStyle("-fx-background-color: #BED3C3;");

        //padding onglet
        bpDescri.setPadding(new Insets(0,0,500,0));

        // Ajout des onglets
        tabPane.getTabs().addAll(tab1, tab3, tab4);
        tabPane.setStyle("-fx-background-color: #BED3C3;");


        vbPage.getChildren().addAll(titre, tabPane );
        VBox.setMargin(titre, new Insets(50,0,50,825));
        vbPage.setAlignment(Pos.CENTER_LEFT);
        bpPage.setCenter(vbPage);
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        BorderPane rootLeft = new BorderPane();
        if(userModele.getRole() == 2){
            rootLeft = new MenuLeft(acceuil, quitter, message, info, setting, enchere, panier, ajouterEnchere, bpPage);
        }
        else {
            rootLeft = rootLeftAdmin;
        }
        hBoxLeft.getChildren().addAll(rootLeft, rootLeftOption);
        bpPage.setLeft(hBoxLeft);
        //BorderPane rootTop = new MenuTop(userModele, textMenu, deconnexion, profil, menu, bpPage);
        bpPage.setTop(rootTop);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        StackPane.setMargin(rootLeftOption, new Insets(48,0,0,56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);



    }

}