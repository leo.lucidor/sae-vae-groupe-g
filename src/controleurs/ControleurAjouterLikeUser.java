package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import modeles.Utilisateur;

import java.sql.SQLException;

import javafx.event.ActionEvent;


public class ControleurAjouterLikeUser implements EventHandler<ActionEvent> {
    
    /**
     * Attributs prives ou proteges
     */
    private ApplicationVAE appli;
    /**
     * Attributs publics
     */
    private int idUtilisateur;
    /**
     * Attributs publics
     */
    private Utilisateur userModele;
    
    /**
     * Constructeur
     * @param appli  l'application vae
     * @param idUtilisateur l'identifiant de l'utilisateur
     * @param userModele le modele de l'utilisateur
     */
    public ControleurAjouterLikeUser(ApplicationVAE appli, int idUtilisateur, Utilisateur userModele){
        this.appli = appli;
        this.idUtilisateur = idUtilisateur;
        this.userModele = userModele;
    }

    /**
     * Methode qui ajoute un like a un utilisateur
     * @param event evenement
     */
    @Override
    public void handle(ActionEvent event) {
        try {
            this.userModele.incrementerLike(this.idUtilisateur);
            this.appli.pageProfilExterne(idUtilisateur);
        } catch (SQLException e) {
           e.printStackTrace();
        }
    }
}