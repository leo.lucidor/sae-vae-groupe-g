package controleurs;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import vues.FenetreSetting;

public class ControleurModifierCarte implements EventHandler<ActionEvent>{
    
    /**
     * FenetreSetting
     */
    private FenetreSetting fenetreModifierCarte;

    /**
     * Constructeur de ControleurModifierCarte
     * @param fenetreModifierCarte
     */
    public ControleurModifierCarte(FenetreSetting fenetreModifierCarte){
        this.fenetreModifierCarte = fenetreModifierCarte;
    }

    /**
     * Méthode handle
     * @param event evenement
     */
    @Override
    public void handle(ActionEvent event){
        Button btn = (Button) event.getSource();
        if (btn.getText().equals("Modifier")){
            this.fenetreModifierCarte.modifierCarte();
        }
        else if (btn.getText().equals("Valider")){
            this.fenetreModifierCarte.validerCarte();
        }
    }

}
