package modeles;
import java.util.Comparator;
import java.util.List;

public class ComparatorPanierPrix implements Comparator<List<String>>{
    
    /**
     * Compare deux listes de String selon le prix total du panier
     */
    public int compare(List<String> l1, List<String> l2){
        return Integer.parseInt(l1.get(9)) - Integer.parseInt(l2.get(9));
    }
}