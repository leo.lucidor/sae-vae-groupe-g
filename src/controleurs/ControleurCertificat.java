package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurCertificat implements EventHandler<ActionEvent> {
    
    /**
     * L'application 
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur de la classe
     * @param appli l'application
     */
    public ControleurCertificat(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Gère l'action sur le bouton certificat
     * @param event l'évènement
     */
    @Override
    public void handle(ActionEvent event) {
        try {
			this.appli.pageCertificatVente();
		} catch (SQLException e) {
			e.printStackTrace();
        }
    }
}
