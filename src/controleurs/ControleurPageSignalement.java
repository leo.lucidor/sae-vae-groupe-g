package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurPageSignalement implements EventHandler<ActionEvent> {
    
    /**
     * Application principale
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur
     * @param appli l'application principale
     */
    public ControleurPageSignalement(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Méthode qui permet de gérer les actions sur les boutons de la page de signalement
     * @param event l'action sur le bouton
     */
    @Override
    public void handle(ActionEvent event) {
        try {
            this.appli.pageSignalement();
        } catch (SQLException e) {
           e.printStackTrace();
        }
    }
}