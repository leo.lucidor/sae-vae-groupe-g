package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import modeles.Vente;
import javafx.event.ActionEvent;

public class ControleurSignalerVente implements EventHandler<ActionEvent> {
    
    /**
     * application principale
     */
    private ApplicationVAE appli;
    /**
     * modele de la vente
     */
    private Vente venteModele;
    /**
     * id de la vente
     */
    private int idVente;
    /**
     * id de l'utilisateur
     */
    private int idUtilisateur;
    
    /**
     * Constructeur du controleur ControleurSignalerVente
     * @param appli application principale
     * @param venteModele modele de la vente
     * @param idVente id de la vente
     * @param idUtilisateur id de l'utilisateur
     */
    public ControleurSignalerVente(ApplicationVAE appli, Vente venteModele, int idVente, int idUtilisateur){
        this.appli = appli;
        this.venteModele = venteModele;
        this.idVente = idVente;
        this.idUtilisateur = idUtilisateur;
    }

    /**
     * Méthode handle
     * @param event 
     */
    @Override
    public void handle(ActionEvent event) {
        String raisonReport = this.appli.popUpSignalerVente();
        try {
            venteModele.signalerVente(raisonReport, idUtilisateur, idVente);
            System.out.println("Signalement envoyé : "+raisonReport);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
