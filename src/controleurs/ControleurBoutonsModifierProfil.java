package controleurs;
import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import vues.ApplicationVAE;
import modeles.EmailValidator;
import modeles.Utilisateur;
import vues.FenetreModifierProfil;
import javafx.scene.control.Button;

public class ControleurBoutonsModifierProfil implements EventHandler<ActionEvent>{
    
    /**
     * Fenetre de modification du profil
     */
    private FenetreModifierProfil fenetreModifProfil;
    /**
     * Application principale
     */
    private ApplicationVAE appli;
    /**
     * Utilisateur connecté
     */
    private Utilisateur user;

    /**
     * Constructeur du controleur de la fenetre de modification du profil
     * @param fenetreModifProfil fenetre de modification du profil
     * @param appli application principale
     */
    public ControleurBoutonsModifierProfil(FenetreModifierProfil fenetreModifProfil, ApplicationVAE appli){
        this.fenetreModifProfil = fenetreModifProfil;
        this.appli = appli;
        this.user = this.fenetreModifProfil.getUserModele();
    }

    /**
     * Gestion des actions sur les boutons de la fenetre de modification du profil
     * @param event evenement
     */
    public void handle(ActionEvent event){
        Button boutonAppuyer = (Button) event.getSource();

        switch(boutonAppuyer.getText()){
            case "Valider":
                try {
                    if (this.fenetreModifProfil.getNom().isEmpty() || this.fenetreModifProfil.getPrenom().isEmpty() || this.fenetreModifProfil.getEmail().isEmpty() || this.fenetreModifProfil.getPseudo().isEmpty()){
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Erreur");
                    alert.setHeaderText("Erreur de saisie");
                    alert.setContentText("Veuillez remplir les champs obligatoires annotés *");
                    alert.showAndWait();
                    } 
                    else if (!(this.fenetreModifProfil.getNumero() == null) || !this.fenetreModifProfil.getNumero().isEmpty()){
                        if (this.fenetreModifProfil.getNumero().length() > 10 || this.fenetreModifProfil.getNumero().length() < 10){
                            Alert alert = new Alert(AlertType.ERROR);
                            alert.setTitle("Erreur");
                            alert.setHeaderText("Erreur de saisie");
                            alert.setContentText("Veuillez saisir un numéro de téléphone valide");
                            alert.showAndWait();
                        }
                        else{
                            if (!EmailValidator.isValidEmail(this.fenetreModifProfil.getEmail())){
                            Alert alert = new Alert(AlertType.ERROR);
                            alert.setTitle("Erreur");
                            alert.setHeaderText("Erreur de saisie");
                            alert.setContentText("Veuillez saisir une adresse mail valide");
                            alert.showAndWait();
                            }
                            else{
    
                            this.user.setNom(this.fenetreModifProfil.getNom());
                            this.user.setPseudo(this.fenetreModifProfil.getPseudo());
                            this.user.setMail(this.fenetreModifProfil.getEmail());
                            this.user.setPrenom(this.fenetreModifProfil.getPrenom());
                            this.user.setGenre(this.fenetreModifProfil.getGenre());
                            this.user.setTel(this.fenetreModifProfil.getNumero());
                            this.user.setAge(this.fenetreModifProfil.getAge());
                            this.user.majInfoUtilisateur();
                            this.appli.pageProfil();}
                        }
                    }
                    
                    
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }
                break;


            case "Réinitialiser":
                this.fenetreModifProfil.resetText();
                break;

            case "Retour" :
                try {
                    this.appli.pageProfil();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            default:
                System.out.println("Erreur");
                break;
        }
    }
}