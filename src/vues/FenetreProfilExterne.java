package vues;
import java.sql.SQLException;
import java.util.List;

import controleurs.ControleurAjouterLikeUser;
import controleurs.ControleurSignalerProfil;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import modeles.Utilisateur;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;


public class FenetreProfilExterne extends StackPane {

    private String nom;
    private String prenom;
    private String pseudo;
    private String mail;
    private String tel;
    private String genre;
    private int age;
    private int nbLike;
    private int idPhUt;

    public FenetreProfilExterne(int idUtilisateur, BorderPane bpPage, BorderPane rootTop, BorderPane rootLeftAdmin, Utilisateur userModele, ApplicationVAE appli, BorderPane borderMenuOption, Label textMenu, Label textPageAccueil, Label textVendreUnProduit, Label textVenteAuxEncheres, Label textMessagerie, Label textMonPanier, Label textInformations, Label textSetting, Label textQuitter, Button quitter, Button deconnexion, Button menu,  Button acceuil, Button message, Button info, Button setting, Button profil, Button enchere, Button panier, Button ajouterEnchere) throws SQLException{
        List<String> lesInfosActuel = userModele.getAllinfoUtilisateur(idUtilisateur);
        this.nom = lesInfosActuel.get(7);
        this.prenom = lesInfosActuel.get(6);
        this.pseudo = lesInfosActuel.get(1);
        this.mail = lesInfosActuel.get(2);
        this.tel = lesInfosActuel.get(10);
        this.genre = lesInfosActuel.get(9);
        this.age = Integer.parseInt(lesInfosActuel.get(8));
        this.nbLike = Integer.parseInt(lesInfosActuel.get(12));
        this.idPhUt = Integer.parseInt(lesInfosActuel.get(11));

        //  BorderPane bpPage = new BorderPane();
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        Label text = new Label("profil");
        text.setStyle("-fx-text-fill: #00aef0;");
        bpPage.setCenter(text);
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        BorderPane rootLeft = new BorderPane();
        if(userModele.getRole() == 2){
            rootLeft = new MenuLeft(acceuil, quitter, message, info, setting, enchere, panier, ajouterEnchere, bpPage);
        }
        else {
            rootLeft = rootLeftAdmin;
        }
        hBoxLeft.getChildren().addAll(rootLeft, rootLeftOption);
        bpPage.setLeft(hBoxLeft);
        //BorderPane rootTop = new MenuTop(userModele, textMenu, deconnexion, profil, menu, bpPage);
        bpPage.setTop(rootTop);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        StackPane.setMargin(rootLeftOption, new Insets(48,0,0,56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);
        
        // BorderPane principal du profil
        BorderPane zonePrincipal = new BorderPane();

        //Le top
        VBox vbTop = new VBox();
        
        vbTop.setAlignment(Pos.TOP_CENTER);
        Text titrePage = new Text("Profil");
        titrePage.setTextAlignment(TextAlignment.CENTER);
        titrePage.setFill(Color.WHITE);
        titrePage.setFont(Font.font("arial", FontWeight.BOLD, FontPosture.REGULAR, 50));
        VBox.setMargin(titrePage, new Insets(25,0,25,0));

        vbTop.getChildren().addAll(titrePage);
        zonePrincipal.setTop(vbTop);


        // Le center
        VBox center = new VBox();
        // Création des conteneurs principaux
        HBox infosPrincipales = new HBox();
        HBox.setMargin(infosPrincipales, new Insets(15));
        VBox.setMargin(infosPrincipales, (new Insets(5,5,100,5)));
        HBox infosImportantes = new HBox();
        HBox.setMargin(infosImportantes, new Insets(15));
        infosImportantes.setPadding(new Insets(5,5,50,5));
        VBox rectangle = new VBox(20);
        Rectangle blocSeparateur = new Rectangle(500,2);
        VBox.setMargin(blocSeparateur, new Insets(10,10,10,150));
        blocSeparateur.setFill(Color.rgb(190,211,195));
        VBox infosDeContact = new VBox();
        VBox.setMargin(infosDeContact, new Insets(50,10,10,15));

        //Création des Text du TilePane
        //Nom de l'utilisateur
        Text titreNom = new Text("Nom :");
        titreNom.setFill(Color.WHITE);
        titreNom.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Text nom = new Text(this.nom);
        nom.setFill(Color.WHITE);
        nom.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        HBox.setMargin(titreNom, new Insets(0,25,0,15));
        HBox.setMargin(nom, new Insets(0,75,0,0));

        //Prénom de l'utilisateur
        Text titrePrenom = new Text("Prénom :");
        titrePrenom.setFill(Color.WHITE);
        titrePrenom.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Text prenom = new Text(this.prenom);
        prenom.setFill(Color.WHITE);
        prenom.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        HBox.setMargin(titrePrenom, new Insets(0,25,0,0));
        HBox.setMargin(prenom, new Insets(0,75,0,0));

        //Pseudo de l'utilisateur
        Text titrePseudo = new Text("Pseudo :");
        titrePseudo.setFill(Color.WHITE);
        titrePseudo.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Text pseudo = new Text(this.pseudo);
        pseudo.setFill(Color.WHITE);
        pseudo.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        HBox.setMargin(titrePseudo, new Insets(0,25,0,0));
        HBox.setMargin(pseudo, new Insets(0,75,0,0));

        //age de l'utilisateur
        Text titreAge = new Text("Age :");
        titreAge.setFill(Color.WHITE);
        titreAge.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Text age = new Text(String.valueOf(this.age));
        age.setFill(Color.WHITE);
        age.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        HBox.setMargin(titreAge, new Insets(0,25,0,15));
        HBox.setMargin(age, new Insets(0,75,0,0));

        //Sexe de l'utilisateur
        Text titreGenre = new Text("Genre :");
        titreGenre.setFill(Color.WHITE);
        titreGenre.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Text genre = new Text(this.genre);
        genre.setFill(Color.WHITE);
        genre.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        HBox.setMargin(titreGenre, new Insets(0,25,0,0));

        
        // HBox pour l'email de l'utilisateur
        HBox hbEmail = new HBox();
        hbEmail.setPadding(new Insets(0,0,75,0));

        Text titreEmail = new Text("Adresse Mail :");
        titreEmail.setFill(Color.WHITE);
        titreEmail.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Text email = new Text(this.mail);
        email.setFill(Color.WHITE);
        email.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        HBox.setMargin(titreEmail, new Insets(0,25,0,0));

        hbEmail.getChildren().addAll(titreEmail, email);

        // HBox pour le numéro de l'utilisateur
        HBox hbNumero = new HBox();
        hbNumero.setPadding(new Insets(0,0,75,0));

        Text titreNumero = new Text("N° Téléphone :");
        titreNumero.setFill(Color.WHITE);
        titreNumero.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Text numero = new Text(this.tel);
        numero.setFill(Color.WHITE);
        numero.setFont(Font.font("arial", FontPosture.REGULAR, 20));

        hbNumero.getChildren().addAll(titreNumero, numero);

        //Description de l'utilisateur
        HBox hBoxSignaler = new HBox();
        Text titreDescription = new Text("Signaler le profil : ");
        titreDescription.setFill(Color.WHITE);
        titreDescription.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Label description = new Label("                                                   ");
        description.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        description.setPadding(new Insets(0,500,200,0));
        //description.setBackground(new Background(new BackgroundFill(Color.web("#BED3C3"), CornerRadii.EMPTY, Insets.EMPTY)));
        Button boutonSignaler = new Button();
        boutonSignaler.setOnAction(new ControleurSignalerProfil(appli, userModele, userModele.getId(), idUtilisateur));
        boutonSignaler.setBackground(new Background(
                new BackgroundFill(Color.web("#EBACA2"), new CornerRadii(5), null)
        ));
        ImageView imageViewSignaler = new ImageView(new Image("file:./images/iconSignaler.png"));
        imageViewSignaler.setFitWidth(20);
        imageViewSignaler.setFitHeight(20);
        boutonSignaler.setGraphic(imageViewSignaler);
        hBoxSignaler.getChildren().addAll(titreDescription, boutonSignaler);

        
        // Insertions des Text dans les conteneurs
        infosPrincipales.getChildren().addAll(titreNom, nom, titrePrenom, prenom, titrePseudo, pseudo);
        infosImportantes.getChildren().addAll(titreAge,age, titreGenre, genre);
        infosDeContact.getChildren().addAll(hbEmail, hbNumero, hBoxSignaler, description);

        rectangle.getChildren().add(blocSeparateur);

        // Insertion des éléments dans la VBox principale
        center.getChildren().addAll(infosPrincipales,infosImportantes, rectangle, infosDeContact);

        // Insertion de la Vbox principale dans le BorderPane
        zonePrincipal.setLeft(center);

        //Centre du BorderPane
        VBox vbSeparateur = new VBox(20);
        Rectangle rectangle2 = new Rectangle(2,500);
        VBox.setMargin(vbSeparateur, new Insets(10,10,10,10));
        rectangle2.setFill(Color.rgb(190,211,195));
        vbSeparateur.setAlignment(Pos.CENTER);

        vbSeparateur.getChildren().addAll(rectangle2);
        zonePrincipal.setCenter(vbSeparateur);
        

        // Le Right
        VBox vbRight = new VBox();
        vbRight.setPadding(new Insets(5, 250, 5, 5));
        
        
        //Elements du conteneur "vbRight"
        VBox vbPP = new VBox();
        VBox vbTypes = new VBox();
        GridPane likes = new GridPane();

        //ELements du conteneur "PP"
        vbPP.setAlignment(Pos.TOP_CENTER);
        Text infoPP = new Text("Photo de profil");
        VBox.setMargin(infoPP, new Insets(0,0,25,0));
        infoPP.setFont(Font.font("arial", FontWeight.BOLD, 23));
        infoPP.setFill(Color.WHITE);
        ImageView pp = new ImageView(new Image("file:./images/photo_UT/image"+this.idPhUt+".png"));
        pp.setFitHeight(300);
        pp.setFitWidth(300);
        pp.setPreserveRatio(true);
        VBox.setMargin(pp, new Insets(0,0,50,0));
        vbPP.getChildren().addAll(infoPP, pp);

        //Elements du conteneur "types"
        Text infoType = new Text("Type de produit vendu");
        infoType.setFont(Font.font("arial", FontWeight.BOLD, 20));        
        infoType.setFill(Color.WHITE);
        VBox.setMargin(infoType, new Insets(0,0,25,0));
        HBox types = new HBox();
        types.setAlignment(Pos.BASELINE_LEFT);
        types.setPadding(new Insets(50,5,50,5));

        vbTypes.getChildren().addAll(infoType, types);

        //Elements du conteneur "likes"
        Button boutonLike = new Button();
        ImageView pLikes = new ImageView(new Image("file:./images/iconLike.png"));
        pLikes.setFitHeight(150);
        pLikes.setFitWidth(150);
        boutonLike.setGraphic(pLikes);
        boutonLike.setStyle("-fx-background-color: transparent;");
        boutonLike.setOnAction(new ControleurAjouterLikeUser(appli, idUtilisateur, userModele));
        Label nbLikes = new Label(String.valueOf(this.nbLike));
        likes.setAlignment(Pos.BASELINE_LEFT);
        likes.setPadding(new Insets(5,5,5,5));
        nbLikes.setTextFill(Color.rgb(255,255,255));
        nbLikes.setFont(Font.font("arial", FontPosture.REGULAR, 30));
        nbLikes.setPadding(new Insets(5,5,5,5));

        likes.add(boutonLike, 0, 0);
        likes.add(nbLikes, 1, 0);

        


        vbRight.getChildren().addAll(vbPP, infoType,types,likes);

        zonePrincipal.setRight(vbRight);
        bpPage.setCenter(zonePrincipal);
    }   
}