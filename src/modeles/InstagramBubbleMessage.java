package modeles;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class InstagramBubbleMessage extends Application {

    /**
     * Créer une bulle de message comme dans Instagram
     * 
     */
    @Override
    public void start(Stage primaryStage) {
        // Créer le contenu de la bulle de message
        Label messageLabel = new Label("Hello, World!");
        messageLabel.setStyle("-fx-text-fill: white;"); // Couleur du texte
        messageLabel.setPadding(new Insets(10)); // Marge interne du label

        // Créer l'indicateur de l'expéditeur
        Label senderLabel = new Label("Vous");
        senderLabel.setStyle("-fx-text-fill: gray;"); // Couleur de l'indicateur
        senderLabel.setPadding(new Insets(5)); // Marge interne de l'indicateur

        // Créer une HBox pour le contenu du message et l'indicateur de l'expéditeur
        HBox messageContent = new HBox(messageLabel, senderLabel);
        messageContent.setAlignment(Pos.CENTER_LEFT);
        HBox.setMargin(senderLabel, new Insets(0, 0, 0, 10)); // Marge entre le contenu du message et l'indicateur

        // Créer une VBox pour la bulle de message
        VBox bubbleBox = new VBox(messageContent);
        bubbleBox.setBackground(new Background(new BackgroundFill(Color.BLUE, new CornerRadii(10), null)));
        bubbleBox.setPadding(new Insets(10)); // Marge externe de la bulle de message
        bubbleBox.setAlignment(Pos.CENTER_LEFT); // Alignement du contenu à gauche

        // Créer la scène et afficher la fenêtre
        Scene scene = new Scene(bubbleBox, 300, 100);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Instagram Bubble Message");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
