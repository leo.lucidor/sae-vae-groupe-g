package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurRecapVente implements EventHandler<ActionEvent> {
    
    /**
     * L'application principale
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur
     * @param appli l'application principale
     */
    public ControleurRecapVente(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Gère l'action sur le bouton "Retour"
     * @param event l'événement
     */
    @Override
    public void handle(ActionEvent event) {
        try {
			this.appli.pageRecapVente();
		} catch (SQLException e) {
			e.printStackTrace();
        }
    }
}
