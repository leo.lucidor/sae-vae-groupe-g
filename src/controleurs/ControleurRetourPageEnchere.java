package controleurs;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import vues.ApplicationVAE;

public class ControleurRetourPageEnchere implements EventHandler<ActionEvent> {

    /**
     * L'application principale
     */
    private ApplicationVAE appli;

    /**
     * Constructeur de la classe
     * @param appli l'application principale
     */
    public ControleurRetourPageEnchere(ApplicationVAE appli) {
        this.appli = appli;
    }

    /**
     * Méthode qui permet de revenir à la page d'enchère
     * @param actionEvent l'action qui a déclenché l'événement
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        this.appli.pageEnchere();
    }
}