package modeles;
import java.sql.SQLException;
import java.util.List;

public class Vente {

    /**
     * la base de donnée
     */
    private BaseDeDonnee bd;
    
    /**
     * Constructor
     */
    public Vente(BaseDeDonnee bd) throws SQLException, ClassNotFoundException {
        this.bd = bd;
    }

    // public List<List<String>> getVente(String nomOb, String nomVille) throws SQLException{
    //     return this.bd.getVente(nomOb, nomVille);
    // }

    /**
     * Methode qui permet de récupérer toutes les ventes
     * @return List<Lisr<String>> une liste qui contient des listes qui contient les informations d'une ventes
     * @throws SQLException
     * 
     */
    public List<List<String>> getVente() throws SQLException{
        return this.bd.getVente();
    }


    /**
     * Methode qui permet de signaler une vente
     * 
     */
    public void signalerVente(String raisonReport, int idUtilisateur, int idVente) throws SQLException{
        this.bd.creerSignalementVente(raisonReport, idUtilisateur, idVente);
    }

    /**
     * Methode qui renvoit le prix max des ventes
     * @return int : le prix max d'une vente dans une liste de ventes 
     */
    public int getMaxPrixVente(List<List<String>> listeVente) throws SQLException{
        int max = 0;
        for (List<String> vente : listeVente) {
            if (Integer.parseInt(vente.get(3)) > max) {
                max = Integer.parseInt(vente.get(3));
            }
        }
        return max;
    }

    // public List<Image> getImageObjet(int idOb) throws SQLException{
    //     return this.bd.getImageObjet(idOb);
    // }
}