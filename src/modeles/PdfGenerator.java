package modeles;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class PdfGenerator {

    /**
    * Génère un certificat de vente au format PDF.
    *
    * @param filename        le nom du fichier PDF à générer
    * @param listeDeLaVente  la liste contenant les informations de la vente
    * @throws DocumentException en cas d'erreur lors de la création du document PDF
    * @throws IOException       en cas d'erreur d'entrée/sortie lors de la création du fichier PDF
    */
    public static void generateCertificat(String filename, List<String> listeDeLaVente) throws DocumentException, IOException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(filename));

        document.open();
        document.add(new Paragraph("Le certificat de la vente n°" + listeDeLaVente.get(0)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Le vendeur : " + listeDeLaVente.get(1)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("L'objet : " + listeDeLaVente.get(2)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Prix de base : " + listeDeLaVente.get(3)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Prix minimum : " + listeDeLaVente.get(4)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Description de l'objet : " + listeDeLaVente.get(5)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Ville de la vente : " + listeDeLaVente.get(6)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Début de la vente : " + listeDeLaVente.get(7)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Fin de la vente : " + listeDeLaVente.get(8)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Catégorie de l'objet : " + listeDeLaVente.get(9)));
        document.close();

        System.out.println("PDF généré : " + filename);
    }

    /**
    * Génère un récapitulatif de vente au format PDF.
    *
    * @param filename        le nom du fichier PDF à générer
    * @param listeDeLaVente  la liste contenant les informations de la vente
    * @throws DocumentException en cas d'erreur lors de la création du document PDF
    * @throws IOException       en cas d'erreur d'entrée/sortie lors de la création du fichier PDF
    */
    public static void generateRecapitulatif(String filename, List<String> listeDeLaVente) throws DocumentException, IOException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(filename));

        document.open();
        document.add(new Paragraph("Recapitulatif de la vente n°" + listeDeLaVente.get(0)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Le vendeur : " + listeDeLaVente.get(1)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("L'objet : " + listeDeLaVente.get(2)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Prix de base : " + listeDeLaVente.get(3)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Prix minimum : " + listeDeLaVente.get(4)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Description de l'objet : " + listeDeLaVente.get(5)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Ville de la vente : " + listeDeLaVente.get(6)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Début de la vente : " + listeDeLaVente.get(7)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Fin de la vente : " + listeDeLaVente.get(8)));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Catégorie de l'objet : " + listeDeLaVente.get(9)));
        document.close();

        System.out.println("PDF généré avec iText : " + filename);
    }
}
