package modeles;
import java.sql.SQLException;

/**
 * La classe ModeleEncherir représente un modèle pour les enchères.
 */
public class ModeleEncherir{

    /**
     * La base de données utilisée pour les enchères.
     */
    private BaseDeDonnee bd;
    /**
     * L'identifiant de l'utilisateur.
     */
    private int idUt;
    /**
     * L'identifiant de la vente aux enchères.
     */
    private int idVe;
    /**
     * Le montant de l'offre d'enchère.
     */
    private int montant;
    /**
     * La date et l'heure de l'enchère.
     */
    private String dateheure;

  /**
     * Constructeur de la classe ModeleEncherir.
     *
     * @param bd    La base de données utilisée pour les enchères.
     * @param idUt  L'identifiant de l'utilisateur.
     * @param idVe  L'identifiant de la vente aux enchères.
     * @throws SQLException           Si une erreur de base de données se produit.
     * @throws ClassNotFoundException Si la classe de la base de données n'est pas trouvée.
     */
    public ModeleEncherir(BaseDeDonnee bd, int idUt, int idVe) throws SQLException, ClassNotFoundException{
        this.bd = bd;
        this.idUt = idUt;
        this.idVe = idVe;
        //this.montant = Integer.parseInt(this.bd.getInfoOffreEnchere(this.idUt,this.idVe).get(3));

    }
    /**
     * Obtient le montant de l'offre d'enchère.
     *
     * @return Le montant de l'offre d'enchère.
     */
    public int getMontant(){
        return this.montant;
    }

    /**
     * Définit la date et l'heure de l'enchère.
     *
     * @param dateheure La date et l'heure de l'enchère.
     */
    public void setDateHeure(String dateheure){
        this.dateheure = dateheure;
    }

    /**
     * Obtient le pseudo de l'utilisateur.
     *
     * @return Le pseudo de l'utilisateur.
     * @throws SQLException Si une erreur de base de données se produit.
     */
    public String getPseudo() throws SQLException{
        return this.bd.getInfoUserById(this.idUt).get(1);
    }

    /**
     * Obtient le montant actuel de l'enchère.
     *
     * @return Le montant actuel de l'enchère.
     * @throws SQLException Si une erreur de base de données se produit.
     */
    public int getMontantActuel() throws SQLException{
        return this.bd.getMontantActuelEnchere(this.idVe);
    }

    /**
     * Obtient l'identifiant de l'utilisateur.
     *
     * @return L'identifiant de l'utilisateur.
     */
    public int getIdUt(){
        return this.idUt;
    }

    /**
     * Obtient l'identifiant de la vente aux enchères.
     *
     * @return L'identifiant de la vente aux enchères.
     */
    public int getIdVe(){
        return this.idVe;
    }

    /**
     * Obtient la date et l'heure de l'enchère.
     *
     * @return La date et l'heure de l'enchère.
     */
    public String getDateHeure(){
        return this.dateheure;
    }

    /**
     * Crée une nouvelle offre d'enchère.
     *
     * @param idUt      L'identifiant de l'utilisateur.
     * @param idVe      L'identifiant de la vente aux enchères.
     * @param dateheure La date et l'heure de l'enchère.
     * @param montant   Le montant de l'offre d'enchère.
     * @throws SQLException Si une erreur de base de données se produit.
     */
    public void newOffreEnchere(int idUt, int idVe, String dateheure, int montant) throws SQLException{
        this.bd.insertOffreEncherir(idUt,idVe,dateheure,montant);
    }
    
}