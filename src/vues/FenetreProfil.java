package vues;
import java.sql.SQLException;
import java.util.List;

import controleurs.ControleurModifierProfil;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import modeles.Utilisateur;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;


public class FenetreProfil extends StackPane {

    public FenetreProfil(BorderPane bpPage, BorderPane rootTop, BorderPane rootLeftAdmin, Utilisateur userModele, ApplicationVAE appli, BorderPane borderMenuOption, Label textMenu, Label textPageAccueil, Label textVendreUnProduit, Label textVenteAuxEncheres, Label textMessagerie, Label textMonPanier, Label textInformations, Label textSetting, Label textQuitter, Button quitter, Button deconnexion, Button menu,  Button acceuil, Button message, Button info, Button setting, Button profil, Button enchere, Button panier, Button ajouterEnchere) throws SQLException{
      //  BorderPane bpPage = new BorderPane();
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        Label text = new Label("profil");
        text.setStyle("-fx-text-fill: #00aef0;");
        bpPage.setCenter(text);
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        BorderPane rootLeft = new BorderPane();
        if(userModele.getRole() == 2){
            rootLeft = new MenuLeft(acceuil, quitter, message, info, setting, enchere, panier, ajouterEnchere, bpPage);
        }
        else {
            rootLeft = rootLeftAdmin;
        }
        hBoxLeft.getChildren().addAll(rootLeft, rootLeftOption);
        bpPage.setLeft(hBoxLeft);
        //BorderPane rootTop = new MenuTop(userModele, textMenu, deconnexion, profil, menu, bpPage);
        bpPage.setTop(rootTop);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        StackPane.setMargin(rootLeftOption, new Insets(48,0,0,56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);
        
        // BorderPane principal du profil
        BorderPane zonePrincipal = new BorderPane();

        //Le top
        VBox vbTop = new VBox();
        
        vbTop.setAlignment(Pos.TOP_CENTER);
        Text titrePage = new Text("Profil");
        titrePage.setTextAlignment(TextAlignment.CENTER);
        titrePage.setFill(Color.WHITE);
        titrePage.setFont(Font.font("arial", FontWeight.BOLD, FontPosture.REGULAR, 50));
        VBox.setMargin(titrePage, new Insets(25,0,25,0));

        vbTop.getChildren().addAll(titrePage);
        zonePrincipal.setTop(vbTop);


        // Le center
        VBox center = new VBox();
        // Création des conteneurs principaux
        HBox infosPrincipales = new HBox();
        HBox.setMargin(infosPrincipales, new Insets(15));
        VBox.setMargin(infosPrincipales, (new Insets(5,5,100,5)));
        HBox infosImportantes = new HBox();
        HBox.setMargin(infosImportantes, new Insets(15));
        infosImportantes.setPadding(new Insets(5,5,50,5));
        VBox rectangle = new VBox(20);
        Rectangle blocSeparateur = new Rectangle(500,2);
        VBox.setMargin(blocSeparateur, new Insets(10,10,10,150));
        blocSeparateur.setFill(Color.rgb(190,211,195));
        VBox infosDeContact = new VBox();
        VBox.setMargin(infosDeContact, new Insets(50,10,10,15));

        //Création des Text du TilePane
        //Nom de l'utilisateur
        Text titreNom = new Text("Nom :");
        titreNom.setFill(Color.WHITE);
        titreNom.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Text nom = new Text(userModele.getNom());
        nom.setFill(Color.WHITE);
        nom.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        HBox.setMargin(titreNom, new Insets(0,25,0,15));
        HBox.setMargin(nom, new Insets(0,75,0,0));

        //Prénom de l'utilisateur
        Text titrePrenom = new Text("Prénom :");
        titrePrenom.setFill(Color.WHITE);
        titrePrenom.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Text prenom = new Text(userModele.getPrenom());
        prenom.setFill(Color.WHITE);
        prenom.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        HBox.setMargin(titrePrenom, new Insets(0,25,0,0));
        HBox.setMargin(prenom, new Insets(0,75,0,0));

        //Pseudo de l'utilisateur
        Text titrePseudo = new Text("Pseudo :");
        titrePseudo.setFill(Color.WHITE);
        titrePseudo.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Text pseudo = new Text(userModele.getPseudo());
        pseudo.setFill(Color.WHITE);
        pseudo.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        HBox.setMargin(titrePseudo, new Insets(0,25,0,0));
        HBox.setMargin(pseudo, new Insets(0,75,0,0));

        //age de l'utilisateur
        Text titreAge = new Text("Age :");
        titreAge.setFill(Color.WHITE);
        titreAge.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Text age = new Text(String.valueOf(userModele.getAge()));
        age.setFill(Color.WHITE);
        age.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        HBox.setMargin(titreAge, new Insets(0,25,0,15));
        HBox.setMargin(age, new Insets(0,75,0,0));

        //Sexe de l'utilisateur
        Text titreGenre = new Text("Genre :");
        titreGenre.setFill(Color.WHITE);
        titreGenre.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Text genre = new Text(userModele.getGenre());
        genre.setFill(Color.WHITE);
        genre.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        HBox.setMargin(titreGenre, new Insets(0,25,0,0));

        
        // HBox pour l'email de l'utilisateur
        HBox hbEmail = new HBox();
        hbEmail.setPadding(new Insets(0,0,75,0));

        Text titreEmail = new Text("Adresse Mail :");
        titreEmail.setFill(Color.WHITE);
        titreEmail.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Text email = new Text(userModele.getMail());
        email.setFill(Color.WHITE);
        email.setFont(Font.font("arial", FontPosture.REGULAR, 20));
        HBox.setMargin(titreEmail, new Insets(0,25,0,0));

        hbEmail.getChildren().addAll(titreEmail, email);

        // HBox pour le numéro de l'utilisateur
        HBox hbNumero = new HBox();
        hbNumero.setPadding(new Insets(0,0,75,0));

        Text titreNumero = new Text("N° Téléphone :");
        titreNumero.setFill(Color.WHITE);
        titreNumero.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Text numero = new Text(userModele.getTel());
        numero.setFill(Color.WHITE);
        numero.setFont(Font.font("arial", FontPosture.REGULAR, 20));

        hbNumero.getChildren().addAll(titreNumero, numero);

        TabPane tabPaneActivitees = new TabPane();
        tabPaneActivitees.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        tabPaneActivitees.setPrefSize(1000, 500);
        tabPaneActivitees.setBackground(new Background(new BackgroundFill(Color.web("#BED3C3"), CornerRadii.EMPTY, Insets.EMPTY)));

        Tab historiqueDesEncheresNonRemportées = new Tab("Historique de mes enchères non remportées");
        Tab historiqueDesVentes = new Tab("Historique de mes ventes");
        Tab historiqueDesAchats = new Tab("Historique de mes achats");

        ScrollPane scrollPaneDesEncheresNonRemportees = new ScrollPane();
        ScrollPane scrollPaneDesVentes = new ScrollPane();
        ScrollPane scrollPaneDesAchats = new ScrollPane();
        scrollPaneDesEncheresNonRemportees.setPrefSize(1000, 500);
        scrollPaneDesVentes.setPrefSize(1000, 500);
        scrollPaneDesAchats.setPrefSize(1000, 500);


        VBox vbEncheresNonRemportees = new VBox();
        VBox vbVentes = new VBox();
        VBox vbAchats = new VBox();

        List<List<String>> listeEncheresNonRemportees = userModele.getHistoriqueDesEncheresNonRemportees(String.valueOf(userModele.getId()));
        for (List<String> enchere1 : listeEncheresNonRemportees) {
            HBox hbEnchere = new HBox();
            VBox vbEnchereLeft = new VBox();
            VBox vbEnchereRight = new VBox();
            Label nomObjet = new Label("Objet :" + enchere1.get(2));
            Label vendeur = new Label("Vendeur : " + enchere1.get(1));
            Label montantFinal = new Label("Montant final :");
            Label montant = new Label(enchere1.get(3) + " €");
            vbEnchereLeft.getChildren().addAll(nomObjet, vendeur);
            vbEnchereRight.getChildren().addAll(montantFinal, montant);
            hbEnchere.getChildren().addAll(vbEnchereLeft, vbEnchereRight);
            hbEnchere.setPrefHeight(100);
            hbEnchere.setPrefWidth(1000);
            vbEnchereLeft.setPadding(new Insets(0,0,10,0));
            vbEnchereRight.setPadding(new Insets(0,0,10,0));
            vbEnchereLeft.setSpacing(10);
            vbEnchereRight.setSpacing(10);
            vbEnchereLeft.setPrefWidth(450);
            vbEnchereRight.setPrefWidth(450);
            vbEnchereLeft.setBackground(new Background(new BackgroundFill(Color.web("#4A919E"), CornerRadii.EMPTY, Insets.EMPTY)));
            vbEnchereRight.setBackground(new Background(new BackgroundFill(Color.web("#4A919E"), CornerRadii.EMPTY, Insets.EMPTY)));
            nomObjet.setFont(new Font("Arial", 20));
            vendeur.setFont(new Font("Arial", 20));
            montantFinal.setFont(new Font("Arial", 20));
            montant.setFont(new Font("Arial", 20));
            hbEnchere.setPadding(new Insets(0,0,10,0));
            vbEnchereLeft.setAlignment(Pos.CENTER);
            vbEnchereRight.setAlignment(Pos.CENTER);
            hbEnchere.setAlignment(Pos.CENTER);
            vbEncheresNonRemportees.getChildren().add(hbEnchere);
        }

        List<List<String>> listeVentes = userModele.getInfoVenteUtilisateur(userModele.getId());
        for (List<String> vente : listeVentes) {
            HBox hbVente = new HBox();
            VBox vbVenteLeft = new VBox();
            VBox vbVenteRight = new VBox();
            Label nomObjet = new Label("Objet :" + vente.get(1));
            Label id = new Label("identifiant : " + vente.get(0));
            Label statut = new Label("Statut :");
            Label st = new Label();
            switch (vente.get(2)) {
                case "1":
                    st.setText("A venir");
                    break;
                case "2":
                    st.setText("En cours");
                    break;
                case "3":
                    st.setText("A valider");
                    break;
                case "4":
                    st.setText("Validée");
                    break;
                case "5":
                    st.setText("Non conclue");
                    break;
                default:
                    break;
            }
            vbVenteLeft.getChildren().addAll(nomObjet, id);
            vbVenteRight.getChildren().addAll(statut, st);
            hbVente.getChildren().addAll(vbVenteLeft, vbVenteRight);
            hbVente.setPrefHeight(100);
            hbVente.setPrefWidth(1000);
            vbVenteLeft.setPadding(new Insets(0,0,10,0));
            vbVenteRight.setPadding(new Insets(0,0,10,0));
            vbVenteLeft.setSpacing(10);
            vbVenteRight.setSpacing(10);
            vbVenteLeft.setPrefWidth(450);
            vbVenteRight.setPrefWidth(450);
            vbVenteLeft.setBackground(new Background(new BackgroundFill(Color.web("#4A919E"), CornerRadii.EMPTY, Insets.EMPTY)));
            vbVenteRight.setBackground(new Background(new BackgroundFill(Color.web("#4A919E"), CornerRadii.EMPTY, Insets.EMPTY)));
            nomObjet.setFont(new Font("Arial", 20));
            id.setFont(new Font("Arial", 20));
            statut.setFont(new Font("Arial", 20));
            st.setFont(new Font("Arial", 20));
            hbVente.setPadding(new Insets(0,0,10,0));
            vbVenteLeft.setAlignment(Pos.CENTER);
            vbVenteRight.setAlignment(Pos.CENTER);
            hbVente.setAlignment(Pos.CENTER);
            vbVentes.getChildren().add(hbVente);
        }

        List<List<String>> listeAchats = userModele.getInfoVenteUtilisateurGagnee(String.valueOf(userModele.getId()));
        for (List<String> achat : listeAchats) {
            HBox hbAchat = new HBox();
            VBox vbEnchereLeft = new VBox();
            VBox vbEnchereRight = new VBox();
            Label nomObjet = new Label("Objet :" + achat.get(2));
            Label vendeur = new Label("Vendeur : " + achat.get(1));
            Label montantFinal = new Label("Montant final :");
            Label montant = new Label(achat.get(3) + " €");
            vbEnchereLeft.getChildren().addAll(nomObjet, vendeur);
            vbEnchereRight.getChildren().addAll(montantFinal, montant);
            hbAchat.getChildren().addAll(vbEnchereLeft, vbEnchereRight);
            hbAchat.setPrefHeight(100);
            hbAchat.setPrefWidth(1000);
            vbEnchereLeft.setPadding(new Insets(0,0,10,0));
            vbEnchereRight.setPadding(new Insets(0,0,10,0));
            vbEnchereLeft.setSpacing(10);
            vbEnchereRight.setSpacing(10);
            vbEnchereLeft.setPrefWidth(450);
            vbEnchereRight.setPrefWidth(450);
            vbEnchereLeft.setBackground(new Background(new BackgroundFill(Color.web("#4A919E"), CornerRadii.EMPTY, Insets.EMPTY)));
            vbEnchereRight.setBackground(new Background(new BackgroundFill(Color.web("#4A919E"), CornerRadii.EMPTY, Insets.EMPTY)));
            nomObjet.setFont(new Font("Arial", 20));
            vendeur.setFont(new Font("Arial", 20));
            montantFinal.setFont(new Font("Arial", 20));
            montant.setFont(new Font("Arial", 20));
            hbAchat.setPadding(new Insets(0,0,10,0));
            vbEnchereLeft.setAlignment(Pos.CENTER);
            vbEnchereRight.setAlignment(Pos.CENTER);
            hbAchat.setAlignment(Pos.CENTER);
            vbAchats.getChildren().add(hbAchat);
        }



        scrollPaneDesEncheresNonRemportees.setContent(vbEncheresNonRemportees);
        scrollPaneDesVentes.setContent(vbVentes);
        scrollPaneDesAchats.setContent(vbAchats);
        historiqueDesEncheresNonRemportées.setContent(scrollPaneDesEncheresNonRemportees);
        historiqueDesVentes.setContent(scrollPaneDesVentes);
        historiqueDesAchats.setContent(scrollPaneDesAchats);



        tabPaneActivitees.getTabs().addAll(historiqueDesEncheresNonRemportées, historiqueDesVentes, historiqueDesAchats);


        
        // Insertions des Text dans les conteneurs
        infosPrincipales.getChildren().addAll(titreNom, nom, titrePrenom, prenom, titrePseudo, pseudo);
        infosImportantes.getChildren().addAll(titreAge,age, titreGenre, genre);
        infosDeContact.getChildren().addAll(hbEmail, hbNumero, tabPaneActivitees);

        rectangle.getChildren().add(blocSeparateur);

        // Insertion des éléments dans la VBox principale
        center.getChildren().addAll(infosPrincipales,infosImportantes, rectangle, infosDeContact);

        // Insertion de la Vbox principale dans le BorderPane
        zonePrincipal.setLeft(center);

        //Centre du BorderPane
        VBox vbSeparateur = new VBox(20);
        Rectangle rectangle2 = new Rectangle(2,500);
        VBox.setMargin(vbSeparateur, new Insets(10,10,10,10));
        rectangle2.setFill(Color.rgb(190,211,195));
        vbSeparateur.setAlignment(Pos.CENTER);

        vbSeparateur.getChildren().addAll(rectangle2);
        zonePrincipal.setCenter(vbSeparateur);
        

        // Le Right
        VBox vbRight = new VBox();
        vbRight.setPadding(new Insets(5, 250, 5, 5));
        
        
        //Elements du conteneur "vbRight"
        VBox vbPP = new VBox();
        VBox vbTypes = new VBox();
        GridPane likes = new GridPane();

        //ELements du conteneur "PP"
        vbPP.setAlignment(Pos.TOP_CENTER);
        Text infoPP = new Text("Photo de profil");
        VBox.setMargin(infoPP, new Insets(0,0,25,0));
        infoPP.setFont(Font.font("arial", FontWeight.BOLD, 23));
        infoPP.setFill(Color.WHITE);
        ImageView pp = new ImageView(userModele.getImageUser());
        pp.setFitHeight(300);
        pp.setFitWidth(300);
        pp.setPreserveRatio(true);
        VBox.setMargin(pp, new Insets(0,0,50,0));
        vbPP.getChildren().addAll(infoPP, pp);

        //Elements du conteneur "types"
        Text infoType = new Text("Type de produit vendu");
        infoType.setFont(Font.font("arial", FontWeight.BOLD, 20));        
        infoType.setFill(Color.WHITE);
        VBox.setMargin(infoType, new Insets(0,0,25,0));
        HBox types = new HBox();
        types.setAlignment(Pos.BASELINE_LEFT);
        types.setPadding(new Insets(50,5,50,5));

        vbTypes.getChildren().addAll(infoType, types);

        //Elements du conteneur "likes"
        ImageView pLikes = new ImageView(new Image("file:./images/iconLike.png"));
        pLikes.setFitHeight(150);
        pLikes.setFitWidth(150);
        Label nbLikes = new Label(String.valueOf(userModele.getLike()));
        likes.setAlignment(Pos.BASELINE_LEFT);
        likes.setPadding(new Insets(5,5,5,5));
        nbLikes.setTextFill(Color.rgb(255,255,255));
        nbLikes.setFont(Font.font("arial", FontPosture.REGULAR, 30));
        nbLikes.setPadding(new Insets(5,5,5,5));

        likes.add(pLikes, 0, 0);
        likes.add(nbLikes, 1, 0);


        vbRight.getChildren().addAll(vbPP, infoType,types,likes);

        //Création du bouton 
        VBox bot = new VBox();
        Button modifierBouton = new Button("Modifier");
        bot.setAlignment(Pos.BOTTOM_RIGHT);
        modifierBouton.setOnAction(new ControleurModifierProfil(appli));
        modifierBouton.setStyle("-fx-background-color: #4A919E; -fx-text-fill: black;");
        modifierBouton.setMinWidth(200);
        modifierBouton.setMinHeight(25);
        VBox.setMargin(modifierBouton, new Insets(25,25,25,25));
        bot.getChildren().addAll(modifierBouton);

        



        zonePrincipal.setRight(vbRight);
        zonePrincipal.setBottom(bot);
        bpPage.setCenter(zonePrincipal);
    }   
}