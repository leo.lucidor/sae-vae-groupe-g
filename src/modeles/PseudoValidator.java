package modeles;
public class PseudoValidator {
    
    /**
     * Vérifie si le pseudo est valide
     * @param pseudo
     * @return true si le pseudo est valide, false sinon
     */
    public static boolean isValidPseudo(String pseudo) {
        return pseudo.length() >= 3;
    }
}
