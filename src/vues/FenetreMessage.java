package vues;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import controleurs.ControleurCreerBulleMessage;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import modeles.ComparatorMessageDate;
import modeles.Message;
import modeles.Utilisateur;

/**
 * Classe représentant la fenêtre de messagerie.
 * Cette classe hérite de la classe StackPane.
 */
public class FenetreMessage extends StackPane {

    private ApplicationVAE appli;
    private VBox vBoxLeft;
    private VBox vBoxLeftContainer;
    private ObservableList<Button> itemsConversation = FXCollections.observableArrayList();
    private List<List<String>> messageRecu;
    private List<List<String>> messageEnvoyer;
    private List<List<String>> listeMessageTrier;
    private Set<Integer> ensIdUtilisateurCommunication;
    private Message messageModele;
    private Utilisateur userModele;
    private int idUtDestinataire;
    private VBox vBoxMessagesEnvoyer;
    private VBox vBoxMessagesRecu;
    private VBox vBoxChercher;
    private ScrollPane scrollPaneMessage;
    private BorderPane bpPageMessage;
    private Label titreDestinataireEnvoyeur;
    private TextField textFieldChercher;
    private Button boutonChercher;
    private HBox hBoxChercher;
    private Timeline timeline;


    /**
     * Constructeur de la classe FenetreMessage.
     *
     * @param appli          L'instance de la classe ApplicationVAE.
     * @param messageModele  L'instance de la classe Message.
     * @param bpPage         L'instance de la classe BorderPane pour la page actuelle.
     * @param rootTop        L'instance de la classe BorderPane pour la partie supérieure de l'interface.
     * @param rootleftAdmin  L'instance de la classe BorderPane pour le menu latéral (pour l'administrateur).
     * @param userModele     L'instance de la classe Utilisateur représentant l'utilisateur actuel.
     * @param borderMenuOption  L'instance de la classe BorderPane pour les options du menu latéral.
     * @param textMenu       L'instance de la classe Label pour le texte du menu.
     * @param textPageAccueil  L'instance de la classe Label pour le texte de la page d'accueil.
     * @param textVendreUnProduit  L'instance de la classe Label pour le texte de la page "Vendre un produit".
     * @param textVenteAuxEncheres  L'instance de la classe Label pour le texte de la page "Vente aux enchères".
     * @param textMessagerie  L'instance de la classe Label pour le texte de la page "Messagerie".
     * @param textMonPanier  L'instance de la classe Label pour le texte de la page "Mon panier".
     * @param textInformations  L'instance de la classe Label pour le texte de la page "Informations".
     * @param textSetting    L'instance de la classe Label pour le texte de la page "Paramètres".
     * @param textQuitter    L'instance de la classe Label pour le texte de la page "Quitter".
     * @param quitter        L'instance du bouton "Quitter".
     * @param deconnexion    L'instance du bouton "Déconnexion".
     * @param menu           L'instance du bouton "Menu".
     * @param acceuil        L'instance du bouton "Accueil".
     * @param message        L'instance du bouton "Messagerie".
     * @param info           L'instance du bouton "Informations".
     * @param setting        L'instance du bouton "Paramètres".
     * @param profil         L'instance du bouton "Profil".
     * @param enchere        L'instance du bouton "Enchère".
     * @param panier         L'instance du bouton "Panier".
     * @param ajouterEnchere L'instance du bouton "Ajouter une enchère".
     *
     * @throws ClassNotFoundException  Si une classe n'est pas trouvée lors de l'exécution.
     * @throws SQLException            Si une erreur SQL se produit lors de l'exécution de la requête.
     */
    public FenetreMessage(ApplicationVAE appli, Message messageModele, BorderPane bpPage, BorderPane rootTop, BorderPane rootleftAdmin, Utilisateur userModele, BorderPane borderMenuOption, Label textMenu, Label textPageAccueil, Label textVendreUnProduit, Label textVenteAuxEncheres, Label textMessagerie, Label textMonPanier, Label textInformations, Label textSetting, Label textQuitter, Button quitter, Button deconnexion, Button menu,  Button acceuil, Button message, Button info, Button setting, Button profil, Button enchere, Button panier, Button ajouterEnchere) throws ClassNotFoundException, SQLException{
        
        this.bpPageMessage = new BorderPane();
        this.vBoxLeft = new VBox();
        this.vBoxLeftContainer = new VBox();
        this.vBoxLeftContainer.getChildren().add(this.vBoxLeft);
        this.textFieldChercher = new TextField();
        this.boutonChercher = new Button("Premier message");
        this.hBoxChercher = new HBox();
        this.scrollPaneMessage = new ScrollPane();
        this.scrollPaneMessage.setFitToHeight(true);
        this.scrollPaneMessage.setFitToWidth(true);
        this.vBoxMessagesEnvoyer = new VBox();
        this.vBoxMessagesRecu = new VBox();
        this.vBoxChercher = new VBox();
        this.vBoxMessagesEnvoyer.setAlignment(Pos.TOP_RIGHT);
        this.vBoxMessagesRecu.setAlignment(Pos.TOP_LEFT);
        this.scrollPaneMessage.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        this.scrollPaneMessage.setContent(this.bpPageMessage);
        this.bpPageMessage.setLeft(this.vBoxMessagesRecu);
        this.bpPageMessage.setRight(this.vBoxMessagesEnvoyer);
        this.appli = appli;
        this.ensIdUtilisateurCommunication = new HashSet<>();
        this.messageRecu = new ArrayList<>();
        this.messageEnvoyer = new ArrayList<>();
        this.listeMessageTrier = new ArrayList<>();
        this.messageModele = messageModele;
        this.userModele = userModele;
        this.boutonChercher.setOnAction(new ControleurCreerBulleMessage(this.appli, messageModele, this, this.userModele));
        this.itemsConversation = FXCollections.observableArrayList();
        this.titreDestinataireEnvoyeur = new Label("Destinataire : ?       Envoyeur : "+this.userModele.getPseudo());
        this.titreDestinataireEnvoyeur.setStyle("-fx-font-size: 18px; -fx-font-weight: bold;");
        this.idUtDestinataire = 0;


        
        BorderPane bpPageMessage = new BorderPane();
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        Label text = new Label("message");
        text.setStyle("-fx-text-fill: #00aef0;");
        bpPage.setCenter(text);
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        BorderPane rootLeft = new BorderPane();
        if(userModele.getRole() == 2){
            rootLeft = new MenuLeft(acceuil, quitter, message, info, setting, enchere, panier, ajouterEnchere, bpPage);
        }
        else {
            rootLeft = rootleftAdmin;
        }
        hBoxLeft.getChildren().addAll(rootLeft, rootLeftOption);
        bpPage.setLeft(hBoxLeft);
        // BorderPane rootTop = new MenuTop(userModele, textMenu, deconnexion, profil, menu, bpPage);
        bpPage.setTop(rootTop);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        StackPane.setMargin(rootLeftOption, new Insets(48,0,0,56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);

        // Création d'une timeline pour appeler la méthode majMessages() toutes les 10 secondes
        timeline = new Timeline(
                new KeyFrame(Duration.seconds(3), event -> {
                    // Appeler la méthode à chaque intervalle de 10 secondes
                    try {
                        if(this.idUtDestinataire != 0){
                            this.afficherMessages();
                        }
                    } catch (ClassNotFoundException | SQLException e) {
                        e.printStackTrace();
                    }
                })
        );
        timeline.setCycleCount(Timeline.INDEFINITE);
        // Démarrer la timeline
        timeline.play();


        // Création des composants de la page de messagerie
        Label titleLabel = new Label("Messagerie");
        titleLabel.setStyle("-fx-font-size: 24px; -fx-font-weight: bold; -fx-text-fill: #FFFFFF;");
        titleLabel.setPadding(new Insets(10, 0, 0, 90));
        
        ListView<Button> conversationsListView = new ListView<>(this.itemsConversation);
        conversationsListView.setStyle("-fx-background-color: #BED3C3;");
        VBox contentBox = new VBox();
        
        
         
         TextArea messageTextArea = new TextArea();
         messageTextArea.setPrefRowCount(2);
         messageTextArea.setWrapText(true);
         messageTextArea.setPromptText("Saisissez votre message...");
         Button sendButton = new Button("Envoyer");
         sendButton.setStyle("-fx-background-color: #EBACA2;");
         
         // Ajout d'une action lors du clic sur le bouton "Envoyer"
         sendButton.setOnAction(event -> {
            try {
                this.sendMessage(messageTextArea);
                this.afficherMessages();
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        });

 
         // Ajout des composants à la boîte de contenu
         contentBox.getChildren().addAll(this.titreDestinataireEnvoyeur, this.scrollPaneMessage, messageTextArea, sendButton);
         contentBox.setSpacing(10);
 
         // Configuration de l'alignement des composants
         titleLabel.setAlignment(Pos.CENTER);
         contentBox.setAlignment(Pos.CENTER);
 
         // Configuration des marges intérieures
         contentBox.setPadding(new Insets(10));
 
         // Configuration de la couleur de fond
         contentBox.setBackground(new Background(new BackgroundFill(Color.web("#BED3C3"), new CornerRadii(0), Insets.EMPTY)));
 
         // Configuration de la taille préférée
         vBoxLeft.setPrefWidth(300);
         this.textFieldChercher.setPrefWidth(180);
         this.textFieldChercher.setPromptText("Chercher un utilisateur...");
         this.boutonChercher.setPrefWidth(120);
         this.boutonChercher.setStyle("-fx-background-color: #EBACA2;");
         this.hBoxChercher.setSpacing(10);
         this.hBoxChercher.setAlignment(Pos.CENTER);
         this.hBoxChercher.setPadding(new Insets(0,0,10,0));
         this.vBoxLeftContainer.setPrefWidth(300);
        this.vBoxLeftContainer.setPrefHeight(900);
         this.vBoxLeftContainer.setStyle("-fx-background-color: #BED3C3;");
        this.hBoxChercher.getChildren().addAll(this.textFieldChercher, this.boutonChercher);
         this.vBoxChercher.getChildren().addAll(this.hBoxChercher, vBoxLeftContainer);
 
         // Configuration de la croissance verticale de la liste des messages
         VBox.setVgrow(this.scrollPaneMessage, javafx.scene.layout.Priority.ALWAYS);
 
         // Ajout des composants à la disposition de la page
        bpPageMessage.setTop(titleLabel);
        bpPageMessage.setLeft(vBoxChercher);
        bpPageMessage.setCenter(contentBox);
        bpPage.setCenter(bpPageMessage);
 
         // Configuration des marges
         BorderPane.setMargin(titleLabel, new Insets(10));
         BorderPane.setMargin(vBoxLeft, new Insets(10));
         BorderPane.setMargin(vBoxChercher, new Insets(10));
         BorderPane.setMargin(contentBox, new Insets(10));
 
         // Ajout des composants à la fenêtre principale
         StackPane.setMargin(borderMenuOption, new Insets(48, 0, 0, 56));
         StackPane.setAlignment(borderMenuOption, Pos.TOP_LEFT);

         // lancer premiere fois
         this.majMessages();
    }
 
    private void sendMessage(TextArea messageTextArea) throws ClassNotFoundException, SQLException {
        String message = messageTextArea.getText();
        if(!message.isEmpty()) {
            if(this.messageModele.envoyerMessage(this.userModele.getId(), this.idUtDestinataire, message)){
                System.out.println("Message envoyé");
            }
            messageTextArea.clear();
        }
    }


    private void majMessages() throws ClassNotFoundException, SQLException{

        // clear les listes
        this.messageRecu.clear();
        this.messageEnvoyer.clear();
        this.listeMessageTrier.clear();
        this.ensIdUtilisateurCommunication.clear();
       // this.itemsConversation.clear();
        this.vBoxLeft.getChildren().clear();

        // recupere les messages
        this.messageEnvoyer = this.messageModele.getMessageExpediteur(this.userModele.getId());
        this.messageRecu = this.messageModele.getMessageDestinataire(this.userModele.getId());
        for(List<String> lesMessage : this.messageRecu){
            this.ensIdUtilisateurCommunication.add(Integer.parseInt(lesMessage.get(2)));
        }
        for(List<String> lesMessage : this.messageEnvoyer){
            this.ensIdUtilisateurCommunication.add(Integer.parseInt(lesMessage.get(3)));
        }
        this.listeMessageTrier.addAll(this.messageRecu);
        this.listeMessageTrier.addAll(this.messageEnvoyer);
        Collections.sort(this.listeMessageTrier, new ComparatorMessageDate());

        for(Integer idUt : this.ensIdUtilisateurCommunication){
            List<String> infosUser = userModele.getInfoUtilisateur(idUt);
            String pseudo = infosUser.get(1);
            int idPhUt = Integer.parseInt(infosUser.get(11));
            ImageView image = new ImageView("file:./images/photo_UT/image"+idPhUt+".png");
            image.setFitHeight(50);
            image.setFitWidth(50);
            Button conversationButton = new Button(pseudo);
            conversationButton.setGraphic(image);
            conversationButton.setStyle("-fx-background-color: #BED3C3;");
            conversationButton.setOnAction(new ControleurCreerBulleMessage(this.appli, messageModele, this, this.userModele));
            //this.itemsConversation.add(conversationButton);
            this.vBoxLeft.getChildren().add(conversationButton);
        }
    }

    public void afficherMessages() throws ClassNotFoundException, SQLException {
        
        // maj label
        this.titreDestinataireEnvoyeur.setText("Destinataire : "+this.userModele.getInfoUtilisateur(idUtDestinataire).get(1)+"       Envoyeur : "+this.userModele.getPseudo());
        
        // clear
        this.vBoxMessagesRecu.getChildren().clear();
        this.vBoxMessagesEnvoyer.getChildren().clear();
        this.majMessages();

        for(List<String> lesMessages : this.listeMessageTrier){
            if(Integer.parseInt(lesMessages.get(2)) == this.idUtDestinataire && Integer.parseInt(lesMessages.get(3)) == this.userModele.getId()){
                Label label = new Label(" \n"+"  "+lesMessages.get(1)+"  \n"+" ");
                label.setBackground(new Background(new BackgroundFill(Color.web("#f5150b"), new CornerRadii(10), null)));
                this.vBoxMessagesRecu.getChildren().add(label);
                Label labelVide = new Label("");    
                Label labelVide2 = new Label(" \n ");
                Label labelVide3 = new Label("");
                this.vBoxMessagesEnvoyer.getChildren().addAll(labelVide, labelVide2);
                this.vBoxMessagesRecu.getChildren().add(labelVide3);
            }
            else if(Integer.parseInt(lesMessages.get(3)) == this.idUtDestinataire && Integer.parseInt(lesMessages.get(2)) == this.userModele.getId()){
                Label label = new Label(" \n"+"  "+lesMessages.get(1)+"  \n"+" ");
                label.setBackground(new Background(new BackgroundFill(Color.web("#0b75f5"), new CornerRadii(10), null)));
                this.vBoxMessagesEnvoyer.getChildren().add(label);
                Label labelVide = new Label("");
                Label labelVide2 = new Label(" \n ");
                Label labelVide3 = new Label("");
                this.vBoxMessagesRecu.getChildren().addAll(labelVide, labelVide2);
                this.vBoxMessagesEnvoyer.getChildren().add(labelVide3);
            }
        }
    }

    public void setIdUtDestinataire(int idUtDestinataire){
        this.idUtDestinataire = idUtDestinataire;
    }
    
    public String getTextFieldChercher(){
        String text = this.textFieldChercher.getText();
        this.textFieldChercher.clear();
        return text;
    }
}
