package vues;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import modeles.Admin;
import java.sql.SQLException;
import java.util.List;
import controleurs.ControleurGenererCertificat;

/**
 * Classe représentant une fenêtre graphique affichant les certificats de vente.
 * Cette classe étend javafx.scene.layout.StackPane.
 * Elle contient plusieurs éléments graphiques tels que des étiquettes, des boîtes de texte,
 * des conteneurs et des boutons.
 */
public class FenetreCertificatVente extends StackPane {

    /**
     * Constantes de classe
     */
    private static final int WIDTH_VENTE = 600;
    private static final int HEIGHT_VENTE = 200;

    /**
     * Constructeur de la classe FenetreCertificatVente.
     * Il crée et organise les éléments graphiques nécessaires pour afficher les certificats de vente.
     *
     * @param bpPage              Le BorderPane principal de la page.
     * @param rootTop             Le BorderPane du menu supérieur.
     * @param rootLeft            Le BorderPane du menu latéral gauche.
     * @param borderMenuOption    Le BorderPane des options du menu.
     * @param AdminModele         L'instance de la classe Admin utilisée pour récupérer les données des ventes.
     * @throws SQLException       Une exception qui peut être levée lors de l'accès à la base de données.
     */
    public FenetreCertificatVente(BorderPane bpPage, BorderPane rootTop, BorderPane rootLeft, BorderPane borderMenuOption, Admin AdminModele) throws SQLException {
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        hBoxLeft.getChildren().addAll(rootLeft);
        bpPage.setLeft(hBoxLeft);
        bpPage.setTop(rootTop);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        StackPane.setMargin(rootLeftOption, new Insets(48, 0, 0, 56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);
        Label labelCertificat = new Label("Certificat des ventes :");
        labelCertificat.setStyle("-fx-font-size: 30px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
        labelCertificat.setAlignment(Pos.CENTER);
        VBox vBoxCertificatContainer = new VBox();
        VBox vBoxCertificat = new VBox();

        List<List<String>> lesVentesValider = AdminModele.getAllVente();
        int count = 0;
        HBox currentHBox = new HBox();
        currentHBox.setAlignment(Pos.CENTER);
        currentHBox.setSpacing(10);
        vBoxCertificat.getChildren().add(currentHBox);

        for (List<String> vente : lesVentesValider) {

            int idVe = Integer.parseInt(vente.get(0));
            List<String> infoVente = AdminModele.getInfoVente(idVe);
            int prixBase = Integer.parseInt(infoVente.get(1));
            int prixMin = Integer.parseInt(infoVente.get(2));
            String debutVente = infoVente.get(3);
            String finVente = infoVente.get(4);
            int idOb = Integer.parseInt(infoVente.get(5));
            int idSt = Integer.parseInt(infoVente.get(6));
            String nomVille = infoVente.get(7); 

            Label labelIdVe = new Label("Id de la vente : " + idVe);
            labelIdVe.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelPrixBase = new Label("Prix de base : " + prixBase);
            labelPrixBase.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelPrixMin = new Label("Prix minimum : " + prixMin);
            labelPrixMin.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelDebutVente = new Label("Début de la vente : " + debutVente);
            labelDebutVente.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelFinVente = new Label("Fin de la vente : " + finVente);
            labelFinVente.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelIdOb = new Label("Id de l'objet : " + idOb);
            labelIdOb.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelIdSt;
            if(idSt == 1){
                labelIdSt = new Label("Statut de la vente : A venir");
            }
            else if(idSt == 2){
                labelIdSt = new Label("Statut de la vente : En cours");
            }
            else if(idSt == 3){
                labelIdSt = new Label("Statut de la vente : A validée");
            }
            else if(idSt == 4){
                labelIdSt = new Label("Statut de la vente : Validée");
            }
            else{
                labelIdSt = new Label("Statut de la vente : Non conclue");
            }
            labelIdSt.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");
            Label labelNomVille = new Label("Nom de la ville de la vente : " + nomVille);
            labelNomVille.setStyle("-fx-font-size: 12px; -fx-font-weight: bold; -fx-text-fill: #D9D9D9;");

            HBox hboxPrix = new HBox();
            hboxPrix.getChildren().addAll(labelPrixBase, labelPrixMin);
            hboxPrix.setSpacing(10);
            hboxPrix.setAlignment(Pos.CENTER);
            hboxPrix.setPadding(new Insets(0, 0, 10, 0));
            HBox hboxVenteObjet = new HBox();
            hboxVenteObjet.getChildren().addAll(labelIdVe, labelIdOb);
            hboxVenteObjet.setSpacing(10);
            hboxVenteObjet.setAlignment(Pos.CENTER);
            hboxVenteObjet.setPadding(new Insets(0, 0, 10, 0));
            HBox hboxDateVente = new HBox();
            hboxDateVente.getChildren().addAll(labelDebutVente, labelFinVente);
            hboxDateVente.setSpacing(10);
            hboxDateVente.setAlignment(Pos.CENTER);
            hboxDateVente.setPadding(new Insets(0, 0, 10, 0));
            HBox hboxStatutVente = new HBox();
            hboxStatutVente.getChildren().addAll(labelIdSt, labelNomVille);
            hboxStatutVente.setSpacing(10);
            hboxStatutVente.setAlignment(Pos.CENTER);
            hboxStatutVente.setPadding(new Insets(0, 0, 10, 0));

            VBox vBoxCertificatVenteChild = new VBox();
            vBoxCertificatVenteChild.setBackground(new Background(new BackgroundFill(Color.web("#D9D9D9"), new CornerRadii(0), Insets.EMPTY)));
            vBoxCertificatVenteChild.setPrefSize(WIDTH_VENTE, HEIGHT_VENTE);

            HBox hBoxBoutons = new HBox();
            Button buttonGenererCertificat = new Button("Générer certificat " + idVe);
            buttonGenererCertificat.setOnAction(new ControleurGenererCertificat(this, AdminModele));
            buttonGenererCertificat.setStyle("-fx-background-color: #EBACA2; -fx-font-size: 15px;");

            hBoxBoutons.getChildren().addAll(buttonGenererCertificat);
            hBoxBoutons.setAlignment(Pos.CENTER);
            hBoxBoutons.setSpacing(10);

            vBoxCertificatVenteChild.getChildren().addAll(hboxVenteObjet, hboxPrix, hboxDateVente, hboxStatutVente, hBoxBoutons);
            vBoxCertificatVenteChild.setAlignment(Pos.CENTER);
            vBoxCertificatVenteChild.setStyle("-fx-background-color: #4A919E;");

            currentHBox.getChildren().add(vBoxCertificatVenteChild);
            count++;

            if (count % 3 == 0) {
                currentHBox = new HBox();
                currentHBox.setAlignment(Pos.CENTER);
                currentHBox.setSpacing(10);
                vBoxCertificat.getChildren().add(currentHBox);
            }
        }

        vBoxCertificat.setAlignment(Pos.CENTER);
        vBoxCertificat.setSpacing(30);
        vBoxCertificat.setPadding(new Insets(10, 100, 10, 0));

        ScrollPane scrollPaneCertificat = new ScrollPane(vBoxCertificat);
        scrollPaneCertificat.setStyle("-fx-background: #BED3C3;");
        scrollPaneCertificat.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        vBoxCertificatContainer.getChildren().addAll(labelCertificat, scrollPaneCertificat);
        vBoxCertificatContainer.setAlignment(Pos.CENTER);
        vBoxCertificatContainer.setSpacing(10);
        vBoxCertificatContainer.setPadding(new Insets(20, 0, 0, 0));
        bpPage.setCenter(vBoxCertificatContainer);
    }
}
