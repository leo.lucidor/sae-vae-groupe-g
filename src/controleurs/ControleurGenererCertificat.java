package controleurs;
import java.io.IOException;
import java.sql.SQLException;
import com.itextpdf.text.DocumentException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import modeles.Admin;
import modeles.PdfGenerator;
import vues.FenetreCertificatVente;

public class ControleurGenererCertificat implements EventHandler<ActionEvent>{
    
    /**
     * Le modele admin
     */
    private Admin admin;

    /**
     * Constructeur du controleur
     * @param fenetreCertificatVente la fenetre de certificat de vente
     * @param admin le modele admin
     */
    public ControleurGenererCertificat(FenetreCertificatVente fenetreCertificatVente, Admin admin) {;
        this.admin = admin;
    }

    /**
     * Methode qui genere le certificat de vente
     * @param event l'evenement
     */
    @Override
    public void handle(ActionEvent event) {
        Button button = (Button) event.getSource();
        int idVe = Integer.parseInt(button.getText().replaceAll("[^0-9]", ""));
        try {
            PdfGenerator.generateRecapitulatif("CertificatVente"+ idVe + ".pdf", admin.getInfoVente(idVe));
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText("Certificat de vente généré");
            alert.setContentText("Le certificat de vente a été généré avec succès");
            alert.showAndWait();
        } catch (IOException | DocumentException | SQLException e) {
            e.printStackTrace();
        }
    }
}
