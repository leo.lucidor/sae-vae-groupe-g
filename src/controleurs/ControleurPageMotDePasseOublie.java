package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import javafx.event.ActionEvent;

public class ControleurPageMotDePasseOublie  implements EventHandler<ActionEvent> {
    
    /**
     * L'application principale
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur
     * @param appli l'application principale
     */
    public ControleurPageMotDePasseOublie(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Méthode qui gère l'action sur le bouton "Mot de passe oublié ?"
     * @param event l'évènement
     */
    @Override
    public void handle(ActionEvent event) {
        this.appli.pageMotDePasseOublie();
    }
}
