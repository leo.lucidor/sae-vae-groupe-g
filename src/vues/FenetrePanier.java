package vues;
import java.sql.SQLException;
import java.util.List;

import controleurs.ControleurBoutonPanierRetracter;
import controleurs.ControleurBoutonPanierSurencherir;
import controleurs.ControleurFiltragePanier;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import modeles.BaseDeDonnee;
import modeles.Panier;
import modeles.Timer;
import modeles.Utilisateur;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * La page Panier de l'application qui extend de la Class StackPane
 */
public class FenetrePanier extends StackPane {

    /**
     * La vue principale de l'application
     */
    private ApplicationVAE appli;

    /**
     * Un sous modèle qui regroupe les infos utiles pour la classe panier
     */
    private Panier panier;

    /**
     * la liste de chaque objet ou l'utilisateur a enchéri ou les objets sont une liste de chaques attributs(String)
     */
    private List<List<String>> lePanier;

    /**
     * Un Pane permettant de scroll
     */
    private ScrollPane scrollPaneObjetsEncherie = new ScrollPane();

    /**
     * L'utilisateur connecté à l'application
     */
    private Utilisateur user;

    /**
     * Le modèle lié à la base de données
     */
    private BaseDeDonnee bd;

    /**
     * Un radioButton utile pour le filtrage, ici celui pour les vêtements
     */
    private RadioButton radioButtonVetement;
    
    /**
     * Un radioButton utile pour le filtrage, ici celui pour les meubles
     */
    private RadioButton radioButtonMeuble;

    /**
     * Un radioButton utile pour le filtrage, ici celui pour les ustensiles de cuisine
     */
    private RadioButton radioButtonUstensileCuisine;

    /**
     * Un radioButton utile pour le filtrage, ici celui pour les outils
     */
    private RadioButton radioButtonOutil;

    /**
     * Un radioButton utile pour le filtrage, ici celui pour le prix croissant
     */
    private RadioButton radioButtonPrixCroissant;

    /**
     * Un radioButton utile pour le filtrage, ici celui pour le prix decroissant
     */
    private RadioButton radioButtonPrixDecroissant;

    /**
     * Un radioButton utile pour le filtrage, ici celui pour la date croissante
     */
    private RadioButton radioButtonDateCroissante;

    /**
     * Un radioButton utile pour le filtrage, ici celui pour la date decroissante
     */
    private RadioButton radioButtonDateDecroissante;

    /**
     * Un radioButton utile pour le filtrage, ici celui pour les vendeurs sous ordre alphabétique
     */
    private RadioButton radioButtonVendeurAlphabetique;

    /**
     * Un radioButton utile pour le filtrage, ici celui pour les vendeurs sous ordre Anti-alphabétique
     */
    private RadioButton radioButtonAntiAlphabetique;


    public FenetrePanier(ApplicationVAE appli, BorderPane bpPage, BorderPane rootTop, BorderPane rootLeft, BaseDeDonnee bd, Utilisateur user, BorderPane borderMenuOption, Label textMenu, Label textPageAccueil, Label textVendreUnProduit, Label textVenteAuxEncheres, Label textMessagerie, Label textMonPanier, Label textInformations, Label textSetting, Label textQuitter, Button quitter, Button deconnexion, Button menu,  Button acceuil, Button message, Button info, Button setting, Button profil, Button enchere, Button panier, Button ajouterEnchere) throws SQLException{
        this.appli = appli;
        this.bd = bd;
        this.panier = new Panier(user, bd);
        this.lePanier = this.panier.trierParDate(this.panier.getPanier(user.getId()));
        this.user = user;

        
       // BorderPane bpPage = new BorderPane();
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        BorderPane rootLeft2 = new MenuLeft(acceuil, quitter, message, info, setting, enchere, panier, ajouterEnchere, bpPage);
        hBoxLeft.getChildren().addAll(rootLeft2, rootLeftOption);
        
       // BorderPane rootTop = new MenuTop(user, textMenu, deconnexion, profil, menu, bpPage);
        // this.setMargin(rootLeftOption, new Insets(48,0,0,56));
        // this.setAlignment(rootLeftOption, Pos.TOP_LEFT);

        BorderPane boxPanier = new BorderPane();
        

        Label titrePanier = new Label("Mon Panier");
    
        //center the text titrePanier
        BorderPane.setAlignment(titrePanier, Pos.CENTER);
        titrePanier.setFont(new Font(40));
        titrePanier.setTextFill(Color.WHITE);
        titrePanier.setPadding(new Insets(20));
        boxPanier.setTop(titrePanier);

        VBox boxMesObjet = new VBox();
        boxPanier.setCenter(boxMesObjet);

        //Label titreMesObjets
        Label titreMesObjets = new Label("Mes objets enchéris");
        titreMesObjets.setFont(new Font(20));
        titreMesObjets.setTextFill(Color.WHITE);




        updatePanier();
        scrollPaneObjetsEncherie.setFitToWidth(true);
        scrollPaneObjetsEncherie.setFitToHeight(true);
        scrollPaneObjetsEncherie.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        


        VBox.setMargin(titreMesObjets, new Insets(0,0,0,20));
        VBox.setMargin(scrollPaneObjetsEncherie, new Insets(20,20,20,20));
        boxMesObjet.getChildren().addAll(titreMesObjets, scrollPaneObjetsEncherie);
       
    //    filtrage
        Label titreFiltrage = new Label("Filtrage");
        VBox vBoxFiltrage = new VBox();
        ToggleGroup toggleGroupCatetorie = new ToggleGroup();
        ToggleGroup toggleGroupPrix = new ToggleGroup();
        ToggleGroup toggleGroupDate = new ToggleGroup();
        ToggleGroup toggleGroupVendeur = new ToggleGroup();
        TitledPane titledPaneFiltrage = new TitledPane();
        ControleurFiltragePanier controleurFiltrage = new ControleurFiltragePanier(FenetrePanier.this.panier, FenetrePanier.this);
        this.radioButtonVetement = new RadioButton("Vêtement") {{ setToggleGroup(toggleGroupCatetorie); setOnAction(controleurFiltrage); setFont(new Font(15)); }};
        this.radioButtonMeuble = new RadioButton("Meuble") {{ setToggleGroup(toggleGroupCatetorie); setOnAction(controleurFiltrage); setFont(new Font(15)); }};
        this.radioButtonUstensileCuisine = new RadioButton("Ustensile de cuisine") {{ setToggleGroup(toggleGroupCatetorie); setOnAction(controleurFiltrage); setFont(new Font(15)); }};
        this.radioButtonOutil = new RadioButton("Outil") {{ setToggleGroup(toggleGroupCatetorie); setOnAction(controleurFiltrage); setFont(new Font(15)); }};
        this.radioButtonPrixCroissant = new RadioButton("Prix croissant") {{ setToggleGroup(toggleGroupPrix); setOnAction(controleurFiltrage); setFont(new Font(15)); }};
        this.radioButtonPrixDecroissant = new RadioButton("Prix décroissant") {{ setToggleGroup(toggleGroupPrix); setOnAction(controleurFiltrage); setFont(new Font(15)); }};
        this.radioButtonDateCroissante = new RadioButton("La plus ancienne") {{ setToggleGroup(toggleGroupDate); setOnAction(controleurFiltrage); setFont(new Font(15)); }};
        this.radioButtonDateDecroissante = new RadioButton("La plus récente") {{ setToggleGroup(toggleGroupDate); setOnAction(controleurFiltrage); setFont(new Font(15)); }};
        this.radioButtonVendeurAlphabetique = new RadioButton("Vendeur alphabétique") {{ setToggleGroup(toggleGroupVendeur); setOnAction(controleurFiltrage); setFont(new Font(15)); }};
        this.radioButtonAntiAlphabetique = new RadioButton("Vendeur anti-alphabétique") {{ setToggleGroup(toggleGroupVendeur); setOnAction(controleurFiltrage); setFont(new Font(15)); }};
        Button buttonFiltrer = new Button("Filtrer") {{ setOnAction(controleurFiltrage); setFont(new Font(15)); }};
        Button buttonEffacerFiltrage = new Button("Réinitialiser") {{ setOnAction(controleurFiltrage); setFont(new Font(15)); }};

        titledPaneFiltrage.setText("Trier par :");
        titledPaneFiltrage.setFont(new Font(16));
        titreFiltrage.setFont(new Font(20));
        titreFiltrage.setTextFill(Color.WHITE);
        titledPaneFiltrage.setPadding(new Insets(10, 10, 10, 10));
        VBox.setMargin(titledPaneFiltrage, new Insets(10, 20, 20, 0));
        VBox.setMargin(titreFiltrage, new Insets(0, 20, 0, 0));
        
        

        VBox vBoxCategorie = new VBox() {{ getChildren().addAll(radioButtonVetement, radioButtonUstensileCuisine, radioButtonMeuble, radioButtonOutil); }};
        VBox vBoxPrixFiltrage = new VBox() {{ getChildren().addAll(radioButtonPrixCroissant, radioButtonPrixDecroissant); }};
        VBox vBoxDateFiltrage = new VBox() {{ getChildren().addAll(radioButtonDateCroissante, radioButtonDateDecroissante); }};
        VBox vBoxVendeurFiltrage = new VBox() {{ getChildren().addAll(radioButtonVendeurAlphabetique, radioButtonAntiAlphabetique); }};
        HBox hBoxFiltrageButtons = new HBox() {{ getChildren().addAll(buttonEffacerFiltrage, buttonFiltrer); }};

        vBoxCategorie.setSpacing(5);
        vBoxPrixFiltrage.setSpacing(5);
        vBoxDateFiltrage.setSpacing(5);
        vBoxVendeurFiltrage.setSpacing(5);

        vBoxCategorie.getChildren().add(0, new Label("Catégorie :") {{ setFont(new Font(15)); underlineProperty().setValue(true); }});
        vBoxCategorie.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0, 0, 1, 0))));
        vBoxCategorie.setPadding(new Insets(10, 0, 5, 0));

        vBoxPrixFiltrage.getChildren().add(0, new Label("Prix :") {{ setFont(new Font(15)); underlineProperty().setValue(true); }});
        vBoxPrixFiltrage.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0, 0, 1, 0))));
        vBoxPrixFiltrage.setPadding(new Insets(0, 0, 5, 0));

        vBoxDateFiltrage.getChildren().add(0, new Label("Date de fin :") {{ setFont(new Font(15)); underlineProperty().setValue(true); }});
        vBoxDateFiltrage.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0, 0, 1, 0))));
        vBoxDateFiltrage.setPadding(new Insets(0, 0, 5, 0));

        vBoxVendeurFiltrage.getChildren().add(0, new Label("Vendeur :") {{ setFont(new Font(15)); underlineProperty().setValue(true); }});
        vBoxVendeurFiltrage.setPadding(new Insets(0, 0, 5, 0));

        hBoxFiltrageButtons.setSpacing(20);
        VBox.setMargin(hBoxFiltrageButtons, new Insets(0, 0, 0, 23));
        buttonEffacerFiltrage.setPrefWidth(100);
        buttonFiltrer.setPrefWidth(100);

        
        VBox vBoxButtons = new VBox();
        vBoxButtons.setSpacing(20);
        vBoxButtons.getChildren().addAll(vBoxCategorie, vBoxPrixFiltrage, vBoxDateFiltrage, vBoxVendeurFiltrage);
        vBoxButtons.setPadding(new Insets(0, 20, 0, 20));
        titledPaneFiltrage.setContent(vBoxButtons);
        vBoxFiltrage.getChildren().addAll(titreFiltrage, titledPaneFiltrage, hBoxFiltrageButtons);
        boxPanier.setRight(vBoxFiltrage);
        

        bpPage.setTop(rootTop);
        bpPage.setLeft(hBoxLeft);
        bpPage.setCenter(boxPanier);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
    }


    /**
     * Une méthode permettant de générer la box ou les objets enchéris sont présent
     */
    public void updatePanier() {
        //Box mes objets enchéris
        VBox boxObjetsEncherie = new VBox();
        boxObjetsEncherie.setStyle("-fx-background-color: #BED3C3;");

        for(List<String> objet : this.lePanier){

                //box root de l'objet
            HBox boxObjet = new HBox();
            boxObjet.setMinHeight(275);
            boxObjet.setStyle("-fx-background-color: #4A919E;");
            VBox.setMargin(boxObjet, new Insets(20));

            //box de l'image avec le vendeur
            VBox boxImgVendeur = new VBox();
            boxImgVendeur.setMinWidth(300);
            boxImgVendeur.setAlignment(Pos.BOTTOM_CENTER);
            ImageView imgObjet = new ImageView(new Image("file:" + objet.get(1)));
            imgObjet.setFitHeight(200);
            imgObjet.setFitWidth(200);
            imgObjet.setPreserveRatio(true);
            Label labelTitreVendeur = new Label("Vendeur(se) : ");
            labelTitreVendeur.setFont(new Font(20));
            labelTitreVendeur.setAlignment(Pos.CENTER);
            Label labelVendeur = new Label(objet.get(2));
            labelVendeur.setFont(new Font(20));
            labelVendeur.setAlignment(Pos.CENTER);
            boxImgVendeur.getChildren().addAll(imgObjet, labelTitreVendeur, labelVendeur);
            HBox.setMargin(boxImgVendeur, new Insets(10));

            //box info objets enchéris
            VBox boxInfoObjet = new VBox();
            Label labelNomObjet = new Label(objet.get(3));
            labelNomObjet.setFont(new Font(30));
            labelNomObjet.setPadding(new Insets(0,0,40,0));
            Label labelCATObjet = new Label("CATÉGORIE : " + objet.get(4));
            labelCATObjet.setFont(new Font(20));
            Label labelStatutObjet = new Label("STATUT : " + objet.get(5));
            labelStatutObjet.setFont(new Font(20));
            Label labelDateDebObjet = new Label("DATE DE DÉBUT : " + objet.get(6));
            labelDateDebObjet.setFont(new Font(20));
            Label labelDatefinObjet = new Label("DATE DE FIN : " + objet.get(7));
            labelDatefinObjet.setFont(new Font(20));
            boxInfoObjet.getChildren().addAll(labelNomObjet, labelCATObjet ,labelStatutObjet, labelDateDebObjet, labelDatefinObjet);
            HBox.setMargin(boxInfoObjet, new Insets(10,20,10,20));

            //box des infos de l'enchère

            VBox boxInfoEnchere = new VBox();
            boxInfoEnchere.setAlignment(Pos.CENTER);
            boxInfoEnchere.setPadding(new Insets(0,10,0,10));
            boxInfoEnchere.setSpacing(40);

            VBox boite1 = new VBox();
            boite1.setAlignment(Pos.TOP_CENTER);
            Label titreTpsRestant = new Label("Temps restant :");
            titreTpsRestant.setFont(new Font(20));
            titreTpsRestant.setPadding(new Insets(0,10,0,10));
            Timer time = new Timer(objet.get(7));
            time.setFont(new Font(20));
            time.start();
            Label formatTime = new Label("Années:Jours:Heures:Minutes:Secondes");
            formatTime.setFont(new Font(10));
            boite1.getChildren().addAll(titreTpsRestant, time, formatTime);

            VBox boite2 = new VBox();
            boite2.setAlignment(Pos.BOTTOM_CENTER);
            Label titreEncherisseur = new Label("Dernier Enchérisseur :");
            titreEncherisseur.setFont(new Font(20));
            Label labelLastEnchsseur = new Label(objet.get(8));
            labelLastEnchsseur.setFont(new Font(20));
            boite2.getChildren().addAll(titreEncherisseur, labelLastEnchsseur);

            boxInfoEnchere.getChildren().addAll(boite1, boite2);
            if(this.user.getPseudo().equals(labelLastEnchsseur.getText())){
                boxInfoEnchere.setStyle("-fx-background-radius: 10px; -fx-background-color: #6ED286;");
            }else{
                boxInfoEnchere.setStyle("-fx-background-radius: 10px; -fx-background-color: #E57E7E;");
            }
            HBox.setMargin(boxInfoEnchere, new Insets(10,20,10,20));


            //box bouton enchère et prix actuel
            BorderPane boxBoutonEnchere = new BorderPane();
            Label prixEnchere = new Label("Prix actuel : " + objet.get(9));
            prixEnchere.setFont(new Font(30));
            boxBoutonEnchere.setTop(prixEnchere);
            VBox boxBouton = new VBox();
            boxBouton.setAlignment(Pos.CENTER);
            boxBouton.setSpacing(10);
            if (time.getTempsEcoule() > 0) {
                Button boutonSurencherir = new Button("Surenchérir");
                boutonSurencherir.setFont(new Font(20));
                boutonSurencherir.setPrefSize(350, 100);
                boutonSurencherir.setStyle("-fx-background-radius: 10px; -fx-background-color: #EBACA2; -fx-margin-bottom: 10em;");
                boutonSurencherir.setOnAction(new ControleurBoutonPanierSurencherir(this.appli, this.bd, objet.get(0), labelNomObjet.getText(), labelVendeur.getText(), objet.get(9)));
                Button boutonRetracter = new Button("Se rétracter");
                boutonRetracter.setPrefSize(350, 100);
                boutonRetracter.setStyle("-fx-background-radius: 10px; -fx-background-color: #EBACA2;");
                boutonRetracter.setFont(new Font(20));
                boutonRetracter.setOnAction(new ControleurBoutonPanierRetracter(this.appli, this.bd, objet.get(0), this.user.getId()));
                boxBouton.getChildren().addAll(boutonSurencherir, boutonRetracter);
            }
            boxBoutonEnchere.setCenter(boxBouton);
            
            HBox.setMargin(boxBoutonEnchere, new Insets(10,20,10,20));
            
            boxObjet.getChildren().addAll(boxImgVendeur,boxInfoObjet, boxInfoEnchere, boxBoutonEnchere);
            boxObjetsEncherie.getChildren().add(boxObjet);
        }
        scrollPaneObjetsEncherie.setContent(boxObjetsEncherie);

    }

    /**
     * Getteur des objets enchéris par l'utilisateurs
     */
    public List<List<String>> getLePanier() {
        return lePanier;
    }

    /**
     * Setteur des objets enchéris par l'utilisateur
     */
    public void setLePanier(List<List<String>> lePanier) {
        this.lePanier = lePanier;
    }

    /**
     * méthode permettant de réinitialiser le filtrage
     */
    public void resetButtonsFiltrage() {
        this.radioButtonVetement.setSelected(false);
        this.radioButtonMeuble.setSelected(false);
        this.radioButtonUstensileCuisine.setSelected(false);
        this.radioButtonOutil.setSelected(false);
        this.radioButtonPrixCroissant.setSelected(false);
        this.radioButtonPrixDecroissant.setSelected(false);
        this.radioButtonDateCroissante.setSelected(false);
        this.radioButtonDateDecroissante.setSelected(false);
        this.radioButtonVendeurAlphabetique.setSelected(false);
        this.radioButtonAntiAlphabetique.setSelected(false);
    }
}
