package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import modeles.Utilisateur;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurChoixTypeUserAdmin implements EventHandler<ActionEvent> {
    
    /**
     * application principale
     */
    private ApplicationVAE appli;
    /**
     * utilisateur modele
     */
    private Utilisateur utilisateur;
    
    /**
     * Constructeur ControleurChoixTypeUserAdmin
     * @param appli application principale
     * @param utilisateur utilisateur modele
     */
    public ControleurChoixTypeUserAdmin(ApplicationVAE appli, Utilisateur utilisateur){
        this.appli = appli;
        this.utilisateur = utilisateur;
    }

    /**
     * Action à effectuer lors de l'appui sur le bouton "connexion"
     * @param event permet de récupérer l'identifiant et le mot de passe
     */
    @Override
    public void handle(ActionEvent event) {
        try {
            String pseudo = this.appli.getIdentifiant();
            String tenterMdp = this.appli.getMotDePasse();
            String mdp = this.utilisateur.getMdp(pseudo);
            int id = this.utilisateur.getId(pseudo);
            // recréer l'utilisateur avec les bonnes données
            this.utilisateur.setInfoUser(id);
            String role = String.valueOf(this.utilisateur.getRole());
            if(utilisateur.estActif(id)){
                if(mdp.equals(tenterMdp)){
                    if(role.equals("1")){
                        this.appli.pageAcceuilAdministrateur();
                        this.appli.setGrandPage(true);
                        this.utilisateur.setRole(1);
                    }
                    else {
                        this.appli.pageAcceuilUtilisateur();
                        this.appli.setGrandPage(true);
                        this.utilisateur.setRole(2);
                    }
                }
                else{
                    this.appli.changerVisibiliteConnexion();
                    System.out.println("le pseudo ou mdp est incorrect");
                }
            }
            else{
                System.out.println("le compte est inactif");
            }   
        } catch (SQLException e) {
            System.out.println("le pseudo ou mdp est incorrect");
            this.appli.changerVisibiliteConnexion();
        }
    }
}
