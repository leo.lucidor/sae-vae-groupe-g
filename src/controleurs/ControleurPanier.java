package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurPanier implements EventHandler<ActionEvent> {
    
    /**
     * L'application principale
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur de la classe
     * @param appli l'application principale
     */
    public ControleurPanier(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Méthode qui permet de gérer l'action sur le bouton "Panier"
     * @param event l'événement
     */
    @Override
    public void handle(ActionEvent event) {
        try {
			this.appli.pagePanier();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
