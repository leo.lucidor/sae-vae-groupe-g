package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import javafx.event.ActionEvent;

public class ControleurEnchere implements EventHandler<ActionEvent> {
    
    /**
     * L'application à gérer
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du contrôleur
     * @param appli l'application à gérer
     */
    public ControleurEnchere(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Gère l'action de l'utilisateur
     * @param event l'événement à gérer
     */
    @Override
    public void handle(ActionEvent event) {
        this.appli.pageEnchere();
    }
}