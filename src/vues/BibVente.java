package vues;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import modeles.Utilisateur;
import modeles.Vente;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import java.util.List;
import controleurs.ControleurEncherir;
import controleurs.ControleurProfilVendeur;
import controleurs.ControleurSignalerVente;


/**
 * La classe BibVente contient des méthodes utilitaires pour créer des éléments visuels liés aux ventes.
 */
public class BibVente {

    /**
     * Constantes de dimensions des éléments visuels.
     */
    private static final int BOX_WIDHT = 500;
    private static final int BOX_HEIGHT = 200;
    private static final int IMAGE_WIDHT = 180;
    private static final int IMAGE_HEIGHT = 180;

    /**
     * Crée un conteneur HBox contenant plusieurs éléments VBox représentant des ventes.
     *
     * @param ventes       La liste des ventes à afficher.
     * @param appli        L'application principale.
     * @param venteModele  Le modèle de vente.
     * @param userModele   Le modèle de l'utilisateur.
     * @return             Le conteneur HBox contenant les éléments VBox des ventes.
     */
    public static HBox createHBoxVentes(List<List<String>> ventes, ApplicationVAE appli, Vente venteModele, Utilisateur userModele) {
        HBox hbox = new HBox();
        hbox.setSpacing(10);
        hbox.setPadding(new Insets(10,0,0,0));

        for (List<String> vente : ventes) {
            VBox vbox = createVBoxElement(vente, appli, venteModele, userModele);
            hbox.getChildren().addAll(vbox);
        }
        hbox.setAlignment(Pos.CENTER);
        return hbox;
    }

    /**
     * Crée un élément VBox représentant une vente.
     *
     * @param vente        Les informations de la vente.
     * @param appli        L'application principale.
     * @param venteModele  Le modèle de vente.
     * @param userModele   Le modèle de l'utilisateur.
     * @return             L'élément VBox représentant la vente.
     */
    public static VBox createVBoxElement(List<String> vente, ApplicationVAE appli, Vente venteModele, Utilisateur userModele) {
        VBox vbox = new VBox();
        VBox vBoxAll = new VBox();

        int statut = Integer.parseInt(vente.get(11));
        String statutString = "";
        switch (statut) {
            case 1:
                statutString = "A venir";
                break;
            case 2:
                statutString = "En cours";
                break;
            default:
                break;
        }
        Label label = new Label("Vendeur : " + vente.get(1) + "\n" +
                "Nom objet : " + vente.get(2) + "\n" +
                "Prix base : " + vente.get(3) + "€" + "\n" +
                "Prix mini : " + vente.get(4) + "€" + "\n" +
                "Ville : " + vente.get(6) + "\n" +
                "Statut : " + statutString);

        HBox hBoxAll = new HBox();
        ImageView imageView = new ImageView(new Image("file:"+vente.get(10)));
        //add radius to imageview border
        
        imageView.setFitWidth(IMAGE_WIDHT);
        imageView.setFitHeight(IMAGE_HEIGHT);
        HBox hboxBouton = new HBox();
        Button boutonSignaler = new Button();
        boutonSignaler.setOnAction(new ControleurSignalerVente(appli, venteModele, Integer.parseInt(vente.get(0)), userModele.getId()));
        boutonSignaler.setBackground(new Background(
                new BackgroundFill(Color.web("#EBACA2"), new CornerRadii(5), null)
        ));
        ImageView imageViewSignaler = new ImageView(new Image("file:./images/iconSignaler.png"));
        imageViewSignaler.setFitWidth(20);
        imageViewSignaler.setFitHeight(20);
        boutonSignaler.setGraphic(imageViewSignaler);
        Button buttonVoirPlus = new Button("Voir plus");
        buttonVoirPlus.setOnAction(new ControleurEncherir(appli, vente));
        buttonVoirPlus.setStyle("-fx-background-color: #EBACA2; -fx-text-fill: #000000;");
        Button buttonProfilVendeur = new Button("Profil vendeur");
        buttonProfilVendeur.setStyle("-fx-background-color: #EBACA2; -fx-text-fill: #000000;");
        buttonProfilVendeur.setOnAction(new ControleurProfilVendeur(appli, Integer.parseInt(vente.get(12))));
        hboxBouton.getChildren().addAll(buttonVoirPlus, buttonProfilVendeur, boutonSignaler);

        buttonVoirPlus.setPrefHeight(30);
        boutonSignaler.setPrefHeight(30);
        buttonProfilVendeur.setPrefHeight(30);

        hboxBouton.setAlignment(Pos.CENTER);

        hboxBouton.setSpacing(10);
        hboxBouton.setPadding(new Insets(20,0,0,25));
        hBoxAll.getChildren().addAll(imageView, vbox);
        hBoxAll.setPadding(new Insets(10,0,0,10));
        vbox.getChildren().addAll(label, hboxBouton);
        vBoxAll.getChildren().addAll(hBoxAll);
        vbox.setSpacing(10);
        label.setStyle("-fx-text-fill: #000000;");
        label.setPadding(new Insets(5,0,0,10));
        vBoxAll.setMinSize(BOX_WIDHT, BOX_HEIGHT);
        vBoxAll.setPrefSize(BOX_WIDHT, BOX_HEIGHT);
        vBoxAll.setBackground(new Background(
                new BackgroundFill(Color.web("#4A919E"), new CornerRadii(10), null)
        ));
        return vBoxAll;
    }

    /**
     * retourne l'id de la vente
     */
    public static String getIdVe(List<String> enchere) {
        return enchere.get(0);
    }
    /**
     * retourne le nom d'un objet
     */
    public static String getNomObjet(List<String> enchere) {
        return enchere.get(2);
    }

    /**
     * retourne le prix de base d'une vente
     */
    public static String getPrixBase(List<String> enchere) {
        return enchere.get(3);
    }

    /**
     * retourne le prix mini d'une vente
     */
    public static String getPrixMini(List<String> enchere) {
        return enchere.get(4);
    }

    /**
     * retourne la ville d'une vente
     */
    public static String getVille(List<String> enchere) {
        return enchere.get(6);
    }

    /**
     * retourne la photo d'une vente
     */
    public static String getPhoto(List<String> enchere) {
        return enchere.get(10);
    }

    /**
     * retourne le vendeur d'une vente
     */
    public static String getVendeur(List<String> enchere) {
        return enchere.get(1);
    }

}
