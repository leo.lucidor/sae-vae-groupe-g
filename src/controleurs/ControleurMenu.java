package controleurs;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.layout.BorderPane;
import vues.ApplicationVAE;

public class ControleurMenu implements EventHandler<ActionEvent> {
    
    /**
     * L'application principale
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur
     * @param appli l'application principale
     * @param borderMenuOption le menu de l'application
     */
    public ControleurMenu(ApplicationVAE appli, BorderPane borderMenuOption){
        this.appli = appli;
    }

    /**
     * Méthode qui gère l'action de l'utilisateur sur le menu
     * @param event l'évènement qui a déclenché l'action
     */
    @Override
    public void handle(ActionEvent event) {
        this.appli.changerVisibiliteMenu();
    }
}