package vues;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import modeles.ComparatorEncherePrix;
import modeles.Utilisateur;
import modeles.Vente;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.sql.SQLException;

public class FenetreEnchere extends StackPane {

    private Vente venteModele;
    private List<List<String>> lesVentes;
    private List<List<String>> lesVentesFiltrees;
    private static final int VENTES_PAR_LIGNE = 3;
    private VBox vBoxVente;
    private ApplicationVAE appli;
    private Utilisateur userModele;
    private TextField recherche;
    private ComboBox<String> triePrix;
    private TextField villeRayon;
    private ComboBox<String> trierStatut;
    private ComboBox<String> categorie;

    public FenetreEnchere(BorderPane bpPage, BorderPane rootTop, BorderPane rootLeft, Utilisateur userModele, ApplicationVAE appli, Vente venteModele, BorderPane borderMenuOption, Label textMenu, Label textPageAccueil, Label textVendreUnProduit, Label textVenteAuxEncheres, Label textMessagerie, Label textMonPanier, Label textInformations, Label textSetting, Label textQuitter, Button quitter, Button deconnexion, Button menu,  Button acceuil, Button message, Button info, Button setting, Button profil, Button enchere, Button panier, Button ajouterEnchere){
        this.appli = appli;
        this.userModele = userModele;
        this.venteModele = venteModele;

        HBox hbTopContainer = new HBox();
        VBox vbTopContainer = new VBox();
        HBox hbTop = new HBox();
        HBox hbTop2 = new HBox();
        HBox hbRecherche = new HBox();
        HBox hbVilleRayon = new HBox();
        HBox hbCategorie = new HBox();
        HBox hbTriePrix = new HBox();
        VBox vBoxCenter = new VBox();
        this.categorie = new ComboBox<>();
        this.triePrix = new ComboBox<>();
        this.trierStatut = new ComboBox<>();
        Label labelTopTitre = new Label("Vous êtes à la recherche d’un produit en particulier ? Vous êtes au bon endroit !");
        Label titreEnchere = new Label("Les enchères en cours et à venir");
        titreEnchere.setStyle("-fx-background-color: #012245; -fx-text-fill: #FFFFFF; -fx-font-size: 40px; -fx-font-weight: bold;");
        
        BorderPane rootLeftOption = borderMenuOption;
        BorderPane rootLeft2 = new MenuLeft(acceuil, quitter, message, info, setting, enchere, panier, ajouterEnchere, bpPage);
        HBox hBoxLeft = new HBox();
        bpPage.setStyle("-fx-background-color: #012245;");
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        hBoxLeft.getChildren().addAll(rootLeft2, rootLeftOption);
        bpPage.setCenter(vBoxCenter);
        bpPage.setLeft(hBoxLeft);
        bpPage.setTop(rootTop);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);

        titreEnchere.setPadding(new Insets(20));
        recherche = new TextField();
        this.villeRayon = new TextField();

        trierStatut.getItems().addAll("Tous les statuts", "En cours", "A venir");
        this.trierStatut.getSelectionModel().selectFirst();
        vBoxCenter.setAlignment(Pos.CENTER);
        this.vBoxVente = new VBox();
        
                
        this.categorie.getItems().addAll("Toutes les catégories", "Vêtements", "Ustensiles de cuisine", "Meubles", "Outils");
        this.categorie.getSelectionModel().selectFirst();
        this.triePrix.getItems().addAll("Aucun tri par prix", "Prix croissant", "Prix décroissant");
        this.triePrix.getSelectionModel().selectFirst();
        hbRecherche.getChildren().addAll(this.recherche);
        hbVilleRayon.getChildren().add(this.villeRayon);
        hbCategorie.getChildren().add(this.categorie);
        hbTriePrix.getChildren().add(this.triePrix);

        vbTopContainer.setBackground(new Background(new BackgroundFill(Color.web("#FFFFFF"), new CornerRadii(20), Insets.EMPTY)));

        this.vBoxVente.setStyle("-fx-background-color: #012245;");

        labelTopTitre.setAlignment(Pos.CENTER);

        hbTop.getChildren().addAll(hbRecherche, hbVilleRayon);
        hbTop2.getChildren().addAll(this.categorie, this.trierStatut, this.triePrix);
        vbTopContainer.getChildren().addAll(labelTopTitre, hbTop, hbTop2);
        hbTopContainer.getChildren().addAll(vbTopContainer);
        this.recherche.setPromptText("Rechercher un produit");
        this.villeRayon.setPromptText("Rechercher une ville");
        //add border radius
        this.recherche.setStyle("-fx-background-radius: 10;");
        this.villeRayon.setStyle("-fx-background-radius: 10;");
        this.categorie.setStyle("-fx-background-radius: 10;");
        this.triePrix.setStyle("-fx-background-radius: 10;");
        this.trierStatut.setStyle("-fx-background-radius: 10;");
        this.recherche.setPrefHeight(20);
        this.recherche.setMinWidth(300);
        this.villeRayon.setPrefHeight(20);
        this.villeRayon.setMinWidth(150);

        this.categorie.setPrefHeight(20);
        this.categorie.setMinWidth(200);
        this.triePrix.setPrefHeight(20);
        this.triePrix.setMinWidth(200);
        this.trierStatut.setPrefHeight(20);
        this.trierStatut.setMinWidth(200);
        

        this.recherche.setPadding(new Insets(5));
        this.villeRayon.setPadding(new Insets(5));
        this.categorie.setPadding(new Insets(5));
        this.triePrix.setPadding(new Insets(5));
        this.trierStatut.setPadding(new Insets(5));

        hbTop.setPadding(new Insets(5));
        hbTop2.setPadding(new Insets(5));
        vbTopContainer.setPadding(new Insets(5));
        hbTopContainer.setPadding(new Insets(5));


        hbTop.setSpacing(10);
        hbTop2.setSpacing(10);
        vbTopContainer.setSpacing(10);
        hbTopContainer.setSpacing(10);



        HBox.setMargin(this.recherche, new Insets(0, 10, 0, 0));
        HBox.setMargin(this.villeRayon, new Insets(0, 10, 0, 0));
        HBox.setMargin(this.categorie, new Insets(0, 10, 0, 0));
        HBox.setMargin(this.triePrix, new Insets(0, 10, 0, 0));
        HBox.setMargin(this.trierStatut, new Insets(0, 10, 0, 0));


        hbTop.setAlignment(Pos.CENTER);
        hbTop2.setAlignment(Pos.CENTER);
        vbTopContainer.setAlignment(Pos.CENTER);
        hbTopContainer.setAlignment(Pos.CENTER);
        ScrollPane scrollPane = new ScrollPane(vBoxVente);
        vBoxCenter.getChildren().addAll(titreEnchere, hbTopContainer, scrollPane);
        vBoxCenter.setStyle("-fx-background-color: #012245;");
        this.vBoxVente.prefWidthProperty().bind(scrollPane.widthProperty());
        this.setStyle("-fx-background-color: #012245;");
        this.vBoxVente.setAlignment(Pos.CENTER);
        scrollPane.setStyle("-fx-background-color: transparent; -fx-background-insets: 0; -fx-padding: 0;");
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);

        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        StackPane.setMargin(rootLeftOption, new Insets(48,0,0,56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);
        try {
            this.lesVentes = this.venteModele.getVente();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        updateEncheres(appli, venteModele, userModele, this.lesVentes);
        addListenerPrix(triePrix);
        addListenerCategorie(categorie);
        addListenerRecherche(this.recherche);
        addListenerVille(this.villeRayon);
        addListenerStatut(this.trierStatut);
    }

    public void updateEncheres(ApplicationVAE appli, Vente venteModele, Utilisateur userModele, List<List<String>> lesVentes) {
        this.vBoxVente.getChildren().clear();
        for (int i = 0; i < lesVentes.size(); i += VENTES_PAR_LIGNE) {
        //for (int i = 0; i < 5; i += VENTES_PAR_LIGNE) {
            List<List<String>> sublist = lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, lesVentes.size()));
            HBox hbox = BibVente.createHBoxVentes(sublist, appli, venteModele, userModele);
            this.vBoxVente.getChildren().add(hbox);
        }
    }

    public void addListenerPrix(ComboBox<String> triePrix) {
        triePrix.valueProperty().addListener(( observable, oldValue, newValue) -> {
            this.recherche.setText("");
            this.villeRayon.setText("");
            if (newValue.equals("Aucun tri par prix")) {
                this.lesVentesFiltrees = null;
                updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentes);
                this.triePrix.setValue("Aucun tri par prix");
                this.trierStatut.setValue("Tous les statuts");
            }
                switch (newValue) {
                    case "Prix croissant":
                        if (this.lesVentesFiltrees != null) {
                            Collections.sort(this.lesVentesFiltrees, new ComparatorEncherePrix());

                            updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                        }
                        else {
                            Collections.sort(this.lesVentes, new ComparatorEncherePrix());
                            updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentes);
                        }
                        break;
                    case "Prix décroissant":
                        if (this.lesVentesFiltrees != null) {
                            Collections.sort(this.lesVentesFiltrees, new ComparatorEncherePrix().reversed());
                            updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                        }
                        else {
                            Collections.sort(this.lesVentes, new ComparatorEncherePrix().reversed());
                            updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentes);
                        }
                        break;
                    default:
                        break;
                }
        });
    }

    /**
     * Ajoute un listener sur la catégorie
     */
    public void addListenerCategorie(ComboBox<String> categorie) {
        categorie.valueProperty().addListener(( observable, oldValue, newValue) -> {
            this.recherche.setText("");
            this.villeRayon.setText("");
            if (newValue.equals("Toutes les catégories")) {
                    this.lesVentesFiltrees = null;
                    updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentes);
                    this.triePrix.setValue("Aucun tri par prix");
                    this.trierStatut.setValue("Tous les statuts");
            }
            else {
                switch (newValue) {
                    case "Vêtements":
                        this.lesVentesFiltrees = trierParCategorie(this.lesVentes, "Vêtement");
                        break;
                    case "Ustensiles de cuisine":
                        this.lesVentesFiltrees = trierParCategorie(this.lesVentes, "Ustensile Cuisine");
                        break;
                    case "Meubles":
                        this.lesVentesFiltrees = trierParCategorie(this.lesVentes, "Meuble");
                        break;
                    case "Outils":
                        this.lesVentesFiltrees = trierParCategorie(this.lesVentes, "Outil");
                        break;
                    default:
                            break;
                }
                switch (this.triePrix.getValue()) {
                    case "Prix croissant":
                        Collections.sort(this.lesVentesFiltrees, new ComparatorEncherePrix());
                        break;
                    case "Prix décroissant":
                        Collections.sort(this.lesVentesFiltrees, new ComparatorEncherePrix().reversed());
                        break;
                    default:
                        break;
                }
                switch (this.trierStatut.getValue()) {
                    case "En cours":
                        this.lesVentesFiltrees = trierParStatut(this.lesVentesFiltrees, "En cours");

                        break;
                    case "A venir":
                        this.lesVentesFiltrees = trierParStatut(this.lesVentesFiltrees, "A venir");
                        break;
                }
                updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
            }
        });
    }

    /**
     * Ajoute un listener sur la recherche
     */
    public void addListenerRecherche(TextField recherche) {
        recherche.textProperty().addListener(( observable, oldValue, newValue) -> {
            if (oldValue.length() > newValue.length() && newValue != "") {
                if (this.lesVentesFiltrees != null) {
                    this.lesVentesFiltrees = trierParRechercheInverser(this.lesVentes, newValue, oldValue);
                    updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                }
            }
            else {
                if (this.lesVentesFiltrees != null) {
                    this.lesVentesFiltrees = trierParRecherche(this.lesVentesFiltrees, newValue);
                    updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                }
                else {
                    this.lesVentesFiltrees = trierParRecherche(this.lesVentes, newValue);
                    updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                }
            }
        });
    }

    /**
     * Ajoute un listener sur la ville
     */
    public void addListenerVille(TextField ville) {
        ville.textProperty().addListener(( observable, oldValue, newValue) -> {
            if (oldValue.length() > newValue.length() && newValue != "") {
                if (this.lesVentesFiltrees != null) {
                    this.lesVentesFiltrees = trierParVilleInverser(this.lesVentes, newValue, oldValue);
                    updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                }
            }
            else {
                if (this.lesVentesFiltrees != null) {
                    this.lesVentesFiltrees = trierParVille(this.lesVentesFiltrees, newValue);
                    updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                }
                else {
                    this.lesVentesFiltrees = trierParVille(this.lesVentes, newValue);
                    updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                }
            }
        });
    }

    /**
     * Ajoute un listener sur le statut
     */
    public void addListenerStatut(ComboBox<String> statut) {
        statut.valueProperty().addListener(( observable, oldValue, newValue) -> {
            this.recherche.setText("");
            this.villeRayon.setText("");
            if (newValue.equals("Tous les statuts")) {
                this.lesVentesFiltrees = null;
                updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentes);
                this.triePrix.setValue("Aucun tri par prix");
                this.trierStatut.setValue("Tous les statuts");
            }
            else {
                switch (newValue) {
                    case "A venir":
                        if (this.lesVentesFiltrees != null && !this.lesVentesFiltrees.get(0).get(11).equals("2")) {
                            this.lesVentesFiltrees = trierParStatut(this.lesVentesFiltrees, "A venir");
                            updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                        }
                        else {
                            this.lesVentesFiltrees = trierParStatut(this.lesVentes, "A venir");
                            updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                        }
                        break;
                    case "En cours":
                        if (this.lesVentesFiltrees != null && !this.lesVentesFiltrees.get(0).get(11).equals("1")) {
                            this.lesVentesFiltrees = trierParStatut(this.lesVentesFiltrees, "En cours");
                            updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                        }
                        else {
                            this.lesVentesFiltrees = trierParStatut(this.lesVentes, "En cours");
                            updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                        }
                        break;
                    default:
                        break;
                }
                switch (this.categorie.getValue()) {
                    case "Vêtements":
                            this.lesVentesFiltrees = trierParCategorie(this.lesVentesFiltrees, "Vêtement");
                            updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                        break;
                    case "Meubles":
                        this.lesVentesFiltrees = trierParCategorie(this.lesVentesFiltrees, "Meuble");
                        updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                        break;
                        case "Ustensiles de cuisine":
                            this.lesVentesFiltrees = trierParCategorie(this.lesVentesFiltrees, "Ustensile Cuisine");
                            updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                        break;
                    case "Outils":
                        this.lesVentesFiltrees = trierParCategorie(this.lesVentesFiltrees, "Outil");
                        updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                        break;
                }
                switch (this.triePrix.getValue()) {
                    case "Prix croissant":
                        Collections.sort(this.lesVentesFiltrees, new ComparatorEncherePrix());
                        updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                        break;
                case "Prix décroissant":
                        Collections.sort(this.lesVentesFiltrees, new ComparatorEncherePrix().reversed());
                        updateEncheres(this.appli, this.venteModele, this.userModele, this.lesVentesFiltrees);
                        break;
                }

            }
        });
    }

    /**
     * Trie les ventes par statut
     */
    public List<List<String>> trierParStatut(List<List<String>> lesVentes, String statut) {
        List<List<String>> lesVentesTrie = new ArrayList<>();
        int aVenir = 1;
        int enCours = 2;
        int statutVente = 0;
        if (statut.equals("A venir")) {
            statutVente = aVenir;
        }
        else if (statut.equals("En cours")) {
            statutVente = enCours;
        }
        for (List<String> vente : lesVentes) {
            if (Integer.parseInt(vente.get(11)) == statutVente) {
                lesVentesTrie.add(vente);
            }
        }
        switch (this.categorie.getValue()) {
            case "Vêtements":
                lesVentesTrie = trierParCategorie(lesVentesTrie, "Vêtement");
                break;
            case "Meubles":
                lesVentesTrie = trierParCategorie(lesVentesTrie, "Meuble");
                break;
            case "Ustensiles de cuisine":
                lesVentesTrie = trierParCategorie(lesVentesTrie, "Ustensile Cuisine");
                break;
            case "Outils":
                lesVentesTrie = trierParCategorie(lesVentesTrie, "Outil");
                break;
        }
        switch (this.triePrix.getValue()) {
            case "Croissant":
                Collections.sort(lesVentesTrie, new ComparatorEncherePrix());
                break;
            case "Décroissant":
                Collections.sort(lesVentesTrie, new ComparatorEncherePrix().reversed());
                break;
        }
        return lesVentesTrie;
    }
    
    /**
     * Trie par la recherche
     */
    public List<List<String>> trierParRecherche(List<List<String>> lesVentes, String recherche) {
        List<List<String>> lesVentesTrie = new ArrayList<>();
        for (List<String> vente : lesVentes) {
            if (vente.get(2).toLowerCase().contains(recherche.toLowerCase())) {
                lesVentesTrie.add(vente);
            }
        }
        switch (this.categorie.getValue()) {
            case "Vêtements":
                lesVentesTrie = trierParCategorie(lesVentesTrie, "Vêtement");
                break;
            case "Meubles":
                lesVentesTrie = trierParCategorie(lesVentesTrie, "Meuble");
                break;
            case "Ustensiles de cuisine":
                lesVentesTrie = trierParCategorie(lesVentesTrie, "Ustensile Cuisine");
                break;
            case "Outils":
                lesVentesTrie = trierParCategorie(lesVentesTrie, "Outil");
                break;
        }
        switch (this.triePrix.getValue()) {
            case "Croissant":
                Collections.sort(lesVentesTrie, new ComparatorEncherePrix());
                break;
            case "Décroissant":
                Collections.sort(lesVentesTrie, new ComparatorEncherePrix().reversed());
                break;
        }
        switch (this.trierStatut.getValue()) {
            case "A venir":
                lesVentesTrie = trierParStatut(lesVentesTrie, "A venir");
                break;
            case "En cours":
                lesVentesTrie = trierParStatut(lesVentesTrie, "En cours");
                break;
        }
        return lesVentesTrie;
    }

    /**
     * Trie les ventes par rapport à une recherche inversée
     */
    public List<List<String>> trierParRechercheInverser(List<List<String>> lesVentes, String recherche, String ancienneRecherche) {
        List<List<String>> lesVentesTrie = new ArrayList<>();
        for (List<String> vente : lesVentes) {
            if (vente.get(2).toLowerCase().contains(recherche.toLowerCase()) && !vente.get(2).toLowerCase().contains(ancienneRecherche.toLowerCase())) {
                lesVentesTrie.add(vente);
            }
            else if (vente.get(2).toLowerCase().contains(recherche.toLowerCase())) {
                lesVentesTrie.add(vente);
            }
        }

        switch (this.categorie.getValue()) {
            case "Vêtements":
            lesVentesTrie = trierParCategorie(lesVentesTrie, "Vêtement");
                break;
            case "Meubles":
                lesVentesTrie = trierParCategorie(lesVentesTrie, "Meuble");
                break;
            case "Ustensiles de cuisine":
                lesVentesTrie = trierParCategorie(lesVentesTrie, "Ustensile Cuisine");
                break;
            case "Outils":
                lesVentesTrie = trierParCategorie(lesVentesTrie, "Outil");
                break;
            default:
                break;
        }
        switch (this.trierStatut.getValue()) {
            case "A venir":
                lesVentesTrie = trierParStatut(lesVentesTrie, "A venir");
                break;
            case "En cours":
                lesVentesTrie = trierParStatut(lesVentesTrie, "En cours");
                break;
            default:
                break;
        }
        return lesVentesTrie;
    }

    /**
     * Trie par la ville
     */
    public List<List<String>> trierParVille(List<List<String>> lesVentes, String ville) {
        List<List<String>> lesVentesTrie = new ArrayList<>();
        for (List<String> vente : lesVentes) {
            if (vente.get(6).toLowerCase().contains(ville.toLowerCase())) {
                lesVentesTrie.add(vente);
            }
        }
        return lesVentesTrie;
    }

    /**
     * Trie par la ville inverser
     */
    public List<List<String>> trierParVilleInverser(List<List<String>> lesVentes, String ville, String ancienneVille) {
        List<List<String>> lesVentesTrie = new ArrayList<>();
        for (List<String> vente : lesVentes) {
            if (vente.get(6).toLowerCase().contains(ville.toLowerCase()) && !vente.get(6).toLowerCase().contains(ancienneVille.toLowerCase())) {
                lesVentesTrie.add(vente);
            }
            else if (vente.get(6).toLowerCase().contains(ville.toLowerCase())) {
                lesVentesTrie.add(vente);
            }
        }
        return lesVentesTrie;
    }

    /**
     * Trie par le prix
     */
    public List<List<String>> trierParPrix(List<List<String>> lesVentes, int prixMax) {
        List<List<String>> lesVentesTrie = new ArrayList<>();
        for (List<String> vente : lesVentes) {
            if (Integer.parseInt(vente.get(4)) <= prixMax) {
                lesVentesTrie.add(vente);
            }
        }
        switch (this.categorie.getValue()) {
            case "Vêtements":
            lesVentesTrie = trierParCategorie(lesVentesTrie, "Vêtement");
                break;
            case "Meubles":
                lesVentesTrie = trierParCategorie(lesVentesTrie, "Meuble");
                break;
            case "Ustensiles de cuisine":
                lesVentesTrie = trierParCategorie(lesVentesTrie, "Ustensile Cuisine");
                break;
            case "Outils":
                lesVentesTrie = trierParCategorie(lesVentesTrie, "Outil");
                break;
            default:
                break;
        }
        switch (this.trierStatut.getValue()) {
            case "A venir":
                lesVentesTrie = trierParStatut(lesVentesTrie, "A venir");
                break;
            case "En cours":
                lesVentesTrie = trierParStatut(lesVentesTrie, "En cours");
                break;
            default:
                break;
        }
        return lesVentesTrie;
    }
    
    /**
     * Trie les ventes par la catégorie
     */
    public List<List<String>> trierParCategorie(List<List<String>> lesVentes, String categorie) {
        List<List<String>> lesVentesTrie = new ArrayList<>();
        for (List<String> vente : lesVentes) {
            if (vente.get(9).equals(categorie)) {
                lesVentesTrie.add(vente);
            }
        }
        return lesVentesTrie;
    }

}