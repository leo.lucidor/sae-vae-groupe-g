package vues;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import modeles.Utilisateur;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;

public class MenuTop extends BorderPane {
    
    /**
    * Bouton pour accéder au profil de l'utilisateur.
    */
    private Button boutonProfil;

    /**
    * Bouton pour se déconnecter.
    */
    private Button boutonDeconnexion;

    /**
    * Constructeur de la classe MenuTop.
    *
    * @param userModele   le modèle de l'utilisateur
    * @param textMenu     le label pour le texte du menu
    * @param deconnexion  le bouton de déconnexion
    * @param profil       le bouton du profil
    * @param menu         le bouton du menu
    * @param root         le BorderPane racine
    */
    public MenuTop(Utilisateur userModele, Label textMenu, Button deconnexion, Button profil, Button menu, BorderPane root){
        this.boutonDeconnexion = deconnexion;
        this.boutonProfil = profil;
        this.setBackground(new Background(new BackgroundFill(Color.web("#111111"), new CornerRadii(0), Insets.EMPTY)));
        HBox hBoxTopRight = new HBox();
        HBox hBoxTopLeft = new HBox();
        HBox hBoxDeconnexion = new HBox();
        ImageView imgProfil = userModele.getImageViewUser();
        imgProfil.setFitHeight(30);
        imgProfil.setFitWidth(30);
        // Créer un rectangle avec le rayon souhaité
        double radius = 50; // Changer cette valeur selon votre préférence
        Rectangle clip = new Rectangle(imgProfil.getFitWidth(), imgProfil.getFitHeight());
        clip.setArcWidth(radius * 2);
        clip.setArcHeight(radius * 2);
        // Appliquer le rectangle comme clip à l'ImageView
        imgProfil.setClip(clip);
        boutonProfil.setGraphic(imgProfil);
        boutonProfil.setStyle("-fx-background-color: #111111");
        Label titrePage = new Label("VAE - Vente aux enchères");
        this.boutonDeconnexion.setStyle("-fx-background-color: #111111; -fx-text-fill: #C0B3B0");
        titrePage.setStyle("-fx-text-fill: white;");
        textMenu.setStyle("-fx-text-fill: white;");
        titrePage.setPadding(new Insets(0, 55, 0, 150));
        hBoxTopRight.setPadding(new Insets(5, 0, 0, 0));
        textMenu.setPadding(new Insets(17, 0, 0, 20));
        hBoxDeconnexion.setPadding(new Insets(6, 0, 0, 0));
        Button boutonMenu = menu;
        ImageView navIMG = new ImageView(new Image("file:./images/iconMenuNav.png")); 
        navIMG.setFitHeight(40);
        navIMG.setFitWidth(40);
        boutonMenu.setGraphic(navIMG);
        boutonMenu.setStyle("-fx-background-color: #111111");
        hBoxDeconnexion.getChildren().add(boutonDeconnexion);
        hBoxTopRight.getChildren().addAll(boutonProfil, hBoxDeconnexion);
        hBoxTopLeft.getChildren().addAll(boutonMenu, textMenu);
        // mettre les toolTip
        Tooltip tooltipProfil = new Tooltip();
        tooltipProfil.setText("Page profil");
        //tooltipProfil.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        this.boutonProfil.setTooltip(tooltipProfil);
        Tooltip tooltipDeconnexion = new Tooltip();
        tooltipDeconnexion.setText("Se déconnecter");
        //tooltipDeconnexion.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        this.boutonDeconnexion.setTooltip(tooltipDeconnexion);
        this.setCenter(titrePage);
        this.setRight(hBoxTopRight);
        this.setLeft(hBoxTopLeft);
        root.setTop(this);
    }
}
