package modeles;
import java.sql.SQLException;
import java.util.List;

public class 
Message {

    private BaseDeDonnee bd;
    
    /**
     * le controleur de la classe Message
     */
    public Message(BaseDeDonnee bd) throws SQLException, ClassNotFoundException {
        this.bd = bd;
    }

       /**
     * Envoie un message d'un expéditeur à un destinataire.
     *
     * @param idUtExpediteur l'ID de l'expéditeur
     * @param idUtDestinataire l'ID du destinataire
     * @param message le contenu du message
     * @return true si le message est envoyé avec succès, false sinon
     * @throws SQLException si une erreur de base de données se produit
     * @throws ClassNotFoundException si la classe de base de données n'est pas trouvée
     */
    public boolean envoyerMessage(int idUtExpediteur, int idUtDestinataire, String message) throws SQLException, ClassNotFoundException {
        if(idUtExpediteur != idUtDestinataire){
            this.bd.nouveauMessage(message, idUtExpediteur, idUtDestinataire);
            return true;
        }
        return false;

    }

    /**
     * Récupère tous les messages envoyés par un expéditeur.
     * @param idUtExpediteur l'ID de l'expéditeur
     * @return une liste contenant tous les messages envoyés par l'expéditeur
     * @throws SQLException si une erreur de base de données se produit
     * @throws ClassNotFoundException si la classe de base de données n'est pas trouvée
     */
    public List<List<String>> getMessageExpediteur(int idUtExpediteur) throws SQLException, ClassNotFoundException {
        return this.bd.getMessage("expediteur", idUtExpediteur);
    }
    /**
     * Récupère tous les messages reçus par un destinataire.
     *
     * @param idUtDestinataire l'ID du destinataire
     * @return une liste contenant tous les messages reçus par le destinataire
     * @throws SQLException si une erreur de base de données se produit
     * @throws ClassNotFoundException si la classe de base de données n'est pas trouvée
     */
    public List<List<String>> getMessageDestinataire(int idUtDestinataire) throws SQLException, ClassNotFoundException {
        return this.bd.getMessage("destinataire", idUtDestinataire);
    }

    /**
     * Obtient l'ID d'un utilisateur à partir de son nom.
     *
     * @param nom le nom de l'utilisateur
     * @return l'ID de l'utilisateur
     * @throws SQLException si une erreur de base de données se produit
     * @throws ClassNotFoundException si la classe de base de données n'est pas trouvée
     */
    public int getIdUt(String nom) throws SQLException, ClassNotFoundException {
        return this.bd.getId(nom);
    }

    /**
     * Obtient l'ID d'un utilisateur à partir de l'ID d'un véhicule.
     *
     * @param idVe l'ID du véhicule
     * @return l'ID de l'utilisateur
     * @throws SQLException si une erreur de base de données se produit
     * @throws ClassCastException si une erreur de conversion de classe se produit
     */
    public int getIdUtByIdVe(int idVe) throws SQLException, ClassCastException {
        return this.bd.getIdUserByIdVe(idVe);
    }
}