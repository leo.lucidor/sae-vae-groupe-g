package controleurs;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modeles.Admin;
import vues.ApplicationVAE;

public class ControleurSignalementProfil implements EventHandler<ActionEvent> {
    
    /**
     * modele admin
     */
    private Admin admin;
    /**
     * id de l'utilisateur
     */
    private int idUtilisateur;
    /**
     * id du signalement
     */
    private int idSignalement;
    /**
     * application principale
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur ControleurSignalementProfil
     * @param admin modele admin
     * @param idUtilisateur id de l'utilisateur
     * @param idSignalement id du signalement
     * @param appli application principale
     */
    public ControleurSignalementProfil(Admin admin, int idUtilisateur, int idSignalement, ApplicationVAE appli){
        this.admin = admin;
        this.idUtilisateur = idUtilisateur;
        this.idSignalement = idSignalement;
        this.appli = appli;
    }
    
    /**
     * Action à effectuer lors de l'appui sur le bouton "Supprimer le signalement"
     * @param event 
     */
    public void handle(ActionEvent event){
        try {
            this.admin.desactiverUtilisateur(this.idUtilisateur);
            this.admin.supprimerSignalementProfil(this.idSignalement);
            this.appli.pageSignalement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

