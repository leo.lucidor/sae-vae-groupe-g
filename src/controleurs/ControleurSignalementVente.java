package controleurs;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import modeles.Admin;
import vues.ApplicationVAE;

public class ControleurSignalementVente implements EventHandler<ActionEvent> {
    
    /**
     * modele admin
     */
    private Admin admin;
    /**
     * id du signalement
     */
    private int idSignalement;
    /**
     * id de la vente
     */
    private int idVente;
    /**
     * id de l'utilisateur
     */
    private int idUtilisateur;
    /**
     * application principale
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur de signalement de vente
     * @param admin modele admin
     * @param idSignalement id du signalement
     * @param idVente id de la vente
     * @param idUtilisateur id de l'utilisateur
     * @param appli application principale
     */
    public ControleurSignalementVente(Admin admin, int idSignalement, int idVente, int idUtilisateur, ApplicationVAE appli){
        this.admin = admin;
        this.idSignalement = idSignalement;
        this.idVente = idVente;
        this.idUtilisateur = idUtilisateur;
        this.appli = appli;
    }
    
    /**
     * Methode qui gere les actions sur les boutons
     * @param event evenement
     */
    public void handle(ActionEvent event){
        try{
            Button button = (Button) event.getSource();
            String source = button.getText();
            switch (source){
                case "Supprimer le signalement":
                    this.admin.supprimerSignalementVente(this.idSignalement);
                    this.appli.pageSignalement();
                    break;
                case "Supprimer la vente":
                    this.admin.supprimerVente(this.idVente);
                    this.admin.supprimerSignalementVente(this.idSignalement);
                    this.appli.pageSignalement();
                    break;
                case "Desactiver l'utilisateur":    
                    this.admin.desactiverUtilisateur(this.idUtilisateur);
                    this.admin.supprimerSignalementVente(this.idSignalement);
                    this.appli.pageSignalement();
                    break;
            }
        }
        catch(Exception e){
            System.out.println(e);
        }
    }
}
