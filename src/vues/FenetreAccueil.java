package vues;
import javafx.scene.control.Button;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.image.Image;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;

/**
 * La classe FenetreAccueil représente la fenêtre d'accueil de l'application.
 * Elle étend la classe BorderPane et fournit une interface utilisateur pour se connecter et s'inscrire.
 */
public class FenetreAccueil extends BorderPane {
    
    /**
     * La boîte verticale (VBox) pour les composants de connexion.
     */
    private VBox vBoxConnexion;

    /**
     * La boîte horizontale (HBox) pour les boutons.
     */
    private HBox hBoxBouton;

    /**
     * La boîte horizontale (HBox) pour le bouton "Mot de passe oublié".
     */
    private HBox hBoxMdpOublie;

    /**
     * Le bouton "Connexion".
     */
    private Button boutonConnexion;

    /**
     * Le bouton "Inscription".
     */
    private Button boutonInscription;
    
    /**
     * Le bouton "Mot de passe oublié".
     */
    private Button mdpOublie;

    /**
     * Le champ de texte pour le nom d'utilisateur.
     */
    private TextField identifiant;

    /**
     * Le champ de texte pour le mot de passe.
     */
    private PasswordField motDePasse;

    /**
     * L'étiquette pour afficher un espace vide dans la section de connexion.
     */
    private Label videConnexion;

    /**
     * L'étiquette pour afficher un espace vide dans la section des boutons.
     */
    private Label videBouton;

    /**
     * L'étiquette pour afficher un message d'erreur de connexion.
     */
    private Label mauvaiseConnexion;
    
    /**
     * Construit un objet FenetreAccueil avec les composants spécifiés.
     *
     * @param mdpOublie           le bouton "Mot de passe oublié"
     * @param inscription         le bouton "Inscription"
     * @param connexion           le bouton "Connexion"
     * @param identifiant         le champ de texte pour le nom d'utilisateur
     * @param mdp                 le champ de texte pour le mot de passe
     * @param mauvaiseConnexion   l'étiquette pour afficher un message d'erreur de connexion
     */
    public FenetreAccueil(Button mdpOublie, Button inscription, Button connexion, TextField identifiant, PasswordField mdp, Label mauvaiseConnexion){
        vBoxConnexion = new VBox();
        hBoxBouton = new HBox();
        hBoxMdpOublie = new HBox();
        // création des boutons
        this.boutonConnexion = connexion;
        this.boutonInscription = inscription;
        this.boutonConnexion.getStyleClass().add("button");
        // styles des boutons
        boutonInscription.setStyle("-fx-background-color: #00aef0; -fx-text-fill: black;");
        boutonConnexion.setStyle("-fx-background-color: #00aef0; -fx-text-fill: black;");
        // création des textField
        this.identifiant = identifiant;
        this.motDePasse = mdp;
        this.identifiant.setPromptText("Enter your username");
        this.motDePasse.setPromptText("Enter your password");
        this.identifiant.getStyleClass().add("text");
        this.motDePasse.getStyleClass().add("text");
        // style textfield
        this.identifiant.setStyle( "-fx-background-color: #012245;" + "-fx-border-color: #00aef0;" + "-fx-border-width: 0 0 1 0;" +  "-fx-font-size: 14px;" + "-fx-text-fill: #00aef0; -fx-prompt-text-fill: #00aef0;");
        this.motDePasse.setStyle( "-fx-background-color: #012245;" + "-fx-border-color: #00aef0;" + "-fx-border-width: 0 0 1 0;" +  "-fx-font-size: 14px;" + "-fx-text-fill: #00aef0; -fx-prompt-text-fill: #00aef0;");
        // création des labels vide
        this.videConnexion = new Label("    ");
        this.videBouton = new Label("    ");
        this.mauvaiseConnexion = mauvaiseConnexion;
        this.mdpOublie = mdpOublie;
        this.mauvaiseConnexion.setStyle("-fx-text-fill: red;");
        this.mauvaiseConnexion.setVisible(false);
        this.mdpOublie.setStyle("-fx-text-fill: #00aef0; -fx-background-color: #012245;");
        // ajout des textField
        vBoxConnexion.getChildren().addAll(this.identifiant, this.videConnexion, this.motDePasse, this.mauvaiseConnexion, this.hBoxMdpOublie);
        // ajout des boutons
        hBoxBouton.getChildren().addAll(this.boutonInscription, this.videBouton, this.boutonConnexion);
        hBoxMdpOublie.getChildren().add(this.mdpOublie);
        // placement
        vBoxConnexion.setPadding(new Insets(400, 150, 0, 150));
        hBoxBouton.setPadding(new Insets(0, 0, 100, 250));
        this.hBoxMdpOublie.setPadding(new Insets(15, 0, 0, 120));
        this.mauvaiseConnexion.setPadding(new Insets(10, 0, 0, 60));
        // background
        Image backgroundConnexion = new Image("file:./images/backgroundConnexion.png");
        BackgroundImage backgroundConnexionIMG = new BackgroundImage(backgroundConnexion, BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background backgroundConnexionFinal = new Background(backgroundConnexionIMG);
        this.setBackground(backgroundConnexionFinal);
        // placement
        this.setCenter(vBoxConnexion);
        this.setBottom(hBoxBouton);
    }
}
