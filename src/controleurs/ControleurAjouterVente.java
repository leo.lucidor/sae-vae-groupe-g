package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import javafx.event.ActionEvent;

public class ControleurAjouterVente implements EventHandler<ActionEvent> {
    
    /**
     * L'application (vue) du VAE.
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur
     * @param appli L'application (vue) du VAE.
     */
    public ControleurAjouterVente(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * Méthode de traitement de l'événement.
     * @param event L'événement.
     */
    @Override
    public void handle(ActionEvent event) {
        this.appli.pageAjouterVente();
    }
}