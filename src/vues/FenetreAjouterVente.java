package vues;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import modeles.Utilisateur;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import controleurs.ControleurAjoutPhotosVente;
import controleurs.ControleurCategorieVente;
import controleurs.ControleurValiderVente;
import javafx.animation.Animation;
import javafx.event.EventHandler;
import modeles.Objet;

public class FenetreAjouterVente extends StackPane {

    /**
     * les file chooser pour choisir les images
     */
    private FileChooser.ExtensionFilter imageChoisi1;
    private FileChooser.ExtensionFilter imageChoisi2;
    private FileChooser.ExtensionFilter imageChoisi3;
    private FileChooser.ExtensionFilter imageChoisi4;
    /**
     * les images de la vente
     */
    private ImageView imageVente1;
    private ImageView imageVente2;
    private ImageView imageVente3;
    private ImageView imageVente4;
    /**
     * le modele utilisateur
     */
    private Utilisateur userModele;
    /**
     * le nom du produit
     */
    private TextField nomProduit;
    /**
     * la date de debut de la vente
     */
    private DatePicker dateDebut;
    /**
     * le prix de depart de la vente
     */
    private TextField prixDepart;
    /**
     * le prix minimum de la vente
     */
    private TextField prixMin;
    /**
     * la date de fin de la vente
     */
    private DatePicker dateFin;
    /**
     * la description de la vente
     */
    private TextArea description;
    /**
     * la ville de la vente
     */
    private TextField ville;
    /**
     * la categorie de la vente
     */
    private int categorie;

    /**
     * Constructeur FenetreAjouterVente
     * @param appli l'application
     * @param bpPage le borderpane de la page
     * @param rootTop le borderpane du top
     * @param rootLeft le borderpane du left
     * @param userModele le modele utilisateur
     * @param borderMenuOption le borderpane du menu
     * @param textMenu le label du menu
     * @param textPageAccueil le label de la page d'accueil
     * @param textVendreUnProduit le label de la page vendre un produit
     * @param textVenteAuxEncheres le label de la page vente aux encheres
     * @param textMessagerie le label de la page messagerie
     * @param textMonPanier le label de la page mon panier
     * @param textInformations le label de la page informations
     * @param textSetting le label de la page setting
     * @param textQuitter le label de la page quitter
     * @param quitter le bouton quitter
     * @param deconnexion le bouton deconnexion
     * @param menu le bouton menu
     * @param acceuil le bouton accueil
     * @param message le bouton message
     * @param info le bouton info
     * @param setting le bouton setting
     * @param profil le bouton profil
     * @param enchere le bouton enchere
     * @param panier le bouton panier
     * @param ajouterEnchere le bouton ajouter une enchere
     */
    public FenetreAjouterVente(ApplicationVAE appli, BorderPane bpPage, BorderPane rootTop, BorderPane rootLeft, Utilisateur userModele, BorderPane borderMenuOption, Label textMenu, Label textPageAccueil, Label textVendreUnProduit, Label textVenteAuxEncheres, Label textMessagerie, Label textMonPanier, Label textInformations, Label textSetting, Label textQuitter, Button quitter, Button deconnexion, Button menu,  Button acceuil, Button message, Button info, Button setting, Button profil, Button enchere, Button panier, Button ajouterEnchere){
       // BorderPane bpPage = new BorderPane();
        BorderPane bpCentre = new BorderPane();
        Button reinitialiser = new Button("Réinitialiser");
        Button valider = new Button("Valider");
        Label text = new Label("Vendre un produit");
        Label descri = new Label("Description");
        Label nomPro = new Label("Nom du produit");
        Label dateD = new Label("Début de vente");
        Label prixD = new Label("Prix de départ");
        Label prixM = new Label("Prix minimum");
        Label dateF = new Label("Fin de la vente");
        Label img = new Label("Image du produit");
        BorderPane rootLeftOption = borderMenuOption;
        BorderPane rootLeft2 = new MenuLeft(acceuil, quitter, message, info, setting, enchere, panier, ajouterEnchere, bpPage);
       // BorderPane rootTop = new MenuTop(userModele, textMenu, deconnexion, profil, menu, bpPage);
        HBox hBoxLeft = new HBox();
        HBox lesTextField = new HBox();
        VBox droite = new VBox();
        VBox gauche = new VBox();
        VBox toutGauche = new VBox();
        VBox toutDroite = new VBox();
        HBox bot = new HBox(reinitialiser, valider);
        this.nomProduit = new TextField();
        this.nomProduit.setStyle("-fx-background-color: #BED3C3;");
        this.dateDebut = new DatePicker();
        this.dateDebut.setStyle("-fx-background-color: #BED3C3;");
        this.prixDepart = new TextField();
        this.prixDepart.setStyle("-fx-background-color: #BED3C3;");
        this.prixMin = new TextField();
        this.prixMin.setStyle("-fx-background-color: #BED3C3;");
        this.dateFin = new DatePicker();
        this.dateFin.setStyle("-fx-background-color: #BED3C3;");
        this.description = new TextArea();
        this.description.setStyle("-fx-control-inner-background: #BED3C3;");
        this.ville = new TextField();
        this.ville.setStyle("-fx-background-color: #BED3C3;");

        HBox hBoxVenteIMG1 = new HBox();
        HBox hBoxVenteIMG2 = new HBox();
        this.imageVente1 = new ImageView(new Image("file:./images/ajouterEnchereIMG.png"));
        this.imageVente1.setFitHeight(150);
        this.imageVente1.setFitWidth(250);
        this.imageVente1.setEffect(new DropShadow(BlurType.GAUSSIAN, Color.web("#ffffff"), 10, 0.5, 0, 0));
        this.imageVente2 = new ImageView(new Image("file:./images/ajouterEnchereIMG.png"));
        this.imageVente2.setFitHeight(150);
        this.imageVente2.setFitWidth(250);
        this.imageVente2.setEffect(new DropShadow(BlurType.GAUSSIAN, Color.web("#ffffff"), 10, 0.5, 0, 0));
        this.imageVente3 = new ImageView(new Image("file:./images/ajouterEnchereIMG.png"));
        this.imageVente3.setFitHeight(150);
        this.imageVente3.setFitWidth(250);
        this.imageVente3.setEffect(new DropShadow(BlurType.GAUSSIAN, Color.web("#ffffff"), 10, 0.5, 0, 0));
        this.imageVente4 = new ImageView(new Image("file:./images/ajouterEnchereIMG.png"));
        this.imageVente4.setFitHeight(150);
        this.imageVente4.setFitWidth(250);
        this.imageVente4.setEffect(new DropShadow(BlurType.GAUSSIAN, Color.web("#ffffff"), 10, 0.5, 0, 0));

        // Ajouter des files dans les ImagesView
        this.imageChoisi1 = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG", "*.JPEG", "*.jpeg", "*.JPG", "*.png", "*.PNG");
        this.imageChoisi2 = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG", "*.JPEG", "*.jpeg", "*.JPG", "*.png", "*.PNG");
        this.imageChoisi3 = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG", "*.JPEG", "*.jpeg", "*.JPG", "*.png", "*.PNG");
        this.imageChoisi4 = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG", "*.JPEG", "*.jpeg", "*.JPG", "*.png", "*.PNG");

        // Ajouter des files dans les ImagesView
        imageVente1.setOnMouseClicked(new ControleurAjoutPhotosVente(this, imageChoisi1, 1));
        imageVente2.setOnMouseClicked(new ControleurAjoutPhotosVente(this, imageChoisi2, 2));
        imageVente3.setOnMouseClicked(new ControleurAjoutPhotosVente(this, imageChoisi3, 3));
        imageVente4.setOnMouseClicked(new ControleurAjoutPhotosVente(this, imageChoisi4, 4));

        hBoxVenteIMG1.getChildren().addAll(imageVente1, imageVente2);
        hBoxVenteIMG2.getChildren().addAll(imageVente3, imageVente4);
        hBoxVenteIMG1.setSpacing(10);
        hBoxVenteIMG2.setSpacing(10);
        hBoxVenteIMG1.setPadding(new Insets(100,0,0,40));
        hBoxVenteIMG2.setPadding(new Insets(10,0,0,40));
        reinitialiser.setStyle("-fx-background-color: #CE6A6B");
        valider.setStyle("-fx-background-color: #4A919E");
        valider.setOnAction(new ControleurValiderVente(this, appli.getBd()));
        reinitialiser.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nomProduit.clear();
                dateDebut.getEditor().clear();
                prixDepart.clear();
                prixMin.clear();
                dateFin.getEditor().clear();
                description.clear();
            }
        });

        this.userModele = userModele;

        descri.setStyle("-fx-text-fill: #ffffff;");
        nomPro.setStyle("-fx-text-fill: #ffffff;");
        dateD.setStyle("-fx-text-fill: #ffffff;");
        prixD.setStyle("-fx-text-fill: #ffffff;");
        prixM.setStyle("-fx-text-fill: #ffffff;");
        dateF.setStyle("-fx-text-fill: #ffffff;");
        img.setStyle("-fx-text-fill: #ffffff;");
        nomPro.setFont(new Font("Arial", 20));
        dateD.setFont(new Font("Arial", 20));
        prixD.setFont(new Font("Arial", 20));
        dateF.setFont(new Font("Arial", 20));
        prixM.setFont(new Font("Arial", 20));
        descri.setFont(new Font("Arial", 20));
        img.setFont(new Font("Arial", 20));
        img.setPadding(new Insets(50,0,0,0));
        nomProduit.setPromptText("ex: tracteur"); 
        dateDebut.setPromptText("AAAA/MM/JJ");
        prixDepart.setPromptText("ex: 10 €");
        prixMin.setPromptText("ex: 30 €");
        dateFin.setPromptText("AAAA/MM/JJ");
        description.setPromptText("Une description de l'objet à vendre");
        gauche.getChildren().addAll(nomPro,nomProduit, dateD,dateDebut, prixD,prixDepart);
        droite.getChildren().addAll(dateF,dateFin, prixM,prixMin);
        lesTextField.getChildren().addAll(gauche, droite);
        VBox.setMargin(nomProduit, new Insets(5, 20, 10, 100));
        VBox.setMargin(dateDebut, new Insets(5, 20, 10, 100));
        VBox.setMargin(prixDepart, new Insets(5, 20, 0, 100));
        VBox.setMargin(nomPro, new Insets(30, 50, 0, 100));
        VBox.setMargin(dateD, new Insets(30, 50, 0, 100));
        VBox.setMargin(prixD, new Insets(30, 50, 0, 100));
        VBox.setMargin(dateFin, new Insets(5, 20, 0, 20));
        VBox.setMargin(prixMin, new Insets(5, 20, 0, 20));
        VBox.setMargin(dateF, new Insets(135, 50, 0, 20));
        VBox.setMargin(prixM, new Insets(30, 50, 0, 20));
        toutGauche.getChildren().addAll(lesTextField,descri ,description);
        VBox.setMargin(description, new Insets(10,0,0,100));
        VBox.setMargin(descri, new Insets(50,500,0,100));
        toutGauche.setAlignment(Pos.CENTER_RIGHT);
        bot.setAlignment(Pos.CENTER_RIGHT);
        HBox.setMargin(reinitialiser, new Insets(0,20,80,0));
        HBox.setMargin(valider, new Insets(0,375,80,0));

                // Liste des catégories d'objets
        ObservableList<String> categories = FXCollections.observableArrayList(
                "Vêtements",
                "Meubles",
                "Ustensible de cuisine",
                "Outil"
        );

        // ComboBox des catégories d'objets
        Label labelCategories = new Label("Choisir une catégorie :");
        labelCategories.setPadding(new Insets(100,0,0,190));
        labelCategories.setStyle("-fx-text-fill: #ffffff;");
        labelCategories.setFont(new Font("Arial", 20));
        ComboBox<String> comboBoxCategories = new ComboBox<>(categories);
        comboBoxCategories.setOnAction(new ControleurCategorieVente(this));
        comboBoxCategories.setPromptText("Sélectionner une catégorie");
        comboBoxCategories.setStyle("-fx-background-color: #4A919E"); // Définir une largeur préférée pour la ComboBox
        VBox.setMargin(img, new Insets(0,400,10,200));
        VBox.setMargin(comboBoxCategories, new Insets(30,0,0,190));
        toutDroite.getChildren().addAll(img, hBoxVenteIMG1, hBoxVenteIMG2, labelCategories, comboBoxCategories);

        text.setStyle("-fx-text-fill: #ffffff;");
        text.setFont(new Font("Arial", 35));
        BorderPane.setMargin(text, new Insets(50, 100, 0, 825));
        hBoxLeft.getChildren().addAll(rootLeft2, rootLeftOption);
        bpPage.setLeft(hBoxLeft);
        bpPage.setCenter( bpCentre);
        bpCentre.setTop(text);
        bpCentre.setLeft(toutGauche);
        bpCentre.setRight(toutDroite);
        bpCentre.setBottom(bot);
        bpPage.setTop(rootTop);
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        StackPane.setMargin(rootLeftOption, new Insets(48,0,0,56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);


        // animation photo 1
        // Créer une transition de mise à l'échelle
        ScaleTransition scaleTransitionVente1 = new ScaleTransition(Duration.seconds(1), imageVente1);
        scaleTransitionVente1.setFromX(1.0); // Facteur d'échelle initial en X
        scaleTransitionVente1.setFromY(1.0); // Facteur d'échelle initial en Y
        scaleTransitionVente1.setToX(1.5);   // Facteur d'échelle final en X
        scaleTransitionVente1.setToY(1.5);   // Facteur d'échelle final en Y

        // Créer une transition de translation
        TranslateTransition translateTransitionVente1 = new TranslateTransition(Duration.seconds(1), imageVente1);
        translateTransitionVente1.setFromX(0); // Position X initiale
        translateTransitionVente1.setFromY(0); // Position Y initiale
        translateTransitionVente1.setToX(-imageVente1.getFitWidth() / 3); // Déplacement horizontal vers le coin supérieur gauche
        translateTransitionVente1.setToY(-imageVente1.getFitHeight() / 3); // Déplacement vertical vers le coin supérieur gauche

        // Créer les transitions inversées
        ScaleTransition reverseScaleTransitionVente1 = new ScaleTransition(Duration.seconds(1), imageVente1);
        reverseScaleTransitionVente1.setFromX(1.5); // Facteur d'échelle initial en X (inverse)
        reverseScaleTransitionVente1.setFromY(1.5); // Facteur d'échelle initial en Y (inverse)
        reverseScaleTransitionVente1.setToX(1.0);   // Facteur d'échelle final en X (inverse)
        reverseScaleTransitionVente1.setToY(1.0);   // Facteur d'échelle final en Y (inverse)

        TranslateTransition reverseTranslateTransitionVente1 = new TranslateTransition(Duration.seconds(1), imageVente1);
        reverseTranslateTransitionVente1.setFromX(-imageVente1.getFitWidth() / 3); // Position X initiale (inverse)
        reverseTranslateTransitionVente1.setFromY(-imageVente1.getFitHeight() / 3); // Position Y initiale (inverse)
        reverseTranslateTransitionVente1.setToX(0); // Déplacement horizontal vers le coin supérieur gauche (inverse)
        reverseTranslateTransitionVente1.setToY(0); // Déplacement vertical vers le coin supérieur gauche (inverse)


        // animation photo 2
        // Créer une transition de mise à l'échelle
        ScaleTransition scaleTransitionVente2 = new ScaleTransition(Duration.seconds(1), imageVente2);
        scaleTransitionVente2.setFromX(1.0); // Facteur d'échelle initial en X
        scaleTransitionVente2.setFromY(1.0); // Facteur d'échelle initial en Y
        scaleTransitionVente2.setToX(1.5);   // Facteur d'échelle final en X
        scaleTransitionVente2.setToY(1.5);   // Facteur d'échelle final en Y

        // Créer une transition de translation
        TranslateTransition translateTransitionVente2 = new TranslateTransition(Duration.seconds(1), imageVente2);
        translateTransitionVente2.setFromX(0); // Position X initiale
        translateTransitionVente2.setFromY(0); // Position Y initiale
        translateTransitionVente2.setToX(imageVente2.getFitWidth() / 3); // Déplacement horizontal vers le coin supérieur gauche
        translateTransitionVente2.setToY(-imageVente2.getFitHeight() / 3); // Déplacement vertical vers le coin supérieur gauche

        // Créer les transitions inversées
        ScaleTransition reverseScaleTransitionVente2 = new ScaleTransition(Duration.seconds(1), imageVente2);
        reverseScaleTransitionVente2.setFromX(1.5); // Facteur d'échelle initial en X (inverse)
        reverseScaleTransitionVente2.setFromY(1.5); // Facteur d'échelle initial en Y (inverse)
        reverseScaleTransitionVente2.setToX(1.0);   // Facteur d'échelle final en X (inverse)
        reverseScaleTransitionVente2.setToY(1.0);   // Facteur d'échelle final en Y (inverse)

        TranslateTransition reverseTranslateTransitionVente2 = new TranslateTransition(Duration.seconds(1), imageVente2);
        reverseTranslateTransitionVente2.setFromX(imageVente2.getFitWidth() / 3); // Position X initiale (inverse)
        reverseTranslateTransitionVente2.setFromY(-imageVente2.getFitHeight() / 3); // Position Y initiale (inverse)
        reverseTranslateTransitionVente2.setToX(0); // Déplacement horizontal vers le coin supérieur gauche (inverse)
        reverseTranslateTransitionVente2.setToY(0); // Déplacement vertical vers le coin supérieur gauche (inverse)



        // animation photo 3
        // Créer une transition de mise à l'échelle
        ScaleTransition scaleTransitionVente3 = new ScaleTransition(Duration.seconds(1), imageVente3);
        scaleTransitionVente3.setFromX(1.0); // Facteur d'échelle initial en X
        scaleTransitionVente3.setFromY(1.0); // Facteur d'échelle initial en Y
        scaleTransitionVente3.setToX(1.5);   // Facteur d'échelle final en X
        scaleTransitionVente3.setToY(1.5);   // Facteur d'échelle final en Y

        // Créer une transition de translation
        TranslateTransition translateTransitionVente3 = new TranslateTransition(Duration.seconds(1), imageVente3);
        translateTransitionVente3.setFromX(0); // Position X initiale
        translateTransitionVente3.setFromY(0); // Position Y initiale
        translateTransitionVente3.setToX(-imageVente3.getFitWidth() / 3); // Déplacement horizontal vers le coin supérieur gauche
        translateTransitionVente3.setToY(imageVente3.getFitHeight() / 3); // Déplacement vertical vers le coin supérieur gauche

        // Créer les transitions inversées
        ScaleTransition reverseScaleTransitionVente3 = new ScaleTransition(Duration.seconds(1), imageVente3);
        reverseScaleTransitionVente3.setFromX(1.5); // Facteur d'échelle initial en X (inverse)
        reverseScaleTransitionVente3.setFromY(1.5); // Facteur d'échelle initial en Y (inverse)
        reverseScaleTransitionVente3.setToX(1.0);   // Facteur d'échelle final en X (inverse)
        reverseScaleTransitionVente3.setToY(1.0);   // Facteur d'échelle final en Y (inverse)

        TranslateTransition reverseTranslateTransitionVente3 = new TranslateTransition(Duration.seconds(1), imageVente3);
        reverseTranslateTransitionVente3.setFromX(-imageVente3.getFitWidth() / 3); // Position X initiale (inverse)
        reverseTranslateTransitionVente3.setFromY(imageVente3.getFitHeight() / 3); // Position Y initiale (inverse)
        reverseTranslateTransitionVente3.setToX(0); // Déplacement horizontal vers le coin supérieur gauche (inverse)
        reverseTranslateTransitionVente3.setToY(0); // Déplacement vertical vers le coin supérieur gauche (inverse)



        // animation photo 4
        // Créer une transition de mise à l'échelle
        ScaleTransition scaleTransitionVente4 = new ScaleTransition(Duration.seconds(1), imageVente4);
        scaleTransitionVente4.setFromX(1.0); // Facteur d'échelle initial en X
        scaleTransitionVente4.setFromY(1.0); // Facteur d'échelle initial en Y
        scaleTransitionVente4.setToX(1.5);   // Facteur d'échelle final en X
        scaleTransitionVente4.setToY(1.5);   // Facteur d'échelle final en Y

        // Créer une transition de translation
        TranslateTransition translateTransitionVente4 = new TranslateTransition(Duration.seconds(1), imageVente4);
        translateTransitionVente4.setFromX(0); // Position X initiale
        translateTransitionVente4.setFromY(0); // Position Y initiale
        translateTransitionVente4.setToX(imageVente4.getFitWidth() / 3); // Déplacement horizontal vers le coin supérieur gauche
        translateTransitionVente4.setToY(imageVente4.getFitHeight() / 3); // Déplacement vertical vers le coin supérieur gauche

        // Créer les transitions inversées
        ScaleTransition reverseScaleTransitionVente4 = new ScaleTransition(Duration.seconds(1), imageVente4);
        reverseScaleTransitionVente4.setFromX(1.5); // Facteur d'échelle initial en X (inverse)
        reverseScaleTransitionVente4.setFromY(1.5); // Facteur d'échelle initial en Y (inverse)
        reverseScaleTransitionVente4.setToX(1.0);   // Facteur d'échelle final en X (inverse)
        reverseScaleTransitionVente4.setToY(1.0);   // Facteur d'échelle final en Y (inverse)

        TranslateTransition reverseTranslateTransitionVente4 = new TranslateTransition(Duration.seconds(1), imageVente4);
        reverseTranslateTransitionVente4.setFromX(imageVente4.getFitWidth() / 3); // Position X initiale (inverse)
        reverseTranslateTransitionVente4.setFromY(imageVente4.getFitHeight() / 3); // Position Y initiale (inverse)
        reverseTranslateTransitionVente4.setToX(0); // Déplacement horizontal vers le coin supérieur gauche (inverse)
        reverseTranslateTransitionVente4.setToY(0); // Déplacement vertical vers le coin supérieur gauche (inverse)



        // Ajouter des écouteurs d'événements de souris pour démarrer les transitions au survol de la souris
        imageVente1.setOnMouseEntered(event -> {  
            if(reverseScaleTransitionVente1.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente2.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente3.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente4.getStatus() == Animation.Status.STOPPED && scaleTransitionVente2.getStatus() == Animation.Status.STOPPED && scaleTransitionVente3.getStatus() == Animation.Status.STOPPED && scaleTransitionVente4.getStatus() == Animation.Status.STOPPED){
                reverseScaleTransitionVente1.stop();
                reverseTranslateTransitionVente1.stop();
                scaleTransitionVente1.play();
                translateTransitionVente1.play();
            }
        });
        imageVente1.setOnMouseExited(event -> {
            if(scaleTransitionVente1.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente2.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente3.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente4.getStatus() == Animation.Status.STOPPED && scaleTransitionVente2.getStatus() == Animation.Status.STOPPED && scaleTransitionVente3.getStatus() == Animation.Status.STOPPED && scaleTransitionVente4.getStatus() == Animation.Status.STOPPED){
                scaleTransitionVente1.stop();
                translateTransitionVente1.stop();
                reverseScaleTransitionVente1.play();
                reverseTranslateTransitionVente1.play();
            }
        });

        scaleTransitionVente1.setOnFinished(event -> {
        });

        
        imageVente2.setOnMouseEntered(event -> {  
            if(reverseScaleTransitionVente2.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente1.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente3.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente4.getStatus() == Animation.Status.STOPPED && scaleTransitionVente1.getStatus() == Animation.Status.STOPPED && scaleTransitionVente3.getStatus() == Animation.Status.STOPPED && scaleTransitionVente4.getStatus() == Animation.Status.STOPPED){
                reverseScaleTransitionVente2.stop();
                reverseTranslateTransitionVente2.stop();
                scaleTransitionVente2.play();
                translateTransitionVente2.play();
            }
        });
        imageVente2.setOnMouseExited(event -> {
            if(scaleTransitionVente2.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente1.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente3.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente4.getStatus() == Animation.Status.STOPPED && scaleTransitionVente1.getStatus() == Animation.Status.STOPPED && scaleTransitionVente3.getStatus() == Animation.Status.STOPPED && scaleTransitionVente4.getStatus() == Animation.Status.STOPPED){
                scaleTransitionVente2.stop();
                translateTransitionVente2.stop();
                reverseScaleTransitionVente2.play();
                reverseTranslateTransitionVente2.play();
            }
        });


        imageVente3.setOnMouseEntered(event -> {  
            if(reverseScaleTransitionVente3.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente2.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente1.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente4.getStatus() == Animation.Status.STOPPED && scaleTransitionVente2.getStatus() == Animation.Status.STOPPED && scaleTransitionVente1.getStatus() == Animation.Status.STOPPED && scaleTransitionVente4.getStatus() == Animation.Status.STOPPED){
                reverseScaleTransitionVente3.stop();
                reverseTranslateTransitionVente3.stop();
                scaleTransitionVente3.play();
                translateTransitionVente3.play();
            }
        });
        imageVente3.setOnMouseExited(event -> {
            if(scaleTransitionVente3.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente2.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente1.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente4.getStatus() == Animation.Status.STOPPED && scaleTransitionVente2.getStatus() == Animation.Status.STOPPED && scaleTransitionVente1.getStatus() == Animation.Status.STOPPED && scaleTransitionVente4.getStatus() == Animation.Status.STOPPED){
                scaleTransitionVente3.stop();
                translateTransitionVente3.stop();
                reverseScaleTransitionVente3.play();
                reverseTranslateTransitionVente3.play();
            }
        });

        

        imageVente4.setOnMouseEntered(event -> {  
            if(reverseScaleTransitionVente4.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente2.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente3.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente1.getStatus() == Animation.Status.STOPPED && scaleTransitionVente2.getStatus() == Animation.Status.STOPPED && scaleTransitionVente3.getStatus() == Animation.Status.STOPPED && scaleTransitionVente1.getStatus() == Animation.Status.STOPPED){
                reverseScaleTransitionVente4.stop();
                reverseTranslateTransitionVente4.stop();
                scaleTransitionVente4.play();
                translateTransitionVente4.play();
            }
        });
        imageVente4.setOnMouseExited(event -> {
            if(scaleTransitionVente4.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente2.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente3.getStatus() == Animation.Status.STOPPED && reverseScaleTransitionVente1.getStatus() == Animation.Status.STOPPED && scaleTransitionVente2.getStatus() == Animation.Status.STOPPED && scaleTransitionVente3.getStatus() == Animation.Status.STOPPED && scaleTransitionVente1.getStatus() == Animation.Status.STOPPED){
                scaleTransitionVente4.stop();
                translateTransitionVente4.stop();
                reverseScaleTransitionVente4.play();
                reverseTranslateTransitionVente4.play();
            }
        });

    }

    public String getImageVente1() {
        return this.imageChoisi1.toString();
    }

    public String getImageVente2() {
        return this.imageChoisi2.toString();
    }

    public String getImageVente3() {
        return this.imageChoisi3.toString();
    }

    public String getImageVente4() {
        return this.imageChoisi4.toString();
    }

    public String getNomVente() {
        return this.nomProduit.getText();
    }

    public String getDateDebut(){
        return this.dateDebut.getValue().toString();
    }

    public String getDateFin(){
        return this.dateFin.getValue().toString();
    }

    public int getPrixDepart(){
        return Integer.parseInt(this.prixDepart.getText());
    }

    public int getPrixMin(){
        return Integer.parseInt(this.prixMin.getText());
    }

    public String getDescription(){
        return this.description.getText();
    }

    public String getVille(){
        return this.ville.getText();
    }

    public Utilisateur getUtilisateur(){
        return this.userModele;
    }

    public void ajouterImageVente1(Image imageVente1) {
        this.imageVente1.setImage(imageVente1);
    }

    public void ajouterImageVente2(Image imageVente2) {
        this.imageVente2.setImage(imageVente2);
    }

    public void ajouterImageVente3(Image imageVente3) {
        this.imageVente3.setImage(imageVente3);
    }

    public void ajouterImageVente4(Image imageVente4) {
        this.imageVente4.setImage(imageVente4);
    }

    public Objet getObjetVente() {
        Objet objet = new Objet();
        objet.setNom(this.nomProduit.getText());
        objet.setDescription(this.description.getText());
        objet.setPrix(Integer.parseInt(this.prixDepart.getText()));
        objet.setPrixMin(Integer.parseInt(this.prixMin.getText()));
        objet.setVille(this.ville.getText());
        objet.setDateDebut(this.dateDebut.getValue().toString());
        objet.setDateFin(this.dateFin.getValue().toString());
        objet.setImage1(this.imageChoisi1.toString());
        objet.setImage2(this.imageChoisi2.toString());
        objet.setImage3(this.imageChoisi3.toString());
        objet.setImage4(this.imageChoisi4.toString());
        objet.setIdUtilisateur(this.userModele.getId());
        objet.setCategorie(this.categorie);
        return objet;
    }

    public int setCategorie(int idCategorie){
        return this.categorie = idCategorie;
    }

    public Alert afficherErreurDate(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Erreur");
        alert.setHeaderText("Erreur de date");
        alert.setContentText("La date de fin de vente doit être supérieure à la date de début de vente");
        return alert;
    }
}