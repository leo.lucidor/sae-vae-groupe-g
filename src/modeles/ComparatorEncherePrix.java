package modeles;
import java.util.Comparator;
import java.util.List;

public class ComparatorEncherePrix implements Comparator<List<String>> {
    
    /**
     * Compare deux listes d'enchères selon le prix
     */
    @Override
    public int compare(List<String> l1, List<String> l2) {
        return l1.get(4).compareTo(l2.get(4));
    }
}
