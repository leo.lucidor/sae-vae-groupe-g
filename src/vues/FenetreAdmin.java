package vues;
import java.sql.SQLException;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * La classe FenetreAdmin représente la fenêtre principale de l'interface d'administration.
 * Elle hérite de la classe StackPane.
 * Elle affiche les éléments graphiques et les fonctionnalités spécifiques à l'administration de l'application.
 */
public class FenetreAdmin extends StackPane {

    /**
     * la largeur de l'écran
     */
    private double screenWidth;
    /**
     * la hauteur de l'écran
     */
    private double screenHeight;

    /**
     * Constructeur de la classe FenetreAdmin.
     * Crée une nouvelle instance de FenetreAdmin avec les composants graphiques nécessaires.
     *
     * @param bpPage            Le BorderPane de la page principale.
     * @param rootTop           Le BorderPane de la partie supérieure de la fenêtre.
     * @param rootLeft          Le BorderPane de la partie gauche de la fenêtre.
     * @param borderMenuOption  Le BorderPane de la section des options de menu.
     * @throws SQLException    Une exception SQL peut être levée lors de l'exécution.
     */
    public FenetreAdmin(BorderPane bpPage, BorderPane rootTop, BorderPane rootLeft, BorderPane borderMenuOption) throws SQLException{
       this.setScreenSize();
        //BorderPane bpPage = new BorderPane();
        BorderPane bpPageCenter = new BorderPane();
       // BorderPane rootTop = new MenuTop(userModele, textMenu, deconnexion, profil, menu, bpPage);
        VBox vBoxPresentationAccueil = new VBox();
        BorderPane bpQualite = new BorderPane();
        VBox vBoxQualite1 = new VBox();
        VBox vBoxQualite2 = new VBox();
        VBox vBoxQualite3 = new VBox();
        Label labelQualiteTitre1 = new Label("Rapide");
        Label labelQualiteTitre2 = new Label("Sécurisé");
        Label labelQualiteTitre3 = new Label("Efficace");
        Label labelQualiteTexte1 = new Label("La rapidité de notre site vous permettra de \n       vendre ou d'acheter vos produits \n                en un rien de temps");
        Label labelQualiteTexte2 = new Label("La sécurité de notre site vous permettra de \n       vendre ou d'acheter vos produits \n                  en toute sécurité");
        Label labelQualiteTexte3 = new Label("L'efficacité de notre site vous permettra de \n        vendre ou d'acheter vos produits \n                   en toute tranquilité");
        Label labelPresentationAccueilTitre = new Label("Trouvez la chose qu\u2019il vous faut");
        Label labelPresentationAccueil = new Label("\n \n            Vous faites partis des heureux élus administrateur \n              Vous avez pour mission de gérer l'application");
        ImageView imgAccueil = new ImageView("file:./images/accueilUser.png");
        // permet d'adapter les pixels de l'image à la taille de l'écran
        imgAccueil.setPreserveRatio(true);
        imgAccueil.setFitWidth(this.screenWidth*0.5); // Ou utilisez une valeur en pourcentage, comme : setFitWidth(parent.getWidth() * 0.5);
        imgAccueil.setFitHeight(this.screenHeight*0.5); // Ou utilisez une valeur en pourcentage, comme : setFitHeight(parent.getHeight() * 0.5);
        // change la couleur de fond de la fenêtre
        this.setBackground(new Background(new BackgroundFill(Color.web("#012345"), new CornerRadii(0), Insets.EMPTY)));
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        hBoxLeft.getChildren().addAll(rootLeft);
        bpPage.setLeft(hBoxLeft);
        bpPage.setTop(rootTop);
        bpPage.setCenter(bpPageCenter);
        bpPageCenter.setLeft(vBoxPresentationAccueil);
        bpPageCenter.setRight(imgAccueil);
        bpPageCenter.setBottom(bpQualite);
        vBoxPresentationAccueil.setStyle("-fx-background-color: #D9D9D9");
        bpQualite.setStyle("-fx-background-color: #4A919E");
        vBoxQualite1.setPadding(new Insets(0,0,0,30));
        vBoxQualite3.setPadding(new Insets(0,30,0,0));
        vBoxPresentationAccueil.getChildren().addAll(labelPresentationAccueilTitre, labelPresentationAccueil);
        bpQualite.setPrefHeight(this.screenHeight*0.5-100);
        vBoxPresentationAccueil.setPrefHeight(this.screenHeight*0.5-60);
        vBoxPresentationAccueil.setPadding(new Insets(100,50,0,50));
        bpQualite.setLeft(vBoxQualite1);
        bpQualite.setCenter(vBoxQualite2);
        bpQualite.setRight(vBoxQualite3);
        vBoxQualite1.setAlignment(Pos.CENTER);
        vBoxQualite2.setAlignment(Pos.CENTER);
        vBoxQualite3.setAlignment(Pos.CENTER);
        vBoxQualite1.getChildren().addAll(labelQualiteTitre1, labelQualiteTexte1);
        vBoxQualite2.getChildren().addAll(labelQualiteTitre2, labelQualiteTexte2);
        vBoxQualite3.getChildren().addAll(labelQualiteTitre3, labelQualiteTexte3);
        labelQualiteTitre1.setStyle("-fx-text-fill: #FFFFFF; -fx-font-size: 20px; -fx-font-weight: bold;");
        labelQualiteTitre2.setStyle("-fx-text-fill: #FFFFFF; -fx-font-size: 20px; -fx-font-weight: bold;");
        labelQualiteTitre3.setStyle("-fx-text-fill: #FFFFFF; -fx-font-size: 20px; -fx-font-weight: bold;");
        labelQualiteTexte1.setStyle("-fx-text-fill: #FFFFFF; -fx-font-size: 15px;");
        labelQualiteTexte2.setStyle("-fx-text-fill: #FFFFFF; -fx-font-size: 15px;");
        labelQualiteTexte3.setStyle("-fx-text-fill: #FFFFFF; -fx-font-size: 15px;");
        labelPresentationAccueilTitre.setStyle("-fx-text-fill: #000000; -fx-font-size: 30px; -fx-font-weight: bold;");
        labelPresentationAccueil.setStyle("-fx-text-fill: #000000; -fx-font-size: 15px;");
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        StackPane.setMargin(rootLeftOption, new Insets(48,0,0,56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);
    }

    /**
     * Méthode pour définir la taille de l'écran.
     * Récupère la largeur et la hauteur de l'écran principal et les stocke dans les attributs screenWidth et screenHeight.
     */
    public void setScreenSize(){
        ObservableList<Screen> screens = Screen.getScreens();
        if (!screens.isEmpty()) {
            Screen primaryScreen = screens.get(0); // Récupérer l'écran principal
            // Récupérer la largeur et la hauteur de l'écran principal en pixels
            this.screenWidth = primaryScreen.getBounds().getWidth();
            this.screenHeight = primaryScreen.getBounds().getHeight();
        } else {
            System.out.println("Aucun écran disponible.");
        }
    }
}

