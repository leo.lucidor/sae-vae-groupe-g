package controleurs;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import vues.ApplicationVAE;
import modeles.Message;
import modeles.Utilisateur;
import vues.FenetreMessage;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurCreerBulleMessage implements EventHandler<ActionEvent> {
    
    /**
     * modele du message
     */
    private Message messageModele;
    /**
     * vue du message
     */
    private FenetreMessage fenetreMessage;
    /**
     * modele de l'utilisateur
     */
    private Utilisateur utilisateur;
    
    /**
     * Constructeur du controleur qui permet de créer une bulle de message
     * @param appli application
     * @param messageModele modele du message
     * @param fenetreMessage vue du message
     * @param utilisateur modele de l'utilisateur
     */
    public ControleurCreerBulleMessage(ApplicationVAE appli, Message messageModele, FenetreMessage fenetreMessage, Utilisateur utilisateur){
        this.messageModele = messageModele;
        this.fenetreMessage = fenetreMessage;
        this.utilisateur = utilisateur;
    }

    /**
     * Permet de gérer les actions sur les boutons
     * @param event evenement
     */
    @Override
    public void handle(ActionEvent event) {
        try {
            Button bouton = (Button) event.getSource();
            String boutonText = bouton.getText();
            if(boutonText.equals("Premier message")){
                String nom = this.fenetreMessage.getTextFieldChercher();
                int idUtDestinataire = messageModele.getIdUt(nom);
                this.messageModele.envoyerMessage(utilisateur.getId(), idUtDestinataire, "Bonjour ! Je suis " + utilisateur.getPseudo() + " et je suis intéressé par votre profil.");
                this.fenetreMessage.setIdUtDestinataire(idUtDestinataire);
                this.fenetreMessage.afficherMessages();
            }
            else {
                int idUtDestinataire = messageModele.getIdUt(boutonText);
                this.fenetreMessage.setIdUtDestinataire(idUtDestinataire);
                this.fenetreMessage.afficherMessages();
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
