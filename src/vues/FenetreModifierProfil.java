package vues;
import controleurs.ControleurBoutonsModifierProfil;
import controleurs.ControleurModifierProfil;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import modeles.Utilisateur;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class FenetreModifierProfil extends StackPane{
    
    private TextField nom;
    private TextField prenom;
    private TextField pseudo;
    private TextField age;
    private TextField email;
    private TextField numero;
    private TextArea description;
    private Utilisateur userModele;
    private ToggleGroup toggleGroup;

    public FenetreModifierProfil(BorderPane bpPage, BorderPane rootTop, BorderPane rootLeftAdmin, Utilisateur userModele, ApplicationVAE appli, BorderPane borderMenuOption, Label textMenu, Label textPageAccueil, Label textVendreUnProduit, Label textVenteAuxEncheres, Label textMessagerie, Label textMonPanier, Label textInformations, Label textSetting, Label textQuitter, Button quitter, Button deconnexion, Button menu,  Button acceuil, Button message, Button info, Button setting, Button profil, Button enchere, Button panier, Button ajouterEnchere){
        //BorderPane bpPage = new BorderPane();
        this.userModele = userModele;
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        Label text = new Label("profil");
        text.setStyle("-fx-text-fill: #00aef0;");
        bpPage.setCenter(text);
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        BorderPane rootLeft = new BorderPane();
        if(userModele.getRole() == 2){
            rootLeft = new MenuLeft(acceuil, quitter, message, info, setting, enchere, panier, ajouterEnchere, bpPage);
        }
        else {
            rootLeft = rootLeftAdmin;
        }
        hBoxLeft.getChildren().addAll(rootLeft, rootLeftOption);
        bpPage.setLeft(hBoxLeft);
      //  BorderPane rootTop = new MenuTop(userModele, textMenu, deconnexion, profil, menu, bpPage);
        bpPage.setTop(rootTop);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        StackPane.setMargin(rootLeftOption, new Insets(48,0,0,56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);
        
        // BorderPane principal du profil
        BorderPane zonePrincipal = new BorderPane();

        //Le top
        VBox vbTop = new VBox();
        vbTop.setAlignment(Pos.TOP_CENTER);
        Text titrePage = new Text("Profil");
        titrePage.setTextAlignment(TextAlignment.CENTER);
        titrePage.setFill(Color.WHITE);
        titrePage.setFont(Font.font("arial", FontWeight.BOLD, FontPosture.REGULAR, 50));
        VBox.setMargin(titrePage, new Insets(25,0,25,0));

        vbTop.getChildren().addAll(titrePage);
        zonePrincipal.setTop(vbTop);


        // Le center
        VBox center = new VBox();
        // Création des conteneurs principaux
        HBox infosPrincipales = new HBox();
        HBox infosImportantes = new HBox();
        infosPrincipales.setPadding(new Insets(20,0,50,20));
        infosImportantes.setPadding(new Insets(20,0,50,20));
        VBox rectangle = new VBox(30);
        Rectangle blocSeparateur = new Rectangle(250,2, Color.ALICEBLUE);
        VBox infosDeContact = new VBox();
        VBox.setMargin(infosDeContact, new Insets(20));
        VBox.setMargin(blocSeparateur, new Insets(30));
        rectangle.setAlignment(Pos.CENTER);

        //Création des Text du profil
        Text titreNom = new Text("Nom* :");
        titreNom.setFill(Color.WHITE);
        titreNom.setFont(Font.font("arial", FontWeight.BOLD, 20));
        this.nom = new TextField(userModele.getNom());
        nom.setStyle("-fx-background-color: #BED3C3;");
        HBox.setMargin(nom, new Insets(0,75,0,0));
       

        Text titrePrenom = new Text("Prénom* :");
        titrePrenom.setFill(Color.WHITE);
        titrePrenom.setFont(Font.font("arial", FontWeight.BOLD, 20));
        this.prenom = new TextField(userModele.getPrenom());
        prenom.setStyle("-fx-background-color: #BED3C3;");
        HBox.setMargin(prenom, new Insets(0,75,0,0));
      

        Text titrePseudo = new Text("Pseudo* :");
        titrePseudo.setFill(Color.WHITE);
        titrePseudo.setFont(Font.font("arial", FontWeight.BOLD, 20));
        this.pseudo = new TextField(userModele.getPseudo());
        pseudo.setStyle("-fx-background-color: #BED3C3;");
        HBox.setMargin(pseudo, new Insets(0,75,0,0));
    

        Text titreAge = new Text("Age :");
        titreAge.setFill(Color.WHITE);
        titreAge.setFont(Font.font("arial", FontWeight.BOLD, 20));
        this.age = new TextField(String.valueOf(userModele.getAge()));
        age.setStyle("-fx-background-color: #BED3C3;");
        HBox.setMargin(age, new Insets(0,75,0,0));
        

        Text titreGenre = new Text("Genre :");
        titreGenre.setFill(Color.WHITE);
        titreGenre.setFont(Font.font("arial", FontWeight.BOLD, 20));
        HBox.setMargin(titreGenre, new Insets(0,20,0,0));
        
        this.toggleGroup = new ToggleGroup();
        RadioButton homme = new RadioButton("Homme");
        homme.setTextFill(Color.rgb(255,255,255));
        homme.setSelected(true);
        HBox.setMargin(homme, new Insets(0,20,0,0));
        RadioButton femme = new RadioButton("Femme");
        femme.setTextFill(Color.rgb(255,255,255));
        toggleGroup.getToggles().addAll(homme,femme);


        Text titreEmail = new Text("Adresse Mail* :");
        titreEmail.setFill(Color.WHITE);
        titreEmail.setFont(Font.font("arial", FontWeight.BOLD, 20));
        this.email = new TextField(userModele.getMail());
        email.setStyle("-fx-background-color: #BED3C3;");
       

        Text titreNumero = new Text("N° Téléphone :");
        titreNumero.setFill(Color.WHITE);
        titreNumero.setFont(Font.font("arial", FontWeight.BOLD, 20));
        this.numero = new TextField(userModele.getTel());
        numero.setStyle("-fx-background-color: #BED3C3;");
       

        Text titreDescription = new Text("Description :");
        titreDescription.setFill(Color.WHITE);
        titreDescription.setFont(Font.font("arial", FontWeight.BOLD, 20));
        this.description = new TextArea("");
        description.setStyle("-fx-control-inner-background: #BED3C3;");
    

        // Espacement dans la VBox center
        VBox.setMargin(titreEmail, new Insets(20));
        VBox.setMargin(titreNumero, new Insets(20));
        VBox.setMargin(titreDescription, new Insets(20));

        //création du bouton retour
        Button retour = new Button("Retour");
        retour.setStyle("-fx-background-color: #CE6A6B;");
        retour.setOnAction(new ControleurBoutonsModifierProfil(this, appli));
        retour.setMinWidth(200);
        retour.setMinHeight(25);
        VBox.setMargin(retour, new Insets(25,25,25,25));
        
        // Insertions des Text dans les conteneurs
        infosPrincipales.getChildren().addAll(titreNom, nom, titrePrenom, prenom, titrePseudo, pseudo);
        infosImportantes.getChildren().addAll(titreAge, age, titreGenre,homme,femme);
        infosDeContact.getChildren().addAll(titreEmail, email, titreNumero, numero, titreDescription, description);

        rectangle.getChildren().add(blocSeparateur);

        // Insertion des éléments dans la VBox principale
        center.getChildren().addAll(infosPrincipales, infosImportantes,rectangle, infosDeContact, retour);

        // Insertion de la Vbox principale dans le BorderPane
        zonePrincipal.setCenter(center);
        
        // Le Right
        VBox vbRight = new VBox();
        vbRight.setPadding(new Insets(5, 150, 5, 5));
        
        
        //Elements du conteneur "vbRight"
        VBox vbPP = new VBox();
        VBox vbTypes = new VBox();
        GridPane likes = new GridPane();

        //ELements du conteneur "PP"
        vbPP.setAlignment(Pos.TOP_CENTER);
        Text infoPP = new Text("Photo de profil");
        infoPP.setFont(Font.font("arial", FontPosture.REGULAR, 23));
        infoPP.setFill(Color.WHITE);
        ImageView pp = new ImageView(userModele.getImageUser());
        pp.setFitHeight(300);
        pp.setFitWidth(300);
        Button modifierPP = new Button();
        modifierPP.setGraphic(pp);
        modifierPP.setOnAction(new ControleurModifierProfil(appli));
        modifierPP.setStyle("-fx-background-color: #012345;");
        vbPP.getChildren().addAll(infoPP,modifierPP);

        //Elements du conteneur "types"
        vbTypes.setAlignment(Pos.CENTER);
        HBox hbTitreTypes = new HBox();
        hbTitreTypes.setPadding(new Insets(0,0,25,0));
        hbTitreTypes.setAlignment(Pos.CENTER); // Alignement horizontal au centre
        Text infoType = new Text("Type de produit vendu");
        infoType.setTextAlignment(TextAlignment.CENTER);
        infoType.setFont(Font.font("arial", FontPosture.REGULAR, 20));        
        infoType.setFill(Color.WHITE);
        hbTitreTypes.getChildren().add(infoType);

        ComboBox<String> categories = new ComboBox<>();
        categories.getItems().addAll("Vêtements", "Meuble", "Ustensiles de cuisine", "Outils");
        categories.setValue("Veuillez choisir ");
        HBox types = new HBox(20);
        types.setAlignment(Pos.CENTER);
        types.getChildren().add(categories);

        vbTypes.getChildren().addAll(hbTitreTypes, types);

        //Elements du conteneur "likes"
        likes.setAlignment(Pos.CENTER);
        ImageView pLikes = new ImageView(new Image("file:./images/iconLike.png"));
        pLikes.setFitHeight(200);
        pLikes.setFitWidth(200);
        Label nbLikes = new Label(String.valueOf(userModele.getLike()));
        nbLikes.setTextFill(Color.rgb(255,255,255));
        nbLikes.setFont(Font.font("arial", FontPosture.REGULAR, 30));
        nbLikes.setPadding(new Insets(5,5,5,5));


        likes.add(pLikes, 0, 0);
        likes.add(nbLikes, 1, 0);

        // création des boutons de retour, validation et réinitialisation
        HBox conteneurBoutons = new HBox();
        Button valider = new Button("Valider");
        valider.setStyle("-fx-background-color: #4A919E;");
        Button reinitialiser = new Button("Réinitialiser");
        valider.setMinWidth(200);
        valider.setMinHeight(25);
        HBox.setMargin(valider, new Insets(25,25,25,25));
        reinitialiser.setStyle("-fx-background-color: #CE6A6B");
        reinitialiser.setMinWidth(200);
        reinitialiser.setMinHeight(25);
        HBox.setMargin(reinitialiser, new Insets(25,25,25,25));
        valider.setOnAction(new ControleurBoutonsModifierProfil(this, appli));
        reinitialiser.setOnAction(new ControleurBoutonsModifierProfil(this, appli));
        HBox.setMargin(reinitialiser, new Insets(15));
        HBox.setMargin(valider, new Insets(15));

        conteneurBoutons.getChildren().addAll(reinitialiser, valider);
        vbRight.getChildren().addAll(vbPP, vbTypes,likes, conteneurBoutons);


        zonePrincipal.setRight(vbRight);
        bpPage.setCenter(zonePrincipal);
    }

    public void resetText(){
        this.nom.clear();
        this.prenom.clear();
        this.pseudo.clear();
        this.age.clear();
        this.email.clear();
        this.numero.clear();
        this.description.clear();
    }

    public String getNom(){
        return this.nom.getText();
    }

    public String getPrenom(){
        return this.prenom.getText();
    }

    public String getPseudo(){
        return this.pseudo.getText();
    }

    public int getAge(){
        return Integer.parseInt(this.age.getText());
    }

    public String getEmail(){
        return this.email.getText();
    }

    public String getNumero(){
        return this.numero.getText();
    }

    public String getDescription(){
        return this.description.getText();
    }

    public String getGenre(){
        RadioButton selectedRadioButton = (RadioButton) this.toggleGroup.getSelectedToggle();
        String selectedText = selectedRadioButton.getText();
        return selectedText;
    }

    public Utilisateur getUserModele(){
        return this.userModele;
    }
}
