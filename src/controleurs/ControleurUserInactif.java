package controleurs;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import modeles.Admin;
import vues.ApplicationVAE;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurUserInactif implements EventHandler<ActionEvent> {
    
    /**
     * modele admin
     */
    private Admin adminModele;
    /**
     * id de l'utilisateur
     */
    private int idUtilisateur;
    /**
     * application principale
     */
    private ApplicationVAE appli;
    
    /**
     * Constructeur du controleur de la page UserInactif
     * @param adminModele modele admin
     * @param idUtilisateur id de l'utilisateur
     * @param appli application principale
     */
    public ControleurUserInactif(Admin adminModele, int idUtilisateur, ApplicationVAE appli){
        this.adminModele = adminModele;
        this.idUtilisateur = idUtilisateur;
        this.appli = appli;
    }

    /**
     * Méthode qui gère les actions sur les boutons de la page UserInactif
     * @param event 
     */
    @Override
    public void handle(ActionEvent event) {
        Button b = (Button) event.getSource();
        String source = b.getText();
        switch(source){
            case "Réactiver l'utilisateur":
                try{
                    System.out.println("Réactivation de l'utilisateur " + this.idUtilisateur);
                    this.adminModele.activerUtilisateur(this.idUtilisateur);
                    this.appli.pageUserInactif();
                }catch(SQLException e){
                    System.out.println(e);
                }
                break;
            case "Surpprimer l'utilisateur":
                try{
                    System.out.println("Suppression de l'utilisateur " + this.idUtilisateur);
                    this.adminModele.supprimerUtilisateur(this.idUtilisateur);
                    this.appli.pageUserInactif();
                }catch(SQLException e){
                    System.out.println(e);
                }
                break;
        }
    }
}