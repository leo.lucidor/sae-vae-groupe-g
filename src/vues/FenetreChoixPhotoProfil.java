package vues;
import java.io.File;
import java.sql.SQLException;
import controleurs.ControleurChoixPhotoProfil;
import controleurs.ControleurModifierProfil;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import modeles.Utilisateur;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;


/**
 * Classe représentant une fenêtre graphique permettant à l'utilisateur de choisir une photo de profil.
 * Cette classe étend javafx.scene.layout.StackPane.
 * Elle contient plusieurs éléments graphiques tels que des étiquettes, des boutons et des conteneurs.
 */
public class FenetreChoixPhotoProfil extends StackPane {

    /**
     * Un String représentant le chemin pour accéder à la photo de profil de l'utilisateur
     */
    private String lienPhoto;

    /**
 * Constructeur de la classe FenetreChoixPhotoProfil.
 * Initialise une nouvelle instance de la classe FenetreChoixPhotoProfil avec les paramètres spécifiés.
 *
 * @param bpPage             Le BorderPane principal de la page.
 * @param rootTop            Le BorderPane du menu supérieur.
 * @param rootLeftAdmin      Le BorderPane du menu latéral gauche pour l'administrateur.
 * @param userModele         Le modèle de l'utilisateur utilisé pour obtenir les informations.
 * @param appli              L'instance de l'application principale.
 * @param borderMenuOption   Le BorderPane du menu d'options.
 * @param textMenu           L'étiquette du menu.
 * @param textPageAccueil    L'étiquette de la page d'accueil.
 * @param textVendreUnProduit    L'étiquette de la page de vente d'un produit.
 * @param textVenteAuxEncheres   L'étiquette de la page des ventes aux enchères.
 * @param textMessagerie         L'étiquette de la page de messagerie.
 * @param textMonPanier          L'étiquette de la page de panier.
 * @param textInformations       L'étiquette de la page d'informations.
 * @param textSetting            L'étiquette de la page de paramètres.
 * @param textQuitter            L'étiquette de l'option de quitter.
 * @param quitter                Le bouton de quitter.
 * @param deconnexion            Le bouton de déconnexion.
 * @param menu                   Le bouton de menu.
 * @param acceuil                Le bouton de retour à l'accueil.
 * @param message                Le bouton de messagerie.
 * @param info                   Le bouton d'informations.
 * @param setting                Le bouton de paramètres.
 * @param profil                 Le bouton de profil.
 * @param enchere                Le bouton de vente aux enchères.
 * @param panier                 Le bouton de panier.
 * @param ajouterEnchere         Le bouton d'ajout d'enchère.
 */
    public FenetreChoixPhotoProfil(BorderPane bpPage, BorderPane rootTop, BorderPane rootLeftAdmin, Utilisateur userModele, ApplicationVAE appli, BorderPane borderMenuOption, Label textMenu, Label textPageAccueil, Label textVendreUnProduit, Label textVenteAuxEncheres, Label textMessagerie, Label textMonPanier, Label textInformations, Label textSetting, Label textQuitter, Button quitter, Button deconnexion, Button menu,  Button acceuil, Button message, Button info, Button setting, Button profil, Button enchere, Button panier, Button ajouterEnchere){
      //  BorderPane bpPage = new BorderPane();
        BorderPane bpPageCentrale = new BorderPane();
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        BorderPane rootLeft = new BorderPane();
        if(userModele.getRole() == 2){
            rootLeft = new MenuLeft(acceuil, quitter, message, info, setting, enchere, panier, ajouterEnchere, bpPage);
        }
        else {
            rootLeft = rootLeftAdmin;
        }
        hBoxLeft.getChildren().addAll(rootLeft, rootLeftOption);
        bpPage.setLeft(hBoxLeft);
        //BorderPane rootTop = new MenuTop(userModele, textMenu, deconnexion, profil, menu, bpPage);
        bpPage.setTop(rootTop);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        StackPane.setMargin(rootLeftOption, new Insets(48,0,0,56));
        StackPane.setAlignment(rootLeftOption, Pos.TOP_LEFT);
        // configuration de la page centrale
        TilePane tilePanePhotoBasic = new TilePane();
        tilePanePhotoBasic.setPadding(new Insets(15, 15, 15, 15));
        // configuration des boutons de choix de photo
        ImageView photoBasic1 = new ImageView(new Image("file:./images/photo_UT/image1.png"));
        photoBasic1.setFitHeight(300);
        photoBasic1.setFitWidth(300);
        Button boutonPhotoBasic1 = new Button();
        boutonPhotoBasic1.setGraphic(photoBasic1);
        boutonPhotoBasic1.setStyle("-fx-background-color: #012345;");
        boutonPhotoBasic1.setOnAction(new ControleurChoixPhotoProfil(appli, userModele, 1));
        ImageView photoBasic2 = new ImageView(new Image("file:./images/photo_UT/image2.png"));
        photoBasic2.setFitHeight(300);
        photoBasic2.setFitWidth(300);
        Button boutonPhotoBasic2 = new Button();
        boutonPhotoBasic2.setGraphic(photoBasic2);
        boutonPhotoBasic2.setStyle("-fx-background-color: #012345;");
        boutonPhotoBasic2.setOnAction(new ControleurChoixPhotoProfil(appli, userModele, 2));
        ImageView photoBasic3 = new ImageView(new Image("file:./images/photo_UT/image3.png"));
        photoBasic3.setFitHeight(300);
        photoBasic3.setFitWidth(300);
        Button boutonPhotoBasic3 = new Button();
        boutonPhotoBasic3.setGraphic(photoBasic3);
        boutonPhotoBasic3.setStyle("-fx-background-color: #012345;");
        boutonPhotoBasic3.setOnAction(new ControleurChoixPhotoProfil(appli, userModele, 3));
        ImageView photoBasic4 = new ImageView(new Image("file:./images/photo_UT/image4.png"));
        photoBasic4.setFitHeight(300);
        photoBasic4.setFitWidth(300);
        Button boutonPhotoBasic4 = new Button();
        boutonPhotoBasic4.setGraphic(photoBasic4);
        boutonPhotoBasic4.setStyle("-fx-background-color: #012345;");
        boutonPhotoBasic4.setOnAction(new ControleurChoixPhotoProfil(appli, userModele, 4));
        ImageView photoBasic5 = new ImageView(new Image("file:./images/photo_UT/image5.png"));
        photoBasic5.setFitHeight(300);
        photoBasic5.setFitWidth(300);
        Button boutonPhotoBasic5 = new Button();
        boutonPhotoBasic5.setGraphic(photoBasic5);
        boutonPhotoBasic5.setStyle("-fx-background-color: #012345;");
        boutonPhotoBasic5.setOnAction(new ControleurChoixPhotoProfil(appli, userModele, 5));
        ImageView photoBasic6 = new ImageView(new Image("file:./images/photo_UT/image6.png"));
        photoBasic6.setFitHeight(300);
        photoBasic6.setFitWidth(300);
        Button boutonPhotoBasic6 = new Button();
        boutonPhotoBasic6.setGraphic(photoBasic6);
        boutonPhotoBasic6.setStyle("-fx-background-color: #012345;");
        boutonPhotoBasic6.setOnAction(new ControleurChoixPhotoProfil(appli, userModele, 6));
        ImageView photoBasic7 = new ImageView(new Image("file:./images/photo_UT/image7.png"));
        photoBasic7.setFitHeight(300);
        photoBasic7.setFitWidth(300);
        Button boutonPhotoBasic7 = new Button();
        boutonPhotoBasic7.setGraphic(photoBasic7);
        boutonPhotoBasic7.setStyle("-fx-background-color: #012345;");
        boutonPhotoBasic7.setOnAction(new ControleurChoixPhotoProfil(appli, userModele, 7));
        ImageView photoBasic8 = new ImageView(new Image("file:./images/photo_UT/image8.png"));
        photoBasic8.setFitHeight(300);
        photoBasic8.setFitWidth(300);
        Button boutonPhotoBasic8 = new Button();
        boutonPhotoBasic8.setGraphic(photoBasic8);
        boutonPhotoBasic8.setStyle("-fx-background-color: #012345;");
        boutonPhotoBasic8.setOnAction(new ControleurChoixPhotoProfil(appli, userModele, 8));
        ImageView photoBasic9 = new ImageView(new Image("file:./images/photo_UT/image9.png"));
        photoBasic9.setFitHeight(300);
        photoBasic9.setFitWidth(300);
        Button boutonPhotoBasic9 = new Button();
        boutonPhotoBasic9.setGraphic(photoBasic9);
        boutonPhotoBasic9.setStyle("-fx-background-color: #012345;");
        boutonPhotoBasic9.setOnAction(new ControleurChoixPhotoProfil(appli, userModele, 9));
        ImageView photoBasic10 = new ImageView(new Image("file:./images/photo_UT/image10.png"));
        photoBasic10.setFitHeight(300);
        photoBasic10.setFitWidth(300);
        Button boutonPhotoBasic10 = new Button();
        boutonPhotoBasic10.setGraphic(photoBasic10);
        boutonPhotoBasic10.setStyle("-fx-background-color: #012345;");
        boutonPhotoBasic10.setOnAction(new ControleurChoixPhotoProfil(appli, userModele, 10));
        tilePanePhotoBasic.getChildren().addAll(boutonPhotoBasic1, boutonPhotoBasic2, boutonPhotoBasic3, boutonPhotoBasic4, boutonPhotoBasic5, boutonPhotoBasic6, boutonPhotoBasic7, boutonPhotoBasic8, boutonPhotoBasic9, boutonPhotoBasic10);
        tilePanePhotoBasic.setHgap(25);
        tilePanePhotoBasic.setVgap(25);
        tilePanePhotoBasic.setAlignment(Pos.CENTER);
        VBox vBoxPhoto = new VBox();
        Label labelPhoto = new Label("Choisissez une photo parmi les photos de base : ");
        labelPhoto.setPadding(new Insets(25, 25, 25, 25));
        labelPhoto.setStyle("-fx-text-fill: #FFFFFF; -fx-font-size: 30px;");
        labelPhoto.setAlignment(Pos.CENTER);
        vBoxPhoto.getChildren().addAll(labelPhoto, tilePanePhotoBasic);
        VBox vBoxPhotoPerso = new VBox();
        HBox hBoxBoutonRetour = new HBox();
        Button boutonRetour = new Button("Retour");
        boutonRetour.setStyle("-fx-background-color: #CE6A6B");
        boutonRetour.setOnAction(new ControleurModifierProfil(appli));
        hBoxBoutonRetour.setPadding(new Insets(0, 0, 40, 40));
        HBox hBoxFileChooser = new HBox();
        Label labelFileChooser = new Label("Ou sélectionnez une photo depuis votre ordinateur : ");
        labelFileChooser.setPadding(new Insets(0, 25, 75, 25));
        labelFileChooser.setStyle("-fx-text-fill: #FFFFFF; -fx-font-size: 16px;");
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Sélectionnez un fichier");
        fileChooser.getExtensionFilters().addAll(
            new ExtensionFilter("Images", "*.png")
        );
        Button openButton = new Button("Choisir un fichier");
        openButton.setStyle("-fx-background-color: #4A919E;");
        openButton.setOnAction(event -> {
            File file = fileChooser.showOpenDialog(null); // Affiche la boîte de dialogue de sélection de fichier
            if (file != null) {
                // Faire quelque chose avec le fichier sélectionné, par exemple l'afficher dans la console
                this.lienPhoto = file.getAbsolutePath();
                try {
                    userModele.setIdPhotoPersonaliser(lienPhoto, userModele.nbMaxIdPhoto()+1);
                    userModele.inserePhotoPersoUtilisateur("file:"+lienPhoto);
                    userModele.majInfoUtilisateur(userModele.getPseudo(), userModele.getMail(), userModele.getMdp(), userModele.getRole(), userModele.getPrenom(), userModele.getNom(), userModele.getAge(), userModele.getGenre(), userModele.getTel(), userModele.getId(), userModele.nbMaxIdPhoto()+1);
                    appli.pageProfil();
                } catch (SQLException e) {
                    System.out.println("Erreur lors de l'insertion de la photo dans la base de donnée");
                }
                System.out.println(this.lienPhoto);
            }
        });
        hBoxBoutonRetour.getChildren().addAll(boutonRetour);
        hBoxFileChooser.getChildren().addAll(labelFileChooser, openButton);
        vBoxPhotoPerso.getChildren().addAll(hBoxFileChooser, hBoxBoutonRetour);
        bpPageCentrale.setCenter(vBoxPhoto);
        bpPageCentrale.setBottom(vBoxPhotoPerso);
        bpPage.setCenter(bpPageCentrale);
    }
}
