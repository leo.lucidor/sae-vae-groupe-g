package controleurs;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurProfilVendeur implements EventHandler<ActionEvent>  {

    /**
     * Application principale
     */
    private ApplicationVAE appli;
    /**
     * Id du vendeur
     */
    private int idVendeur;


    /**
     * Constructeur du controleur
     * @param appli Application principale
     * @param idVendeur Id du vendeur
     */
    public ControleurProfilVendeur(ApplicationVAE appli, int idVendeur){
        this.appli = appli;
        this.idVendeur = idVendeur;
    }

    /**
     * Méthode qui permet de changer de page pour afficher le profil du vendeur
     * @param event
     */
    @Override
    public void handle(ActionEvent event) {
        try {
            this.appli.pageProfilExterne(idVendeur);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
