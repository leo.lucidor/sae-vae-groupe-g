package controleurs;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import vues.ApplicationVAE;
import modeles.Utilisateur;

public class ControleurChoixPhotoProfil implements EventHandler<ActionEvent>{
   
    /**
     * Application principale
     */
    private ApplicationVAE appli;
    /**
     * Utilisateur qui est en train d'être modifié
     */
    private Utilisateur userModele;
    /**
     * Id de la photo de profil choisie
     */
    private int idPhoto;

    /**
     * Constructeur du controleur ControleurChoixPhotoProfil
     * @param appli Application principale
     * @param userModele Utilisateur qui est en train d'être modifié
     * @param idPhoto Id de la photo de profil choisie
     */
    public ControleurChoixPhotoProfil(ApplicationVAE appli, Utilisateur userModele, int idPhoto){
        this.appli = appli;
        this.userModele = userModele;
        this.idPhoto = idPhoto;
    }

    /**
     * Méthode qui permet de mettre à jour l'utilisateur avec la photo de profil choisie
     * @param event permet de déclencher l'action
     */
    @Override
    public void handle(ActionEvent event){
        this.userModele.setIdPhoto(idPhoto);
        try {
            this.userModele.majInfoUtilisateur(this.userModele.getPseudo(), this.userModele.getMail(), this.userModele.getMdp(), this.userModele.getRole(), this.userModele.getPrenom(), this.userModele.getNom(), this.userModele.getAge(), this.userModele.getGenre(), this.userModele.getTel(), this.userModele.getId(), idPhoto);
        } catch (SQLException e) {
            System.out.println("Erreur lors de la mise à jour de l'utilisateur");
        }
        this.appli.pageModificationProfil();
    }
}