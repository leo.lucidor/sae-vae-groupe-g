package vues;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;

public class MenuLeftAdmin extends BorderPane {

    private Button boutonAccueil;
    private Button boutonMessage;
    private Button boutonInfo;
    private Button boutonSetting;
    private Button boutonQuitter;
    private Button boutonSignalement;
    private Button boutonRecapVente;
    private Button boutonCertificat;
    private Button boutonUserInactif;

    /**
    * Constructeur de la classe MenuLeftAdmin.
    *
    * @param userInactif    Le bouton pour la page des utilisateurs inactifs.
    * @param certificat     Le bouton pour la page des certificats.
    * @param recapVente     Le bouton pour la page des récapitulatifs de vente.
    * @param signalement    Le bouton pour la page des signalements.
    * @param accueil        Le bouton pour la page d'accueil.
    * @param quitter        Le bouton pour quitter l'application.
    * @param message        Le bouton pour la page des messages.
    * @param info           Le bouton pour la page des informations.
    * @param setting        Le bouton pour la page des paramètres.
    * @param root           Le conteneur BorderPane racine.
    */
    public MenuLeftAdmin(Button userInactif, Button certificat, Button recapVente, Button signalement, Button accueil, Button quitter, Button message, Button info, Button setting, BorderPane root){
        this.boutonAccueil = accueil;
        this.boutonMessage = message;
        this.boutonInfo = info;
        this.boutonSetting = setting;
        this.boutonSignalement = signalement;
        this.boutonQuitter = quitter;
        this.boutonRecapVente = recapVente;
        this.boutonCertificat = certificat;
        this.boutonUserInactif = userInactif;
        this.setBackground(new Background(new BackgroundFill(Color.web("#1F1F1F"), new CornerRadii(0), Insets.EMPTY)));
        VBox vBoxTop = new VBox();
        VBox vBoxBottom = new VBox();
        ImageView accueilIMG = new ImageView(new Image("file:./images/logoAppli.png"));
        accueilIMG.setFitHeight(40);
        accueilIMG.setFitWidth(40);
        boutonAccueil.setGraphic(accueilIMG);
        boutonAccueil.setStyle("-fx-background-color: #1F1F1F");
        ImageView infoIMG = new ImageView(new Image("file:./images/iconInfo.png"));
        infoIMG.setFitHeight(40);
        infoIMG.setFitWidth(40);
        boutonInfo.setGraphic(infoIMG);
        boutonInfo.setStyle("-fx-background-color: #1F1F1F");
        ImageView settingIMG = new ImageView(new Image("file:./images/iconSetting.png"));
        settingIMG.setFitHeight(40);
        settingIMG.setFitWidth(40);
        boutonSetting.setGraphic(settingIMG);
        boutonSetting.setStyle("-fx-background-color: #1F1F1F");
        ImageView messageIMG = new ImageView(new Image("file:./images/iconMessage.png"));
        messageIMG.setFitHeight(40);
        messageIMG.setFitWidth(40);
        boutonMessage.setGraphic(messageIMG);
        boutonMessage.setStyle("-fx-background-color: #1F1F1F");
        ImageView quitterIMG = new ImageView(new Image("file:./images/iconQuitter.png"));
        quitterIMG.setFitHeight(40);
        quitterIMG.setFitWidth(40);
        boutonQuitter.setGraphic(quitterIMG);
        boutonQuitter.setStyle("-fx-background-color: #1F1F1F");
        ImageView signalementIMG = new ImageView(new Image("file:./images/iconSignaler.png"));
        signalementIMG.setFitHeight(40);
        signalementIMG.setFitWidth(40);
        boutonSignalement.setGraphic(signalementIMG);
        boutonSignalement.setStyle("-fx-background-color: #1F1F1F");
        ImageView recapVenteIMG = new ImageView(new Image("file:./images/iconRecapVente.png"));
        recapVenteIMG.setFitHeight(40);
        recapVenteIMG.setFitWidth(40);
        boutonRecapVente.setGraphic(recapVenteIMG);
        boutonRecapVente.setStyle("-fx-background-color: #1F1F1F");
        ImageView certificatIMG = new ImageView(new Image("file:./images/iconCertificat.png"));
        certificatIMG.setFitHeight(40);
        certificatIMG.setFitWidth(40);
        boutonCertificat.setGraphic(certificatIMG);
        boutonCertificat.setStyle("-fx-background-color: #1F1F1F");
        ImageView userInactifIMG = new ImageView(new Image("file:./images/iconUserInactif.png"));
        userInactifIMG.setFitHeight(40);
        userInactifIMG.setFitWidth(40);
        boutonUserInactif.setGraphic(userInactifIMG);
        boutonUserInactif.setStyle("-fx-background-color: #1F1F1F");
        vBoxTop.getChildren().addAll(boutonAccueil, boutonMessage, boutonSignalement, boutonRecapVente, boutonCertificat, boutonUserInactif);
        vBoxBottom.getChildren().addAll(boutonInfo, boutonSetting, boutonQuitter);
        // mettre les toolTip
        Tooltip tooltipAccueil = new Tooltip();
        tooltipAccueil.setText("Page d'accueil");
        //tooltipAccueil.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        this.boutonAccueil.setTooltip(tooltipAccueil);
        Tooltip tooltipMessage = new Tooltip();
        tooltipMessage.setText("Page message");
       // tooltipMessage.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        this.boutonMessage.setTooltip(tooltipMessage);
        Tooltip tooltipInfo = new Tooltip();
        tooltipInfo.setText("Page information");
       // tooltipInfo.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        this.boutonInfo.setTooltip(tooltipInfo);
        Tooltip tooltipSetting = new Tooltip();
        tooltipSetting.setText("Page paramètre");
       // tooltipSetting.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        this.boutonSetting.setTooltip(tooltipSetting);
        Tooltip tooltipQuitter = new Tooltip();
        tooltipQuitter.setText("Quitter l'application");
        //tooltipQuitter.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        Tooltip tooltipSignalement = new Tooltip();
        tooltipSignalement.setText("Page signalement");
        this.boutonSignalement.setTooltip(tooltipSignalement);
        Tooltip tooltipRecapVente = new Tooltip();
        tooltipRecapVente.setText("Page recapitulatif des ventes");
        this.boutonRecapVente.setTooltip(tooltipRecapVente);
        Tooltip tooltipCertificat = new Tooltip();
        tooltipCertificat.setText("Page certificat des ventes");
        this.boutonCertificat.setTooltip(tooltipCertificat);
        Tooltip tooltipUserInactif = new Tooltip();
        tooltipUserInactif.setText("Page utilisateurs inactifs");
        this.boutonUserInactif.setTooltip(tooltipUserInactif);
        this.setTop(vBoxTop);
        this.setBottom(vBoxBottom);
        root.setLeft(this);
    }
    
}
