package controleurs;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modeles.Timer;

public class ControleurTimer implements EventHandler<ActionEvent> {

    /**
     * Temps écoulé depuis le début du timer
     */
    private long tempsEcoule;
    /**
     * Timer
     */
    private Timer timer;
    /**
     * Formateur de date
     */
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    /**
     * Constructeur du controleur du timer
     * @param timer Timer
     */
    public ControleurTimer(Timer timer) {
        this.timer = timer;
        this.tempsEcoule = ChronoUnit.SECONDS.between(LocalDateTime.now(), LocalDateTime.parse(this.timer.getFin(), formatter));
    }

    /**
     * Méthode qui gère l'évènement du timer
     * @param event Evènement
     */
    @Override
    public void handle(ActionEvent event) {
        this.tempsEcoule--;
        this.timer.setTime(this.tempsEcoule);
    }
    public long getTempsEcoule() {
        return this.tempsEcoule;
    }

}